/****************************************************************************
 * configs/mB_Heat/src/stm32_emergency_button.c
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/power/emergency_button.h>
#include <stdbool.h>

#include "stm32_gpio.h"
#include "mB_Heat.h"

#ifdef CONFIG_MB_HEAT_EMERGENCY_BUTTON

/****************************************************************************
 * Public functions
 ****************************************************************************/

/************************************************************************************
 * Name: push_emergency_button
 *
 * Description:
 *
 ************************************************************************************/

void push_emergency_button(bool on)
{
    stm32_gpiowrite(GPIO_POWERSWITCH, on);
}

#endif /* CONFIG_BOARDCTL_RESET */

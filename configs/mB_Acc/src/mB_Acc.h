/************************************************************************************
 * configs/mB_Acc/src/mB_Acc.h
 *
 ************************************************************************************/

#ifndef __CONFIGS_MBADXL357_SRC_MBADXL357_H
#define __CONFIGS_MBADXL357_SRC_MBADXL357_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>
#include <nuttx/board.h>
#include <nuttx/compiler.h>
#include <stdint.h>
#include <nuttx/sensors/adxl357.h>

#include "stm32_gpio.h"

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/* Configuration ********************************************************************/

#define HAVE_PROGMEM_CHARDEV  1

#ifdef CONFIG_STM32F7_SPI1
#  define ADXL357_SPI_BUS     1
#else
#  error "SPI BUS 1 has to be defined for ADXL357!"
#endif

#define ADXL357_SPI_MINOR     0  /* should be 0, because it is the first and only device at this bus */

/* mB_IAcc GPIO Pin Definitions **************************************************/

/* LED ***************************************************************************/
#define GPIO_LED1            (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_100MHz|\
                              GPIO_OUTPUT_CLEAR|GPIO_PORTE|GPIO_PIN6)           /* PE6: green */

#define GPIO_LED2            (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_100MHz|\
                              GPIO_OUTPUT_CLEAR|GPIO_PORTE|GPIO_PIN5)           /* PE5: red */


/* SPI ***************************************************************************/

#define GPIO_SPI_CS          (GPIO_OUTPUT | GPIO_PUSHPULL | GPIO_SPEED_50MHz | GPIO_OUTPUT_SET)

#define GPIO_SPI1_CS         (GPIO_SPI_CS | GPIO_PORTA | GPIO_PIN4)            /* PA4 */
#define GPIO_SPI4_CS         (GPIO_SPI_CS | GPIO_PORTE | GPIO_PIN11)           /* PE11 */

/* ADXL357 ***********************************************************************/

#define GPIO_ADXL357_DRDY    (GPIO_INPUT | GPIO_PORTA | GPIO_PIN0)             /* PA0 */
#define GPIO_ADXL357_INT1    (GPIO_INPUT | GPIO_PORTA | GPIO_PIN2)             /* PA2 */
#define GPIO_ADXL357_INT2    (GPIO_INPUT | GPIO_PORTA | GPIO_PIN1)             /* PA1 */

/* messBUS ***********************************************************************/

/************************************************************************************
 * Public data
 ************************************************************************************/

#ifndef __ASSEMBLY__

/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_spidev_initialize
 *
 * Description:
 *   Called to configure SPI chip select GPIO pins for the board.
 *
 ************************************************************************************/

#if defined(CONFIG_SPI)
void stm32_spidev_initialize(void);
#endif

 /************************************************************************************
  * Name: stm32_adxl357_initialize
  *
  * Description:
  *   Called to configure and initialize ADXL357 driver and its SPI bus.
  *
  ************************************************************************************/
#if defined(CONFIG_SENSORS_ADXL357)
void stm32_adxl357_initialize(void);
#endif

/****************************************************************************
 * Name: stm32_hptc_setup
 *
 * Description:
 *   Initialize HPTC and register the HPTC device.
 *
 ****************************************************************************/

#ifdef CONFIG_HPTC
int stm32_hptc_setup(void);
#endif

/************************************************************************************
 * Name: stm32_pwm_setup
 *
 * Description:
 *   Initialize PWM and register the PWM device.
 *
 ************************************************************************************/

#ifdef CONFIG_PWM
int stm32_pwm_setup(void);
#endif

#endif /* __ASSEMBLY__ */
#endif /* __CONFIGS_MBADXL357_SRC_MBADXL357_H */

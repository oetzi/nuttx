/****************************************************************************
 * drivers/sensors/adxl357.c
 *
 *   Author: Björn Brandt
 *   Based on ADXL345 driver
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <unistd.h>
#include <errno.h>
#include <debug.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>

#include <nuttx/board.h>
#include <nuttx/kmalloc.h>
#include <nuttx/signal.h>
#include <nuttx/random.h>
#include <nuttx/time.h>
#include <nuttx/wqueue.h>
#include <nuttx/sensors/adxl357.h>
#include <nuttx/sensors/ioctl.h>

#include <nuttx/messBUS/messBUS_Debug.h>

#include "adxl357.h"

#if defined(CONFIG_SENSORS_ADXL357)

/****************************************************************************
 * Private Types
 ****************************************************************************/
// todo move to stm32 function (board specific)
#define ADXL357_LED_GREEN 0
#define ADXL357_LED_RED   2
#define ADXL357_LED_BLUE  1

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/* Character driver methods */

static int     adxl357_open(FAR struct file *filep);
static int     adxl357_close(FAR struct file *filep);
static ssize_t adxl357_read(FAR struct file *filep, FAR char *buffer,
                            size_t buflen);
static ssize_t adxl357_write(FAR struct file *filep, FAR const char *buffer,
                            size_t buflen);
static int     adxl357_ioctl(FAR struct file *filep, int cmd, unsigned long arg);

static void adxl357_standby(FAR struct adxl357_dev_s *priv, bool standby);
static void adxl357_selftest(FAR struct adxl357_dev_s *priv);
static void adxl357_callback_int1(int irq, FAR void *context, FAR void *arg);
#ifndef CONFIG_ADXL357_CLK_EXT
static void adxl357_callback_int2(int irq, FAR void *context, FAR void *arg);
static void adxl357_callback_drdy(int irq, FAR void *context, FAR void *arg);
#endif
static int  adxl357_checkid(FAR struct adxl357_dev_s *priv);
static uint16_t adxl357_readTemperature(FAR struct adxl357_dev_s *priv);
static void adxl357_reset(FAR struct adxl357_dev_s *priv);
static void adxl357_worker(FAR void *arg);
static void adxl357_datarecieved(FAR struct adxl357_dev_s *priv);
static void adxl357_clearSensorFifo(FAR struct adxl357_dev_s *priv, int residual);
static void adxl357_clearDriverFifo(FAR struct adxl357_dev_s *priv);
int         adxl357_config(struct adxl357_dev_s *priv, FAR struct adxl357_config_s *config);

static int      convertToSignedDecimal(uint32_t binary);
static uint32_t convertToTwosComplement(int32_t binary);
static uint64_t getSampleDeltaT(adxl357_odr_lpf_t odr);




/****************************************************************************
 * Private Data
 ****************************************************************************/

/* This the vtable that supports the character driver interface */

static const struct file_operations g_adxl357fops =
{
  adxl357_open,    /* open */
  adxl357_close,   /* close */
  adxl357_read,    /* read */
  adxl357_write,   /* write */
  0,               /* seek */
  adxl357_ioctl,   /* ioctl */
};

/****************************************************************************
 * Name: adxl357_open
 *
 * Description:
 *   Standard character driver open method.
 *
 ****************************************************************************/

static int adxl357_open(FAR struct file *filep)
{
  return OK;
}

/****************************************************************************
 * Name: adxl357_close
 *
 * Description:
 *   Standard character driver close method.
 *
 ****************************************************************************/

static int adxl357_close(FAR struct file *filep)
{
    const FAR struct inode *inode = filep->f_inode;
    FAR struct adxl357_dev_s *priv = inode->i_private;

    adxl357_switchFastMode(priv, false);
    adxl357_reset(priv);
    adxl357_standby(priv, true);

    kmm_free(priv->rxfifo.buffer);
    kmm_free(priv);

    return OK;
}

/****************************************************************************
 * Name: adxl357_write
 *
 * Description:
 *   Standard character driver write method.
 *
 ****************************************************************************/

static ssize_t adxl357_write(FAR struct file *filep, FAR const char *buffer,
        size_t buflen)
{
  return -ENOSYS;
}

/****************************************************************************
 * Name: adxl357_ioctl
 *
 * Description:
 *   Standard character driver ioctl method.
 *
 *   CMDs:
 *     > SNIOC_START: Start perodic sensor measurement. No arg expected.
 *
 *     > SNIOC_STOP : Stop periodic sensor measurement. No arg expected.
 *
 *     > SNIOC_RESET: Resets the device, similar to a power-on reset (POR). No arg expected.
 *
 *     > SNIOC_START_SELFTEST: Initiates the sensor self test. Enabling self test stimulates the
 *       sensor electrostatically to produce an output corresponding to the test signal applied as
 *       well as the mechanical force exerted. Only the z-axis response is specified to validate
 *       device functionality.
 *
 *     > SNIOC_TEMPERATURE_OUT: Read sensor temperature. Arg = Pointer to
 *       adxl357_temperature_sample_t struct that will be filled by this IOCTL call.
 *
 *     > SNIOC_GET_DEV_ID:: Read sensor device id. Pointer to 16 bit target variable expected.
 *
 *     > SNIOC_CONFIGURE: (Re-)Configure sensor. New configuration is specified by 'arg' that must
 *       be a pointer to adxl357_sensor_config_t struct. After reconfiguration the sensor will
 *       be in standby mode, so measurement needs to be started again by IOCTL cmd SNIOC_START.
 *
 *
 ****************************************************************************/
static int adxl357_ioctl(FAR struct file *filep, int cmd, unsigned long arg)
{
  const FAR struct inode *inode = filep->f_inode;
  FAR struct adxl357_dev_s *priv = inode->i_private;
  int ret = OK;

    switch (cmd)
    {

    case SNIOC_START:
        adxl357_switchFastMode(priv, true);
        adxl357_standby(priv, false);
        break;
    case SNIOC_STOP:
        adxl357_switchFastMode(priv, false);
        adxl357_standby(priv, true);
        break;
    case SNIOC_RESET:
        adxl357_reset(priv);
        break;
    case SNIOC_START_SELFTEST:
        adxl357_selftest(priv);
        break;
    case SNIOC_TEMPERATURE_OUT:
        *((adxl357_temperature_sample_t*)arg) = priv->sensorTemperature;
        break;
    case SNIOC_GET_DEV_ID:
        *((uint16_t*)arg) = ADXL357_DEVID_PARTID_VALUE;
        break;
    case SNIOC_CONFIGURE:
        priv->config->sensor = *((adxl357_sensor_config_t*) arg);
        adxl357_config(priv, priv->config);
        break;
    case SNIOC_CHMEATIME: //TODO rename
        priv->config->triggerSync();

        /* At trigger timepoint no data is sampeled.
         * To align the first fifo entry to the sync, we are not
         * flushing the entire fifo but leaving
         * 3 samples (1x X-data, 1x Y, 1x Z) in the fifo.
         * As the fifo watermark stays constant, the data becomes
         * aligned after the next readout (see adxl357_worker).
         * Though, the driver's fifo data is entirely dropped as
         * it became invalid via sync.
         */

        adxl357_clearSensorFifo(priv, 3);
        adxl357_clearDriverFifo(priv);
        break;
    case SNIOC_SET_INTERVAL: //TODO rename
        priv->config->triggerPhaseShift((int8_t) arg);
        break;
    case SNIOC_SET_CLEAN_INTERVAL:
        adxl357_clearDriverFifo(priv);
        break;

        /* Command was not recognized */

    default:
        snerr("ERROR: Unrecognized cmd: %d\n", cmd);
        ret = -ENOTTY;
        break;
    }

  return ret;
}

/****************************************************************************
 * Name: getSampleDeltaT
 *
 * Description:
 *   Calculates delta time in nanoseconds between two data samples.
 *   Value depends on data frequency (ODR).
 *
 *   Returns delta t in nanoseconds.
 *
 ****************************************************************************/

static uint64_t getSampleDeltaT(adxl357_odr_lpf_t odr)
{
    uint64_t ret = 0;

    if(odr > 63)
    {
        ret = NSEC_PER_SEC / odr;
    }
    else{
        switch (odr)
        {
        case odr_62_5Hz:
            ret = NSEC_PER_SEC / 62.5;
            break;
        case odr_31_2Hz:
            ret = NSEC_PER_SEC / 31.25;
            break;
        case odr_15_6Hz:
            ret = NSEC_PER_SEC / 15.625;
            break;
        case odr_7_81Hz:
            ret = NSEC_PER_SEC / 7.8125;
            break;
        case odr_3_91Hz:
            ret = NSEC_PER_SEC / 3.90625;
            break;
        default:
            break;
        }
    }
    return ret;
}

/****************************************************************************
 * Name: adxl357_read
 *
 * Description:
 *   Standard character driver read method.
 *
 *   Returns number of read bytes.
 *
 ****************************************************************************/

static ssize_t adxl357_read(FAR struct file *filep, FAR char *buffer, size_t buflen)
{
    int ret = 0;

    if (buffer != NULL && buflen > 0)
    {
//      sninfo("len=%d\n", len);
        DEBUGASSERT(filep);
        FAR struct inode *inode = filep->f_inode;

        DEBUGASSERT(inode && inode->i_private);
        FAR struct adxl357_dev_s *priv = (FAR struct adxl357_dev_s *) inode->i_private;

        FAR struct adxl357_buffer_s *rxfifo = &priv->rxfifo;
        int16_t tail = rxfifo->tail;

        /* Verify that the caller has provided a buffer large enough to receive
         * the accelerometer data.
         */

        uint16_t nSamples2Read = buflen / sizeof(struct adxl357_sample_s);

        if (nSamples2Read < 1)
        {
            /* We could provide logic to break up a sample into segments and
             * handle smaller reads... but why?
             */

            return -ENOSYS;
        }

        /* Get exclusive access to the driver data structure */

        ret = nxsem_wait(&priv->exclsem);
        if (ret < 0)
        {
            /* A signal received while waiting for access to the recv.tail will avort
             * the transfer.  After the transfer has started, we are committed and
             * signals will be ignored.
             */

            DEBUGASSERT(ret == -EINTR || ret == -ECANCELED);
            nxsem_post(&priv->exclsem);
            return ret;
        }

        /* Check if there is data to return in the circular buffer. */

        unsigned int fifolen = 0;

        if (rxfifo->head >= tail)
        {
            fifolen = rxfifo->head - tail;
        }
        else
        {
            fifolen = rxfifo->size - tail + rxfifo->head;
        }


        if(fifolen == 0)
        {
            /* Buffer empty -> no data to return */

            if(filep->f_oflags & O_NONBLOCK)
            {
                /* User specified O_NONBLOCK, so do not wait */

                nxsem_post(&priv->exclsem);
                return -EAGAIN;
            }
            else
            {
                /* Wait for data to arrive */

                /* 1. Free buffer semaphore temporaily, so data can be inserted */

                nxsem_post(&priv->exclsem);

                /* 2. Then wait */

                priv->recvwaiting = true;
                ret = nxsem_wait(&priv->recvsem);
                if (ret < 0)
                {
                    /* A signal received while waiting */

                    DEBUGASSERT(ret == -EINTR || ret == -ECANCELED);
                    nxsem_post(&priv->recvsem);
                    return ret;
                }

                /* 3. Recalculate bufsize to check if data actually is available */

                if (rxfifo->head >= tail)
                {
                    fifolen = rxfifo->head - tail;
                }
                else
                {
                    fifolen = rxfifo->size - tail + rxfifo->head;
                }

                /* 4. Re-aquire buffer semaphore */

                ret = nxsem_wait(&priv->exclsem);
                if (ret < 0)
                {
                    /* A signal received while waiting */

                    DEBUGASSERT(ret == -EINTR || ret == -ECANCELED);
                    nxsem_post(&priv->exclsem);
                    return ret;
                }
            }
        }

        if(fifolen > 0)
        {
            /* There is data in the buffer to return */

            /* Calc number of samples actually stored in the fifo */

            const unsigned int nBulksinFifo = fifolen / (sizeof(adxl357_timestamp) + priv->bulksize);
            const unsigned int nSamplesinBulk = priv->config->sensor.acc_read_divider;
            const unsigned int bytesPerSample = ADXL357_ELEMENTS_PER_SAMPLE * ADXL357_BYTE_PER_SAMPLE_ELEMENT;
            const unsigned int nSamplesInFifo = nBulksinFifo * nSamplesinBulk;

            if (nSamplesInFifo < nSamples2Read)
            {
                /* Update number of samples to read because there is no more data in the fifo */

                nSamples2Read = nSamplesInFifo;
            }

            const unsigned int nBulks2Read = nSamples2Read / nSamplesinBulk;

            /* We will return number of read bytes, save value already. */

            ret = nSamples2Read * sizeof(struct adxl357_sample_s);

            /* Read samples from FIFO */

            FAR char* rxbuffer = &rxfifo->buffer[tail];

            int t_idx = 0;
            int x_idx = sizeof(adxl357_timestamp) + 2; // LSB of x

#if 0
          /* check if byte order is valid */

          for (int i = x_idx; i < nSamples2Read; i += bytesPerSample)
          {
              /* bit 0 indicates x value */

              if (!(rxbuffer[i] & 0x01) || (rxbuffer[i + 3] & 0x01) || (rxbuffer[i + 6] & 0x01))
              {
                  x_idx = -1;
                  break;
              }
          }
#endif

            /* delta between to samples */

            const unsigned long delta_sample_ns = getSampleDeltaT(priv->config->sensor.odr);

            /* delta between first and last sample */

            const unsigned long diff_ns = nSamplesinBulk * delta_sample_ns;
            const adxl357_timestamp delta_first_last = {
                    .tv_sec  = diff_ns / NSEC_PER_SEC,
                    .tv_nsec = diff_ns % NSEC_PER_SEC
            };

            if (x_idx > 1)
            {
                adxl357_sample_t *helpBuf = (adxl357_sample_t*) buffer;

                for (int k = 0; k < nBulks2Read; k++)
                {
                    struct adxl357_sample_s sample = { { 0, 0 }, 0, 0, 0 }; // helper

                    /* Get timestamp of the entire data bulk which is equal to the timestamp of
                     * the last sample within this bulk.
                     */

                    adxl357_timestamp bulk_timestamp = {0, 0};
                    memcpy(&bulk_timestamp, ((adxl357_timestamp*) (&rxbuffer[t_idx])), sizeof(adxl357_timestamp));
                    tail += sizeof(adxl357_timestamp);

                    /* Calculate timestamp of the first sample */

                    sample.timestamp.tv_sec = bulk_timestamp.tv_sec - delta_first_last.tv_sec;
                    sample.timestamp.tv_nsec = bulk_timestamp.tv_nsec - delta_first_last.tv_nsec;
                    if (sample.timestamp.tv_nsec < 0)
                    {
                        sample.timestamp.tv_nsec += NSEC_PER_SEC;
                        sample.timestamp.tv_sec--;
                    }

                    for (int i = 0; i < nSamplesinBulk; i++)
                    {
                        /* Calculate timestamp for next sample */

                        sample.timestamp.tv_nsec += delta_sample_ns;
                        if (sample.timestamp.tv_nsec >= NSEC_PER_SEC)
                        {
                            sample.timestamp.tv_nsec -= NSEC_PER_SEC;
                            sample.timestamp.tv_sec++;
                        }

                        sample.acc_x = rxbuffer[x_idx - 2] << 12 | rxbuffer[x_idx - 1] << 4 | rxbuffer[x_idx] >> 4;
                        sample.acc_y = rxbuffer[x_idx + 1] << 12 | rxbuffer[x_idx + 2] << 4 | rxbuffer[x_idx + 3] >> 4;
                        sample.acc_z = rxbuffer[x_idx + 4] << 12 | rxbuffer[x_idx + 5] << 4 | rxbuffer[x_idx + 6] >> 4;

                        /* Convert all values to signed decimals */

                        sample.acc_x = convertToSignedDecimal(sample.acc_x);
                        sample.acc_y = convertToSignedDecimal(sample.acc_y);
                        sample.acc_z = convertToSignedDecimal(sample.acc_z);

                        x_idx += bytesPerSample;
                        tail  += bytesPerSample;

                        if (tail >= rxfifo->size)
                        {
                            tail = 0;
                        }

                        /* Copy read sample and increment buffer pointer */

                        memcpy(helpBuf, &sample, sizeof(adxl357_sample_t));
                        helpBuf++;
                    }

                    /* Increment the tail index. */

                    priv->rxfifo.tail = tail;

                    /* Reset the pointers */

                    rxbuffer = &rxfifo->buffer[tail];
                    t_idx = 0;
                    x_idx = sizeof(adxl357_timestamp) + 2; // LSB of x
                }
            }
            else
            {
                /* unexpected byte order in circular fifo buffer */

                ret = -EILSEQ;
            }
        }
        else
        {
            /* Still no data available. We should never get here */

            ret = -ENODATA;
        }

        /* Free semaphore */

        nxsem_post(&priv->exclsem);
    }

    return ret;
}


/****************************************************************************
 * Name: convertToSignedDecimal
 *
 * Description:
 *    Convert twos complement 20 bit value to signed decimal value.
 *
 ****************************************************************************/

static int convertToSignedDecimal(uint32_t binary)
{
    if(binary & 0x80000)
    {
        /* value negative */
        return (int32_t) binary | ~((1<<20) - 1);
    }

    return (int32_t) binary;
}


/****************************************************************************
 * Name: convertToTwosComplement
 *
 * Description:
 *    Convert 20 bit signed decimal value to twos complement.
 *
 ****************************************************************************/

static uint32_t convertToTwosComplement(int32_t binary)
{
    return (uint32_t) (-1)*(binary & 0xFFFFF);
}

/****************************************************************************
 * Name: adxl357_worker
 *
 * Description:
 *   This is the "bottom half" of the ADXL357 interrupt handler
 *
 *   It reads measured data from sensor's internal fifo.
 *
 *   Resulting Byte order: Timestamp sample1 sample2 sample3 ... sampleN
 *                                   |__________________________________|
 *                                                     |
 *                                                 data bulk
 *
 * Each sample consists of 9 Byte: 3 byte x-data, 3 byte y-data, 3 byte z-data
 *
 * Additionally this function  periodically reads sensor temperature.
 * Only one temperature sample is stored, so this function overwrites last
 * measurement.
 *
 ****************************************************************************/

static void adxl357_worker(FAR void *arg)
{
    const struct adxl357_workParam_s* param = (struct adxl357_workParam_s*) arg;
    FAR struct adxl357_dev_s *priv = param->priv;

    DEBUGASSERT(param && priv && priv->config);

    const uint8_t sensorIRQstatus = adxl357_getreg8(priv, ADXL357_STATUS);

    if (sensorIRQstatus & ADXL357_STATUS_FIFO_FULL)
    {
        /* Get exclusive access to the driver data structure */

        const int ret = nxsem_wait(&priv->exclsem);
        if (ret < 0)
        {
            snerr("ERROR: nxsem_wait failed: %d\n", ret);
            DEBUGASSERT(ret == -EINTR || ret == -ECANCELED);
        }

        /* Get timestamp for this new data bulk */

        adxl357_timestamp timestamp;
        clock_gettime(CLOCK_MONOTONIC, &timestamp);

        /* Insert timestamp before acceleration data */

        memcpy(&priv->rxfifo.buffer[priv->rxfifo.head], &timestamp, sizeof(adxl357_timestamp));
        priv->rxfifo.head += sizeof(adxl357_timestamp);

        /* Get data bulk */

        adxl357_readBulk(priv, &priv->rxfifo, priv->bulksize);

        /* Eventuelly update temperature measurement */

        if((priv->readBulkCnt % priv->config->sensor.temp_read_divider) == 0)
        {
            priv->sensorTemperature.timestamp = timestamp;
            priv->sensorTemperature.temperature = adxl357_readTemperature(priv);
        }

        /* Another data bulk is read successfully... */

        priv->readBulkCnt++;

        nxsem_post(&priv->exclsem);

        /* Inform all potentially waiting reading attempts that data arrived */

        adxl357_datarecieved(priv);
    }

    /* Re-enable interrupt */

    priv->config->enableIRQ(priv->config, ADXL357_IRQ_INT1, true);
}

/****************************************************************************
 * Name: adxl357_callback_drdy
 *
 * Description:
 *  The ADXL357 interrupt handler for DRDY pin
 *
 ****************************************************************************/
#ifndef CONFIG_ADXL357_CLK_EXT
static void adxl357_callback_drdy(int irq, FAR void *context, FAR void *arg)
{
    /* Nothing to do */
}
#endif

/****************************************************************************
 * Name: adxl357_callback_int1
 *
 * Description:
 *  The ADXL357 interrupt handler for INT1 pin
 *
 ****************************************************************************/

static void adxl357_callback_int1(int irq, FAR void *context, FAR void *arg)
{
    DEBUGASSERT(arg != NULL);

    if(arg == NULL) return;

    FAR struct adxl357_dev_s *priv = (FAR struct adxl357_dev_s *) arg;

    /* Read sensor status. Also clears pending interrupt state in ADXL357 */

    const uint8_t sensorIRQstatus = adxl357_getreg8(priv, ADXL357_STATUS);

    if (sensorIRQstatus & ADXL357_STATUS_DATA_RDY)
    {

    }
    if (sensorIRQstatus & ADXL357_STATUS_ACTIVITY)
    {

    }
    if (sensorIRQstatus & ADXL357_STATUS_FIFO_OVR)
    {
        /* FATAL ERROR: adxl357_worker is too slow! */

        priv->status = ADXL357_STAT_PROCESSING_DELAY_DETECTED;
    }
    else
    {
    }
    if (sensorIRQstatus & ADXL357_STATUS_NVM_BUSY)
    {

    }
    if (sensorIRQstatus & ADXL357_STATUS_FIFO_FULL)
    {
        /* Sanity checks */

        DEBUGASSERT(priv != NULL && priv->config);

        if(priv == NULL || priv->config == NULL)
        {
            snerr("FATAL ERRROR: Lower half driver corrupted!\n");
            return;
        }

        /* Check if interrupt work is already queued. If it is already busy, then
         * worker thread was too slow :(
         */

        if (work_available(&priv->work))
        {
            /* No.. Transfer processing to the worker thread. */

            /* IMPORTANT: Disable IRQ because another IRQ would disturb HP work queue!
             * IRQ will be reenabled at the end of worker thread
             */

            priv->config->enableIRQ(priv->config, ADXL357_IRQ_INT1, false);

            g_worker_param.priv = priv;

            const int ret = work_queue(HPWORK, &priv->work, (worker_t) adxl357_worker, &g_worker_param, 0);
            if (ret != 0)
            {
                snerr("ERROR: Failed to queue work: %d\n", ret);
            }
        }
        else
        {
            /* FATAL ERROR: adxl357_worker is too slow! */

            priv->status = ADXL357_STAT_PROCESSING_DELAY_DETECTED;
        }
    }
}

/****************************************************************************
 * Name: adxl357_callback_int2
 *
 * Description:
 *  The ADXL357 interrupt handler for INT2 pin
 *
 ****************************************************************************/
#ifndef CONFIG_ADXL357_CLK_EXT
static void adxl357_callback_int2(int irq, FAR void *context, FAR void *arg)
{
    /* Nothing to do */
}
#endif


/************************************************************************************
 * Name: adxl357_datarecieved
 *
 * Description:
 *   This function is called from interrupt worker thread when new sensor data is
 *   placed in the driver's circular buffer.  This function will wake-up any stalled read()
 *   operations that are waiting for incoming data.
 *
 ************************************************************************************/

static void adxl357_datarecieved(FAR struct adxl357_dev_s *priv)
{
  /* Is there a thread waiting for read data?  */

  if (priv->recvwaiting)
    {
      /* Yes... wake it up */

      priv->recvwaiting = false;
      (void)nxsem_post(&priv->recvsem);
    }
}

/************************************************************************************
 * Name: adxl357_clearSensorFifo
 *
 * Description:
 *       This functions clears the internal fifo of the ADXL357 sensor by reading
 *       the data and piping it to nirvana.
 *       If 'residualSamples' is > 0, not all data is dropped but a number of
 *       'residualSamples' samples is left.
 *
 ************************************************************************************/

static void adxl357_clearSensorFifo(FAR struct adxl357_dev_s *priv, int residualSamples)
{
    DEBUGASSERT(priv);
    DEBUGASSERT(residualSamples >= 0);

    if(!priv) return;
    if(residualSamples < 0) residualSamples = 0;

    const int nSamples = adxl357_getreg8(priv, ADXL357_FIFO_ENTRIES); // value between 0 and 96
    const int nBytes   = (nSamples - residualSamples) * ADXL357_BYTE_PER_SAMPLE_ELEMENT;

    if (nBytes > 0)
    {
        /* Get exclusive access to the driver data structure */

        const int ret = nxsem_wait(&priv->exclsem);
        if (ret < 0)
        {
            snerr("ERROR: nxsem_wait failed: %d\n", ret);
            DEBUGASSERT(ret == -EINTR || ret == -ECANCELED);
        }

        /* Clear sensor's fifo */

        adxl357_readBulk(priv, NULL, nBytes);

        nxsem_post(&priv->exclsem);
    }
}

/************************************************************************************
 * Name: adxl357_clearDriverFifo
 *
 * Description:
 *       This function resets the driver's internal fifo by dropping all data.
 *
 ************************************************************************************/

static void adxl357_clearDriverFifo(FAR struct adxl357_dev_s *priv)
{
    /* Get exclusive access to the driver data structure */

    const int ret = nxsem_wait(&priv->exclsem);
    if (ret < 0)
    {
        snerr("ERROR: nxsem_wait failed: %d\n", ret);
        DEBUGASSERT(ret == -EINTR || ret == -ECANCELED);
    }

    /* Clear driver's fifo */

    priv->rxfifo.head = 0;
    priv->rxfifo.tail = 0;
    priv->readBulkCnt = 0;

    nxsem_post(&priv->exclsem);
}

/****************************************************************************
 * Name: adxl357_checkid
 *
 * Description:
 *   Read and verify the ADXL357 chip ID
 *
 ****************************************************************************/

static int adxl357_checkid(FAR struct adxl357_dev_s *priv)
{
    /* Read device IDs  */

    const uint8_t adid   = adxl357_getreg8(priv, ADXL357_DEVID_AD);
    const uint8_t mstid  = adxl357_getreg8(priv, ADXL357_DEVID_MST);
    const uint8_t partid = adxl357_getreg8(priv, ADXL357_PARTID);

    sninfo("partid: 0x%02x\n", partid);

    if (adid != (uint8_t) ADXL357_DEVID_AD_VALUE || mstid != (uint8_t) ADXL357_DEVID_MST_VALUE
            || partid != (uint8_t) ADXL357_DEVID_PARTID_VALUE)
    {
        /* ID is not Correct */

        return -ENODEV;
    }

    return OK;
}

/****************************************************************************
 * Name: adxl357_reset
 *
 * Description:
 *  Reset the ADXL357
 *
 ****************************************************************************/

static void adxl357_reset(FAR struct adxl357_dev_s *priv)
{
    adxl357_putreg8(priv, ADXL357_RESET, ADXL357_RESET_VALUE);

    /* give hardware some time to reset */

    usleep(10);
}

/****************************************************************************
 * Name: adxl357_readTemperature
 *
 * Description:
 *      Read 12 bit temperature value from sensor
 *
 ****************************************************************************/

static uint16_t adxl357_readTemperature(FAR struct adxl357_dev_s *priv)
{
    uint16_t temperature = 0;

    temperature |= adxl357_getreg8(priv, ADXL357_TEMP2) << 8;
    temperature |= adxl357_getreg8(priv, ADXL357_TEMP1) << 0;

    return (temperature & 0xFFF); // only 12 bit valid
}

/****************************************************************************
 * Name: adxl357_standby
 *
 * Description:
 *     Switches from standby mode to measurement mode and vice versa.
 *
 * Parameter:
 *     standby: True: Enables standby mode
 *              False: Enables measurement mode (disables standby mode)
 *
 ****************************************************************************/

static void adxl357_standby(FAR struct adxl357_dev_s *priv, bool standby)
{
    uint8_t pwr_state = adxl357_getreg8(priv, ADXL357_POWER_CTL);

    if(standby)
    {
        pwr_state |= ADXL357_STANDBY_ON;
    }
    else
    {
        pwr_state &= ~ADXL357_STANDBY_ON;
    }

    adxl357_putreg8(priv, ADXL357_POWER_CTL, pwr_state);
}

/****************************************************************************
 * Name: adxl357_selftest
 *
 * Description:
 *     Initiates the sensor self test.
 *     Enabling self test stimulates the sensor electrostatically to
 *     produce an output corresponding to the test signal applied as
 *     well as the mechanical force exerted. Only the z-axis response is
 *     specified to validate device functionality.
 *
 *
 * Parameter:
 *     standby: True: Enables standby mode
 *              False: Enables measurement mode (disables standby mode)
 *
 ****************************************************************************/

static void adxl357_selftest(FAR struct adxl357_dev_s *priv)
{
    uint8_t regval = 0;

    /* Invoke self test mode */

    regval |= ADXL357_ST1;
    adxl357_putreg8(priv, ADXL357_SELF_TEST, regval);
    usleep(1000);

    /* Do test */

    regval |= ADXL357_ST2;
    adxl357_putreg8(priv, ADXL357_SELF_TEST, regval);
    usleep(1000);

    /* Stop test */

    regval = 0x00;
    adxl357_putreg8(priv, ADXL357_SELF_TEST, regval);
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: adxl357_instantiate
 *
 * Description:
 *   Instantiate and configure the ADXL357 device driver to use the provided
 *   I2C or SPIdevice instance.
 *
 * Input Parameters:
 *   dev     - An I2C or SPI driver instance
 *   config  - Persistent board configuration data
 *
 * Returned Value:
 *   A non-zero handle is returned on success.  This handle may then be used
 *   to configure the ADXL357 driver as necessary.  A NULL handle value is
 *   returned on failure.
 *
 ****************************************************************************/

#ifdef CONFIG_ADXL357_SPI
ADXL357_HANDLE adxl357_instantiate(FAR struct spi_dev_s *dev, FAR struct adxl357_config_s *config)
#else
ADXL357_HANDLE adxl357_instantiate(FAR struct i2c_master_s *dev, FAR struct adxl357_config_s *config)
#endif
{

    /* Sensor needs up to 10ms to power on. So wait a bit to ensure that hardware is ready */

    usleep(10*1000);

    /* Allocate the ADXL357 driver instance */

    FAR struct adxl357_dev_s *priv = (FAR struct adxl357_dev_s *) kmm_zalloc(sizeof(struct adxl357_dev_s));
    if (!priv)
    {
        snerr("ERROR: Failed to allocate the device structure!\n");
        return NULL;
    }

#ifdef CONFIG_ADXL357_SPI
    priv->spi = dev;
    priv->fastMode = false;
#else
    priv->i2c = dev;
#endif

    /* Read and verify the ADXL357 device ID */

    const int ret = adxl357_checkid(priv);
    if (ret < 0)
    {
        snerr("ERROR: Wrong Device ID!\n");
        kmm_free(priv);
        return NULL;
    }

    /* Initialize the device state structure */

    nxsem_init(&priv->exclsem, 0, 1);
    nxsem_init(&priv->recvsem, 0, 0);

    /* The recvsem is used for signaling and, hence, should not have priority inheritance enabled. */

    nxsem_setprotocol(&priv->recvsem, SEM_PRIO_NONE);

    /* Initial configuration */

    adxl357_config(priv, config);

    /* Ensure that standby mode is enabled */

    adxl357_standby(priv, true);

    /* Return our private data structure as an opaque handle */

    return (ADXL357_HANDLE) priv;
}

/****************************************************************************
 * Name: adxl357_config_range
 *
 * Description:
 *
 ****************************************************************************/

void adxl357_config_range(struct adxl357_dev_s *priv, FAR struct adxl357_config_s *config)
{
    if (priv != NULL && config != NULL)
    {
        uint8_t regval = 0;

#ifdef CONFIG_ADXL357_ACTIVELOW
        regval &= ~ADXL357_INT_ACTIVE_HIGH; /* Pin polarity: Active low / falling edge */
#else
        regval |= ADXL357_INT_ACTIVE_HIGH; /* Pin polarity: Active high / rising edge */
#endif

        if (config->sensor.range == range_10g)
        {
            regval |= ADXL357_RANGE_10G;
        }
        else if (config->sensor.range == range_20g)
        {
            regval |= ADXL357_RANGE_20G;
        }
        else if (config->sensor.range == range_40g)
        {
            regval |= ADXL357_RANGE_40G;
        }
        adxl357_putreg8(priv, ADXL357_RANGE, regval);
    }
}

/****************************************************************************
 * Name: adxl357_config_clock
 *
 * Description:
 *
 ****************************************************************************/

void adxl357_config_clock(struct adxl357_dev_s *priv, FAR struct adxl357_config_s *config)
{
    if (priv != NULL && config != NULL)
    {
        uint8_t regval = 0;

#ifdef CONFIG_ADXL357_CLK_EXT
        regval = ADXL357_EXT_SYNC_NO_INTERPOL | ADXL357_EXT_CLK;
#elif defined(ADXL357_CLK_INT_INTERPOLATION)
        regval = ADXL357_EXT_SYNC_INTERPOL;
#else
        regval = ADXL357_SYNC_INT;
#endif

        adxl357_putreg8(priv, ADXL357_SYNC, regval);
    }
}

/****************************************************************************
 * Name: adxl357_config_odr_lpf
 *
 * Description:
 *
 ****************************************************************************/

void adxl357_config_odr_lpf(struct adxl357_dev_s *priv, FAR struct adxl357_config_s *config)
{
    if (priv != NULL && config != NULL)
    {
        uint8_t regval = 0;

        if (config->sensor.odr == odr_4000Hz)
        {
            regval = ADXL357_ODR_4000;
        }
        else if (config->sensor.odr == odr_2000Hz)
        {
            regval = ADXL357_ODR_2000;
        }
        else if (config->sensor.odr == odr_1000Hz)
        {
            regval = ADXL357_ODR_1000;
        }
        else if (config->sensor.odr == odr_500Hz)
        {
            regval = ADXL357_ODR_0500;
        }
        else if (config->sensor.odr == odr_250Hz)
        {
            regval = ADXL357_ODR_0250;
        }
        else if (config->sensor.odr == odr_125Hz)
        {
            regval = ADXL357_ODR_0125;
        }
        else if (config->sensor.odr == odr_62_5Hz)
        {
            regval = ADXL357_ODR_0063;
        }
        else if (config->sensor.odr == odr_31_2Hz)
        {
            regval = ADXL357_ODR_0031;
        }
        else if (config->sensor.odr == odr_15_6Hz)
        {
            regval = ADXL357_ODR_0016;
        }
        else if (config->sensor.odr == odr_7_81Hz)
        {
            regval = ADXL357_ODR_0008;
        }
        else if (config->sensor.odr == odr_3_91Hz)
        {
            regval = ADXL357_ODR_0004;
        }
        adxl357_putreg8(priv, ADXL357_FILTER, regval);
    }
}

/****************************************************************************
 * Name: adxl357_config_interrupts
 *
 * Description:
 *
 ****************************************************************************/

void adxl357_config_interrupts(struct adxl357_dev_s *priv, FAR struct adxl357_config_s *config)
{
    if (priv != NULL && config != NULL)
    {
        /* Set FIFO Watermark */

        uint8_t regval = priv->config->sensor.acc_read_divider * ADXL357_ELEMENTS_PER_SAMPLE;
        adxl357_putreg8(priv, ADXL357_FIFO_SAMPLES, regval);

        /* Attach the ADXL357 callback functions to uC interrupt handler. */

#ifdef CONFIG_ADXL357_CLK_EXT
        config->attachIRQ(config, NULL, NULL, (xcpt_t)adxl357_callback_int1, (FAR void *)priv, NULL, NULL);
#else
        config->attachIRQ(config, (xcpt_t) adxl357_callback_drdy, (FAR void *) priv,
                                  (xcpt_t) adxl357_callback_int1, (FAR void *) priv,
                                  (xcpt_t) adxl357_callback_int2, (FAR void *) priv);
#endif

        /* Enable Interrupts and Configure Interrupt Map */

        regval = 0;
#ifdef CONFIG_ADXL357_CLK_EXT /* Only INT1 can be configured/used in this case */
        regval |= ADXL357_INT_MAP_FULL_EN1;
        adxl357_putreg8(priv, ADXL357_INT_MAP, regval);

        config->enableIRQ(config, ADXL357_IRQ_INT1, true);
#else
        regval |= ADXL357_INT_MAP_FULL_EN1;
        adxl357_putreg8(priv, ADXL357_INT_MAP, regval);

        config->enableIRQ(config, ADXL357_IRQ_DRDY, true);
        config->enableIRQ(config, ADXL357_IRQ_INT1, true);
        config->enableIRQ(config, ADXL357_IRQ_INT2, true);
#endif
    }
}

/****************************************************************************
 * Name: adxl357_config_offsets
 *
 * Description:
 *
 ****************************************************************************/

void adxl357_config_offsets(struct adxl357_dev_s *priv, FAR struct adxl357_config_s *config)
{
    if (priv != NULL && config != NULL)
    {
        uint8_t regval = 0;

        /* Set x offset */

        int32_t x = config->sensor.offsets.x;

        x = convertToTwosComplement(x) >> 4; // offset[15:0] matches the significance of data[19:4]

        regval = (x & 0xFF00) >> 8;
        adxl357_putreg8(priv, ADXL357_OFFSET_X_H, regval);
        regval = (x & 0x00FF) >> 0;
        adxl357_putreg8(priv, ADXL357_OFFSET_X_L, regval);

        /* Set y offset */

        uint32_t y = config->sensor.offsets.y;

        y = convertToTwosComplement(y) >> 4; // offset[15:0] matches the significance of data[19:4]

        regval = (y & 0xFF00) >> 8;
        adxl357_putreg8(priv, ADXL357_OFFSET_Y_H, regval);
        regval = (y & 0x00FF) >> 0;
        adxl357_putreg8(priv, ADXL357_OFFSET_Y_L, regval);

        /* Set z offset */

        uint32_t z = config->sensor.offsets.z;

        z = convertToTwosComplement(z) >> 4; // offset[15:0] matches the significance of data[19:4]

        regval = (z & 0xFF00) >> 8;
        adxl357_putreg8(priv, ADXL357_OFFSET_Z_H, regval);
        regval = (z & 0x00FF) >> 0;
        adxl357_putreg8(priv, ADXL357_OFFSET_Z_L, regval);
    }

}

/****************************************************************************
 * Name: adxl357_config
 *
 * Description:
 *
 ****************************************************************************/

int adxl357_config(struct adxl357_dev_s *priv, FAR struct adxl357_config_s *config)
{
    /* some sanity checks */

    if(config->sensor.acc_read_divider < 1 || config->sensor.acc_read_divider > 32)
    {
        snerr("ERROR: Configuration invalid! Acceleration reading divider must be between 1 and 32.\n");
        return -EINVAL;
    }

    if(priv != NULL)
    {
        priv->config = config;

        /* Generate ADXL357 Software reset and ensure that we are in STANDBY mode */

        adxl357_reset(priv);

        /* Configure the interrupt output pin to generate interrupts on high or low level. And select
         * measurement range.
         */

        adxl357_config_range(priv, config);

        /* Configure trim offsets for acc_x, acc_y, acc_z */

        adxl357_config_offsets(priv, config);

        /* Configure ADXL357 clock mode */

        adxl357_config_clock(priv, config);

        /* Configure output data rate (ODR) and low-pass filter corner */

        adxl357_config_odr_lpf(priv, config);

        /* Set up callback functions and corresponding interrupt handler */

        adxl357_config_interrupts(priv, config);
    }
    return OK;
}

/****************************************************************************
 * Name: adxl357_register
 *
 * Description:
 *  This function will register the accelerometer driver as /dev/accelN where N
 *  is the minor device number
 *
 * Input Parameters:
 *   handle    - The handle previously returned by adxl357_register
 *   minor     - The input device minor number
 *
 * Returned Value:
 *   Zero is returned on success.  Otherwise, a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 ****************************************************************************/

int adxl357_register(ADXL357_HANDLE handle, int minor)
{
    FAR struct adxl357_dev_s *priv = (FAR struct adxl357_dev_s *) handle;
    char devname[DEV_NAMELEN];
    int ret;

    sninfo("handle=%p minor=%d\n", handle, minor);
    DEBUGASSERT(priv);

    /* Get exclusive access to the device structure */

    ret = nxsem_wait(&priv->exclsem);
    if (ret < 0)
    {
        snerr("ERROR: nxsem_wait failed: %d\n", ret);
        DEBUGASSERT(ret == -EINTR || ret == -ECANCELED);
        return ret;
    }

    /* Samplesize = nElements * Elementsize
     * nSamples = data rate / polling rate = odr / (1/divider * odr) = divider
     * Bulksize = Samplesize * nSamples
     */

    priv->bulksize = priv->config->sensor.acc_read_divider
            * ADXL357_ELEMENTS_PER_SAMPLE * ADXL357_BYTE_PER_SAMPLE_ELEMENT;


    /* Driver fifo needs to store bulks + it'ts timestamps
     * adxl357_timestamp
     */

    const uint16_t fifosize = 4 * (priv->bulksize + sizeof(adxl357_timestamp));
    sninfo("Driver: Size of internal fifo: %iByte\n", fifosize);

    /* Initialize the buffer structure fields to their default values */

    struct adxl357_buffer_s rxfifo = {
            .head   = 0,
            .tail   = 0,
            .size   = fifosize,
            .buffer = (FAR char*) kmm_zalloc(fifosize),
    };
    nxsem_init(&rxfifo.sem, 0, 1);

    priv->status = ADXL357_STAT_NORMAL;
    priv->rxfifo = rxfifo;
    priv->fastMode = false;
    priv->recvwaiting = false;
    priv->readBulkCnt = 0;
    priv->sensorTemperature.temperature = 0;

    /* Register the character driver */

    snprintf(devname, DEV_NAMELEN, DEV_FORMAT, minor);
    ret = register_driver(devname, &g_adxl357fops, 0666, priv);
    if (ret < 0)
    {
        snerr("ERROR: Failed to register driver %s: %d\n", devname, ret);
        nxsem_post(&priv->exclsem);
        return ret;
    }

    nxsem_post(&priv->exclsem);
    return ret;
}

#endif /* CONFIG_SENSORS_ADXL357 */

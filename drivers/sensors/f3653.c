/****************************************************************************
 * nuttx/drivers/sensors/f3653.c
 *
 ****************************************************************************/

#include <nuttx/config.h>

/************************************************************************************
 * Included Files
 ************************************************************************************/
#ifdef CONFIG_SENSORS_SSI_ANGLE_F3653

#include <sys/types.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <poll.h>
#include <errno.h>
#include <debug.h>
#include <endian.h>
#include <stdio.h>

#include <nuttx/time.h>
#include <nuttx/fs/fs.h>
#include <nuttx/sensors/f3653.h>
#include <nuttx/sensors/qencoder.h>
#include <sys/ioctl.h>


#ifndef CONFIG_STM32F7_SPI1
# error "SPI1 not configured!"
#endif

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/************************************************************************************
 * Private Types
 ************************************************************************************/

/************************************************************************************
 * Private Function Prototypes
 ************************************************************************************/

/* Character driver methods */

typedef FAR struct file file_t;
static int f3653_open(file_t *filep);
static int f3653_close(file_t *filep);
static ssize_t f3653_read(file_t *filep, FAR char *buffer, size_t buflen);
static ssize_t f3653_write(file_t *filep, FAR const char *buf, size_t buflen);
static int f3653_ioctl(FAR struct file *filep, int cmd, unsigned long arg);

/* helper functions */

static uint32_t getSensorPosition(FAR struct spi_dev_s *ssi_dev);
static uint32_t grayCodeToBinary(uint32_t grayNum);
static f3653_rotating_dir_t getSensorRotateDir(FAR f3653_dev_t *dev);
static float64 getSensorVelocity(FAR f3653_dev_t *dev);


/************************************************************************************
 * Private Data
 ************************************************************************************/

static const struct file_operations g_f3653_ops =
{ 
    f3653_open,    /* open */
    f3653_close,   /* close */
    f3653_read,    /* read */
    f3653_write,   /* write */
    0,             /* seek */
    f3653_ioctl    /* ioctl */
#ifndef CONFIG_DISABLE_POLL
    , 0            /* poll */
#endif
#ifndef CONFIG_DISABLE_PSEUDOFS_OPERATIONS
    , NULL         /* unlink */
#endif
};



/************************************************************************************
 * Private Functions
 ************************************************************************************/

static int f3653_open(file_t *filep)
{
    DEBUGASSERT(filep && filep->f_inode && filep->f_inode->i_private);

    FAR struct inode *inode = filep->f_inode;
    FAR f3653_dev_t *dev    = inode->i_private;
    int ret   = 0;
    int error = 0;  // OK

    /*
     * 1.) Setup of lower half driver (Timer functionality)
     */

    if (dev->open)
    {
        /* Device has already been opened, so deny the access.
         * We don't wont to support multiple opens yet to keep it
         * simple. */

        error = -EACCES;
    }
    else
    {
        /* Open the device and perform initial setup. */

        dev->open = true;
        ret = dev->ops->setup();
        if (ret < 0)
        {
            // Error handling ???
        }

        /*
         * 2.) Setup of private SPI device for SSI transfer with angle sensor
         */

        if(dev->ssi_dev != NULL)
        {
            DEBUGASSERT(dev->ssi_dev);
            SPI_SETFREQUENCY(dev->ssi_dev, F3653_SPI_FREQ);
            SPI_SETBITS(dev->ssi_dev, F3653_SPI_NBITS);
            SPI_SETMODE(dev->ssi_dev, F3653_SPI_MODE);
        }

        /*
         * 3.) Setup quadratur encoder driver for getting rotating direction
         */

        dev->fd_qencoder = fopen(F3653_QENCODER_PATH, "r");
        DEBUGASSERT(dev->fd_qencoder);

        if(dev->fd_qencoder != NULL)
        {
            ret = ioctl(dev->fd_qencoder->fs_fd, QEIOC_MAXVAL, (unsigned long) F3653_QENCODER_RESOLUTION);
        }

        if (ret == OK)
        {
            dev->ops->start();
        }
    }

    return error;
}

static int f3653_close(file_t *filep)
{
    DEBUGASSERT(filep && filep->f_inode && filep->f_inode->i_private);

    FAR struct inode *inode = filep->f_inode;
    FAR f3653_dev_t *dev = inode->i_private;

    if(dev->open)
    {
        if (dev->running)
        {
            dev->ops->stop();
        }
        if (dev->fd_qencoder != NULL)
        {
            fclose(dev->fd_qencoder);
        }

        dev->open = false;
    }

    return 0;
}

static ssize_t f3653_read(file_t *filep, FAR char *buf, size_t buflen)
{
    DEBUGASSERT(filep && filep->f_inode && filep->f_inode->i_private && buf);

    FAR struct inode *inode = filep->f_inode;
    FAR f3653_dev_t *dev    = inode->i_private;

    if(!dev->open || !dev->running) return 0;

    if (buflen > sizeof(f3653_sample_t))
    {
        dev->last_sample = dev->current_sample;

        clock_gettime(CLOCK_REALTIME, &dev->current_sample.timeStamp);

        uint32_t position = getSensorPosition(dev->ssi_dev);

        dev->current_sample.angle     = (float) (position * 360.0f / F3653_POSITION_RESOLUTION);
        dev->current_sample.velocity  = getSensorVelocity(dev);

        *(FAR struct f3653_sample_s *)buf = dev->current_sample;

        return sizeof(f3653_sample_t);
    }
    return 0;
}

static ssize_t f3653_write(file_t *filep, FAR const char *buf, size_t buflen)
{
    /* Nothing to do here */

    return 0;
}

static int f3653_ioctl(FAR struct file *filep, int cmd, unsigned long arg)
{
    /* No IOCTL commands supported */

    return 0;
}

/****************************************************************************
 * Name: grayCodeToBinary
 *
 * Description:
 *       Converts gray coded parameter 'num' to binary coded return value.
 *       Works for 32 bit or less.
 *
 ****************************************************************************/

static uint32_t grayCodeToBinary(uint32_t num)
{
    num = num ^ (num >> 16);
    num = num ^ (num >> 8);
    num = num ^ (num >> 4);
    num = num ^ (num >> 2);
    num = num ^ (num >> 1);
    return num;
}

/****************************************************************************
 * Name: getSensorPostion
 *
 * Description:
 *       Read 17-bit SSI word from angle sensor using SPI interface
 *
 ****************************************************************************/

static uint32_t getSensorPosition(FAR struct spi_dev_s *ssi_dev)
{
    DEBUGASSERT(ssi_dev);

    uint32_t sensorPos = 0;
    uint8_t  numBytes  = 3;           // read only 3 bytes = 24 bit

    if(ssi_dev == NULL) return sensorPos;

    /* Driver supports only blockwise (bytewise) SPI transfer */

    SPI_RECVBLOCK(ssi_dev, &sensorPos, numBytes);

    /* Driver uses wrong Endian codec. Swap Little Endian <-> Big Endian */

    sensorPos = htobe32(sensorPos);

    /* Mask first bit, because it seems to be invalid (always set). */

    sensorPos &= 0x7FFFFF00;

    /* Only 17 valid bits, so discard LSBs */

    sensorPos >>= 14;        // discard LSBs

    /* convert grayCode to binCode */

    sensorPos = grayCodeToBinary(sensorPos);

    return sensorPos;
}

/****************************************************************************
 * Name: getSensorRotateDir
 *
 * Description:
 *       Read direction of sensor rotation using quadratur encoder interface.
 *
 ****************************************************************************/

static f3653_rotating_dir_t getSensorRotateDir(FAR f3653_dev_t *dev)
{
    DEBUGASSERT(dev);

    f3653_rotating_dir_t ret = undef;

    if(dev->fd_qencoder != NULL && dev != NULL)
    {
        ret = dev->ops->getRotatingDir();
    }
    return ret;
}

/****************************************************************************
 * Name: getSensorVelocity
 *
 * Description:
 *      Reads and returns the velocity of the sensor shaft rotation.
 *
 *      Therefore, the following sources are used in descending order:
 *
 *          1.) Quadratur encoder interface (11 bit resolution + hardware
 *              captured time)
 *          2.) Absolut position measurement (17 bit + software captured
 *              time)
 *          3.) Smaller velocities that can not be resolved are assumed as
 *              0.
 *
 *      To use alaways the current absolut sensor position, this function
 *      should always be called after getSensorPosition!!!
 *
 ****************************************************************************/

static float64 getSensorVelocity(FAR f3653_dev_t *dev)
{
    DEBUGASSERT(dev);

    float64 ret = 0;

    if(dev != NULL)
    {
        ret = dev->ops->getRotatingVel();

        if(ret == 0)
        {
            /* Calculate the rotating velocity using the difference quotient */

            float64 dalpha = (dev->current_sample.angle - dev->last_sample.angle);

            if (dalpha == 0) return 0;

            if(dalpha <= -180)
            {
                dalpha = 360 + dalpha;
            }
            else if(dalpha >= 180)
            {
                dalpha = 360 - dalpha;
                dalpha *= (-1);
            }

            float64 dt = ((float64) dev->ops->getTimeDiff(&dev->last_sample.timeStamp,
                        &dev->current_sample.timeStamp)) / NSEC_PER_SEC; // [s]

            ret = dalpha / dt; // [deg/s]
        }
    }
    return ret;
}

/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: f3653_register
 *
 * Description:
 *   Register the upper half with its lower half in the VFS.
 *
 ************************************************************************************/

int f3653_register(FAR const char *path, FAR f3653_dev_t *dev)
{
    DEBUGASSERT(path && dev);

    /* Register the driver */

    return register_driver(path, &g_f3653_ops, 0444, dev);
}

#endif /* CONFIG_SENSORS_SSI_ANGLE */

/****************************************************************************
 * arch/drivers/analog/ltc2642.c
 *
 *   Copyright (C) 2010, 2016 Gregory Nutt. All rights reserved.
 *   Copyright (C) 2011 Li Zhuoyi. All rights reserved.
 *   Author: Li Zhuoyi <lzyy.cn@gmail.com>
 *           Gregory Nutt <gnutt@nuttx.org>
 *
 * This file is a part of NuttX:
 *
 *   Copyright (C) 2010 Gregory Nutt. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdlib.h>
#include <errno.h>
#include <debug.h>
#include <semaphore.h>

#include <nuttx/kmalloc.h>
#include <nuttx/fs/fs.h>
#include <nuttx/fs/ioctl.h>

//#include <stdio.h>
//#include <sys/types.h>
//#include <stdint.h>
//#include <stdbool.h>



//#include <arch/board/board.h>
#include <nuttx/arch.h>
#include <nuttx/analog/ltc2642.h>

#if defined(CONFIG_DAC_LTC2642)

#define LTC2642_EXCLUSIVE_SPI

/****************************************************************************
 * ad_private Types
 ****************************************************************************/

struct ltc2642_dev_s
{
//  int devno;
  FAR struct spi_dev_s  *spi1;      /* Cached SPI device reference */
  unsigned int devno1;
  FAR struct spi_dev_s  *spi2;      /* Cached SPI device reference */
  unsigned int devno2;
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* Character driver methods */

static int     ltc2642_open(FAR struct file *filep);
static int     ltc2642_close(FAR struct file *filep);
static ssize_t ltc2642_read(FAR struct file *filep, FAR char *buffer,
                             size_t buflen);
static ssize_t ltc2642_write(FAR struct file *filep, FAR const char *buffer, size_t buflen);

static int     ltc2642_ioctl(FAR struct file *filep, int cmd, unsigned long arg);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct file_operations g_ltc2642fops =
{
  ltc2642_open,
  ltc2642_close,
  ltc2642_read,
  ltc2642_write,
  NULL,
  ltc2642_ioctl,    /* ioctl */
};


/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: ltc2642_open
 *
 * Description:
 *   This function is called whenever the AD5662 device is opened.
 *
 ****************************************************************************/

static int ltc2642_open(FAR struct file *filep)
{
	return OK;
}

/****************************************************************************
 * Name: ltc2642_close
 *
 * Description:
 *   This routine is called when the AD5662 device is closed.
 *
 ****************************************************************************/

static int ltc2642_close(FAR struct file *filep)
{
	return OK;
}


/****************************************************************************
 * Name: ltc2642_read
 ****************************************************************************/

static ssize_t ltc2642_read(FAR struct file *filep, FAR char *buffer, size_t buflen)
{
	return -ENOSYS;
}


/****************************************************************************
 * Name: ltc2642_write
 ****************************************************************************/

static ssize_t ltc2642_write(FAR struct file *filep, FAR const char *buffer, size_t buflen)
{
	return -ENOSYS;
}


/****************************************************************************
 * Name: ltc2642_ioctl
 *
 * Description:
 *
 *
 * Input Parameters:
 *   filep -
 *   cmd - IOCTL command
 *   arg - must be empty
 *
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/


static int ltc2642_ioctl (FAR struct file *filep, int cmd, unsigned long arg)
{

	FAR struct inode *inode = filep->f_inode;
	FAR struct ltc2642_dev_s *priv;
	int ret=0;


//printf("arg: 0x%08X\n",arg);
	/* Get exclusive access to the SPI driver state structure */

	DEBUGASSERT(filep != NULL && filep->f_inode != NULL);
	inode = filep->f_inode;

	priv = (FAR struct ltc2642_dev_s*)inode->i_private;
	DEBUGASSERT(priv);

#if 0
	ret = sem_wait(&priv->exclsem);

	if (ret < 0)
	{
		int errcode = errno;
		DEBUGASSERT(errcode < 0);
		return -errcode;
	}
#endif

	switch(cmd){
		/*SPI Initialization
		 *set all settings
		 */
#if 0
		case ANIOC_LTC2642_RESET:
//			sem_post(&priv->exclsem);
			SPI_LOCK(priv->spi, true);
			SPI_SETFREQUENCY(priv->spi,CONFIG_LTC2642_SPIFREQUENCY);
			SPI_SETMODE(priv->spi,SPIDEV_MODE0);
			SPI_SETBITS(priv->spi,16);

			SPI_LOCK(priv->spi, false);

//			sem_post(&priv->exclsem);

			break;
#endif

		/*LTC2642 Write Config Register*/
		case ANIOC_LTC2642_SET1:

		        /*limit range*/
		        if(arg>0xFFFF) arg=0xFFFF;

#ifndef LTC2642_EXCLUSIVE_SPI
			SPI_LOCK(priv->spi1, true);
			SPI_SETMODE(spi1, SPIDEV_MODE0);
			SPI_SETBITS(spi1, 16);
			SPI_SETFREQUENCY(spi1, CONFIG_LTC2642_SPIFREQUENCY);
#endif

			SPI_SELECT(priv->spi1, priv->devno1, true);
			SPI_SEND(priv->spi1,(uint16_t)(arg));
			SPI_SELECT(priv->spi1, priv->devno1, false);

#ifndef LTC2642_EXCLUSIVE_SPI
			SPI_LOCK(priv->spi1, false);
#endif

		break;
                /*LTC2642 Write Config Register*/
                case ANIOC_LTC2642_SET2:

                        /*limit range*/
                        if(arg>0xFFFF) arg=0xFFFF;

#ifndef LTC2642_EXCLUSIVE_SPI
                        SPI_LOCK(priv->spi2, true);
                        SPI_SETMODE(spi2, SPIDEV_MODE0);
                        SPI_SETBITS(spi2, 16);
                        SPI_SETFREQUENCY(spi2, CONFIG_LTC2642_SPIFREQUENCY);
#endif

                        SPI_SELECT(priv->spi2, priv->devno2, true);
                        SPI_SEND(priv->spi2,(uint16_t)(arg));
                        SPI_SELECT(priv->spi2, priv->devno2, false);

#ifndef LTC2642_EXCLUSIVE_SPI
                        SPI_LOCK(priv->spi2, false);
#endif

                break;
	}
	return ret;
}





/****************************************************************************
 * Name: dac_lock
 *
 * Description:
 *   Lock and configure the SPI bus.
 *
 ****************************************************************************/

static void dac_lock(FAR struct spi_dev_s *spi)
{
  (void)SPI_LOCK(spi, true);
  SPI_SETMODE(spi, SPIDEV_MODE0);
  SPI_SETBITS(spi, 16);
  SPI_SETFREQUENCY(spi, CONFIG_LTC2642_SPIFREQUENCY);
}

/****************************************************************************
 * Name: dac_unlock
 *
 * Description:
 *   Unlock the SPI bus.
 *
 ****************************************************************************/

static void dac_unlock(FAR struct spi_dev_s *spi)
{
  (void)SPI_LOCK(spi, false);
}



/****************************************************************************
 * Public Functions
 ****************************************************************************/


/****************************************************************************
 * Name: ltc2642_register
 *
 * Description:
 *   Register the LTC2642 character device as 'devpath'
 *
 * Input Parameters:
 *   devpath - The full path to the driver to register. E.g., "/dev/dac0"
 *   spi - An instance of the SPI interface to use to communicate with
 *     LTC2642
 *   devno - SPI device number
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/

int ltc2642_register(FAR const char *devpath, FAR struct spi_dev_s *spi1,
                      unsigned int devno1, FAR struct spi_dev_s *spi2, unsigned int devno2)
{
  FAR struct ltc2642_dev_s *priv;
  int ret=0;

  /* Sanity check */

  DEBUGASSERT(spi1 != NULL);
  DEBUGASSERT(spi2 != NULL);
  /* Initialize the LTC2642 device structure */

  priv = (FAR struct ltc2642_dev_s *)kmm_malloc(sizeof(struct ltc2642_dev_s));
  if (priv == NULL)
    {
      aerr("ERROR: Failed to allocate ltc2642_dev_s instance\n");
      return -ENOMEM;
    }

  priv->spi1 = spi1;
  priv->devno1 = devno1;
  priv->spi2 = spi2;
  priv->devno2 = devno2;
//  ret = nxsem_init(&priv->sem, 1, 1);
//  if (ret < 0)
//    {
//      kmm_free(dacpriv);
//      return ret;
//    }


#ifdef LTC2642_EXCLUSIVE_SPI
			SPI_LOCK(priv->spi1, true);
			SPI_SETMODE(spi1, SPIDEV_MODE0);
			SPI_SETBITS(spi1, 16);
			SPI_SETFREQUENCY(spi1, CONFIG_LTC2642_SPIFREQUENCY);

                        SPI_LOCK(priv->spi2, true);
                        SPI_SETMODE(spi2, SPIDEV_MODE0);
                        SPI_SETBITS(spi2, 16);
                        SPI_SETFREQUENCY(spi2, CONFIG_LTC2642_SPIFREQUENCY);


#endif
  /* Register the character driver */
  ret = register_driver(devpath, &g_ltc2642fops, 0666, priv);
  if (ret < 0)
    {
      aerr("ERROR: Failed to register adc driver: %d\n", ret);
//      nxsem_destroy(&priv->sem);
      kmm_free(priv);
    }

  return ret;
}


#endif

/*
 * stm32_adlx357.c
 *
 *  Created on: 24.04.2019
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <debug.h>
#include <stddef.h>
#include <sys/ioctl.h>

#include <nuttx/board.h>
#include <arch/board/board.h>
#include <nuttx/sensors/adxl357.h>
#include <nuttx/drivers/pwm.h>

#include "mB_Acc.h"
#include "up_arch.h"
#include "stm32_spi.h"
#include "stm32_exti.h"
#include "stm32_tim.h"
#include <nuttx/clock.h>
#include <fcntl.h>
#include <math.h>
#include <nuttx/arch.h>

/****************************************************************************
 * Private Types
 ****************************************************************************/

struct adxl357configPriv_s
{
     /* A reference to a structure of this type must be passed to the ADXL357
      * driver.  This structure provides information about the configuration
      * of the ADXL357 and provides some board-specific hooks.
      */

     struct adxl357_config_s config;

     /* Additional private definitions only known to this driver */

     ADXL357_HANDLE handle;      /* The ADXL345 driver handle */
     xcpt_t handler[3];          /* The ADXL345 interrupt handlers */
     FAR void *arg[3];           /* Arguments to pass to the interrupt handlers */
};


static uint16_t g_tim3_arr = 0;


/****************************************************************************
 * Static Function Prototypes
 ****************************************************************************/

static void stm32_adxl357_extSync_init(void);
static void stm32_adxl357_phaseShift_init(void);
static void stm32_adxl357_tim3_callback(void);
static void adxl357_triggerSync(void);
static void adxl357_triggerPhaseShift(int8_t nPeriods);

/* IRQ/GPIO access callbacks.  These operations all hidden behind callbacks
 * to isolate the ADXL345 driver from differences in GPIO interrupt handling
 * by varying boards and MCUs.
 *
 *   attach  - Attach the ADXL345 interrupt handler to the GPIO interrupt
 *   enable  - Enable or disable the GPIO interrupt
 *   clear   - Acknowledge/clear any pending GPIO interrupt
 */

static int  adxl357_attachIRQ(FAR struct adxl357_config_s *state, xcpt_t handlerDRDY, FAR void *argDRDY,
                          xcpt_t handlerInt1, FAR void* argInt1,
                          xcpt_t handlerInt2, FAR void* argInt2);
static void adxl357_enableIRQ(FAR struct adxl357_config_s *state, adxl357_IRQ_e irq, bool enable);



/****************************************************************************
 * Private Data
 ****************************************************************************/

static struct adxl357configPriv_s g_adxl357config =
{
  .config =
  {
        .sensor =
        {
            .offsets           = {0,0,0},
            .hpfcorner         = off,
            .odr               = odr_1000Hz,        // output data rate (ODR) = 1000Hz and low-pass filter corner = 250 Hz
            .acc_read_divider  = 10,                // = > Read 10 sampels as bulk (100 Hz) [Min: 1, Max: 32]
            .temp_read_divider = 100,               // = > 1 Hz for polling temperature
            .range             = range_20g,
        },

    .attachIRQ         = adxl357_attachIRQ,
    .enableIRQ         = adxl357_enableIRQ,
    .triggerSync       = adxl357_triggerSync,
    .triggerPhaseShift = adxl357_triggerPhaseShift,
  },

  .handle  = NULL,
  .handler = {NULL, NULL, NULL},
  .arg     = {NULL, NULL, NULL},
};

#if defined(CONFIG_ADXL357_CLK_EXT)
static struct pwm_info_s g_tim_pwm_info =
{
  /* Other than described in manual, the ADXL357 seems to need a constant
   * external clock of 1.024 Mhz independently of the selected output data
   * rate (ODR).
   */

  .frequency = 1024000,                 // [Hz] = 64 * 4 * 1000 * 4 = 256 kHz * 4 = 1.024 Mhz
  .duty      = 0x8000                   // 50%
};
#endif

/************************************************************************************
 * Name: stm32_adxl357_phaseShift_init
 *
 * Description:
 *
 ************************************************************************************/

static void stm32_adxl357_phaseShift_init(void)
{
    g_tim3_arr = getreg16(STM32_TIM3_ARR);
}

/************************************************************************************
 * Name: adxl357_triggerPhaseShift
 *
 * Description:
 *
 ************************************************************************************/

static void adxl357_triggerPhaseShift(const int8_t nPeriods)
{
    /* Update (typically unnecessary, but to be sure..) */

    g_tim3_arr = getreg16(STM32_TIM3_ARR);

    const int8_t offset = nPeriods % g_tim3_arr;

    if(offset != 0)
    {
        /* Increase the periode temporally by increasing the ARR value.
         * The value will be reseted in interrupt callback.
         */

        const uint16_t tim3_arr_new = g_tim3_arr + offset;
        putreg16(tim3_arr_new, STM32_TIM3_ARR);

        /* Enable the corresponding interrupt.
         * In this interrupt we will disable the input trigger at TIM5 again
         * to ensure uniqueness of the SYNC signal.
         */

        uint16_t dier = getreg16(STM32_TIM3_DIER);
        dier |= GTIM_DIER_UIE;
        putreg16(dier, STM32_TIM3_DIER);
    }
}

/************************************************************************************
 * Name: stm32_adxl357_extSync_init
 *
 * Description:
 *   Called to configure and initialize ADXL357 driver and its SPI bus.
 *
 ************************************************************************************/

static void stm32_adxl357_extSync_init(void)
{
#ifdef CONFIG_STM32F7_TIM3

    /******* TIM 3 (Master for TIM 5) **************/
    /* ch1: Trigger for TIM5 (SYNC)                */
    /* ch2: Unused                                 */
    /* ch3: Unused                                 */
    /* ch4: Clock signal (PWM)                     */
    /***********************************************/

    /* Deactivate TIM 3 */

    uint16_t tim3_cr1 = getreg16(STM32_TIM3_CR1);
    tim3_cr1 &= ~GTIM_CR1_CEN;
    putreg16(tim3_cr1, STM32_TIM3_CR1);

    /* CCR1 defines t_delay. The edge must be approx. 25 ns BEFORE the PWM edge.
     * We calc the closest ccr value possible with the given timer clock.
     */

    const float delta_t      = 25.0f; // [ns]
    const float frikelfaktor = 2.5f;
    const uint8_t prescaler  = getreg16(STM32_TIM3_PSC) + 1;
    const uint16_t delta_ccr = round(frikelfaktor * STM32_APB1_TIM3_CLKIN / prescaler * delta_t / NSEC_PER_SEC);
    const uint16_t arr2      = getreg16(STM32_TIM3_ARR);
    const uint16_t ccr1      = arr2 - delta_ccr;

    putreg16(ccr1, STM32_TIM3_CCR1);

    /* Enable master mode: Compare pulse (TRGO is send when CC1IF is to be set, as soon as a compare match occurred)*/

    uint16_t tim3_cr2 = getreg16(STM32_TIM3_CR2);
    tim3_cr2 |= GTIM_CR2_MMS_COMPP;
    putreg16(tim3_cr2, STM32_TIM3_CR2);

    /* Attach callback interrupt handler */

    const int ret = irq_attach(STM32_IRQ_TIM3, (xcpt_t)(stm32_adxl357_tim3_callback), NULL);
    if (ret < 0)
    {
        // Error handling ???
    }

    /* Enable the timer interrupt at the NVIC */

    up_enable_irq(STM32_IRQ_TIM3);

    /* Note: Interrupt at timer still deactivated! */

    /* (Re-)Activate TIM 3 */

    tim3_cr1 = getreg16(STM32_TIM3_CR1);
    tim3_cr1 |= GTIM_CR1_CEN;
    putreg16(tim3_cr1, STM32_TIM3_CR1);
#endif


#if defined(CONFIG_STM32F7_TIM5) || defined(CONFIG_STM32F7_HPTC_AUXTIMER_TIM5)

    /******* TIM 5 (Slave) **************/
    /* ch1 : SYNC                       */
    /************************************/

    /* Deactivate TIM 5 */

    uint16_t tim5_cr1 = getreg16(STM32_TIM5_CR1);
    tim5_cr1 &= ~GTIM_CR1_CEN;
    tim5_cr1 |= GTIM_CR1_OPM;
    putreg16(tim5_cr1, STM32_TIM5_CR1);

    /* Already enable ITR1 (TIM3), but let slave mode still disabled */

    uint16_t tim5_scmr = getreg16(STM32_TIM5_SMCR);
    tim5_scmr |= GTIM_SMCR_ITR1;
    tim5_scmr |= GTIM_SMCR_DISAB;
    putreg16(tim5_scmr, STM32_TIM5_SMCR);

    /* Set output compare mode to PWM2 (active when ccr < cnt < arr) and active output pin */

    uint32_t tim5_ccmr1 = getreg32(STM32_TIM5_CCMR1);
    tim5_ccmr1 |= GTIM_CCMR_MODE_PWM2 << ATIM_CCMR1_OC1M_SHIFT;
    tim5_ccmr1 |= GTIM_CCMR_CCS_CCOUT  << ATIM_CCMR1_CC1S_SHIFT;
    putreg32(tim5_ccmr1, STM32_TIM5_CCMR1);

    /* Set Prescaler, CCR1, and ARR */

    putreg16(0, STM32_TIM5_PSC);
    putreg16(1, STM32_TIM5_CCR1); // important to reset output to 0 after reload
    putreg16(400, STM32_TIM5_ARR);

    /* Enable output pin */

    stm32_configgpio(GPIO_TIM5_CH1OUT);
    uint16_t ccer = getreg16(STM32_TIM5_CCER);
    ccer |= GTIM_CCER_CC1E;
    putreg16(ccer, STM32_TIM5_CCER);

#if 0
    /* Activate TIM 5 */

    tim5_cr1 = getreg16(STM32_TIM5_CR1);
    tim5_cr1 |= GTIM_CR1_CEN;
    putreg16(tim5_cr1, STM32_TIM5_CR1);
#endif


#endif
}

/************************************************************************************
 * Name: adxl357_triggerSync
 *
 * Description:
 *
 ************************************************************************************/

static void adxl357_triggerSync(void)
{
    /* Enable slave mode (trigger mode) and configure input trigger TIM3 -> TIM5: ITR1 */

    uint16_t tim5_scmr = getreg16(STM32_TIM5_SMCR);
    tim5_scmr |= GTIM_SMCR_ITR1;
    tim5_scmr |= GTIM_SMCR_TRIGGER;
    putreg16(tim5_scmr, STM32_TIM5_SMCR);

    /* Enable the corresponding interrupt.
     * In this interrupt we will disable the input trigger at TIM5 again
     * to ensure uniqueness of the SYNC signal.
     */

    uint16_t dier = getreg16(STM32_TIM3_DIER);
    dier |= GTIM_DIER_UIE;
    putreg16(dier, STM32_TIM3_DIER);
}

/************************************************************************************
 * Name: stm32_adxl357_tim3_callback
 *
 * Description:
 *
 ************************************************************************************/

static void stm32_adxl357_tim3_callback(void)
{
    const uint16_t sr = getreg16(STM32_TIM3_SR);

    if(sr & GTIM_SR_UIF)
    {
        /* Disable slave mode */

        uint16_t tim5_scmr = getreg16(STM32_TIM5_SMCR);
        tim5_scmr &= ~GTIM_SMCR_TRIGGER;
        putreg16(tim5_scmr, STM32_TIM5_SMCR);

        /* Reset ARR */

        putreg16(g_tim3_arr, STM32_TIM3_ARR);

        /* Auto-Disable this interrupt */

        uint16_t dier = getreg16(STM32_TIM3_DIER);
        dier &= ~GTIM_DIER_UIE;
        putreg16(dier, STM32_TIM3_DIER);
    }
}

/************************************************************************************
 * Name: stm32_adxl357_initialize
 *
 * Description:
 *   Called to configure and initialize ADXL357 driver and its SPI bus.
 *
 ************************************************************************************/

void stm32_adxl357_initialize(void)
{
#if defined(CONFIG_ADXL357_SPI)

    /* Configure SPI bus for ADXL357 */

    struct spi_dev_s *dev = stm32_spibus_initialize(ADXL357_SPI_BUS);
    if (!dev)
    {
        snerr("ERROR: Failed to initialize SPI bus %d\n", ADXL357_SPI_BUS);
        return;
    }
#endif

    g_adxl357config.handle = adxl357_instantiate(dev, &g_adxl357config.config);
    if(g_adxl357config.handle == NULL)
    {
        snerr("ERROR: Failed to instantiate the ADXL357 driver\n");
        return;
    }
    else
    {
        const int ret = adxl357_register(g_adxl357config.handle, ADXL357_SPI_MINOR);

        if (ret < 0)
        {
            snerr("ERROR: Failed to register ADXL357 driver: %d\n", ret);
            return;
        }
    }

    /* ADXL357 is in standby now. If it is configured for external clock,
     * we need to set up this clock source now aswell.
     */

#if defined(CONFIG_ADXL357_CLK_EXT)
# if defined(CONFIG_STM32F7_TIM3_PWM)

    const int fd = open("/dev/pwm2", O_RDONLY);
    if(fd < 0)
    {
        snerr("Open TIM5 for PWM ('/dev/pwm4') failed!");
        return;
    }

    const int ret2 = ioctl(fd, PWMIOC_SETCHARACTERISTICS, (unsigned int) &g_tim_pwm_info);

    /* Start the clock now. It will run for ever, so the ADXL357 driver can be stopped/resumed
     * independently and without worrying about the external clock.
     */

    if (ret2 == OK)
    {
        ioctl(fd, PWMIOC_START, (unsigned int) 0);
    }
# else
#   warning "TIM3 PWM needs to be enabled to clock the ADXL357 chip externally!"
# endif

# if defined(CONFIG_STM32F7_TIM3) && (defined(CONFIG_STM32F7_TIM5) || defined(CONFIG_STM32F7_HPTC_AUXTIMER_TIM5))
    stm32_adxl357_extSync_init();
# else
#   warning "TIM3 and TIM5 need to be enabled to sync the ADXL357 chip externally!"
# endif

# if defined(CONFIG_STM32F7_TIM3)
    stm32_adxl357_phaseShift_init();
# else
#   warning "TIM3 needs to be enabled to enable phase shift functionality!"
# endif

#endif /* CONFIG_ADXL357_CLK_EXT */
}

/************************************************************************************
 ***********************************************************************************/

/* IRQ/GPIO access callbacks.  These operations all hidden behind
 * callbacks to isolate the ADXL345 driver from differences in GPIO
 * interrupt handling by varying boards and MCUs.
 *
 *   attach  - Attach the ADXL345 interrupt handler to the GPIO interrupt
 *   enable  - Enable or disable the GPIO interrupt
 *   clear   - Acknowledge/clear any pending GPIO interrupt
 */

/************************************************************************************
 * Name: adxl357_attachIRQ
 *
 * Description:
 *
 ************************************************************************************/
static int  adxl357_attachIRQ(FAR struct adxl357_config_s *state, xcpt_t handlerDRDY, FAR void *argDRDY,
                          xcpt_t handlerInt1, FAR void* argInt1,
                          xcpt_t handlerInt2, FAR void* argInt2)
{
    FAR struct adxl357configPriv_s *priv = (FAR struct adxl357configPriv_s*)state;

    DEBUGASSERT(priv);

    /* Just save the handlers and their arguments.  We will use them when interrupts
     * are enabled
     */

    priv->handler[0] = handlerDRDY;
    priv->handler[1] = handlerInt1;
    priv->handler[2] = handlerInt2;
    priv->arg[0] = argDRDY;
    priv->arg[1] = argInt1;
    priv->arg[2] = argInt2;

    return OK;
}

/************************************************************************************
 * Name: adxl357_enableIRQ
 *
 * Description:
 *
 ************************************************************************************/
static void adxl357_enableIRQ(FAR struct adxl357_config_s *state, adxl357_IRQ_e irq, bool enable)
{
    FAR struct adxl357configPriv_s *priv = (FAR struct adxl357configPriv_s*)state;

    DEBUGASSERT(priv);

    if (irq == ADXL357_IRQ_DRDY)
    {
        if (enable && priv->handler[0] != NULL && priv->arg[0] != NULL)
        {
            stm32_gpiosetevent(GPIO_ADXL357_DRDY, true, false, false, priv->handler[0],
                    priv->arg[0]);
        }
        else
        {
            /* NULL as func handler disables corresponding interrupt */

            stm32_gpiosetevent(GPIO_ADXL357_DRDY, false, false, false, NULL, NULL);
        }
    }
    else if (irq == ADXL357_IRQ_INT1)
    {
        if (enable && priv->handler[1] != NULL && priv->arg[1] != NULL)
        {
            stm32_gpiosetevent(GPIO_ADXL357_INT1, true, false, false, priv->handler[1],
                    priv->arg[1]);
        }
        else
        {
            /* NULL as func handler disables corresponding interrupt */

            stm32_gpiosetevent(GPIO_ADXL357_INT1, false, false, false, NULL, NULL);
        }
    }
    else if (irq == ADXL357_IRQ_INT2)
    {
        if (enable && priv->handler[2] != NULL && priv->arg[2] != NULL)
        {
            stm32_gpiosetevent(GPIO_ADXL357_INT2, true, false, false, priv->handler[2],
                    priv->arg[2]);
        }
        else
        {
            /* NULL as func handler disables corresponding interrupt */

            stm32_gpiosetevent(GPIO_ADXL357_INT2, false, false, false, NULL, NULL);
        }
    }
}

/****************************************************************************
 * configs/mB_TSIC/src/stm32_tsic.c
 *
 *   Copyright 2020 Leichtwerk Research GmbH
 *   Author: Stefan Nowak
 *
 ****************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <debug.h>
#include <stddef.h>
#include <sys/ioctl.h>

#include <nuttx/board.h>
#include <arch/board/board.h>
#include <nuttx/sensors/tsic.h>

#include "mB_TSIC.h"
#include "up_arch.h"
#include "stm32_exti.h"
//#include "stm32_tim.h"
#include "stm32_capture.h"
#include "stm32_dma.h"

#include <nuttx/clock.h>
#include <fcntl.h>
#include <nuttx/arch.h>

#include <stdio.h> //XXX remove me, debug only

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#if CONFIG_TSIC_CHANNELS >7
#error "CONFIG_TSIC_CHANNELS too high: board only supports 7 TSIC sensors"
#endif


typedef FAR void *TSIC_HANDLE;

/* DMA control words */

#  define TSIC_DMA_CONTROL_WORD      \
              (DMA_SCR_DIR_P2M       | \
               DMA_SCR_CIRC          | \
               DMA_SCR_MINC          | \
               DMA_SCR_PSIZE_16BITS   | \
               DMA_SCR_MSIZE_16BITS   | \
               DMA_SCR_PRIMED  | \
               DMA_SCR_PBURST_SINGLE | \
               DMA_SCR_MBURST_SINGLE)

/*allign to cacheline, size of multiple cachlines (32 byte) */
#define STM32_TSIC_DMA_BUFFER_SIZE 64

#define TSIC_CAP1_PRESCALER (STM32_APB2_TIM1_CLKIN/TSIC_SAMPLE_FREQ)
#if CONFIG_TSIC_CHANNELS > 4
#define TSIC_CAP2_PRESCALER (STM32_APB1_TIM4_CLKIN/TSIC_SAMPLE_FREQ)
#endif

/****************************************************************************
 * Private Types
 ****************************************************************************/

struct tsicconfigPriv_s
{
     /* A reference to a structure of this type must be passed to the TSIC
      * driver.  This structure provides information about the configuration
      * of the TSIC and provides some board-specific hooks.
      */

     struct tsic_config_s config;

     /* Additional private definitions only known to this driver */

     struct stm32_cap_dev_s *cap1;
     uint16_t cap1_cnt;
#if CONFIG_TSIC_CHANNELS > 4
     struct stm32_cap_dev_s *cap2;
     uint16_t cap2_cnt;
#endif
     DMA_HANDLE dma_handle[CONFIG_TSIC_CHANNELS];
//     TSIC_HANDLE handle;      /* The TSIC driver handle */
     //xcpt_t handler;       /* The ADXL345 interrupt handlers */
     //FAR void *arg;        /* Arguments to pass to the interrupt handlers */
};



/****************************************************************************
 * Static Function Prototypes
 ****************************************************************************/

static int stm32_tsic_enable(FAR struct tsic_config_s *conf, uint32_t ch);
static int stm32_tsic_update_refcnt(FAR struct tsic_config_s *conf);
static int32_t stm32_tsic_writepos(FAR struct tsic_config_s *conf, uint32_t ch);

/****************************************************************************
 * Private Data
 ****************************************************************************/

#if CONFIG_TSIC_CHANNELS >0
volatile static uint16_t g_tsic1_fifo[STM32_TSIC_DMA_BUFFER_SIZE] __attribute__((aligned(ARMV7M_DCACHE_LINESIZE)));
#endif
#if CONFIG_TSIC_CHANNELS >1
volatile static uint16_t g_tsic2_fifo[STM32_TSIC_DMA_BUFFER_SIZE] __attribute__((aligned(ARMV7M_DCACHE_LINESIZE)));
#endif
#if CONFIG_TSIC_CHANNELS >2
volatile static uint16_t g_tsic3_fifo[STM32_TSIC_DMA_BUFFER_SIZE] __attribute__((aligned(ARMV7M_DCACHE_LINESIZE)));
#endif
#if CONFIG_TSIC_CHANNELS >3
volatile static uint16_t g_tsic4_fifo[STM32_TSIC_DMA_BUFFER_SIZE] __attribute__((aligned(ARMV7M_DCACHE_LINESIZE)));
#endif
#if CONFIG_TSIC_CHANNELS >4
volatile static uint16_t g_tsic5_fifo[STM32_TSIC_DMA_BUFFER_SIZE] __attribute__((aligned(ARMV7M_DCACHE_LINESIZE)));
#endif
#if CONFIG_TSIC_CHANNELS >5
volatile static uint16_t g_tsic6_fifo[STM32_TSIC_DMA_BUFFER_SIZE] __attribute__((aligned(ARMV7M_DCACHE_LINESIZE)));
#endif
#if CONFIG_TSIC_CHANNELS >6
volatile static uint16_t g_tsic7_fifo[STM32_TSIC_DMA_BUFFER_SIZE] __attribute__((aligned(ARMV7M_DCACHE_LINESIZE)));
#endif


static struct tsicconfigPriv_s g_tsicconfig =
{
  .config =
  {
    .enable          = stm32_tsic_enable,
    .update_refcnt   = stm32_tsic_update_refcnt,
    .writepos        = stm32_tsic_writepos,
  },
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/


void stm32_tsic_capture_initialize(struct tsicconfigPriv_s *conf)
{
    /* FIXME: dma not implemented in stm32_capture, so just use it for
     * timer and related GPIO initialization
     */

    stm32_cap_ch_cfg_t tsic_cap_cfg=STM32_CAP_MAPPED_TI1 | STM32_CAP_INPSC_NO | STM32_CAP_FILTER_DTS_D32_N8 | STM32_CAP_EDGE_RISING;

    conf->cap1=stm32_cap_init(1);
    STM32_CAP_SETCLOCK(conf->cap1,STM32_CAP_CLK_INT,TSIC_CAP1_PRESCALER,0xFFFF);

#if CONFIG_TSIC_CHANNELS >4
    conf->cap2=stm32_cap_init(4);
    STM32_CAP_SETCLOCK(conf->cap2,STM32_CAP_CLK_INT,TSIC_CAP2_PRESCALER,0xFFFF);
#endif
    /* BUG: STM32_CAP_MAPPED_TI1 means direct TI
     *      STM32_CAP_MAPPED_TI2 means indirect TI
     */
#if CONFIG_TSIC_CHANNELS >0
//    tsic_cap_cfg= STM32_CAP_MAPPED_TI1 | STM32_CAP_INPSC_NO | STM32_CAP_FILTER_DTS_D32_N8 | STM32_CAP_EDGE_RISING;
    STM32_CAP_SETCHANNEL(conf->cap1,1,tsic_cap_cfg);
    g_tsicconfig.config.ch[0].refcnt=&g_tsicconfig.cap1_cnt;
#endif

#if CONFIG_TSIC_CHANNELS >1
//    tsic_cap_cfg= STM32_CAP_MAPPED_TI1 | STM32_CAP_INPSC_NO | STM32_CAP_FILTER_DTS_D32_N8 | STM32_CAP_EDGE_RISING;
    STM32_CAP_SETCHANNEL(conf->cap1,2,tsic_cap_cfg);
    g_tsicconfig.config.ch[1].refcnt=&g_tsicconfig.cap1_cnt;
#endif

#if CONFIG_TSIC_CHANNELS >2
//    tsic_cap_cfg= STM32_CAP_MAPPED_TI1 | STM32_CAP_INPSC_NO | STM32_CAP_FILTER_DTS_D32_N8 | STM32_CAP_EDGE_RISING;
    STM32_CAP_SETCHANNEL(conf->cap1,3,tsic_cap_cfg);
    g_tsicconfig.config.ch[2].refcnt=&g_tsicconfig.cap1_cnt;
#endif

#if CONFIG_TSIC_CHANNELS >3
//    tsic_cap_cfg= STM32_CAP_MAPPED_TI1 | STM32_CAP_INPSC_NO | STM32_CAP_FILTER_DTS_D32_N8 | STM32_CAP_EDGE_RISING;/* BUG: STM32_CAP_MAPPED_TI1 means direct TI */
    STM32_CAP_SETCHANNEL(conf->cap1,4,tsic_cap_cfg);
    g_tsicconfig.config.ch[3].refcnt=&g_tsicconfig.cap1_cnt;
#endif

#if CONFIG_TSIC_CHANNELS >4
//    tsic_cap_cfg= STM32_CAP_MAPPED_TI1 | STM32_CAP_INPSC_NO | STM32_CAP_FILTER_DTS_D32_N8 | STM32_CAP_EDGE_RISING;
    STM32_CAP_SETCHANNEL(conf->cap2,1,tsic_cap_cfg);
    g_tsicconfig.config.ch[4].refcnt=&g_tsicconfig.cap2_cnt;
#endif

#if CONFIG_TSIC_CHANNELS >5
//    tsic_cap_cfg= STM32_CAP_MAPPED_TI1 | STM32_CAP_INPSC_NO | STM32_CAP_FILTER_DTS_D32_N8 | STM32_CAP_EDGE_RISING;
    STM32_CAP_SETCHANNEL(conf->cap2,2,tsic_cap_cfg);
    g_tsicconfig.config.ch[5].refcnt=&g_tsicconfig.cap2_cnt;
#endif

#if CONFIG_TSIC_CHANNELS >6
//    tsic_cap_cfg= STM32_CAP_MAPPED_TI1 | STM32_CAP_INPSC_NO | STM32_CAP_FILTER_DTS_D32_N8 | STM32_CAP_EDGE_RISING;
    STM32_CAP_SETCHANNEL(conf->cap2,3,tsic_cap_cfg);
    g_tsicconfig.config.ch[6].refcnt=&g_tsicconfig.cap2_cnt;
#endif

#if CONFIG_TSIC_CHANNELS >7
//    tsic_cap_cfg= STM32_CAP_MAPPED_TI1 | STM32_CAP_INPSC_NO | STM32_CAP_FILTER_DTS_D32_N8 | STM32_CAP_EDGE_RISING;/* BUG: STM32_CAP_MAPPED_TI1 means direct TI */
    STM32_CAP_SETCHANNEL(conf->cap2,4,tsic_cap_cfg);
    g_tsicconfig.config.ch[7].refcnt=&g_tsicconfig.cap2_cnt;
#endif





}



void stm32_tsic_dma_initialize(struct tsicconfigPriv_s *conf){

#if CONFIG_TSIC_CHANNELS >0
    conf->config.ch[0].fifo=g_tsic1_fifo;

   /* Try to take timer's DMA stream in the OS. It will be locked with
    * a semaphore so that no other driver can use the stream. If another
    * driver already uses the stream stm32_dmachannel waits until the
    * specified stream is released. This is a safety feature to make sure
    * the driver has exclusive access to the DMA stream.
    */
    conf->dma_handle[0]=stm32_dmachannel(DMAMAP_TIM1_CH1);

    /* Configure for circular DMA reception into the DMA FIFO */

    stm32_dmasetup(conf->dma_handle[0],
             STM32_TIM1_CCR1,
             (uint32_t)conf->config.ch[0].fifo,
             STM32_TSIC_DMA_BUFFER_SIZE,
             TSIC_DMA_CONTROL_WORD);
#endif


#if CONFIG_TSIC_CHANNELS >1
    conf->config.ch[1].fifo=g_tsic2_fifo;
    /* Try to take timer's DMA stream in the OS. It will be locked with
     * a semaphore so that no other driver can use the stream. If another
     * driver already uses the stream stm32_dmachannel waits until the
     * specified stream is released. This is a safety feature to make sure
     * the driver has exclusive access to the DMA stream.
     */
     conf->dma_handle[1]=stm32_dmachannel(DMAMAP_TIM1_CH2);

     /* Configure for circular DMA reception into the DMA FIFO */

     stm32_dmasetup(conf->dma_handle[1],
              STM32_TIM1_CCR2,
              (uint32_t)conf->config.ch[1].fifo,
              STM32_TSIC_DMA_BUFFER_SIZE,
              TSIC_DMA_CONTROL_WORD);


#endif

#if CONFIG_TSIC_CHANNELS >2
    conf->config.ch[2].fifo=g_tsic3_fifo;

    /* Try to take timer's DMA stream in the OS. It will be locked with
     * a semaphore so that no other driver can use the stream. If another
     * driver already uses the stream stm32_dmachannel waits until the
     * specified stream is released. This is a safety feature to make sure
     * the driver has exclusive access to the DMA stream.
     */
     conf->dma_handle[2]=stm32_dmachannel(DMAMAP_TIM1_CH3);

     /* Configure for circular DMA reception into the DMA FIFO */

     stm32_dmasetup(conf->dma_handle[2],
              STM32_TIM1_CCR3,
              (uint32_t)conf->config.ch[2].fifo,
              STM32_TSIC_DMA_BUFFER_SIZE,
              TSIC_DMA_CONTROL_WORD);

#endif

#if CONFIG_TSIC_CHANNELS >3
    conf->config.ch[3].fifo=g_tsic4_fifo;

    /* Try to take timer's DMA stream in the OS. It will be locked with
     * a semaphore so that no other driver can use the stream. If another
     * driver already uses the stream stm32_dmachannel waits until the
     * specified stream is released. This is a safety feature to make sure
     * the driver has exclusive access to the DMA stream.
     */
     conf->dma_handle[3]=stm32_dmachannel(DMAMAP_TIM1_CH4);

     /* Configure for circular DMA reception into the DMA FIFO */

     stm32_dmasetup(conf->dma_handle[3],
              STM32_TIM1_CCR4,
              (uint32_t)conf->config.ch[3].fifo,
              STM32_TSIC_DMA_BUFFER_SIZE,
              TSIC_DMA_CONTROL_WORD);

#endif

#if CONFIG_TSIC_CHANNELS >4
    conf->config.ch[4].fifo=g_tsic5_fifo;

    /* Try to take timer's DMA stream in the OS. It will be locked with
     * a semaphore so that no other driver can use the stream. If another
     * driver already uses the stream stm32_dmachannel waits until the
     * specified stream is released. This is a safety feature to make sure
     * the driver has exclusive access to the DMA stream.
     */
     conf->dma_handle[4]=stm32_dmachannel(DMAMAP_TIM4_CH1);

     /* Configure for circular DMA reception into the DMA FIFO */

     stm32_dmasetup(conf->dma_handle[4],
              STM32_TIM4_CCR1,
              (uint32_t)conf->config.ch[4].fifo,
              STM32_TSIC_DMA_BUFFER_SIZE,
              TSIC_DMA_CONTROL_WORD);

#endif

#if CONFIG_TSIC_CHANNELS >5
    conf->config.ch[5].fifo=g_tsic6_fifo;

    /* Try to take timer's DMA stream in the OS. It will be locked with
     * a semaphore so that no other driver can use the stream. If another
     * driver already uses the stream stm32_dmachannel waits until the
     * specified stream is released. This is a safety feature to make sure
     * the driver has exclusive access to the DMA stream.
     */
     conf->dma_handle[5]=stm32_dmachannel(DMAMAP_TIM4_CH2);

     /* Configure for circular DMA reception into the DMA FIFO */

     stm32_dmasetup(conf->dma_handle[5],
              STM32_TIM4_CCR2,
              (uint32_t)conf->config.ch[5].fifo,
              STM32_TSIC_DMA_BUFFER_SIZE,
              TSIC_DMA_CONTROL_WORD);

#endif

#if CONFIG_TSIC_CHANNELS >6
    conf->config.ch[6].fifo=g_tsic7_fifo;

    /* Try to take timer's DMA stream in the OS. It will be locked with
     * a semaphore so that no other driver can use the stream. If another
     * driver already uses the stream stm32_dmachannel waits until the
     * specified stream is released. This is a safety feature to make sure
     * the driver has exclusive access to the DMA stream.
     */
     conf->dma_handle[6]=stm32_dmachannel(DMAMAP_TIM4_CH3);

     /* Configure for circular DMA reception into the DMA FIFO */

     stm32_dmasetup(conf->dma_handle[6],
              STM32_TIM4_CCR3,
              (uint32_t)conf->config.ch[6].fifo,
              STM32_TSIC_DMA_BUFFER_SIZE,
              TSIC_DMA_CONTROL_WORD);

#endif



    for(int i=0;i< CONFIG_TSIC_CHANNELS;i++)
    {
        /* set fifosize */
        conf->config.ch[i].fifosize=STM32_TSIC_DMA_BUFFER_SIZE;

        /* Enable DMA for TSIC */
        stm32_dmaenable(conf->dma_handle[i]);
    }

return;
}



static int stm32_tsic_enable(FAR struct tsic_config_s *conf ,uint32_t ch){


//    FAR struct tsicconfigPriv_s *priv = (FAR struct tsicconfigPriv_s *) conf;

    uint16_t regval;
#if CONFIG_TSIC_CHANNELS >0
    if(ch==0 || ch==1){
            regval  = getreg16(STM32_TIM1_DIER);
            regval |= ATIM_DIER_CC1DE;
            putreg16(regval,STM32_TIM1_DIER);
    }
#endif
#if CONFIG_TSIC_CHANNELS >1
    if(ch==0 || ch==2){
            regval  = getreg16(STM32_TIM1_DIER);
            regval |= ATIM_DIER_CC2DE;
            putreg16(regval,STM32_TIM1_DIER);
    }
#endif
#if CONFIG_TSIC_CHANNELS >2
    if(ch==0 || ch==3){
            regval  = getreg16(STM32_TIM1_DIER);
            regval |= ATIM_DIER_CC3DE;
            putreg16(regval,STM32_TIM1_DIER);
    }
#endif
#if CONFIG_TSIC_CHANNELS >3
    if(ch==0 || ch==4){
            regval  = getreg16(STM32_TIM1_DIER);
            regval |= ATIM_DIER_CC4DE;
            putreg16(regval,STM32_TIM1_DIER);
    }
#endif
#if CONFIG_TSIC_CHANNELS >4
    if(ch==0 || ch==5){
    regval  = getreg16(STM32_TIM4_DIER);
    regval |= ATIM_DIER_CC1DE ;
    putreg16(regval,STM32_TIM4_DIER);
    }
#endif
#if CONFIG_TSIC_CHANNELS >5
    if(ch==0 || ch==6){
    regval  = getreg16(STM32_TIM4_DIER);
    regval |= ATIM_DIER_CC2DE;
    putreg16(regval,STM32_TIM4_DIER);
    }
#endif
#if CONFIG_TSIC_CHANNELS >6
    if(ch==0 || ch==7){
    regval  = getreg16(STM32_TIM4_DIER);
    regval |= ATIM_DIER_CC3DE;
    putreg16(regval,STM32_TIM4_DIER);
    }
#endif

    return 0;

}


static int stm32_tsic_update_refcnt(FAR struct tsic_config_s *conf){

    FAR struct tsicconfigPriv_s *priv = (FAR struct tsicconfigPriv_s *) conf;

    priv->cap1_cnt=STM32_CAP_GETCAPTURE(priv->cap1,STM32_CAP_CHANNEL_COUNTER);

#if CONFIG_TSIC_CHANNELS >4
    priv->cap2_cnt=STM32_CAP_GETCAPTURE(priv->cap2,STM32_CAP_CHANNEL_COUNTER);
#endif
    return 0;
}

/****************************************************************************
 * Name: stm32_tsic_writepos
 *
 * Description:
 *   Returns the index into the TSIC FIFO where the DMA will place the next
 *   capture. Invalidate cache for consistent data
 *
 ****************************************************************************/


static int32_t stm32_tsic_writepos(FAR struct tsic_config_s *conf, uint32_t ch){
    int32_t writepos=STM32_TSIC_DMA_BUFFER_SIZE;

    FAR struct tsicconfigPriv_s *priv = (FAR struct tsicconfigPriv_s *) conf;

    writepos-=stm32_dmaresidual(priv->dma_handle[ch]);
    up_invalidate_dcache((uintptr_t)priv->config.ch[ch].fifo, (uintptr_t)&priv->config.ch[ch].fifo[STM32_TSIC_DMA_BUFFER_SIZE]);
    return writepos;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/************************************************************************************
 * Name: stm32_tsic_initialize
 *
 * Description:
 *   Called to configure and initialize TSIC driver.
 *
 ************************************************************************************/

void stm32_tsic_initialize(void)
{
    int ret;

    stm32_tsic_capture_initialize(&g_tsicconfig);

    stm32_tsic_dma_initialize(&g_tsicconfig);

    ret = tsic_instantiate("/dev/tsic", &g_tsicconfig.config);
    if(ret<0)
    {
        snerr("ERROR: Failed to instantiate the TSIC driver\n");
        return;
    }
}


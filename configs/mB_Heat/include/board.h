/************************************************************************************
 * configs/mB_Heat/include/board.h
 *
 ************************************************************************************/

#ifndef __CONFIG_MB_HEAT_INCLUDE_BOARD_H
#define __CONFIG_MB_HEAT_INCLUDE_BOARD_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>


#ifndef __ASSEMBLY__
#  include <stdint.h>
#  include <stdbool.h>
#endif

/* Logic in arch/arm/src and configs/ may need to include these file prior to
 * including board.h:  stm32_rcc.h, stm32_sdio.h, stm32.h.  They cannot be included
 * here because board.h is used in other contexts where the STM32 internal header
 * files are not available.
 */

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/* If defined, HSI is used instead of HSE */

//# define STM32_BOARD_USEHSI      1
#undef STM32_BOARD_USEHSI

#ifndef STM32_BOARD_USEHSI
# define STM32_BOARD_USEHSE      1
#endif

/* Clocking *************************************************************************/
/* The STM32F7 mB_Heat Client board features a 4.096 MHz VCXO.
 *
 * This is the canonical configuration:
 *   System Clock source           : PLL (HSE)
 *   SYSCLK(Hz)                    : 216000000    Determined by PLL configuration
 *   HCLK(Hz)                      : 216000000    (STM32_RCC_CFGR_HPRE)
 *   AHB Prescaler                 : 1            (STM32_RCC_CFGR_HPRE)
 *   APB1 Prescaler                : 4            (STM32_RCC_CFGR_PPRE1)
 *   APB2 Prescaler                : 2            (STM32_RCC_CFGR_PPRE2)
 *   HSE Frequency(Hz)             : 8000000      (STM32_BOARD_XTAL)
 *   PLLM                          : 4            (STM32_PLLCFG_PLLM)
 *   PLLN                          : 216          (STM32_PLLCFG_PLLN)
 *   PLLP                          : 2            (STM32_PLLCFG_PLLP)
 *   PLLQ                          : 9            (STM32_PLLCFG_PLLQ)
 *   Main regulator output voltage : Scale1 mode  Needed for high speed SYSCLK
 *   Flash Latency(WS)             : 5
 *   Prefetch Buffer               : OFF
 *   Instruction cache             : ON
 *   Data cache                    : ON
 *   Require 48MHz for USB OTG FS, : Enabled
 *   SDIO and RNG clock

/* HSI - 16 MHz RC factory-trimmed
 * LSI - 32 KHz RC
 * HSE - On-board oscillator frequency is 8 MHz
 * LSE - N/A
 */

#  define STM32_BOARD_XTAL        8000000ul

#define STM32_HSI_FREQUENCY     16000000ul
#define STM32_LSI_FREQUENCY     32000
#ifdef STM32_BOARD_USEHSE
# define STM32_HSE_FREQUENCY    STM32_BOARD_XTAL
#endif
#define STM32_LSE_FREQUENCY     32768


/* Defines HSE/LSE clock source as 'external clock' */

//# define STM32_HSEBYP_ENABLE

/* Main PLL Configuration
 *
 * PLL source is HSE = 8,000,000  // if not, you need to replace HSE by HSI
 *
 * PLL_VCO = (STM32_HSE_FREQUENCY / PLLM) * PLLN
 * Subject to:
 *
 *     2 <= PLLM <= 63
 *   192 <= PLLN <= 432
 *   192 MHz <= PLL_VCO <= 432MHz
 *
 * SYSCLK  = PLL_VCO / PLLP
 * Subject to
 *
 *   PLLP = {2, 4, 6, 8}
 *   SYSCLK <= 216 MHz
 *
 * USB OTG FS, SDMMC and RNG Clock = PLL_VCO / PLLQ
 * Subject to
 *   The USB OTG FS requires a 48 MHz clock to work correctly. The SDMMC
 *   and the random number generator need a frequency lower than or equal
 *   to 48 MHz to work correctly.
 *
 * 2 <= PLLQ <= 15
 */

/* CASE 1: PLL source is HSE (VCXO)
 * PLL_VCO = (STM32_HSE_FREQUENCY / PLLM) * PLLN
 *         = (4,096,000 / 2) * 200
 *         = 384,000,000
 * SYSCLK  = PLL_VCO / PLLP
 *         = 409,600,000 / 2 = 204,800,000
 * USB OTG FS, SDIO and RNG Clock
 *         =  PLL_VCO / PLLQ
 *         = 384,000,000 / 8
 *         = 48,000,000
 *
 * CASE 2: PLL source is HSE (No VCXO)
 * PLL_VCO = (STM32_HSE_FREQUENCY / PLLM) * PLLN
 *         = (8,000,000 / 4) * 216
 *         = 432,000,000
 * SYSCLK  = PLL_VCO / PLLP
 *         = 432,000,000 / 2 = 216,000,000
 * USB OTG FS, SDIO and RNG Clock
 *         =  PLL_VCO / PLLQ
 *         = 432,000,000 / 9
 *         = 48,000,000
 *
 * CASE 3: PLL source is HSI
 * PLL_VCO = (STM32_HSI_FREQUENCY / PLLM) * PLLN
 *         = (16,000,000 / 8) * 216
 *         = 432,000,000
 * SYSCLK  = PLL_VCO / PLLP
 *         = 432,000,000 / 2 = 216,000,000
 * USB OTG FS, SDIO and RNG Clock
 *         =  PLL_VCO / PLLQ
 *         = 432,000,000 / 9
 *         = 24,000,000
 */

#if defined(STM32_BOARD_USEHSE)
#  define STM32_PLLCFG_PLLM       RCC_PLLCFG_PLLM(4)
#  define STM32_PLLCFG_PLLN       RCC_PLLCFG_PLLN(216)
#  define STM32_PLLCFG_PLLP       RCC_PLLCFG_PLLP_2
#  define STM32_PLLCFG_PLLQ       RCC_PLLCFG_PLLQ(9)
#elif defined(STM32_BOARD_USEHSI)
# define STM32_PLLCFG_PLLM       RCC_PLLCFG_PLLM(8)
# define STM32_PLLCFG_PLLN       RCC_PLLCFG_PLLN(216)
# define STM32_PLLCFG_PLLP       RCC_PLLCFG_PLLP_2
# define STM32_PLLCFG_PLLQ       RCC_PLLCFG_PLLQ(9)
#endif

#if defined(STM32_BOARD_USEHSE)
#  define STM32_VCO_FREQUENCY     ((STM32_HSE_FREQUENCY / 4) * 216)
#  define STM32_SYSCLK_FREQUENCY  (STM32_VCO_FREQUENCY / 2)
#  define STM32_OTGFS_FREQUENCY   (STM32_VCO_FREQUENCY / 9)
#elif defined(STM32_BOARD_USEHSI)
# define STM32_VCO_FREQUENCY     ((STM32_HSI_FREQUENCY / 8) * 216)
# define STM32_SYSCLK_FREQUENCY  (STM32_VCO_FREQUENCY / 2)
# define STM32_OTGFS_FREQUENCY   (STM32_VCO_FREQUENCY / 9
#endif

/* Configure Dedicated Clock Configuration Register 2 */

#define STM32_RCC_DCKCFGR2_USART1SRC  RCC_DCKCFGR2_USART1SEL_APB
#define STM32_RCC_DCKCFGR2_USART2SRC  RCC_DCKCFGR2_USART2SEL_APB
#define STM32_RCC_DCKCFGR2_UART4SRC   RCC_DCKCFGR2_UART4SEL_APB
#define STM32_RCC_DCKCFGR2_UART5SRC   RCC_DCKCFGR2_UART5SEL_APB
#define STM32_RCC_DCKCFGR2_USART6SRC  RCC_DCKCFGR2_USART6SEL_SYSCLK
#define STM32_RCC_DCKCFGR2_UART7SRC   RCC_DCKCFGR2_UART7SEL_APB
#define STM32_RCC_DCKCFGR2_UART8SRC   RCC_DCKCFGR2_UART8SEL_APB
#define STM32_RCC_DCKCFGR2_I2C1SRC    RCC_DCKCFGR2_I2C1SEL_HSI
#define STM32_RCC_DCKCFGR2_I2C2SRC    RCC_DCKCFGR2_I2C2SEL_HSI
#define STM32_RCC_DCKCFGR2_I2C3SRC    RCC_DCKCFGR2_I2C3SEL_HSI
#define STM32_RCC_DCKCFGR2_I2C4SRC    RCC_DCKCFGR2_I2C4SEL_HSI
#define STM32_RCC_DCKCFGR2_LPTIM1SRC  RCC_DCKCFGR2_LPTIM1SEL_APB
#define STM32_RCC_DCKCFGR2_CECSRC     RCC_DCKCFGR2_CECSEL_HSI
#define STM32_RCC_DCKCFGR2_CK48MSRC   RCC_DCKCFGR2_CK48MSEL_PLL
#define STM32_RCC_DCKCFGR2_SDMMCSRC   RCC_DCKCFGR2_SDMMCSEL_48MHZ
#define STM32_RCC_DCKCFGR2_SDMMC2SRC  RCC_DCKCFGR2_SDMMC2SEL_48MHZ

/* Several prescalers allow the configuration of the two AHB buses, the
 * high-speed APB (APB2) and the low-speed APB (APB1) domains. The maximum
 * frequency of the two AHB buses is 216 MHz while the maximum frequency of
 * the high-speed APB domains is 108 MHz. The maximum allowed frequency of
 * the low-speed APB domain is 54 MHz.
 */

/* AHB clock (HCLK) is SYSCLK (216 MHz) */

#define STM32_RCC_CFGR_HPRE     RCC_CFGR_HPRE_SYSCLK  /* HCLK  = SYSCLK / 1 */
#define STM32_HCLK_FREQUENCY    STM32_SYSCLK_FREQUENCY
#define STM32_BOARD_HCLK        STM32_HCLK_FREQUENCY  /* same as above, to satisfy compiler */

/* APB1 clock (PCLK1) is HCLK/4 (54 MHz) */

#define STM32_RCC_CFGR_PPRE1    RCC_CFGR_PPRE1_HCLKd4     /* PCLK1 = HCLK / 4 */
#define STM32_PCLK1_FREQUENCY   (STM32_HCLK_FREQUENCY/4)

/* Timer will be driven from HCLK when TIMPRE is selected
 * (or 4*STM32_PCLK1_FREQUENCY, if PCLK>4)
 *
 * */
#define STM32_RCC_DCKCFGR1_TIMPRE RCC_DCKCFGR1_TIMPRESEL

/* Timers driven from HCLK (216 MHz) */

#define STM32_APB1_TIM2_CLKIN   STM32_HCLK_FREQUENCY
#define STM32_APB1_TIM3_CLKIN   STM32_HCLK_FREQUENCY
#define STM32_APB1_TIM4_CLKIN   STM32_HCLK_FREQUENCY
#define STM32_APB1_TIM5_CLKIN   STM32_HCLK_FREQUENCY
#define STM32_APB1_TIM6_CLKIN   STM32_HCLK_FREQUENCY
#define STM32_APB1_TIM7_CLKIN   STM32_HCLK_FREQUENCY
#define STM32_APB1_TIM12_CLKIN  STM32_HCLK_FREQUENCY
#define STM32_APB1_TIM13_CLKIN  STM32_HCLK_FREQUENCY
#define STM32_APB1_TIM14_CLKIN  STM32_HCLK_FREQUENCY

/* APB2 clock (PCLK2) is HCLK/2 (108 MHz) */

#define STM32_RCC_CFGR_PPRE2    RCC_CFGR_PPRE2_HCLKd2     /* PCLK2 = HCLK / 2 */
#define STM32_PCLK2_FREQUENCY   (STM32_HCLK_FREQUENCY/2)

/* Timers driven from HCLK (216MHz) */

#define STM32_APB2_TIM1_CLKIN   STM32_HCLK_FREQUENCY
#define STM32_APB2_TIM8_CLKIN   STM32_HCLK_FREQUENCY
#define STM32_APB2_TIM9_CLKIN   STM32_HCLK_FREQUENCY
#define STM32_APB2_TIM10_CLKIN  STM32_HCLK_FREQUENCY
#define STM32_APB2_TIM11_CLKIN  STM32_HCLK_FREQUENCY



/* FLASH wait states
 *
 *  --------- ---------- -----------
 *  VDD       MAX SYSCLK WAIT STATES
 *  --------- ---------- -----------
 *  1.7-2.1 V   180 MHz    8
 *  2.1-2.4 V   216 MHz    9
 *  2.4-2.7 V   216 MHz    8
 *  2.7-3.6 V   216 MHz    7 <-
 *  --------- ---------- -----------
 */

#define BOARD_FLASH_WAITSTATES 7

/* LED definitions ******************************************************************/
/* The mB client board has got two GPIO LEDs (Green and Red), that can be controlled
 * by software.
 *
 * If CONFIG_ARCH_LEDS is not defined, then the user can control the LEDs in any way.
 * The following definitions are used to access individual LEDs.
 */

/* LED index values for use with board_userled() */

#define BOARD_LED1        0
#define BOARD_LED2        1
#define BOARD_NLEDS       2

#define BOARD_LED_GREEN   BOARD_LED1
#define BOARD_LED_RED     BOARD_LED2

/* LED bits for use with board_userled_all() */

#define BOARD_LED1_BIT    (1 << BOARD_LED1)
#define BOARD_LED2_BIT    (1 << BOARD_LED2)

/* Alternate function pin selections ************************************************/

/* messBUS interface 
 *

 *  PC6   messBUS TX (USART6_TX)
 *  PC7   messBUS RX (USART6_RX, TIM3_CH2)
 *  PC8   messBUS DE
 *  PC9   messBUS NRE
 */

#define STM32_mB_CH1_USART_BASE   	        STM32_USART6_BASE
#define GPIO_mB_CH1_TX			  	GPIO_USART6_TX_1     /* PC6 */
#define GPIO_mB_CH1_RX			  	GPIO_USART6_RX_1     /* PC7 */

#define mB_CH1_RX_DMAMAP		  	DMAMAP_USART6_RX_1   /* DMA2,DMA_STREAM1,DMA_CHAN5 */
#define mB_CH1_TX_DMAMAP		  	DMAMAP_USART6_TX_2   /* DMA2,DMA_STREAM7,DMA_CHAN5 */

#define GPIO_mB_CH1_DE      	(GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
								GPIO_OUTPUT_CLEAR|GPIO_PORTC|GPIO_PIN8)

#define GPIO_mB_CH1_RE       	(GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
								GPIO_OUTPUT_SET|GPIO_PORTC|GPIO_PIN9)

/* CONSOLE (USART1)
 *
 * TX: USART1_TX  PB14
 * RX: USART1_RX  PB15
 */

# define GPIO_USART1_RX GPIO_USART1_RX_3
# define GPIO_USART1_TX GPIO_USART1_TX_3
# define DMAMAP_USART1_RX DMAMAP_USART1_RX_2 /* (DMA2,DMA_STREAM5,DMA_CHAN4)*/
//# define DMAMAP_USART1_RX DMAMAP_USART1_RX_1 /* (DMA2,DMA_STREAM2,DMA_CHAN4)*/

/* UART3:
 *
 * TX: UART3_TX  PD8
 * RX: UART3_RX  PD9
 */

 # define GPIO_USART3_RX GPIO_USART3_RX_3
 # define GPIO_USART3_TX GPIO_USART3_TX_3

/* SERIAL Interface */

#define MB_HAVE_SERIAL_RX
#define MB_HAVE_SERIAL_TX


/* TIMER1
 *
 * CH1: PE9
 * CH2: PE11
 * CH3: PE13
 * CH4: PE14
 */

#define GPIO_TIM1_CH1IN    GPIO_TIM1_CH1IN_2
#define GPIO_TIM1_CH2IN    GPIO_TIM1_CH2IN_2
//#define GPIO_TIM1_CH3IN    GPIO_TIM1_CH3IN_2
//#define GPIO_TIM1_CH4IN    GPIO_TIM1_CH4IN_2

#define DMAMAP_TIM1_CH1    DMAMAP_TIM1_CH1_3 /* (DMA2,DMA_STREAM3,DMA_CHAN6) */
#define DMAMAP_TIM1_CH2    DMAMAP_TIM1_CH2_2 /* (DMA2,DMA_STREAM2,DMA_CHAN6) */
//#define DMAMAP_TIM1_CH3    DMAMAP_TIM1_CH3_2 /* (DMA2,DMA_STREAM6,DMA_CHAN6) */
/* DMAMAP_TIM1_CH4                              (DMA2,DMA_STREAM4,DMA_CHAN6) */
/* DMAMAP_TIM1_UP                               (DMA2,DMA_STREAM5,DMA_CHAN6) */


/* TIMER4
 *
 * CH1: PD12
 * CH2: PD13
 * CH3: PD14
 * CH4: PD15
 */

#define GPIO_TIM4_CH1IN    GPIO_TIM4_CH1IN_2
#define GPIO_TIM4_CH2IN    GPIO_TIM4_CH2IN_2
#define GPIO_TIM4_CH3IN    GPIO_TIM4_CH3IN_2
#define GPIO_TIM4_CH4IN    GPIO_TIM4_CH4IN_2

/* DMAMAP_TIM4_CH1            (DMA1,DMA_STREAM0,DMA_CHAN2) */
/* DMAMAP_TIM4_CH2            (DMA1,DMA_STREAM3,DMA_CHAN2) */
/* DMAMAP_TIM4_CH3            (DMA1,DMA_STREAM7,DMA_CHAN2) */
/* DMAMAP_TIM4_CH4            not possible??? */


/* Timer 5*/
#define GPIO_TIM5_CH1IN GPIO_TIM5_CH1IN_1 /*PA0 Fan Tachometer Speed Signal*/


/* Timer 9 */
#define GPIO_TIM9_CH1OUT   GPIO_TIM9_CH1OUT_2 /* PE5 Fan PWM speed setting */


/* DAC1: SPI1
 *
 *  PB5   SPI1_MOSI
 *  PB3   SPI1_SCK
 *  PA15  SPI1_NSS
 */




#define GPIO_SPI1_MOSI   GPIO_SPI1_MOSI_2
#define GPIO_SPI1_MISO   (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
                                                                GPIO_OUTPUT_SET|GPIO_PORTB|GPIO_PIN4)
#define GPIO_SPI1_SCK    GPIO_SPI1_SCK_2
//#define GPIO_SPI1_NSS    GPIO_SPI1_NSS_1

/* ADC1: SPI2
 *
 *  PB4   SPI2_MISO
 *  PB5   SPI2_MOSI
 *  PB3   SPI2_SCK
 *  PB12  SPI2_NSS
 *
 */


#define GPIO_SPI2_MISO   GPIO_SPI2_MISO_2
#define GPIO_SPI2_MOSI   GPIO_SPI2_MOSI_4
#define GPIO_SPI2_SCK    GPIO_SPI2_SCK_2
//#define GPIO_SPI2_NSS    GPIO_SPI2_NSS_1

/* ADC2: SPI3
 *
 *  PC11   SPI3_MISO
 *  PC12   SPI3_MOSI
 *  PC10   SPI3_SCK
 *  PA4    SPI3_NSS
 */


#define GPIO_SPI3_MISO   GPIO_SPI3_MISO_2
#define GPIO_SPI3_MOSI   GPIO_SPI3_MOSI_3
#define GPIO_SPI3_SCK    GPIO_SPI3_SCK_2
//#define GPIO_SPI3_NSS    GPIO_SPI3_NSS_2

/* DAC2: SPI4
 *
 *  PE14   SPI4_MOSI
 *  PE12   SPI4_SCK
 *  PE4    SPI4_NSS
 */

#define GPIO_SPI4_MOSI   GPIO_SPI4_MOSI_2
#define GPIO_SPI4_MISO   (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
                                                                GPIO_OUTPUT_SET|GPIO_PORTE|GPIO_PIN13)
#define GPIO_SPI4_SCK    GPIO_SPI4_SCK_2
//#define GPIO_SPI4_NSS    GPIO_SPI4_NSS_1

/* CAN1
 *
 *  PD0   CAN1_RX
 *  PD1   CAN1_TX
 *
 */

#  define GPIO_CAN1_RX GPIO_CAN1_RX_3
#  define GPIO_CAN1_TX GPIO_CAN1_TX_3


/************************************************************************************
 * Public Data
 ************************************************************************************/

#ifndef __ASSEMBLY__

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/************************************************************************************
 * Public Function Prototypes
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_boardinitialize
 *
 * Description:
 *   All STM32 architectures must provide the following entry point.  This entry point
 *   is called early in the initialization -- after all memory has been configured
 *   and mapped but before any devices have been initialized.
 *
 ************************************************************************************/

void stm32_boardinitialize(void);

#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif  /* __ASSEMBLY__ */
#endif  /* __CONFIG_MB_HEAT_INCLUDE_BOARD_H */

/****************************************************************************
 * drivers/sensors/sfm3000.c
 * Character driver for Senserion SFM3000 Mass Flow Meter
 *
 * Based on ms58xx driver.
 *
 *   Copyright (C) 2015 Omni Hoverboards Inc. All rights reserved.
 *   Author: Paul Alexander Patience <paul-a.patience@polymtl.ca>
 *   Updated by: Karim Keddam <karim.keddam@polymtl.ca>
 *
 *   Copyright (C) 2016 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <errno.h>
#include <debug.h>
#include <stdlib.h>
#include <endian.h>

#include <nuttx/kmalloc.h>
#include <nuttx/fs/fs.h>
#include <nuttx/arch.h>
#include <nuttx/i2c/i2c_master.h>
#include <nuttx/sensors/sfm3000.h>
#include <nuttx/random.h>

#if defined(CONFIG_I2C) && defined(CONFIG_SENSORS_SFM3000)

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#ifndef CONFIG_SFM3000_I2C_FREQUENCY
#  define CONFIG_SFM3000_I2C_FREQUENCY I2C_SPEED_FAST
#endif

#ifndef OK
#  define OK 0
#endif

/* Offset and scale factors from datasheet (SFM3000) */

#define OFFSET_FLOW 32000.0F    // offset flow
#define SCALE_FLOW    140.0F    // scale factor flow

/* CRC */

#define CRC_POLYNOMIAL 0x131 /* P(x)=x^8+x^5+x^4+1 = 100110001 */

/* Sensor Commands */

typedef enum{
    TRIGGER_FLOW_MEASUREMENT = 0x1000, // command: flow measurement
    READ_SERIAL_NUMBER_HIGH  = 0x31AE, // command: read serial number (bit 31:16)
    READ_SERIAL_NUMBER_LOW   = 0x31AF, // command: read serial number (bit 15:0)
    SOFT_RESET               = 0X2000  // command: soft reset
}etCommands;

/* Error codes */

typedef enum
{
    NO_ERROR       = OK,   // no error
    ACK_ERROR      = 0x01, // no acknowledgment error
    CHECKSUM_ERROR = 0x02  // checksum mismatch error
} etError;


/****************************************************************************
 * Private Types
 ****************************************************************************/

struct sfm3000_dev_s
{
  FAR struct i2c_master_s   *i2c;    /* I2C interface */
  uint8_t                   addr;    /* I2C address */
  uint32_t                  SN;      /* 32 bit serial number */

  struct sfm3000_measure_s  measure;
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/
/* CRC Calculation */

static uint8_t sfm3000_checkCRC(FAR uint8_t *data, uint8_t nBytes, uint8_t checksum);

/* I2C Helpers */

static int   sfm3000_i2c_write(FAR struct sfm3000_dev_s *priv,
                               FAR const uint8_t *buffer, int buflen);
static int   sfm3000_i2c_read(FAR struct sfm3000_dev_s *priv,
                              FAR uint8_t *buffer, int buflen);
static int   sfm3000_readu16(FAR struct sfm3000_dev_s *priv, uint16_t regaddr,
                             FAR uint16_t *regval);
static int   sfm3000_readSerialNumber(FAR struct sfm3000_dev_s *priv,
                                      FAR uint32_t *SN);
static int   sfm3000_reset(FAR struct sfm3000_dev_s *priv);
static int   sfm3000_triggerFlowMeasurement(FAR struct sfm3000_dev_s *priv);
static int   sfm3000_measure(FAR struct sfm3000_dev_s *priv);
static float sfm3000_convert(FAR struct sfm3000_dev_s *priv,
                             float offset, float scale, uint16_t raw_data);

/* Character Driver Methods */

static int     sfm3000_open(FAR struct file *filep);
static int     sfm3000_close(FAR struct file *filep);
static ssize_t sfm3000_read(FAR struct file *filep, FAR char *buffer,
                           size_t buflen);
static ssize_t sfm3000_write(FAR struct file *filep, FAR const char *buffer,
                            size_t buflen);
static int     sfm3000_ioctl(FAR struct file *filep, int cmd,
                            unsigned long arg);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct file_operations g_fops =
{
  sfm3000_open,
  sfm3000_close,
  sfm3000_read,
  sfm3000_write,
  NULL,
  sfm3000_ioctl,
  NULL
#ifndef CONFIG_DISABLE_PSEUDOFS_OPERATIONS
  , NULL
#endif
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: sfm3000_crc
 *
 * Description:
 *   Calculate the CRC.
 *
 ****************************************************************************/

static uint8_t sfm3000_checkCRC(FAR uint8_t *data, uint8_t nBytes, uint8_t checksum)
{
  uint8_t crc = 0;

  for (int byteCount = 0; byteCount < nBytes; byteCount++) 
  {
      crc ^= (data[byteCount]);

      for (uint8_t bit = 8; bit > 0; --bit) 
      {
          if (crc & 0x80) 
          {
              crc = (crc << 1) ^ CRC_POLYNOMIAL;
          }
          else 
          {
              crc = (crc << 1);
          }
      }
  }

  if (crc != checksum)
  {
      return false;
  }

  return true;
}

/****************************************************************************
 * Name: sfm3000_i2c_write
 *
 * Description:
 *   Write to the I2C device.
 *
 ****************************************************************************/

static int sfm3000_i2c_write(FAR struct sfm3000_dev_s *priv,
                             FAR const uint8_t *buffer, int buflen)
{
  struct i2c_msg_s msg;
  int ret;

  /* Setup for the transfer */

  msg.frequency = CONFIG_SFM3000_I2C_FREQUENCY,
  msg.addr      = priv->addr;
  msg.flags     = 0;
  msg.buffer    = (FAR uint8_t *)buffer;  /* Override const */
  msg.length    = buflen;

  /* Then perform the transfer. */

  ret = I2C_TRANSFER(priv->i2c, &msg, 1);
  return (ret >= 0) ? OK : ret;
}

/****************************************************************************
 * Name: sfm3000_i2c_read
 *
 * Description:
 *   Read from the I2C device.
 *
 ****************************************************************************/

static int sfm3000_i2c_read(FAR struct sfm3000_dev_s *priv,
                            FAR uint8_t *buffer, int buflen)
{
  struct i2c_msg_s msg;
  int ret;

  /* Setup for the transfer */

  msg.frequency = CONFIG_SFM3000_I2C_FREQUENCY,
  msg.addr      = priv->addr,
  msg.flags     = I2C_M_READ;
  msg.buffer    = buffer;
  msg.length    = buflen;

  /* Then perform the transfer. */

  ret = I2C_TRANSFER(priv->i2c, &msg, 1);
  return (ret >= 0) ? OK : ret;
}

/****************************************************************************
 * Name: sfm3000_readu16
 *
 * Description:
 *   Read from a 16-bit register.
 *
 ****************************************************************************/

static int sfm3000_readu16(FAR struct sfm3000_dev_s *priv, uint16_t regaddr,
                          FAR uint16_t *regval)
{
  uint8_t buffer[3];
  int ret;

  sninfo("addr: %02x\n", regaddr);

  regaddr = htobe16(regaddr);

  /* Write the register address */

  ret = sfm3000_i2c_write(priv, (uint8_t*) &regaddr, sizeof(regaddr));
  if (ret < 0)
    {
      snerr("ERROR: i2c_write failed: %d\n", ret);
      return ret;
    }

  /* Restart and read 16 bits from the register */

  ret = sfm3000_i2c_read(priv, buffer, sizeof(buffer));
  if (ret < 0)
    {
      snerr("ERROR: i2c_read failed: %d\n", ret);
      return ret;
    }

  *regval = (uint16_t)buffer[0] << 8 | (uint16_t)buffer[1];
  sninfo("value: %04x ret: %d\n", *regval, ret);
  return ret;
}

/****************************************************************************
 * Name: sfm3000_readSerialNumber
 *
 * Description:
 *   Read Serial Number from the PROM.
 *
 ****************************************************************************/

static int sfm3000_readSerialNumber(FAR struct sfm3000_dev_s *priv, FAR uint32_t *SN)
{
  int ret = OK;

  uint16_t prom[2];

  ret = sfm3000_readu16(priv, READ_SERIAL_NUMBER_HIGH, &prom[0]);
  if (ret < 0)
    {
      snerr("ERROR: tsys01_readu16 failed: %d\n", ret);
      return ret;
    }

  usleep(10*1000);

  ret = sfm3000_readu16(priv, READ_SERIAL_NUMBER_LOW, &prom[1]);
  if (ret < 0)
    {
      snerr("ERROR: tsys01_readu16 failed: %d\n", ret);
      return ret;
    }

  *SN = ((((uint32_t) prom[0]) << 8) | ((uint32_t) prom[1]));

  return ret;
}

/****************************************************************************
 * Name: sfm3000_reset
 *
 * Description:
 *   Reset the device.
 *
 ****************************************************************************/

static int sfm3000_reset(FAR struct sfm3000_dev_s *priv)
{
  uint16_t regaddr;
  int ret;

  regaddr = SOFT_RESET;
  sninfo("addr: %02x\n", regaddr);

  regaddr = htobe16(regaddr);

  /* Write the register address */

  ret = sfm3000_i2c_write(priv, (uint8_t*) &regaddr, sizeof(regaddr));
  if (ret < 0)
    {
      snerr("ERROR: i2c_write failed: %d\n", ret);
      return ret;
    }

  return ret;
}

/****************************************************************************
 * Name: sfm3000_convert
 *
 * Description:
 *      Convert the raw data to a physical flow value.
 *      Target unit is SCCM (standard cubic centimeters per minute).
 *
 ****************************************************************************/

static float sfm3000_convert(FAR struct sfm3000_dev_s *priv,
                             float offset, float scale, uint16_t raw_data)
{
    return (raw_data - offset) / scale;
}

/****************************************************************************
 * Name: sfm3000_measure
 *
 * Description:
 *   Measure the mass flow.
 *   
 *   NOTE: Temperature and VDD measurement currently not supported.
 *
 ****************************************************************************/

static int sfm3000_measure(FAR struct sfm3000_dev_s *priv)
{
  uint8_t buffer[3] = {0, 0, 0};

  /* Read data */

  int ret = sfm3000_i2c_read(priv, buffer, 3);
  if(ret != OK)
  {
      return ret;
  }

  /* Check CRC */

  if(!sfm3000_checkCRC(buffer,  2, buffer[2]))
    {
      return -CHECKSUM_ERROR;
    }

  /* Convert raw data */

  uint16_t raw_data = (((uint16_t) buffer[0]) << 8) | ((uint16_t) buffer[1]);

  priv->measure.massFlow = sfm3000_convert(priv, OFFSET_FLOW, SCALE_FLOW, raw_data);

  /* Update Timestamp */

  clock_gettime(CLOCK_MONOTONIC, &priv->measure.timeStamp);

  return OK;
}

/****************************************************************************
 * Name: sfm3000_triggerFlowMeasurement
 *
 * Description:
 *     Flow measurements are started by writing the start measurement command
 *     (0x1000) to the sensor. Measurement results are continuously updated
 *     until measurement is stopped by sending any other command. After a start
 *     measurement command, the measurement results can be read out
 *     continuously. This means that an I2C header with R/_W bit = 1 can be
 *     sent continuously to read out those results, without preceding it
 *     with command 0x1000.
 *
 ****************************************************************************/

static int sfm3000_triggerFlowMeasurement(FAR struct sfm3000_dev_s *priv)
{
  uint16_t regaddr;
  int ret;

  regaddr = TRIGGER_FLOW_MEASUREMENT;
  sninfo("addr: %02x\n", regaddr);

  regaddr = htobe16(regaddr);

  /* Write the register address */

  ret = sfm3000_i2c_write(priv, (uint8_t*) &regaddr, sizeof(regaddr));
  if (ret < 0)
    {
      snerr("ERROR: i2c_write failed: %d\n", ret);
      return ret;
    }

  return ret;
}

/****************************************************************************
 * Name: sfm3000_open
 *
 * Description:
 *   This method is called when the device is opened.
 *
 ****************************************************************************/

static int sfm3000_open(FAR struct file *filep)
{
  return OK;
}

/****************************************************************************
 * Name: sfm3000_close
 *
 * Description:
 *   This method is called when the device is closed.
 *
 ****************************************************************************/

static int sfm3000_close(FAR struct file *filep)
{
  return OK;
}

/****************************************************************************
 * Name: sfm3000_read
 *
 * Description:
 *   A dummy read method.
 *
 ****************************************************************************/

static ssize_t sfm3000_read(FAR struct file *filep, FAR char *buffer,
                           size_t buflen)
{
  ssize_t size;
  FAR struct inode *inode        = filep->f_inode;
  FAR struct sfm3000_dev_s *priv  = inode->i_private;
  FAR struct sfm3000_measure_s *p = (FAR struct sfm3000_measure_s *)buffer;

  size = buflen;
  while (size >= sizeof(*p))
    {
      /* Fetch the sensor data */

      if (sfm3000_measure(priv) < 0)
        {
          return -1;
        }

      *p = priv->measure;

      p++;
      size -= sizeof(*p);
    }

  return buflen;
}

/****************************************************************************
 * Name: sfm3000_write
 *
 * Description:
 *   A dummy write method.
 *
 ****************************************************************************/

static ssize_t sfm3000_write(FAR struct file *filep, FAR const char *buffer,
                            size_t buflen)
{
  return -ENOSYS;
}

/****************************************************************************
 * Name: sfm3000_ioctl
 *
 * Description:
 *   The standard ioctl method.
 *
 ****************************************************************************/

static int sfm3000_ioctl(FAR struct file *filep, int cmd, unsigned long arg)
{
  FAR struct inode        *inode = filep->f_inode;
  FAR struct sfm3000_dev_s *priv  = inode->i_private;
  int                      ret   = OK;

  /* Handle ioctl commands */

  switch (cmd)
    {
      /* Starts the conversion sequence. Arg: None. */

      case SNIOC_START_CONVERSION:
        ret = sfm3000_triggerFlowMeasurement(priv);
        break;

      /* Measure the mass flow. Arg: None. */

      case SNIOC_MEASURE:
        DEBUGASSERT(arg == 0);
        ret = sfm3000_measure(priv);
        break;

      /* Reset the device. Arg: None. */

      case SNIOC_RESET:
        DEBUGASSERT(arg == 0);
        ret = sfm3000_reset(priv);
        break;

      /* Unrecognized commands */

      default:
        snerr("ERROR: Unrecognized cmd: %d arg: %ld\n", cmd, arg);
        ret = -ENOTTY;
        break;
    }

  return ret;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: sfm3000_register
 *
 * Description:
 *   Register the SFM3000 character device as 'devpath'.
 *
 * Input Parameters:
 *   devpath - The full path to the driver to register, e.g., "/dev/press0".
 *   i2c     - An I2C driver instance.
 *   addr    - The I2C address of the SFM3000.
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/

int sfm3000_register(FAR const char *devpath, FAR struct i2c_master_s *i2c,
                    uint8_t addr)
{
  FAR struct sfm3000_dev_s *priv;
  int ret;

  /* Sanity check */

  DEBUGASSERT(i2c != NULL);

  /* Initialize the device's structure */

  priv = (FAR struct sfm3000_dev_s *)kmm_malloc(sizeof(*priv));
  if (priv == NULL)
    {
      snerr("ERROR: Failed to allocate instance\n");
      return -ENOMEM;
    }

  priv->i2c   = i2c;
  priv->addr  = addr;

  priv->measure.massFlow          = 0;
  priv->measure.timeStamp.tv_sec  = 0;
  priv->measure.timeStamp.tv_nsec = 0;

  usleep(20 * 1000);

  ret = sfm3000_reset(priv);
  if (ret < 0)
    {
      snerr("ERROR: sfm3000_reset failed: %d\n", ret);
      goto errout;
    }

  usleep(100 * 1000);

  ret = sfm3000_readSerialNumber(priv, &priv->SN);
  if (ret < 0)
    {
      snerr("ERROR: sfm3000_readSerialNumber failed: %d\n", ret);
      goto errout;
    }

  usleep(100);

  /* Already trigger initial measurement */

  ret = sfm3000_triggerFlowMeasurement(priv);
  if (ret < 0)
    {
      snerr("ERROR: triggerFlowMeasurement failed: %d\n", ret);
      goto errout;
    }

  /* Register the character driver */

  ret = register_driver(devpath, &g_fops, 0666, priv);
  if (ret < 0)
    {
      snerr("ERROR: Failed to register driver: %d\n", ret);
      goto errout;
    }

  return ret;

errout:
  kmm_free(priv);
  return ret;
}

#endif /* CONFIG_I2C && CONFIG_SENSORS_SFM3000 */

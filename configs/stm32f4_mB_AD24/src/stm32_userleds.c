/****************************************************************************
 * configs/stm32f4_vamex_imu/src/stm32_userleds.c
 *
 *   Copyright (C) 2011, 2015-2016 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdint.h>
#include <stdbool.h>
#include <debug.h>

#include <nuttx/board.h>
#include <nuttx/power/pm.h>
#include <arch/board/board.h>

#include "chip.h"
#include "up_arch.h"
#include "up_internal.h"
#include "stm32.h"
#include "stm32f4_mB_AD24.h"

#ifdef CONFIG_MESSBUS_USERLED
#include <nuttx/messBUS/messBUS_led.h>
#endif

#ifndef CONFIG_ARCH_LEDS

/****************************************************************************
 * Private Data
 ****************************************************************************/
/* This array maps an LED number to GPIO pin configuration */

static uint32_t g_ledcfg[BOARD_NLEDS] =
{
	GPIO_LED_RED,
	GPIO_LED_GREEN,
};

/****************************************************************************
 * Private Function Protototypes
 ****************************************************************************/

/* LED Power Management */

#ifdef CONFIG_PM
static void led_pm_notify(struct pm_callback_s *cb, int domain,
                          enum pm_state_e pmstate);
static int led_pm_prepare(struct pm_callback_s *cb, int domain,
                          enum pm_state_e pmstate);
#endif


/****************************************************************************
 * Private Data
 ****************************************************************************/

#ifdef CONFIG_PM
static struct pm_callback_s g_ledscb =
{
  .notify  = led_pm_notify,
  .prepare = led_pm_prepare,
};
#endif

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: led_pm_notify
 *
 * Description:
 *   Notify the driver of new power state. This callback is called after
 *   all drivers have had the opportunity to prepare for the new power state.
 *
 ****************************************************************************/

#ifdef CONFIG_PM
static void led_pm_notify(struct pm_callback_s *cb, int domain,
                          enum pm_state_e pmstate)
{
  switch (pmstate)
    {
      case(PM_NORMAL):
        {
          /* Restore normal LEDs operation */

        }
        break;

      case(PM_IDLE):
        {
          /* Entering IDLE mode - Turn leds off */

        }
        break;

      case(PM_STANDBY):
        {
          /* Entering STANDBY mode - Logic for PM_STANDBY goes here */

        }
        break;

      case(PM_SLEEP):
        {
          /* Entering SLEEP mode - Logic for PM_SLEEP goes here */

        }
        break;

      default:
        {
          /* Should not get here */

        }
        break;
    }
}
#endif

/****************************************************************************
 * Name: led_pm_prepare
 *
 * Description:
 *   Request the driver to prepare for a new power state. This is a warning
 *   that the system is about to enter into a new power state. The driver
 *   should begin whatever operations that may be required to enter power
 *   state. The driver may abort the state change mode by returning a
 *   non-zero value from the callback function.
 *
 ****************************************************************************/

#ifdef CONFIG_PM
static int led_pm_prepare(struct pm_callback_s *cb, int domain,
                          enum pm_state_e pmstate)
{
  /* No preparation to change power modes is required by the LEDs driver.
   * We always accept the state change by returning OK.
   */

  return OK;
}
#endif

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: board_userled_initialize
 ****************************************************************************/

void board_userled_initialize(void)
{
	  int i;

	  /* Configure LED1-3 GPIOs for output */

	  for (i = 0; i < BOARD_NLEDS; i++)
	    {
	      stm32_configgpio(g_ledcfg[i]);
	    }
}

/****************************************************************************
 * Name: board_userled
 ****************************************************************************/

void board_userled(int led, bool ledon)
{
  if ((unsigned)led < BOARD_NLEDS)
    {
      stm32_gpiowrite(g_ledcfg[led], ledon);
    }
}

/****************************************************************************
 * Name: board_userled_all
 ****************************************************************************/

void board_userled_all(uint8_t ledset)
{
	  int i;

	  /* Configure LED1-3 GPIOs for output */

	  for (i = 0; i < BOARD_NLEDS; i++)
	    {
	      stm32_gpiowrite(g_ledcfg[i], (ledset & (1 << i)) != 0);
	    }
}

#ifdef  CONFIG_MESSBUS_USERLED
/****************************************************************************
 * Name: switch_LED
 ****************************************************************************/

void switch_LED(int led, bool state)
{
	if ((unsigned) led < BOARD_NLEDS)
	{
		board_userled(led, state);
	}
}

/****************************************************************************
 * Name: toggle_LED
 ****************************************************************************/

void toggle_LED(int led)
{
	if ((unsigned) led < BOARD_NLEDS)
	{
		if (stm32_gpioread(g_ledcfg[led]))
			switch_LED(led, false);
		else
			switch_LED(led, true);
	}
}

/****************************************************************************
 * Name: switch_LED_red_on
 ****************************************************************************/
void switch_LED_red_on()
{
	switch_LED(BOARD_LED_RED, true);
//	putreg32(1<<8,STM32_GPIOA_BSRR);
}

/****************************************************************************
 * Name: switch_LED_red_off
 ****************************************************************************/
void switch_LED_red_off()
{
	switch_LED(BOARD_LED_RED, false);
//	putreg32((1<<8)<<16,STM32_GPIOA_BSRR);
}

/****************************************************************************
 * Name: toggle_LED_red
 ****************************************************************************/
void toggle_LED_red()
{
	if (stm32_gpioread(g_ledcfg[BOARD_LED_RED]))
		switch_LED_red_off();
	else
		switch_LED_red_on();
}

/****************************************************************************
 * Name: switch_LED_green_on
 ****************************************************************************/
void switch_LED_green_on()
{
	switch_LED(BOARD_LED_GREEN, true);
//	putreg32(1<<9,STM32_GPIOA_BSRR);
}

/****************************************************************************
 * Name: switch_LED_green_off
 ****************************************************************************/
void switch_LED_green_off()
{
	switch_LED(BOARD_LED_GREEN, false);
//	putreg32((1<<9)<<16,STM32_GPIOA_BSRR);
}

/****************************************************************************
 * Name: toggle_LED_green
 ****************************************************************************/
void toggle_LED_green()
{
	if (stm32_gpioread(g_ledcfg[BOARD_LED_GREEN]))
		switch_LED_green_off();
	else
		switch_LED_green_on();
}

#endif /*  CONFIG_MESSBUS_USERLED */

/****************************************************************************
 * Name: stm32_led_pminitialize
 ****************************************************************************/

#ifdef CONFIG_PM
void stm32_led_pminitialize(void)
{
  /* Register to receive power management callbacks */

  int ret = pm_register(&g_ledscb);
  if (ret != OK)
    {
      board_autoled_on(LED_ASSERTION);
    }
}
#endif /* CONFIG_PM */

#endif /* !CONFIG_ARCH_LEDS */

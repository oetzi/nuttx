/****************************************************************************
 * drivers/sensors/adxl357_spi.c
 *
 *   Author: Björn Brandt
 *   Based on ADXL345 driver
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <unistd.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/kmalloc.h>
#include <nuttx/spi/spi.h>
#include <nuttx/sensors/adxl357.h>

#include "adxl357.h"

#if defined(CONFIG_SENSORS_ADXL357) && defined(CONFIG_ADXL357_SPI)

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: adxl357_configspi
 *
 * Description:
 *
 ****************************************************************************/

static inline void adxl357_configspi(FAR struct spi_dev_s *spi)
{
  /* Configure SPI for the ADXL357 */

  SPI_SETMODE(spi, ADXL357_SPI_MODE);
  SPI_SETBITS(spi, ADXL357_SPI_NBITS);

  const uint32_t freq = (uint32_t) CONFIG_ADXL357_SPI_FREQUENCY * 1000; // kHz -> Hz

  if(freq > ADXL357_SPI_MAXFREQUENCY)
  {
       SPI_SETFREQUENCY(spi, ADXL357_SPI_MAXFREQUENCY);
  }
  if(freq  < ADXL357_SPI_MINFREQUENCY)
  {
       SPI_SETFREQUENCY(spi, ADXL357_SPI_MINFREQUENCY);
  }
  else
  {
       SPI_SETFREQUENCY(spi, freq);
  }
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: adxl357_switchFastMode
 *
 * Description:
 *
 ****************************************************************************/

void adxl357_switchFastMode(FAR struct adxl357_dev_s *priv, bool fastMode)
{
    if(fastMode)
    {
        (void) SPI_LOCK(priv->spi, true);
        adxl357_configspi(priv->spi);
        priv->fastMode = true;
    }
    else
    {
        (void) SPI_LOCK(priv->spi, false);
        priv->fastMode = false;
    }
}

/****************************************************************************
 * Name: adxl357_getreg8
 *
 * Description:
 *   Read from an 8-bit ADXL357 register
 *
 ****************************************************************************/

uint8_t adxl357_getreg8(FAR struct adxl357_dev_s *priv, uint8_t regaddr)
{
  uint8_t regval = 0;

  /* ADXL357 reading protocol:
   * Bit 0 needs to be set indicating the intention (read).
   * Bit 1-7: target register address.
   */

  const uint8_t readbyte = ((regaddr << 1) | 0x01);

  /* If SPI bus is shared then lock and configure it */

  if(!priv->fastMode)
  {
        (void) SPI_LOCK(priv->spi, true);
        adxl357_configspi(priv->spi);
  }

  /* Select the ADXL357 */

  SPI_SELECT(priv->spi, SPIDEV_ACCELEROMETER(0), true);

  /* Send register to read and get the next byte */

  (void)SPI_SEND(priv->spi, readbyte);
  SPI_RECVBLOCK(priv->spi, &regval, 1);

  /* Deselect the ADXL357 */

  SPI_SELECT(priv->spi, SPIDEV_ACCELEROMETER(0), false);

  /* Unlock bus */

  if(!priv->fastMode)
  {
        (void) SPI_LOCK(priv->spi, false);
  }

#ifdef CONFIG_ADXL357_REGDEBUG
  _err("%02x->%02x\n", regaddr, regval);
#endif
  return regval;
}

/****************************************************************************
 * Name: adxl357_putreg8
 *
 * Description:
 *   Write a value to an 8-bit ADXL357 register
 *
 ****************************************************************************/

void adxl357_putreg8(FAR struct adxl357_dev_s *priv, uint8_t regaddr,
                     uint8_t regval)
{
#ifdef CONFIG_ADXL357_REGDEBUG
  _err("%02x<-%02x\n", regaddr, regval);
#endif

  /* ADXL357 writing protocol:
   * Bit 0 needs to be cleared indicating the intention (write).
   * Bit 1-7: target register address.
   */

  regaddr = (regaddr << 1);

  /* If SPI bus is shared then lock and configure it */

  if(!priv->fastMode)
  {
        (void) SPI_LOCK(priv->spi, true);
        adxl357_configspi(priv->spi);
  }

  /* Select the ADXL357 */

  SPI_SELECT(priv->spi, SPIDEV_ACCELEROMETER(0), true);

  /* Send register address and set the value */

  (void)SPI_SEND(priv->spi, regaddr);
  (void)SPI_SEND(priv->spi, regval);

  /* Deselect the ADXL357 */

  SPI_SELECT(priv->spi, SPIDEV_ACCELEROMETER(0), false);

  /* Unlock bus */

  if(!priv->fastMode)
  {
      (void)SPI_LOCK(priv->spi, false);
  }
}

/********************************************************************************************
 * Name: adxl357_readBulk
 *
 * Description:
 *   Read n bytes in stream mode from an ADXL357 FIFO into rxbuffer.
 *   This function is called from the interrupt handler when an interrupt
 *   is received indicating that are bytes available in the sensor fifo.  This
 *   function will add rx data to head of receive buffer.  Driver read() logic will
 *   take data from the tail of the buffer.
 *
 * Note:
 *   If rxbuf is NULL, data will be read from sensor's fifo but data is lost. This can be
 *   used to clear the ADXL357 fifo.
 *
 ********************************************************************************************/

void adxl357_readBulk(FAR struct adxl357_dev_s *priv, FAR struct adxl357_buffer_s* rxbuf, uint8_t nBytes)
{
  /* Fast Mode need to be enabled, so we neither need to lock spi bus nor to configure it */

  if(priv && priv->fastMode)
  {
        /* Select the ADXL357 */

        SPI_SELECT(priv->spi, SPIDEV_ACCELEROMETER(0), true);

        /* Send register to read and get the next n bytes */

        /* ADXL357 reading protocol:
         * Bit 0 needs to be set indicating the intention (read).
         * Bit 1-7: target register address.
         */

        const uint8_t readbyte = ((ADXL357_FIFO_DATA << 1) | 0x01);

        (void) SPI_SEND(priv->spi, readbyte);

        if(rxbuf)
        {
            for (int i = 0; i < nBytes; i++)
            {
                /* read 1 Byte */

                rxbuf->buffer[rxbuf->head] = (char) SPI_SEND(priv->spi, 0xFF);

                /* Increment the head index */

                if (++rxbuf->head >= rxbuf->size)
                {
                    rxbuf->head = 0; /* Head is behind buffer, so reset it */
                }
                if (rxbuf->head == rxbuf->tail)
                {
                    /* Fifo is full! We are going to overwrite data... */

                    priv->status |= ADXL357_STAT_BUFFER_OVERWRITTEN;
                }
            }
        }
        else
        {
            for (int i = 0; i < nBytes; i++)
            {
                /* read 1 Byte to the nirvana */

                SPI_SEND(priv->spi, 0xFF);
            }
        }

        /* Deselect the ADXL357 */

        SPI_SELECT(priv->spi, SPIDEV_ACCELEROMETER(0), false);
    }
}

#endif /* CONFIG_SENSORS_ADXL357 && CONFIG_ADXL357_SPI*/

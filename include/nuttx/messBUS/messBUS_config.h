/*
 * messBUS_config.h
 *
 *  Created on: 07.04.2018
 *      Author: bbrandt
 */

#ifndef NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_CONFIG_H_
#define NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_CONFIG_H_

#include <nuttx/config.h>

/***************************************************************************
 * Configs that should move to nuttx/config.h via menuconfig later on...
 **************************************************************************/

/* Channel configuration for the master. The channels will use USARTx according to their
 * channel number CHx. Config CONFIG_MESSBUS_NCHANNELS according to the
 * number of used channels. The maximum is to use all 4 channels. A channel
 * is basically a single physical RS485-based messBUS network.
 */
#define CONFIG_MESSBUS_NCHANNELS			1
#define CONFIG_MESSBUS_USE_CH1				1
#define CONFIG_MESSBUS_USE_CH2				0
#define CONFIG_MESSBUS_USE_CH3				0
#define CONFIG_MESSBUS_USE_CH4				0

/* Maximum desired RX and TX slots per each channel.
 * TX slots require one more action than RX slots. You are also on the
 * safe side if you only configure TX slots, even though you specify
 * them as RX in the slotlists later. You then just allocate a bit more
 * memory than actually needed */
#define CONFIG_MESSBUS_CH1_MAX_TX_SLOTS		20
#define CONFIG_MESSBUS_CH1_MAX_RX_SLOTS		20
#define CONFIG_MESSBUS_CH2_MAX_TX_SLOTS		10
#define CONFIG_MESSBUS_CH2_MAX_RX_SLOTS		10
#define CONFIG_MESSBUS_CH3_MAX_TX_SLOTS		10
#define CONFIG_MESSBUS_CH3_MAX_RX_SLOTS		10
#define CONFIG_MESSBUS_CH4_MAX_TX_SLOTS		10
#define CONFIG_MESSBUS_CH4_MAX_RX_SLOTS		10

/* Sleep duration in us for the MESSBUSIOC_SYNCWAIT ioctl command */
#define CONFIG_MESSBUS_SYNCWAIT_SLEEP		1

/* Configure the use of callbacks via PendSV. If callbacks shall not be used,
 * high priority interrupts can be served a little bit more efficiently.
 *
 * Currently wake_up callbacks are only supported when using one channel.
 * For a multichannel master, the respective convert_slotlist function must be
 * extended to support wake up actions. Refer to the function for a single channel
 * master to get an idea how to do it.
 */
#define CONFIG_MESSBUS_USE_SINGLE_CALLBACKS		1
#define CONFIG_MESSBUS_USE_WAKE_UP_CALLBACKS	1

/* Read or write slot */
#define MESSBUS_RX	0
#define MESSBUS_TX	1

#ifdef CONFIG_MESSBUS_CLIENT
/* Tolerated positive or negative phase error per timeslice in ns.
 * This value must not exceed 750ns, otherwise reconfiguration
 * of alternate pin functions may not happen before SYNC.
 */
#define CONFIG_MESSBUS_PHASE_TOLERANCE		750

/* Number of messBUS periods (10ms each) to use for (re-)synchronization
 * with the master.
 */
#define CONFIG_N_PERIOD_SCANS			100

/* Maximum tolerable consecutive SYNC and phase errors */
#define CONFIG_MESSBUS_MAX_SYNC_ERR		0
#define CONFIG_MESSBUS_MAX_PHASE_ERR	0
#endif

#endif /* NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_CONFIG_H_ */

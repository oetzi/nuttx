/************************************************************************************
 * configs/mB_Heat/src/mB_Heat.h
 *
 ************************************************************************************/

#ifndef __CONFIGS_TSIC_SRC_TSIC_H
#define __CONFIGS_TSIC_SRC_TSIC_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>
#include <nuttx/board.h>
#include <nuttx/compiler.h>
#include <stdint.h>
#include "stm32_gpio.h"

#ifdef CONFIG_SENSORS_TSIC
#include <nuttx/sensors/tsic.h>
#endif


/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/* Configuration ********************************************************************/

#define HAVE_PROGMEM_CHARDEV  1

/* mB_trig GPIO Pin Definitions **************************************************/

/* LED ***************************************************************************/
#define GPIO_LED1            (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_100MHz|\
                              GPIO_OUTPUT_CLEAR|GPIO_PORTA|GPIO_PIN9)           /* PA9: green */

#define GPIO_LED2            (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_100MHz|\
                              GPIO_OUTPUT_CLEAR|GPIO_PORTA|GPIO_PIN8)           /* PA8: red */


/* SPI ***************************************************************************/
#define MB_HEAT_GPIO_SPI1_CS  \
  (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|GPIO_OUTPUT_SET|GPIO_PORTA|GPIO_PIN15)
#define MB_HEAT_GPIO_SPI2_CS  \
  (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|GPIO_OUTPUT_SET|GPIO_PORTB|GPIO_PIN12)
#define MB_HEAT_GPIO_SPI3_CS  \
  (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|GPIO_OUTPUT_SET|GPIO_PORTA|GPIO_PIN4)
#define MB_HEAT_GPIO_SPI4_CS  \
  (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|GPIO_OUTPUT_SET|GPIO_PORTE|GPIO_PIN4)

/* POWERSWITCH (BATTERY PROTECT) */
#define GPIO_POWERSWITCH \
    (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_2MHz|GPIO_OUTPUT_CLEAR|GPIO_PORTD|GPIO_PIN11)

/* PWM
 *
 * FAN speed is controlled via PWM on TIM9 CH1 on PE5.
 */

#define MB_HEAT_PWMTIMER   9


/************************************************************************************
 * Public data
 ************************************************************************************/

#ifndef __ASSEMBLY__

/************************************************************************************
 * Public Functions
 ************************************************************************************/



/************************************************************************************
 * Name: stm32_spidev_initialize
 *
 * Description:
 *   Called to configure SPI chip select GPIO pins for the mB-imu board.
 *
 ************************************************************************************/

#if defined(CONFIG_SPI)
void stm32_spidev_initialize(void);
#endif


/****************************************************************************
 * Name: stm32_can_setup
 *
 * Description:
 *  Initialize CAN and register the CAN device
 *
 ****************************************************************************/

#ifdef CONFIG_CAN
int stm32_can_setup(void);
#endif



/****************************************************************************
 * Name: stm32_ltc1865_setup
 *
 * Description:
 *   Initialize and register ADC LTC1865
 *
 *
 ****************************************************************************/

#ifdef CONFIG_MB_HEAT_LTC1865
int stm32_ltc1865_setup(void);
#endif


/****************************************************************************
 * Name: stm32_ltc2642_setup
 *
 * Description:
 *   Initialize and register DAC LTC2642
 *
 *
 ****************************************************************************/

#ifdef CONFIG_MB_HEAT_LTC2642
int stm32_ltc2642_setup(void);
#endif


/****************************************************************************************************
 * Name: stm32_pwm_setup
 *
 * Description:
 *   Initialize PWM and register the PWM device.
 *
 ****************************************************************************************************/

#ifdef CONFIG_PWM
int stm32_pwm_setup(void);
#endif


 /************************************************************************************
  * Name: stm32_tsic_initialize
  *
  * Description:
  *   Called to configure and initialize TSIC driver.
  *
  ************************************************************************************/
#if defined(CONFIG_SENSORS_TSIC)
void stm32_tsic_initialize(void);
#endif


/************************************************************************************
 * Name: stm32_smt172_initialize
 *
 * Description:
 *   Called to configure and initialize SMT172 driver.
 *
 ************************************************************************************/
#if defined(CONFIG_SENSORS_SMT172)
void stm32_smt172_initialize(void);
#endif


/****************************************************************************
 * Name: stm32_hptc_setup
 *
 * Description:
 *   Initialize HPTC and register the HPTC device.
 *
 ****************************************************************************/

#ifdef CONFIG_HPTC
int stm32_hptc_setup(void);
#endif

#endif /* __ASSEMBLY__ */
#endif /* __CONFIGS_TSIC_SRC_TSIC_H */

/************************************************************************************
 * configs/mB-master-stm32f756ig/include/board.h
 *
 ************************************************************************************/

#ifndef __CONFIG_MB_MASTER_F7_INCLUDE_BOARD_H
#define __CONFIG_MB_MASTER_F7_INCLUDE_BOARD_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#ifndef __ASSEMBLY__
# include <stdint.h>
#endif

//#ifdef __KERNEL__
//#include "stm32_rcc.h"
//#endif

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/* Clocking *************************************************************************/
/* The STM32F7 mB_Master board features a 10 MHz VCXO.
 *
 * This is the canonical configuration:
 *   System Clock source           : PLL (HSE)
 *   SYSCLK(Hz)                    : 216000000    Determined by PLL configuration
 *   HCLK(Hz)                      : 216000000    (STM32_RCC_CFGR_HPRE)
 *   AHB Prescaler                 : 1            (STM32_RCC_CFGR_HPRE)
 *   APB1 Prescaler                : 4            (STM32_RCC_CFGR_PPRE1)
 *   APB2 Prescaler                : 2            (STM32_RCC_CFGR_PPRE2)
 *   HSE Frequency(Hz)             : 10000000     (STM32_BOARD_XTAL)
 *   PLLM                          : 10           (STM32_PLLCFG_PLLM)
 *   PLLN                          : 432          (STM32_PLLCFG_PLLN)
 *   PLLP                          : 2            (STM32_PLLCFG_PLLP)
 *   PLLQ                          : 7            (STM32_PLLCFG_PLLQ)
 *   Main regulator output voltage : Scale1 mode  Needed for high speed SYSCLK
 *   Flash Latency(WS)             : 5
 *   Prefetch Buffer               : OFF
 *   Instruction cache             : ON
 *   Data cache                    : ON
 *   Require 48MHz for USB OTG FS, : Enabled
 *   SDIO and RNG clock
 */

/* HSI - 16 MHz RC factory-trimmed
 * LSI - 32 KHz RC
 * HSE - On-board oscillator frequency is 10,000 KHz
 * LSE - 32.768 kHz
 */

#define STM32_BOARD_XTAL        10000000ul

#define STM32_HSI_FREQUENCY     16000000ul
#define STM32_LSI_FREQUENCY     32000
#define STM32_HSE_FREQUENCY     STM32_BOARD_XTAL
#define STM32_LSE_FREQUENCY     32768

/* Defines HSE/LSE clock source as 'external clock' */
#define STM32_BOARD_HSE_BYPASS

/* Main PLL Configuration.
 *
 * PLL source is HSE = 10,000,000
 *
 * PLL_VCO = (STM32_HSE_FREQUENCY / PLLM) * PLLN
 * PLL_VCO = (10,000,000 / 10) * 432
 *         = 432,000,000
 * Subject to:
 *
 *     2 <= PLLM <= 63
 *   192 <= PLLN <= 432
 *   192 MHz <= PLL_VCO <= 432MHz
 *
 *
 * SYSCLK  = PLL_VCO / PLLP
 *         = 432,000,000 / 2 = 216,000,000
 * Subject to
 *
 *   PLLP = {2, 4, 6, 8}
 *   SYSCLK <= 216 MHz
 *
 * USB OTG FS, SDMMC and RNG Clock = PLL_VCO / PLLQ
 *                                 = 48,000,000
 *
 * Subject to
 *   The USB OTG FS requires a 48 MHz clock to work correctly. The SDMMC
 *   and the random number generator need a frequency lower than or equal
 *   to 48 MHz to work correctly.
 *
 * 2 <= PLLQ <= 15
 */

#define STM32_PLLCFG_PLLM       RCC_PLLCFG_PLLM(10)
#define STM32_PLLCFG_PLLN       RCC_PLLCFG_PLLN(432)
#define STM32_PLLCFG_PLLP       RCC_PLLCFG_PLLP_2
#define STM32_PLLCFG_PLLQ       RCC_PLLCFG_PLLQ(9)

#define STM32_VCO_FREQUENCY     ((STM32_HSE_FREQUENCY / 10) * 432)
#define STM32_SYSCLK_FREQUENCY  (STM32_VCO_FREQUENCY / 2)
#define STM32_OTGFS_FREQUENCY   (STM32_VCO_FREQUENCY / 9)

/* Configure factors for  PLLSAI clock */

#define CONFIG_STM32F7_PLLSAI 1
#define STM32_RCC_PLLSAICFGR_PLLSAIN    RCC_PLLSAICFGR_PLLSAIN(192)
#define STM32_RCC_PLLSAICFGR_PLLSAIP    RCC_PLLSAICFGR_PLLSAIP(8)
#define STM32_RCC_PLLSAICFGR_PLLSAIQ    RCC_PLLSAICFGR_PLLSAIQ(4)
#define STM32_RCC_PLLSAICFGR_PLLSAIR    RCC_PLLSAICFGR_PLLSAIR(2)

/* Configure Dedicated Clock Configuration Register */

#define STM32_RCC_DCKCFGR1_PLLI2SDIVQ  RCC_DCKCFGR1_PLLI2SDIVQ(1)
#define STM32_RCC_DCKCFGR1_PLLSAIDIVQ  RCC_DCKCFGR1_PLLSAIDIVQ(1)
#define STM32_RCC_DCKCFGR1_PLLSAIDIVR  RCC_DCKCFGR1_PLLSAIDIVR(0)
#define STM32_RCC_DCKCFGR1_SAI1SRC     RCC_DCKCFGR1_SAI1SEL(0)
#define STM32_RCC_DCKCFGR1_SAI2SRC     RCC_DCKCFGR1_SAI2SEL(0)
#define STM32_RCC_DCKCFGR1_TIMPRESRC   0
#define STM32_RCC_DCKCFGR1_DFSDM1SRC   0
#define STM32_RCC_DCKCFGR1_ADFSDM1SRC  0



/* Configure factors for  PLLI2S clock -> generate 52MHz clock for ULPI */

#define CONFIG_STM32F7_PLLI2S 1
#define STM32_RCC_PLLI2SCFGR_PLLI2SN   RCC_PLLI2SCFGR_PLLI2SN(104)
#define STM32_RCC_PLLI2SCFGR_PLLI2SP   RCC_PLLI2SCFGR_PLLI2SP(2)
#define STM32_RCC_PLLI2SCFGR_PLLI2SQ   RCC_PLLI2SCFGR_PLLI2SQ(2)
#define STM32_RCC_PLLI2SCFGR_PLLI2SR   RCC_PLLI2SCFGR_PLLI2SR(2)

/* Configure Dedicated Clock Configuration Register 2 */

#define STM32_RCC_DCKCFGR2_USART1SRC  RCC_DCKCFGR2_USART1SEL_SYSCLK
#define STM32_RCC_DCKCFGR2_USART2SRC  RCC_DCKCFGR2_USART2SEL_SYSCLK
//#define STM32_RCC_DCKCFGR2_UART3SRC   RCC_DCKCFGR2_UART3SEL_SYSCLK
#define STM32_RCC_DCKCFGR2_UART4SRC   RCC_DCKCFGR2_UART4SEL_SYSCLK
#define STM32_RCC_DCKCFGR2_UART5SRC   RCC_DCKCFGR2_UART5SEL_APB
#define STM32_RCC_DCKCFGR2_USART6SRC  RCC_DCKCFGR2_USART6SEL_SYSCLK	//SYSCLK		// Changed to 216MHz input
#define STM32_RCC_DCKCFGR2_UART7SRC   RCC_DCKCFGR2_UART7SEL_APB
#define STM32_RCC_DCKCFGR2_UART8SRC   RCC_DCKCFGR2_UART8SEL_APB
#define STM32_RCC_DCKCFGR2_I2C1SRC    RCC_DCKCFGR2_I2C1SEL_HSI
#define STM32_RCC_DCKCFGR2_I2C2SRC    RCC_DCKCFGR2_I2C2SEL_HSI
#define STM32_RCC_DCKCFGR2_I2C3SRC    RCC_DCKCFGR2_I2C3SEL_HSI
#define STM32_RCC_DCKCFGR2_I2C4SRC    RCC_DCKCFGR2_I2C4SEL_HSI
#define STM32_RCC_DCKCFGR2_LPTIM1SRC  RCC_DCKCFGR2_LPTIM1SEL_APB
#define STM32_RCC_DCKCFGR2_CECSRC     RCC_DCKCFGR2_CECSEL_HSI
#define STM32_RCC_DCKCFGR2_CK48MSRC   RCC_DCKCFGR2_CK48MSEL_PLL
#define STM32_RCC_DCKCFGR2_SDMMCSRC   RCC_DCKCFGR2_SDMMCSEL_48MHZ
#define STM32_RCC_DCKCFGR2_SDMMC2SRC  RCC_DCKCFGR2_SDMMC2SEL_48MHZ
#define STM32_RCC_DCKCFGR2_DSISRC     RCC_DCKCFGR2_DSISEL_48MHZ


/* Several prescalers allow the configuration of the two AHB buses, the
 * high-speed APB (APB2) and the low-speed APB (APB1) domains. The maximum
 * frequency of the two AHB buses is 216 MHz while the maximum frequency of
 * the high-speed APB domains is 108 MHz. The maximum allowed frequency of
 * the low-speed APB domain is 54 MHz.
 */

/* AHB clock (HCLK) is SYSCLK (216 MHz) */

#define STM32_RCC_CFGR_HPRE     RCC_CFGR_HPRE_SYSCLK  /* HCLK  = SYSCLK / 1 */
#define STM32_HCLK_FREQUENCY    STM32_SYSCLK_FREQUENCY
#define STM32_BOARD_HCLK        STM32_HCLK_FREQUENCY  /* same as above, to satisfy compiler */

/* APB1 clock (PCLK1) is HCLK/4 (54 MHz) */

#define STM32_RCC_CFGR_PPRE1    RCC_CFGR_PPRE1_HCLKd4     /* PCLK1 = HCLK / 4 */
#define STM32_PCLK1_FREQUENCY   (STM32_HCLK_FREQUENCY/4)

/* Timers driven from APB1 will be twice PCLK1 */

#define STM32_APB1_TIM2_CLKIN   (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM3_CLKIN   (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM4_CLKIN   (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM5_CLKIN   (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM6_CLKIN   (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM7_CLKIN   (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM12_CLKIN  (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM13_CLKIN  (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM14_CLKIN  (2*STM32_PCLK1_FREQUENCY)

/* APB2 clock (PCLK2) is HCLK/2 (108MHz) */

#define STM32_RCC_CFGR_PPRE2    RCC_CFGR_PPRE2_HCLKd2     /* PCLK2 = HCLK / 2 */
#define STM32_PCLK2_FREQUENCY   (STM32_HCLK_FREQUENCY/2)

/* Timers driven from APB2 will be twice PCLK2 */

#define STM32_APB2_TIM1_CLKIN   (2*STM32_PCLK2_FREQUENCY)
#define STM32_APB2_TIM8_CLKIN   (2*STM32_PCLK2_FREQUENCY)
#define STM32_APB2_TIM9_CLKIN   (2*STM32_PCLK2_FREQUENCY)
#define STM32_APB2_TIM10_CLKIN  (2*STM32_PCLK2_FREQUENCY)
#define STM32_APB2_TIM11_CLKIN  (2*STM32_PCLK2_FREQUENCY)

/* FLASH wait states
 *
 *  --------- ---------- -----------
 *  VDD       MAX SYSCLK WAIT STATES
 *  --------- ---------- -----------
 *  1.7-2.1 V   180 MHz    8
 *  2.1-2.4 V   216 MHz    9
 *  2.4-2.7 V   216 MHz    8
 *  2.7-3.6 V   216 MHz    7
 *  --------- ---------- -----------
 */

#define BOARD_FLASH_WAITSTATES 7

/* LED definitions ******************************************************************/
/* The mB Master F7 board has got two GPIO LEDs (Green and Red), that can be controlled
 * by software.
 *
 * If CONFIG_ARCH_LEDS is not defined, then the user can control the LEDs in any way.
 * The following definitions are used to access individual LEDs.
 */

/* LED index values for use with board_userled() */

#define BOARD_LED1        0
#define BOARD_LED2        1
#define BOARD_NLEDS       2

#define BOARD_LED_GREEN   BOARD_LED1
#define BOARD_LED_RED     BOARD_LED2

/* LED bits for use with board_userled_all() */

#define BOARD_LED1_BIT    (1 << BOARD_LED1)
#define BOARD_LED2_BIT    (1 << BOARD_LED2)

/* If CONFIG_ARCH_LEDS is defined, the usage by the board port is defined in
 * include/board.h and src/stm32_leds.c. The LEDs are used to encode OS-related
 * events as follows:
 *
 *
 *   SYMBOL                     Meaning                      LED state
 *                                                        Red   Green Blue
 *   ----------------------  --------------------------  ------ ------ ----*/

#define LED_STARTED        0 /* NuttX has been started   OFF    OFF   OFF  */
#define LED_HEAPALLOCATE   1 /* Heap has been allocated  OFF    OFF   ON   */
#define LED_IRQSENABLED    2 /* Interrupts enabled       OFF    ON    OFF  */
#define LED_STACKCREATED   3 /* Idle stack created       OFF    ON    ON   */
#define LED_INIRQ          4 /* In an interrupt          N/C    N/C   GLOW */
#define LED_SIGNAL         5 /* In a signal handler      N/C    GLOW  N/C  */
#define LED_ASSERTION      6 /* An assertion failed      GLOW   N/C   GLOW */
#define LED_PANIC          7 /* The system has crashed   Blink  OFF   N/C  */
#define LED_IDLE           8 /* MCU is is sleep mode     ON     OFF   OFF  */

/* Thus if the Green LED is statically on, NuttX has successfully booted and
 * is, apparently, running normally.  If the Red LED is flashing at
 * approximately 2Hz, then a fatal error has been detected and the system
 * has halted.
 */

#ifdef CONFIG_ARCH_HAVE_SPI_LEDS
/* SPI LEDS */
#define LED_SPI_NUM     16      /* Number of SPI leds */
#define LED_SPI_0    0x0100
#define LED_SPI_1    0x0200
#define LED_SPI_2    0x0400
#define LED_SPI_3    0x0800
#define LED_SPI_4    0x1000
#define LED_SPI_5    0x2000
#define LED_SPI_6    0x4000
#define LED_SPI_7    0x8000
#define LED_SPI_8    0x0001
#define LED_SPI_9    0x0002
#define LED_SPI_10   0x0004
#define LED_SPI_11   0x0008
#define LED_SPI_12   0x0010
#define LED_SPI_13   0x0020
#define LED_SPI_14   0x0040
#define LED_SPI_15   0x0080

#define LED_BUS1_GREEN   2     /* LED_SPI_0 */
#define LED_BUS1_RED     3     /* LED_SPI_1 */
#define LED_BUS2_GREEN   4     /* LED_SPI_2 */
#define LED_BUS2_RED     5     /* LED_SPI_3 */
#define LED_BUS3_GREEN   6     /* LED_SPI_4 */
#define LED_BUS3_RED     7     /* LED_SPI_5 */
#define LED_BUS4_GREEN   8     /* LED_SPI_6 */
#define LED_BUS4_RED     9     /* LED_SPI_7 */
#endif


/* Alternate function pin selections ************************************************/

/* DMA channels *************************************************************/

/* SPI
 *
 * F7 master board uses SPI 1 and 4.
 *
 *  PA6   SPI1_MISO
 *  PA7   SPI1_MOSI
 *  PB3   SPI1_SCK
 *
 *  PE13   SPI4_MISO
 *  PE14   SPI4_MOSI
 *  PE12   SPI4_SCK
 */

#define GPIO_SPI1_MISO   GPIO_SPI1_MISO_1 // unused (unidirectional transfer)
#define GPIO_SPI1_MOSI   GPIO_SPI1_MOSI_1
#define GPIO_SPI1_SCK    GPIO_SPI1_SCK_2

#define GPIO_SPI4_MISO   GPIO_SPI4_MISO_2
#define GPIO_SPI4_MOSI   GPIO_SPI4_MOSI_2
#define GPIO_SPI4_SCK    GPIO_SPI4_SCK_2


/* MessBUS:
 *
 * Measured ticks per timeslice to compensate known oscillator inaccuracies.
 */
#define mB_TICKS_PER_TIMESLICE		40000

#ifdef CONFIG_MESSBUS_MASTER
	/* Timer 1 output channel pin selection */
	#define GPIO_TIM1_CH1OUT	GPIO_TIM1_CH1OUT_1
	#define GPIO_TIM1_CH2OUT	GPIO_TIM1_CH2OUT_1
	#define GPIO_TIM1_CH3OUT	GPIO_TIM1_CH3OUT_1
	#define GPIO_TIM1_CH4OUT	GPIO_TIM1_CH4OUT_1

	/* Pin selection for the USARTs. USART 3 not supported yet,
	 * because System clock can not be selected as fck for it
	 * under NuttX. This is probably due to the virtual comport.
	 */
	#define GPIO_USART1_TX		GPIO_USART1_TX_2
	#define GPIO_USART1_RX		GPIO_USART1_RX_2
	#define GPIO_USART2_TX		GPIO_USART2_TX_2
	#define GPIO_USART2_RX		GPIO_USART2_RX_2
	#define GPIO_UART4_TX		GPIO_UART4_TX_2
	#define GPIO_UART4_RX		GPIO_UART4_RX_2

	/* Select DMA streams if necessary. Again Usart 3 is not
	 * supported here because of the fck problem above. */
	#define DMAMAP_USART1_RX	DMAMAP_USART1_RX_1

	/* Data Enable pins (active high) */
	#define GPIO_CH1_DE      	(GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
									GPIO_OUTPUT_CLEAR|GPIO_PORTI|GPIO_PIN5)
	#define GPIO_CH2_DE      	(GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
									GPIO_OUTPUT_CLEAR|GPIO_PORTI|GPIO_PIN6)
	// CH3 missing again
	#define GPIO_CH4_DE      	(GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
									GPIO_OUTPUT_CLEAR|GPIO_PORTI|GPIO_PIN2)

	/* Receive Enable pins (always low, except for sleep) -> PG0 - PG3 */
	#define GPIO_CH1_RE      	(GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
									GPIO_OUTPUT_CLEAR|GPIO_PORTG|GPIO_PIN0)
	#define GPIO_CH2_RE      	(GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
									GPIO_OUTPUT_CLEAR|GPIO_PORTG|GPIO_PIN1)
	// CH3 missing again
	#define GPIO_CH4_RE      	(GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
									GPIO_OUTPUT_CLEAR|GPIO_PORTG|GPIO_PIN3)

    /* FAN is also connected to this pin */
    #define GPIO_PWR_BUS_1      (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
									GPIO_OUTPUT_CLEAR|GPIO_PORTF|GPIO_PIN0)

    #define GPIO_PWR_BUS_2      (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
									GPIO_OUTPUT_CLEAR|GPIO_PORTF|GPIO_PIN1)

    #define GPIO_PWR_BUS_3      GPIO_PWR_BUS_1 

    #define GPIO_PWR_BUS_4      GPIO_PWR_BUS_2

    #define GPIO_PPS_OUT        (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
									GPIO_OUTPUT_CLEAR|GPIO_PORTF|GPIO_PIN7)
#endif //CONFIG_MESSBUS_MASTER

/* Timer 2 */
//#define GPIO_TIM2_CH1IN GPIO_TIM2_CH1IN_1 /*PA0*/
/*PPS: PA0*/
#define GPIO_TIM2_CH1IN (GPIO_ALT|GPIO_AF1|GPIO_SPEED_50MHz|GPIO_PULLUP|GPIO_PORTA|GPIO_PIN0)

//#define GPIO_TIM2_CH1OUT GPIO_TIM2_CH1OUT_1 /*PA0*/

/* Timer 5*/
#define GPIO_TIM5_CH1IN GPIO_TIM5_CH1IN_2 /*PH10*/
#define GPIO_TIM5_CH2IN GPIO_TIM5_CH2IN_2 /*PH11*/
#define GPIO_TIM5_CH3IN GPIO_TIM5_CH3IN_2 /*PH12*/
#define GPIO_TIM5_CH4IN GPIO_TIM5_CH4IN_2 /*PI0*/

#define GPIO_TIM5_CH1OUT GPIO_TIM5_CH1OUT_2 /*PH10*/
#define GPIO_TIM5_CH2OUT GPIO_TIM5_CH2OUT_2 /*PH11*/
#define GPIO_TIM5_CH3OUT GPIO_TIM5_CH3OUT_2 /*PH12*/
#define GPIO_TIM5_CH4OUT GPIO_TIM5_CH4OUT_2 /*PI0*/


#define VCXO_DAC_SPI_BUS 1
#define GPIO_VCXO (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_100MHz|\
									GPIO_OUTPUT_SET|GPIO_PORTA|GPIO_PIN4)


/* USB HS ULPI */
//unused #define BOARD_ENABLE_USBOTG_HSULPI
#define GPIO_OTGHS_ULPI_DIR  GPIO_OTGHS_ULPI_DIR_1
#define GPIO_OTGHS_ULPI_NXT  GPIO_OTGHS_ULPI_NXT_1

//MCO2 used to clock ULPI
#define BOARD_CFGR_MC02_SOURCE  RCC_CFGR_MCO2_PLLI2S
#define BOARD_CFGR_MC02_DIVIDER RCC_CFGR_MCO2PRE_NONE


/************************************************************************************
 * Public Data
 ************************************************************************************/
#ifndef __ASSEMBLY__

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/************************************************************************************
 * Public Function Prototypes
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_boardinitialize
 *
 * Description:
 *   All STM32 architectures must provide the following entry point.  This entry point
 *   is called early in the initialization -- after all memory has been configured
 *   and mapped but before any devices have been initialized.
 *
 ************************************************************************************/

void stm32_boardinitialize(void);

#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* __ASSEMBLY__ */
#endif  /* __CONFIG_MB_MASTER_F7_INCLUDE_BOARD_H */

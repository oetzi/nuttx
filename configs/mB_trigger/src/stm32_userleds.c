/****************************************************************************
 * configs/mB_trigger/src/stm32_userleds.c
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/board.h>
#include <arch/board/board.h>

#include <stdbool.h>
#include <debug.h>
#include "mB_trigger.h"

#include "stm32_gpio.h"

#ifdef CONFIG_MESSBUS_USERLED
#include <nuttx/messBUS/messBUS_led.h>
#endif

#ifndef CONFIG_ARCH_LEDS

/****************************************************************************
 * Private Data
 ****************************************************************************/
/* This array maps an LED number to GPIO pin configuration */

static uint32_t g_ledcfg[BOARD_NLEDS] =
{
GPIO_LED1, GPIO_LED2
};

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: board_userled_initialize
 *
 * Description:
 *   If CONFIG_ARCH_LEDS is defined, then NuttX will control the on-board
 *   LEDs.  If CONFIG_ARCH_LEDS is not defined, then the
 *   board_userled_initialize() is available to initialize the LED from user
 *   application logic.
 *
 ****************************************************************************/

void board_userled_initialize(void)
{
	stm32_configgpio(GPIO_LED1);
	stm32_configgpio(GPIO_LED2);
}

/****************************************************************************
 * Name: board_userled
 ****************************************************************************/

void board_userled(int led, bool ledon)
{
	if ((unsigned) led < BOARD_NLEDS)
    {
		stm32_gpiowrite(g_ledcfg[led], ledon);
    }
}

/****************************************************************************
 * Name: board_userled_all
 ****************************************************************************/

void board_userled_all(uint8_t ledset)
{
	stm32_gpiowrite(GPIO_LED1, (ledset & BOARD_LED1_BIT) == 0);
	stm32_gpiowrite(GPIO_LED2, (ledset & BOARD_LED2_BIT) == 0);
}

#ifdef  CONFIG_MESSBUS_USERLED
/****************************************************************************
 * Name: switch_LED
 ****************************************************************************/

void switch_LED(int led, bool state)
{
	if ((unsigned) led < BOARD_NLEDS)
	{
		board_userled(led, state);
	}
}

/****************************************************************************
 * Name: toggle_LED
 ****************************************************************************/

void toggle_LED(int led)
{
	if ((unsigned) led < BOARD_NLEDS)
	{
		if (stm32_gpioread(g_ledcfg[led]))
			switch_LED(led, false);
		else
			switch_LED(led, true);
	}
}

/****************************************************************************
 * Name: switch_LED_red_on
 ****************************************************************************/
void switch_LED_red_on()
{
	switch_LED(BOARD_LED_RED, true);
}

/****************************************************************************
 * Name: switch_LED_red_off
 ****************************************************************************/
void switch_LED_red_off()
{
	switch_LED(BOARD_LED_RED, false);
}

/****************************************************************************
 * Name: toggle_LED_red
 ****************************************************************************/
void toggle_LED_red()
{
	if (stm32_gpioread(g_ledcfg[BOARD_LED_RED]))
		switch_LED_red_off();
	else
		switch_LED_red_on();
}

/****************************************************************************
 * Name: switch_LED_green_on
 ****************************************************************************/
void switch_LED_green_on()
{
	switch_LED(BOARD_LED_GREEN, true);
}

/****************************************************************************
 * Name: switch_LED_green_off
 ****************************************************************************/
void switch_LED_green_off()
{
	switch_LED(BOARD_LED_GREEN, false);
}

/****************************************************************************
 * Name: toggle_LED_green
 ****************************************************************************/
void toggle_LED_green()
{
	if (stm32_gpioread(g_ledcfg[BOARD_LED_GREEN]))
		switch_LED_green_off();
	else
		switch_LED_green_on();
}

#endif /*  CONFIG_MESSBUS_USERLED */

#endif /* !CONFIG_ARCH_LEDS */

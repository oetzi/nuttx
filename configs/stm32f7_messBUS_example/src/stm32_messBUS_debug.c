/*
 * stm32_messBUS_debug.c
 *
 *  Created on: 27.02.2019
 *      Author: bbrandt
 */


/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/board.h>
#include <arch/board/board.h>
#include <debug.h>

#include "stm32_gpio.h"

#include <nuttx/messBUS/messBUS_Debug.h>

static inline void setbit32(unsigned int target, uint32_t bit) __attribute__((always_inline));
static inline void setbit32(unsigned int target, uint32_t bit)
{
	*(volatile uint32_t *) (target) |= (bit);
}
;

void switchDebugPinOn(void)
{
	setbit32(STM32_GPIOD_BSRR, GPIO_MB_DEBUG_ON);
}

void switchDebugPinOff(void)
{
	setbit32(STM32_GPIOD_BSRR, GPIO_MB_DEBUG_OFF);
}

void toggleDebugPin(void)
{
	switchDebugPinOn();
	switchDebugPinOff();
}

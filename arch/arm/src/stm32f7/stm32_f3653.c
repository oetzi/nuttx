/****************************************************************************
 * arch/arm/src/stm32f7/stm32_f3653.c
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/spi/spi.h>
#include <nuttx/timers/hptc.h>
#include <sys/types.h>

#include <assert.h>
#include <stdint.h>
#include <errno.h>
#include <syslog.h>
#include <stdio.h>                      // printf

#include "up_arch.h"                    // putreg32, modifyreg16 etc.
#include "chip.h"
#include "stm32_gpio.h"
#include "stm32_tim.h"
#include "stm32_spi.h"
#include "stm32_qencoder.h"
#include "stm32_f3653.h"


#ifdef CONFIG_ARCH_CHIP_STM32F7
#    include <nuttx/cache.h>
#endif

#ifndef CONFIG_HPTC
#  error "HPTC is not enabled"
#endif


/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/
#define F3653_QENCODER_EDGE_POS_DELTA   2


/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

static int up_setup(void);
static int up_start(void);
static int up_stop(void);
static int up_shutdown(void);
static f3653_rotating_dir_t up_getRotatingDir(void);
static float64 up_getVelocity(void);
static int32_t up_getTimeDiff(struct timespec *start, struct timespec *stop);



/****************************************************************************
 * Private Types
 ****************************************************************************/


/****************************************************************************
 * Private Data
 ****************************************************************************/

/* Arch-specific instance of f3653_ops_s, which will be attached to
 * g_f3653_ops and thus to the upper half driver.
 */
static const struct f3653_ops_s g_f3653_ops =
{
  .setup            = up_setup,
  .start            = up_start,
  .stop             = up_stop,
  .shutdown         = up_shutdown,
  .getRotatingDir   = up_getRotatingDir,
  .getRotatingVel   = up_getVelocity,
  .getTimeDiff      = up_getTimeDiff
};



/* Arch-specific instance of f3653_dev_t, which will be attached to the
 * upper half driver. This struct is used to bind the lower half to its
 * upper half and to share data between them.
 */
static f3653_dev_t g_f3653_lowerhalf_s =
{
    /* This variable indicates whether the device has already been opened
     * or not. Default is closed (0).
     */

    .open = false,

    /* This variable indicates whether the driver is running or paused.
     * A user always needs to open the device first.
     * Default is paused (0).
     */

    .running = false,

    /* Attach the lower half operations to the upper half */

    .ops = &g_f3653_ops,

    /* Do not initialize SSI device yet. Explicitly done by initialize function */

    .ssi_dev = NULL,

    /* Initialize the measurement helper buffer with default values */

    .current_sample = {
        .timeStamp = {0,0},
        .angle     = 0,
        .velocity  = 0
    },

    .last_sample = {
        .timeStamp = {0,0},
        .angle     = 0,
        .velocity  = 0
    },

};

static struct qencoder_edge_info
{
    struct timespec timeStamp;
    bool updated;              // indicates if data was updated since last read
    f3653_rotating_dir_t dir;  // indicates rotating direction

} g_last_edge, g_current_edge;


/****************************************************************************
 * Private Functions
 ****************************************************************************/


/****************************************************************************
 * Name: f3653_hptc_callback
 *
 * Description:
 *   This is the handler for the hptc timer callback.
 *
 ****************************************************************************/

static int f3653_hptc_callback(FAR void* arg, uint8_t flags, int32_t capture_s, int32_t capture_ns)
{
    const auxchannel_t ch = *(auxchannel_t*)arg;

    if(!g_f3653_lowerhalf_s.open || !g_f3653_lowerhalf_s.running) return -EAGAIN;

    if(ch == 2 && flags == 0)
    {
        /* correct channel and everything okay */

        g_last_edge = g_current_edge;

        g_current_edge.timeStamp.tv_sec  = capture_s;
        g_current_edge.timeStamp.tv_nsec = capture_ns;

        g_current_edge.dir     = up_getRotatingDir();

        g_current_edge.updated = true;
        g_last_edge.updated    = true;

        return OK;
    }
    return -EINVAL;
}


/****************************************************************************
 * Name: up_getTimeDiff
 *
 * Description:
 *    Determine diff of two timespec structs in nanoseconds.
 *
 ****************************************************************************/

static int32_t up_getTimeDiff(struct timespec *start, struct timespec *stop)
{
    DEBUGASSERT(start && stop);

    if(start == NULL || stop == NULL) return -EINVAL;

    int32_t second_diff  = stop->tv_sec - start->tv_sec;
    int32_t nsecond_diff = (stop->tv_nsec - start->tv_nsec) + second_diff * NSEC_PER_SEC;

    return nsecond_diff;
}


/****************************************************************************
 * Name: up_getVelocity
 *
 * Description:
 *   Calculates velocity of encoder input using diffential quotient.
 *
 *   vel = delta pos / delta t;
 *
 *   Returns velocity in [deg/s] as 64 bit float value.
 *
 ****************************************************************************/

static float64 up_getVelocity(void)
{
    float64 velocity = 0;

    if(g_current_edge.updated && g_last_edge.updated)
    {
        float64 dt = ((float64) (up_getTimeDiff(&g_last_edge.timeStamp, &g_current_edge.timeStamp)))
            / NSEC_PER_SEC; // [s]

        /* Between 2 edges the position always changes by 2. */

        int32_t dpos = F3653_QENCODER_EDGE_POS_DELTA * g_current_edge.dir;

        float64 velo = dpos / dt;  // [ticks/s]
        velo = (float64) velo * 360 / F3653_QENCODER_RESOLUTION; //[°/s]

        /* reset 'updated' flag, to schedule waiting for next event and set velocity to 0 meanwhile */

        g_current_edge.updated = false;
        g_last_edge.updated    = false;

        velocity = velo;
    }

    return velocity;
}



/****************************************************************************
 * Name: up_setup
 *
 * Description:
 *   Setup the hardware but don't start it already.
 *   This method is called by the upper half after a successful call to the
 *   upper half's open method from application logic.
 *
 ****************************************************************************/

static int up_setup(void)
{
#ifdef CONFIG_HPTC
    static const auxchannel_t g_channel = ch2; //TODO
    const int ret = hptc_set_async_input_callback(g_channel, &f3653_hptc_callback, &g_channel);
    hptc_enable_async_input_callback(g_channel);
    return ret;
#endif

    return 0;
}

/*
 * If F3653_QENCODER_TIMER is set up properly, Bit 4 (direction) of CR1 register is updated
 * automatically depending on input on TI1 and TI2. (Encoder Mode)
 * So we only need to read this bit and return rotating direction depending on its value.
 */
static f3653_rotating_dir_t up_getRotatingDir(void)
{
    uint16_t regval = 0;
#if F3653_QENCODER_TIMER == 1
    regval = getreg16(STM32_TIM1_CR1);
#endif

    if(regval & ATIM_CR1_DIR)
    {
        return anticlockwise;
    }
    else
    {
        return clockwise;
    }
}

/****************************************************************************
 * Name: up_start
 *
 * Description:
 *      Starts the driver.
 *
 ****************************************************************************/

static int up_start(void)
{
    g_f3653_lowerhalf_s.running = true;

    return 0;
}

/****************************************************************************
 * Name: up_stop
 *
 * Description:
 *      Stops the driver.
 *
 ****************************************************************************/

static int up_stop(void)
{
    g_f3653_lowerhalf_s.running = false;

    return 0;
}

/****************************************************************************
 * Name: up_shutdown
 *
 * Description:
 *   Not provided at this stage of development. Should principally be the
 *   counterpart to up_setup.
 *
 ****************************************************************************/

static int up_shutdown(void)
{
    /* Maybe needed later, so the function is already provided and can be
     * called from the upper half as it is bound to it via f3653_ops.
     */

    return 0;
}


/****************************************************************************
 * Public Functions
 ****************************************************************************/

void f3653_initialize(void)
{
    /* Perform registration of the driver.
     * This method should be called from board-specific stm32_bringup.c
     */

    int ret = 0;

    /* Initialization of SSI device (= SPI device) */

    g_f3653_lowerhalf_s.ssi_dev = (struct spi_dev_s *) stm32_spibus_initialize(F3653_SPI_BUS);

    if (!g_f3653_lowerhalf_s.ssi_dev)
    {
        syslog(LOG_ERR, "ERROR Failed to initialize SPI bus for F3653 angle sensor\n");
    }

    /* Initialization of Quadratur encoder driver */

    stm32_qeinitialize(F3653_QENCODER_PATH, F3653_QENCODER_TIMER);

    /* Call the upper half to register the whole driver in the VFS. */

    ret = f3653_register(F3653_DEV_PATH, &g_f3653_lowerhalf_s);
    if (ret < 0)
    {
        syslog(LOG_ERR, "ERROR Failed to register the F3653 device in filesystem!\n");
    }
}


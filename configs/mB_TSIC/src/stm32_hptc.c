/************************************************************************************
 * configs/mB_TSIC/src/stm32_hptc.c
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>
#include <nuttx/timers/hptc.h>

#include <errno.h>
#include <debug.h>

#include "stm32_hptc.h"
#include "mB_TSIC.h"

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/
/* Configuration *******************************************************************/

/* HPTC
 *
 * The mB_Acc board allows TIM2 and TIM5 to be used as HPTC
 */

#define HAVE_HPTC 1

#ifndef CONFIG_HPTC
#  undef HAVE_HPTC
#endif

/************************************************************************************
 * Private Functions
 ************************************************************************************/

/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_hptc_setup
 *
 * Description:
 *   Initialize HPTC and register the HPTC device.
 *
 ************************************************************************************/

int stm32_hptc_setup(void)
{
#ifdef HAVE_HPTC
  static bool initialized = false;

  int ret;

  /* Have we already initialized? */

  if (!initialized)
    {
      /* Call stm32_hptcinitialize() to get an instance of the HPTC interface */

	  ret=stm32_hptc_initialize();


      if (ret < 0)
        {
          aerr("ERROR: stm32_hptc_initialize failed: %d\n", ret);
          return ret;
        }

      /* Now we are initialized */

      initialized = true;
    }

  return OK;
#else
  return -ENODEV;
#endif /* HAVE_HPTC */
}



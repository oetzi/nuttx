/****************************************************************************
 * configs/mB_Heat/src/stm32_messBUS_gpio.c
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/messBUS/messBUS_gpio.h>
#include <stdbool.h>
#include <stdint.h>

#include "stm32_gpio.h"

#ifdef CONFIG_MESSBUS_GPIO

/****************************************************************************
 * Public functions
 ****************************************************************************/

/************************************************************************************
 * Name: messBUS_gpiowrite
 *
 * Description:
 *   Write one or zero to the selected GPIO pin.
 *
 *   NOTE: This is a dirty shortcut to allow access to GPIO Pin from user code.
 *         This should be used for test purposes only, when writing a driver is too
 *         much overhead.
 *
 ************************************************************************************/

void messBUS_gpiowrite(uint32_t pinset, bool value)
{
    stm32_gpiowrite(pinset, value);
}

/************************************************************************************
 * Name: messBUS_gpioread
 *
 * Description:
 *   Read one or zero from the selected GPIO pin
 *
 *   NOTE: This is a dirty shortcut to allow access to GPIO Pin from user code.
 *         This should be used for test purposes only, when writing a driver is too
 *         much overhead.
 *
 ************************************************************************************/

bool messBUS_gpioread(uint32_t pinset)
{
    return stm32_gpioread(pinset);
}

#endif /* CONFIG_CONFIG_MESSBUS_GPIO */

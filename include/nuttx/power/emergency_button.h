/*
 * emergency_button.h
 *
 *  Created on: 27.04.2020
 */

#ifndef NUTTX_INCLUDE_NUTTX_POWER_EMERGENCY_BUTTON_H_
#define NUTTX_INCLUDE_NUTTX_POWER_EMERGENCY_BUTTON_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include <stdbool.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#ifndef __ASSEMBLY__

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/************************************************************************************
 * Public Function Prototypes
 ************************************************************************************/

/************************************************************************************
 * Name: push_emergency_button
 *
 * Description:
 *       If invoked, this function activates the board-specifc emergency operation.
 *
 ************************************************************************************/

void push_emergency_button(bool on);

#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* __ASSEMBLY__ */
#endif /* NUTTX_INCLUDE_NUTTX_POWER_EMERGENCY_BUTTON_H_ */

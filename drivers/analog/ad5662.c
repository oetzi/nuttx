/****************************************************************************
 * drivers/sensors/ad5662.c
 * Character driver for the AD5662
 *
 * based on driver MAX31855 from Alan Carvalho de Assis
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdlib.h>
#include <fixedmath.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/kmalloc.h>
#include <nuttx/fs/fs.h>
#include <nuttx/spi/spi.h>
#include <nuttx/analog/ad5662.h>

#if CONFIG_SPI && CONFIG_DAC_AD5662

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
#ifndef OK
#  define OK 0
#endif

/****************************************************************************
 * Private
 ****************************************************************************/

struct ad5662_dev_s
{
	FAR struct spi_dev_s *spi; /* Saved SPI driver instance */
	sem_t exclsem; /*  exclusion */
	int devno;
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* Character driver methods */

static int ad5662_open(FAR struct file *filep);
static int ad5662_close(FAR struct file *filep);
static ssize_t ad5662_read(FAR struct file *filep, FAR char *buffer, size_t buflen);
static ssize_t ad5662_write(FAR struct file *filep, FAR const char *buffer, size_t buflen);

static int ad5662_ioctl(FAR struct file *filep, int cmd, unsigned long arg);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct file_operations g_ad5662fops =
{
  ad5662_open,
  ad5662_close,
  ad5662_read,
  ad5662_write,
  NULL,
  ad5662_ioctl,    /* ioctl */
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: ad5662_open
 *
 * Description:
 *   This function is called whenever the AD5662 device is opened.
 *
 ****************************************************************************/

static int ad5662_open(FAR struct file *filep)
{
	return OK;
}

/****************************************************************************
 * Name: ad5662_close
 *
 * Description:
 *   This routine is called when the AD5662 device is closed.
 *
 ****************************************************************************/

static int ad5662_close(FAR struct file *filep)
{
	return OK;
}

/****************************************************************************
 * Name: ad5662_read
 ****************************************************************************/

static ssize_t ad5662_read(FAR struct file *filep, FAR char *buffer, size_t buflen)
{
	return OK;
}

/****************************************************************************
 * Name: ad5662_write
 *
 * Writes value of 'buffer' to AD5662 24 bit shift register.
 * Therefore buflen has to be 2 or 3 bytes.
 * If only 2 bytes are passed, 'normal mode' is assumed and bits PD1 and PD0
 * are set to 0x00 both.
 *
 * Input Parameters:
 *   filep - file descriptor
 *   buffer -pointer to Byte Array, which contains all data to send
 *   buflen - size from 2 to 3 Byte
 *
 *
 ****************************************************************************/

static ssize_t ad5662_write(FAR struct file *filep, FAR const char *buffer, size_t buflen)
{
	if (buflen >= 2 && buflen <= 4)
	{
		FAR struct inode *inode = filep->f_inode;
		FAR struct ad5662_dev_s *priv;
		int ret;

		/* Get exclusive access to the SPI driver state structure */

		DEBUGASSERT(filep != NULL && filep->f_inode != NULL);
		inode = filep->f_inode;

		priv = (FAR struct ad5662_dev_s*) inode->i_private;
		DEBUGASSERT(priv);

		ret = sem_wait(&priv->exclsem);

		if (ret < 0)
		{
			int errcode = errno;
			DEBUGASSERT(errcode < 0);
			return -errcode;
		}

		SPI_LOCK(priv->spi, true);

		SPI_SELECT(priv->spi, priv->devno, true);

		switch (buflen)
		{
		case 2:
		{
			unsigned char empty = 0x00; // assume 'normale mode '
			/*first Byte empty*/
			SPI_SNDBLOCK(priv->spi, &empty, 1);
			++buffer;
			/*TX Byte 1*/
			SPI_SNDBLOCK(priv->spi, buffer, 1); // send bytewise for correct byte order (big endian vs little endian)
			++buffer;
			/*TX Byte 2*/
			SPI_SNDBLOCK(priv->spi, buffer, 1);

		}
			break;
		case 3:
		{
			/*first Byte empty*/
			SPI_SNDBLOCK(priv->spi, buffer, 1);
			++buffer;
			/*TX Byte 1*/
			SPI_SNDBLOCK(priv->spi, buffer, 1);
			++buffer;
			/*TX Byte 2*/
			SPI_SNDBLOCK(priv->spi, buffer, 1);
		}
			break;
		default:
			break;
		}

		SPI_SELECT(priv->spi, priv->devno, false);
		SPI_LOCK(priv->spi, false);

		sem_post(&priv->exclsem);

		return 0;
	}
	return -1;
}

/****************************************************************************
 * Name: ad5662_ioctl
 *
 * Description:
 *
 *
 * Input Parameters:
 *   filep -
 *   cmd - IOCTL command
 *   arg - must be empty
 *
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/

static int ad5662_ioctl(FAR struct file *filep, int cmd, unsigned long arg)
{
	FAR struct inode *inode = filep->f_inode;
	FAR struct ad5662_dev_s *priv;
	int ret;

	/* Get exclusive access to the SPI driver state structure */

	DEBUGASSERT(filep != NULL && filep->f_inode != NULL);
	inode = filep->f_inode;

	priv = (FAR struct ad5662_dev_s*) inode->i_private;
	DEBUGASSERT(priv);

	ret = sem_wait(&priv->exclsem);

	if (ret < 0)
	{
		int errcode = errno;
		DEBUGASSERT(errcode < 0);
		return -errcode;
	}

	/*ioctl command*/

	switch (cmd)
	{
	/* Command:    	AD566s_IOC_SPI_INT
	 * Description:	initialized the SPI - BUS
	 * Argument:		none
	 * Dependencies:
	 */
	case AD5662_IOC_SPI_INIT:
	{

		SPI_LOCK(priv->spi, true);
		SPI_SETFREQUENCY(priv->spi, AD5662_SPI_FREQ);
		SPI_SETMODE(priv->spi, AD5662_SPI_MODE);
		SPI_SETBITS(priv->spi, AD5662_SPI_NBITS);
		SPI_LOCK(priv->spi, false);
	}
		break;
	default:
		break;
	}
	sem_post(&priv->exclsem);

	return 0;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: ad5662_register
 *
 * Description:
 *   Register the ad5662 character device as 'devpath'
 *
 * Input Parameters:
 *   devpath - The full path to the driver to register. E.g., "/dev/temp0"
 *   spi - An instance of the SPI interface to use to communicate with
 *   bus - the bus address of the connected SPI Device (devno)
 *
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/

int ad5662_register(FAR const char *devpath, FAR struct spi_dev_s *spi, int bus)
{
	FAR struct ad5662_dev_s *priv;
	int ret = OK;

	/* Sanity check */

	DEBUGASSERT(spi != NULL);

	/* Initialize the MAX31855 device structure */

	priv = (FAR struct ad5662_dev_s *) kmm_zalloc(sizeof(struct ad5662_dev_s));
	if (priv == NULL)
	{
		snerr("ERROR: Failed to allocate instance\n");
		return -ENOMEM;
	}

	priv->spi = spi;
	priv->exclsem.semcount = 1;
	priv->devno = bus;

	/* Register the character driver */
	ret = register_driver(devpath, &g_ad5662fops, 0666, priv);
	if (ret < 0)
	{
		snerr("ERROR: Failed to register AD5662 driver: %d\n", ret);
		kmm_free(priv);
	}

	return ret;
}
#endif /* CONFIG_SPI && CONFIG_AD5662 */

/****************************************************************************
 * include/nuttx/sensors/tsic.h
 *
 *   Copyright 2020 Leichtwerk Research GmbH
 *   Author: Stefan Nowak
 *
 ****************************************************************************/

#ifndef __INCLUDE_NUTTX_SENSORS_TSIC_H
#define __INCLUDE_NUTTX_SENSORS_TSIC_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>
#include <time.h>
#include <nuttx/fs/ioctl.h>

#if defined(CONFIG_SENSORS_TSIC)

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
/* FIXME: TSIC 306 hardcoded */
#define TSIC_HT        150.0
#define TSIC_LT        -50.0
#define TSIC_MAXVAL   2047.0


#ifndef NS_PER_SEC
#define NS_PER_SEC 1000000000
#endif

#define TSIC_SAMPLE_FREQ     250000
#define TSIC_BAUD              8000
#define TSIC_COUNT_PER_BIT   (TSIC_SAMPLE_FREQ/TSIC_BAUD)
#define TSIC_SAMPLE_PERIOD_NS (NS_PER_SEC/TSIC_SAMPLE_FREQ)


/****************************************************************************
 * Public Types
 ****************************************************************************/

struct tsic_config_s;

/* A reference to a structure of this type must be passed to the tsic
 * driver. This structure provides information about the configuration
 * of the sensor and provides some board-specific hooks.
 *
 * Memory for this structure is provided by the caller.  It is not copied
 * by the driver and is presumed to persist while the driver is active.
 */


struct tsic_channel_s {
    struct timespec timestamp;
    volatile uint16_t* fifo;
    int32_t fifosize;
    int32_t readpos;
    int32_t data;
    int32_t bitcnt;
    int32_t prevbit;
    int32_t parity;
    int32_t state;
    float   scale;
    float   offset;
    uint16_t last_capture;
    uint16_t first_capture;
    uint16_t* refcnt;
};


struct tsic_config_s
{
  struct tsic_channel_s ch[CONFIG_TSIC_CHANNELS];
  int (*enable)(FAR struct tsic_config_s *state,uint32_t channel);
  int (*writepos)(FAR struct tsic_config_s *state,uint32_t channel);
  int32_t (*update_refcnt)(FAR struct tsic_config_s *conf);
};

struct tsic_msg_s
{
  int32_t   time_s;
  int32_t   time_ns;
  int32_t   channel;
  float     value;
};


/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/* IOCTL Commands ***********************************************************/

/* Enable TSIC readout
 *
 * Arg: none
 */

//#define SNIOC_TSIC_ENABLE           _SNIOC(0x0001)

/* Flush sensor buffer
 *
 * Arg: none
 */

#define SNIOC_TSIC_FLUSH            _SNIOC(0x0002)

/* Update timestamp for reference count, run at least with 10Hz
 *
 * Arg: none
 */

#define SNIOC_TSIC_UPDATE           _SNIOC(0x0003)


/****************************************************************************
 * Name: tsic_instantiate
 *
 * Description:
 *   Instantiate an optional register the TSIC character device as 'devpath'
 *
 * Input Parameters:
 *   devpath - The full path to the driver to register. E.g., "/dev/tsic"
 *   config  - Persistent board configuration data
 *             TSIC
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/
int tsic_instantiate(FAR const char *devpath, FAR struct tsic_config_s *config);

#undef EXTERN
#ifdef __cplusplus
}
#endif

#endif /* CONFIG_SENSORS_TSIC */
#endif /* __INCLUDE_NUTTX_TSIC_H */

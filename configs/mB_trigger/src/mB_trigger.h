/************************************************************************************
 * configs/mB_trigger/src/mB_trigger.h
 *
 ************************************************************************************/

#ifndef __CONFIGS_TRIGGER_SRC_TRIGGER_H
#define __CONFIGS_TRIGGER_SRC_TRIGGER_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>
#include <nuttx/board.h>
#include <nuttx/compiler.h>
#include <stdint.h>
#include "stm32_gpio.h"

#ifdef CONFIG_SENSORS_TRIGGER
#include <nuttx/sensors/trigger.h>
#endif


/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/* Configuration ********************************************************************/

#define HAVE_PROGMEM_CHARDEV  1

/* mB_trig GPIO Pin Definitions **************************************************/

/* LED ***************************************************************************/
#define GPIO_LED1            (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_100MHz|\
                              GPIO_OUTPUT_CLEAR|GPIO_PORTA|GPIO_PIN9)           /* PA9: green */

#define GPIO_LED2            (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_100MHz|\
                              GPIO_OUTPUT_CLEAR|GPIO_PORTA|GPIO_PIN8)           /* PA8: red */

/***********************************************************************************
 * Trigger input buffer enable GPIO Pin Definitions
 * 5V tolerant, 3V3 level mirrored on output
 ***********************************************************************************/

#define GPIO_TRIG1_INPUT_BUFFER_ENABLE (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|GPIO_OUTPUT_CLEAR|GPIO_PORTE|GPIO_PIN8)
#define GPIO_TRIG2_INPUT_BUFFER_ENABLE (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|GPIO_OUTPUT_CLEAR|GPIO_PORTE|GPIO_PIN9)
#define GPIO_TRIG3_INPUT_BUFFER_ENABLE (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|GPIO_OUTPUT_CLEAR|GPIO_PORTE|GPIO_PIN10)
#define GPIO_TRIG4_INPUT_BUFFER_ENABLE (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|GPIO_OUTPUT_CLEAR|GPIO_PORTE|GPIO_PIN11)
#define GPIO_TRIG5_INPUT_BUFFER_ENABLE (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|GPIO_OUTPUT_CLEAR|GPIO_PORTE|GPIO_PIN12)
#define GPIO_TRIG6_INPUT_BUFFER_ENABLE (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|GPIO_OUTPUT_CLEAR|GPIO_PORTE|GPIO_PIN13)



/* messBUS ***********************************************************************/

/************************************************************************************
 * Public data
 ************************************************************************************/

#ifndef __ASSEMBLY__

/************************************************************************************
 * Public Functions
 ************************************************************************************/

 /************************************************************************************
  * Name: stm32_adxl357_initialize
  *
  * Description:
  *   Called to configure and initialize TRIGGER driver.
  *
  ************************************************************************************/
#if defined(CONFIG_SENSORS_TRIGGER)
void stm32_trigger_initialize(void);
#endif

/****************************************************************************
 * Name: stm32_hptc_setup
 *
 * Description:
 *   Initialize HPTC and register the HPTC device.
 *
 ****************************************************************************/

#ifdef CONFIG_HPTC
int stm32_hptc_setup(void);
#endif

#endif /* __ASSEMBLY__ */
#endif /* __CONFIGS_TRIGGER_SRC_TRIGGER_H */

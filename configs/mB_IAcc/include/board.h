/************************************************************************************
 * configs/mB_IAcc/include/board.h
 *
 ************************************************************************************/

#ifndef __CONFIG_mB_IAcc_INCLUDE_BOARD_H
#define __CONFIG_mB_IAcc_INCLUDE_BOARD_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>


#ifndef __ASSEMBLY__
#  include <stdint.h>
#  include <stdbool.h>
#endif

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/* If defined, HSI is used instead of HSE */
//# define STM32_BOARD_USEHSI      1

#ifndef STM32_BOARD_USEHSI
//# define MB_IACC_HAVE_VCX0
# define STM32_BOARD_USEHSE      1
#endif

/* Clocking *************************************************************************/
/* The STM32F7 mB_IAcc Client board features a 4.096 MHz VCXO.
 *
 * This is the canonical configuration:
 *   System Clock source           : PLL (HSE)
 *   SYSCLK(Hz)                    : 192000000    Determined by PLL configuration
 *   HCLK(Hz)                      : 192000000    (STM32_RCC_CFGR_HPRE)
 *   AHB Prescaler                 : 1            (STM32_RCC_CFGR_HPRE)
 *   APB1 Prescaler                : 4            (STM32_RCC_CFGR_PPRE1)
 *   APB2 Prescaler                : 2            (STM32_RCC_CFGR_PPRE2)
 *   HSE Frequency(Hz)             : 4096000      (STM32_BOARD_XTAL)
 *   PLLM                          : 4            (STM32_PLLCFG_PLLM)
 *   PLLN                          : 375          (STM32_PLLCFG_PLLN)
 *   PLLP                          : 2            (STM32_PLLCFG_PLLP)
 *   PLLQ                          : 8            (STM32_PLLCFG_PLLQ)
 *   Main regulator output voltage : Scale1 mode  Needed for high speed SYSCLK
 *   Flash Latency(WS)             : 5
 *   Prefetch Buffer               : OFF
 *   Instruction cache             : ON
 *   Data cache                    : ON
 *   Require 48MHz for USB OTG FS, : Enabled
 *   SDIO and RNG clock
 
/* HSI - 16 MHz RC factory-trimmed
 * LSI - 32 KHz RC
 * HSE - On-board oscillator frequency is 4.096MHz
 * LSE - N/A
 */

#ifdef STM32_BOARD_USEHSE
# ifdef MB_IACC_HAVE_VCX0
#  define STM32_BOARD_XTAL        4096000ul
# else
#  define STM32_BOARD_XTAL        10000000ul
# endif
#endif

#define STM32_HSI_FREQUENCY     16000000ul
#define STM32_LSI_FREQUENCY     32000
#ifdef STM32_BOARD_USEHSE
# define STM32_HSE_FREQUENCY    STM32_BOARD_XTAL
#endif
#define STM32_LSE_FREQUENCY     32768


/* Defines HSE/LSE clock source as 'external clock' */

#ifdef MB_IACC_HAVE_VCX0
# define STM32_HSEBYP_ENABLE
#endif

/* Main PLL Configuration
 *
 * PLL source is HSE = 4,096,000  // if not, you need to replace HSE by HSI
 *
 * PLL_VCO = (STM32_HSE_FREQUENCY / PLLM) * PLLN
 * Subject to:
 *
 *     2 <= PLLM <= 63
 *   192 <= PLLN <= 432
 *   192 MHz <= PLL_VCO <= 432MHz
 *
 * SYSCLK  = PLL_VCO / PLLP
 * Subject to
 *
 *   PLLP = {2, 4, 6, 8}
 *   SYSCLK <= 216 MHz
 *
 * USB OTG FS, SDMMC and RNG Clock = PLL_VCO / PLLQ
 * Subject to
 *   The USB OTG FS requires a 48 MHz clock to work correctly. The SDMMC
 *   and the random number generator need a frequency lower than or equal
 *   to 48 MHz to work correctly.
 *
 * 2 <= PLLQ <= 15
 */

/* CASE 1: PLL source is HSE (VCXO)
 * PLL_VCO = (STM32_HSE_FREQUENCY / PLLM) * PLLN
 *         = (4,096,000 / 4) * 375
 *         = 384,000,000
 * SYSCLK  = PLL_VCO / PLLP
 *         = 384,000,000 / 2 = 192,000,000
 * USB OTG FS, SDIO and RNG Clock
 *         =  PLL_VCO / PLLQ
 *         = 384,000,000 / 8
 *         = 48,000,000
 *
 * CASE 2: PLL source is HSE (No VCXO)
 * PLL_VCO = (STM32_HSE_FREQUENCY / PLLM) * PLLN
 *         = (10,000,000 / 5) * 216
 *         = 432,000,000
 * SYSCLK  = PLL_VCO / PLLP
 *         = 432,000,000 / 2 = 216,000,000
 * USB OTG FS, SDIO and RNG Clock
 *         =  PLL_VCO / PLLQ
 *         = 432,000,000 / 9
 *         = 48,000,000
 *
 * CASE 3: PLL source is HSI
 * PLL_VCO = (STM32_HSE_FREQUENCY / PLLM) * PLLN
 *         = (16,000,000 / 8) * 216
 *         = 512,000,000
 * SYSCLK  = PLL_VCO / PLLP
 *         = 512,000,000 / 2 = 216,000,000
 * USB OTG FS, SDIO and RNG Clock
 *         =  PLL_VCO / PLLQ
 *         = 512,000,000 / 9
 *         = 48,000,000
 */

#if defined(STM32_BOARD_USEHSE)
# ifdef MB_IACC_HAVE_VCX0
#  define STM32_PLLCFG_PLLM       RCC_PLLCFG_PLLM(4)
#  define STM32_PLLCFG_PLLN       RCC_PLLCFG_PLLN(375)
#  define STM32_PLLCFG_PLLP       RCC_PLLCFG_PLLP_2
#  define STM32_PLLCFG_PLLQ       RCC_PLLCFG_PLLQ(8)
# else // external resonator
#  define STM32_PLLCFG_PLLM       RCC_PLLCFG_PLLM(5)
#  define STM32_PLLCFG_PLLN       RCC_PLLCFG_PLLN(216)
#  define STM32_PLLCFG_PLLP       RCC_PLLCFG_PLLP_2
#  define STM32_PLLCFG_PLLQ       RCC_PLLCFG_PLLQ(9)
# endif
#elif defined(STM32_BOARD_USEHSI)
# define STM32_PLLCFG_PLLM       RCC_PLLCFG_PLLM(8)
# define STM32_PLLCFG_PLLN       RCC_PLLCFG_PLLN(216)
# define STM32_PLLCFG_PLLP       RCC_PLLCFG_PLLP_2
# define STM32_PLLCFG_PLLQ       RCC_PLLCFG_PLLQ(9)
#endif

#if defined(STM32_BOARD_USEHSE)
# ifdef MB_IACC_HAVE_VCX0
#  define STM32_VCO_FREQUENCY     ((STM32_HSE_FREQUENCY / 4) * 375)
#  define STM32_SYSCLK_FREQUENCY  (STM32_VCO_FREQUENCY / 2)
#  define STM32_OTGFS_FREQUENCY   (STM32_VCO_FREQUENCY / 8)
# else // external resonator
#  define STM32_VCO_FREQUENCY     ((STM32_HSE_FREQUENCY / 5) * 216)
#  define STM32_SYSCLK_FREQUENCY  (STM32_VCO_FREQUENCY / 2)
#  define STM32_OTGFS_FREQUENCY   (STM32_VCO_FREQUENCY / 9)
# endif
#elif defined(STM32_BOARD_USEHSI)
# define STM32_VCO_FREQUENCY     ((STM32_HSI_FREQUENCY / 8) * 216)
# define STM32_SYSCLK_FREQUENCY  (STM32_VCO_FREQUENCY / 2)
# define STM32_OTGFS_FREQUENCY   (STM32_VCO_FREQUENCY / 9
#endif

/* Configure Dedicated Clock Configuration Register 2 */

#define STM32_RCC_DCKCFGR2_USART1SRC  RCC_DCKCFGR2_USART1SEL_SYSCLK
#define STM32_RCC_DCKCFGR2_USART2SRC  RCC_DCKCFGR2_USART2SEL_APB
#define STM32_RCC_DCKCFGR2_UART4SRC   RCC_DCKCFGR2_UART4SEL_APB
#define STM32_RCC_DCKCFGR2_UART5SRC   RCC_DCKCFGR2_UART5SEL_APB
#define STM32_RCC_DCKCFGR2_USART6SRC  RCC_DCKCFGR2_USART6SEL_APB
#define STM32_RCC_DCKCFGR2_UART7SRC   RCC_DCKCFGR2_UART7SEL_APB
#define STM32_RCC_DCKCFGR2_UART8SRC   RCC_DCKCFGR2_UART8SEL_APB
#define STM32_RCC_DCKCFGR2_I2C1SRC    RCC_DCKCFGR2_I2C1SEL_HSI
#define STM32_RCC_DCKCFGR2_I2C2SRC    RCC_DCKCFGR2_I2C2SEL_HSI
#define STM32_RCC_DCKCFGR2_I2C3SRC    RCC_DCKCFGR2_I2C3SEL_HSI
#define STM32_RCC_DCKCFGR2_I2C4SRC    RCC_DCKCFGR2_I2C4SEL_HSI
#define STM32_RCC_DCKCFGR2_LPTIM1SRC  RCC_DCKCFGR2_LPTIM1SEL_APB
#define STM32_RCC_DCKCFGR2_CECSRC     RCC_DCKCFGR2_CECSEL_HSI
#define STM32_RCC_DCKCFGR2_CK48MSRC   RCC_DCKCFGR2_CK48MSEL_PLL
#define STM32_RCC_DCKCFGR2_SDMMCSRC   RCC_DCKCFGR2_SDMMCSEL_48MHZ
#define STM32_RCC_DCKCFGR2_SDMMC2SRC  RCC_DCKCFGR2_SDMMC2SEL_48MHZ

/* Several prescalers allow the configuration of the two AHB buses, the
 * high-speed APB (APB2) and the low-speed APB (APB1) domains. The maximum
 * frequency of the two AHB buses is 216 MHz while the maximum frequency of
 * the high-speed APB domains is 108 MHz. The maximum allowed frequency of
 * the low-speed APB domain is 54 MHz.
 */

/* AHB clock (HCLK) is SYSCLK (192 MHz) */

#define STM32_RCC_CFGR_HPRE     RCC_CFGR_HPRE_SYSCLK  /* HCLK  = SYSCLK / 1 */
#define STM32_HCLK_FREQUENCY    STM32_SYSCLK_FREQUENCY
#define STM32_BOARD_HCLK        STM32_HCLK_FREQUENCY  /* same as above, to satisfy compiler */

/* APB1 clock (PCLK1) is HCLK/4 (48 MHz) */

#define STM32_RCC_CFGR_PPRE1    RCC_CFGR_PPRE1_HCLKd4     /* PCLK1 = HCLK / 4 */
#define STM32_PCLK1_FREQUENCY   (STM32_HCLK_FREQUENCY/4)

/* Timers driven from APB1 will be twice PCLK1 */

#define STM32_APB1_TIM2_CLKIN   (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM3_CLKIN   (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM4_CLKIN   (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM5_CLKIN   (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM6_CLKIN   (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM7_CLKIN   (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM12_CLKIN  (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM13_CLKIN  (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM14_CLKIN  (2*STM32_PCLK1_FREQUENCY)

/* APB2 clock (PCLK2) is HCLK/2 (96 MHz) */

#define STM32_RCC_CFGR_PPRE2    RCC_CFGR_PPRE2_HCLKd2     /* PCLK2 = HCLK / 2 */
#define STM32_PCLK2_FREQUENCY   (STM32_HCLK_FREQUENCY/2)

/* Timers driven from APB2 will be twice PCLK2 */

#define STM32_APB2_TIM1_CLKIN   (2*STM32_PCLK2_FREQUENCY)
#define STM32_APB2_TIM8_CLKIN   (2*STM32_PCLK2_FREQUENCY)
#define STM32_APB2_TIM9_CLKIN   (2*STM32_PCLK2_FREQUENCY)
#define STM32_APB2_TIM10_CLKIN  (2*STM32_PCLK2_FREQUENCY)
#define STM32_APB2_TIM11_CLKIN  (2*STM32_PCLK2_FREQUENCY)

/* Timer Frequencies, if APBx is set to 1, frequency is same to APBx
 * otherwise frequency is 2xAPBx.
 * Note: TIM1,8 are on APB2, others on APB1
 */

#define BOARD_TIM1_FREQUENCY    STM32_HCLK_FREQUENCY
#define BOARD_TIM2_FREQUENCY    (STM32_HCLK_FREQUENCY / 2)
#define BOARD_TIM3_FREQUENCY    (STM32_HCLK_FREQUENCY / 2)
#define BOARD_TIM4_FREQUENCY    (STM32_HCLK_FREQUENCY / 2)
#define BOARD_TIM5_FREQUENCY    (STM32_HCLK_FREQUENCY / 2)
#define BOARD_TIM6_FREQUENCY    (STM32_HCLK_FREQUENCY / 2)
#define BOARD_TIM7_FREQUENCY    (STM32_HCLK_FREQUENCY / 2)
#define BOARD_TIM8_FREQUENCY    STM32_HCLK_FREQUENCY

#define BOARD_TIM9_FREQUENCY    STM32_HCLK_FREQUENCY
/* FLASH wait states
 *
 *  --------- ---------- -----------
 *  VDD       MAX SYSCLK WAIT STATES
 *  --------- ---------- -----------
 *  1.7-2.1 V   180 MHz    8
 *  2.1-2.4 V   216 MHz    9
 *  2.4-2.7 V   216 MHz    8
 *  2.7-3.6 V   216 MHz    7
 *  --------- ---------- -----------
 */

#define BOARD_FLASH_WAITSTATES 7

/* LED definitions ******************************************************************/
/* 
 */

/* LED index values for use with board_userled() */

#define BOARD_LED1        0
#define BOARD_LED2        1
#define BOARD_NLEDS       2

#define BOARD_LED_GREEN   BOARD_LED1
#define BOARD_LED_RED     BOARD_LED2

/* LED bits for use with board_userled_all() */

#define BOARD_LED1_BIT    (1 << BOARD_LED1)
#define BOARD_LED2_BIT    (1 << BOARD_LED2)


/* If CONFIG_ARCH_LEDS is defined, the usage by the board port is defined in
 * include/board.h and src/stm32_leds.c. The LEDs are used to encode OS-related
 * events as follows:
 *Description	Resource	Path	Location	Type
#error ""	nuttx_STM32F7		line 256	C/C++ Problem
 *
 *
 *   SYMBOL                     Meaning                      LED state
 *                                                        Red   Green Blue
 *   ----------------------  --------------------------  ------ ------ ----*/

#define LED_STARTED        0 /* NuttX has been started   OFF    OFF   OFF  */
#define LED_HEAPALLOCATE   1 /* Heap has been allocated  OFF    OFF   ON   */
#define LED_IRQSENABLED    2 /* Interrupts enabled       OFF    ON    OFF  */
#define LED_STACKCREATED   3 /* Idle stack created       OFF    ON    ON   */
#define LED_INIRQ          4 /* In an interrupt          N/C    N/C   GLOW */
#define LED_SIGNAL         5 /* In a signal handler      N/C    GLOW  N/C  */
#define LED_ASSERTION      6 /* An assertion failed      GLOW   N/C   GLOW */
#define LED_PANIC          7 /* The system has crashed   Blink  OFF   N/C  */
#define LED_IDLE           8 /* MCU is is sleep mode     ON     OFF   OFF  */

/* Thus if the Green LED is statically on, NuttX has successfully booted and
 * is, apparently, running normally.  If the Red LED is flashing at
 * approximately 2Hz, then a fatal error has been detected and the system
 * has halted.
 */
/* Alternate function pin selections ************************************************/

/* messBUS interface */

#define STM32_mB_CH1_USART_BASE   STM32_USART1_BASE
#define GPIO_mB_CH1_TX		GPIO_USART1_TX_2
#define GPIO_mB_CH1_RX		GPIO_USART1_RX_2

#define mB_CH1_RX_DMAMAP	DMAMAP_USART1_RX_1
#define mB_CH1_TX_DMAMAP	DMAMAP_USART1_TX

#define GPIO_mB_CH1_DE      	(GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
								GPIO_OUTPUT_CLEAR|GPIO_PORTB|GPIO_PIN8)

#define GPIO_mB_CH1_RE       	(GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
								GPIO_OUTPUT_SET|GPIO_PORTB|GPIO_PIN5)

/* Timer 2 */

//#define GPIO_TIM2_CH1IN GPIO_TIM2_CH1IN_1 /*PA0*/
#define GPIO_TIM2_CH1IN (GPIO_ALT|GPIO_AF1|GPIO_SPEED_50MHz|GPIO_PULLUP|GPIO_PORTA|GPIO_PIN0)
//#define GPIO_TIM2_CH2IN GPIO_TIM2_CH2IN_1 /*PA1*/
#define GPIO_TIM2_CH2IN (GPIO_ALT|GPIO_AF1|GPIO_SPEED_50MHz|GPIO_PULLUP|GPIO_PORTA|GPIO_PIN1)
#define GPIO_TIM2_CH3IN GPIO_TIM2_CH3IN_1 /*PA2*/
//#define GPIO_TIM2_CH4IN GPIO_TIM2_CH4IN_1 /*PA3*/

#define GPIO_TIM2_CH1OUT GPIO_TIM2_CH1OUT_1 /*PA0*/
#define GPIO_TIM2_CH2OUT GPIO_TIM2_CH2OUT_1 /*PA1*/
#define GPIO_TIM2_CH3OUT GPIO_TIM2_CH3OUT_1 /*PA2*/
//#define GPIO_TIM2_CH4OUT GPIO_TIM2_CH4OUT_1 /*PA3*/

/* Timer 5*/

#define GPIO_TIM5_CH4IN GPIO_TIM5_CH4IN_1 /*PA3 IMU data ready*/

/* SPI1
 *
 *  PA6   SPI1_MISO CN12-13
 *  PA7   SPI1_MOSI CN12-15
 *  PA5   SPI1_SCK  CN12-11
 *  PA4   SPI1_CS
 *
 */

#define GPIO_SPI1_MISO   GPIO_SPI1_MISO_1
#define GPIO_SPI1_MOSI   GPIO_SPI1_MOSI_1
#define GPIO_SPI1_SCK    GPIO_SPI1_SCK_1

/* UART7:
 *
 *  PB3   UART7_RX
 *  PA15  UART7_TX
 */
 # define GPIO_UART7_RX GPIO_UART7_RX_4
 # define GPIO_UART7_TX GPIO_UART7_TX_3


/************************************************************************************
 * Public Data
 ************************************************************************************/

#ifndef __ASSEMBLY__

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/************************************************************************************
 * Public Function Prototypes
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_boardinitialize
 *
 * Description:
 *   All STM32 architectures must provide the following entry point.  This entry point
 *   is called early in the initialization -- after all memory has been configured
 *   and mapped but before any devices have been initialized.
 *
 ************************************************************************************/

void stm32_boardinitialize(void);

#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif  /* __ASSEMBLY__ */
#endif  /* __CONFIG_mB_IAcc_INCLUDE_BOARD_H */

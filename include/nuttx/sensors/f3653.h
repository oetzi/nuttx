/****************************************************************************
 * include/nuttx/sensors/f3653.h
 *
 * Upper half driver for Kügler Sendix F3653 Singleturn absolute angle
 * sensor (17 bit).
 *
 * Product number: 8.F3653.4581.G712
 * Interface     : SSI
 *
 ****************************************************************************/

#ifndef NUTTX_INCLUDE_SSI_ANGLE_SENSOR_H
#define NUTTX_INCLUDE_SSI_ANGLE_SENSOR_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#ifdef CONFIG_SENSORS_SSI_ANGLE_F3653
#include <nuttx/fs/ioctl.h>
#include <nuttx/spi/spi.h>
#include <time.h>
#include <stdio.h>


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#ifdef CONFIG_STM32F7_SPI1
# define F3653_SPI_BUS                 1
# define F3653_SPI_FREQ                125000          //125 kHz
# define F3653_SPI_MODE                SPIDEV_MODE2    /* CPOL: 1, CPHA: 0 */
# define F3653_SPI_NBITS               8
#endif

#define F3653_DEV_PATH                 "/dev/f3653"
#define F3653_QENCODER_TIMER           1
#define F3653_QENCODER_PATH            "/dev/qencoder"
#define F3653_QENCODER_RESOLUTION      2048
#define F3653_POSITION_RESOLUTION      131072

/* IOCTL Commands ***********************************************************/
/*
 *
 */


/****************************************************************************
 * Public Types
 ****************************************************************************/

/* Enum indication the direction of rotation of F3653 sensor */

enum f3653_rotating_dir_e
{
    anticlockwise = -1,
    undef = 0,
    clockwise = 1
};
typedef enum f3653_rotating_dir_e f3653_rotating_dir_t;


/*
 * This structure represents all data that are measured by f3653 hardware and lower driver.
 * Used as return value for character driver read() function.
 */

struct f3653_sample_s
{
    struct timespec            timeStamp;
    float64                    angle;         /* postion in 0-360 deg range */
    float64                    velocity;      /* velocity in deg/s     */
};
typedef struct f3653_sample_s f3653_sample_t;




/*************************************
 * Helper functions
 *************************************/

/* This structure defines all of the operations provided by the architecture
 * specific logic (lower half driver).
 * All fields must be provided with non-NULL function pointers
 * by the caller of f3653_register().
 */
struct f3653_ops_s
{
    /* Setup the sensor driver. */
    CODE int (*setup)(void);

    /* Start the sensor driver. */
    CODE int (*start)(void);

    /* Stop the sensor driver. */
    CODE int (*stop)(void);

    /* Shutdwon the sensor driver. */
    CODE
        int (*shutdown)(void);

    /* Read the timestamp. */
    CODE int (*getTimeStamp)(FAR struct timespec *time);

    /* Read sensor rotating direction */
    CODE f3653_rotating_dir_t (*getRotatingDir)(void);

    /* Read sensor rotating velocity */
    CODE float64 (*getRotatingVel)(void);

    /* Calculate difference between to timestamps */
    CODE int32_t (*getTimeDiff)(struct timespec *start, struct timespec *stop);
};


/* This is the device structure used by the driver.  The caller of
 * f3653_register() must allocate and initialize this structure.
 */
struct f3653_dev_s
{
    /* State data */

    bool open;
    bool running;

    /* Arch-specific operations as driver interface */

    FAR const struct f3653_ops_s *ops;

    /* SPI dev for SSI sensor communication */

    struct spi_dev_s *ssi_dev;

    /* File descriptor to the QEncoder driver */

    FILE* fd_qencoder;

    /* Storage for the last two samples */

    f3653_sample_t last_sample;
    f3653_sample_t current_sample;
};

typedef struct f3653_dev_s f3653_dev_t;




/****************************************************************************
 * Public Data
 ****************************************************************************/

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

/************************************************************************************
 * Name: f3653_register
 *
 * Description:
 *   Register the sensor driver in the VFS.
 *
 ************************************************************************************/

int f3653_register(FAR const char *path, FAR f3653_dev_t *dev);

/****************************************************************************
 * Name: f3653_initialize
 *
 * Description:
 *   Perform initialization and registration of the driver.
 *	 This method should be called from board-specific stm32_bringup.c
 *
 ****************************************************************************/

void f3653_initialize(void);

#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* CONFIG_SENSORS_SSI_ANGLE_F3653 */

#endif /* NUTTX_INCLUDE_SSI_ANGLE_SENSOR_H */

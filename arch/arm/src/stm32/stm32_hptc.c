/****************************************************************************
 * arch/arm/src/stm32/stm32_hptc.c
 *
 *   Copyright (C) 2018 Stefan Nowak. All rights reserved.
 *   Authors: Stefan Nowak <stefan.nowak@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/arch.h>
#include "clock/clock.h" //systimer USEC_PER_TICK

#include "nvic.h"  //NVIC register NVIC_SYSTICK_CTRL

#include "stm32_dma.h"

#include <nuttx/irq.h>
#include <semaphore.h>

#include <nuttx/timers/hptc.h>
#include <arch/board/board.h>

#include "up_internal.h"
#include "ram_vectors.h"
#include "up_arch.h"


#include "chip.h"
#include "stm32_hptc.h"
#include "stm32.h"


#ifdef VCXO_DAC_SPI_BUS
    #include <nuttx/spi/spi.h>
    #include "stm32_spi.h"
#endif

#include <nuttx/messBUS/messBUS_led.h>

/* This module then only compiles if there is at least one enabled timer
 * intended for use with the HPTC upper half driver.
 */

#include <string.h>

#include <nuttx/timers/hptc.h>
#include <nuttx/timers/hptc_io.h>

#if defined(CONFIG_HPTC) && \
    (defined(CONFIG_STM32_HPTC_TICKTIMER_TIM1) || \
	 defined(CONFIG_STM32_HPTC_TICKTIMER_TIM2) || \
	 defined(CONFIG_STM32_HPTC_TICKTIMER_TIM5) )

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#if VCXO_DAC_SPI_BUS == 1
#define VCXO_DAC_SPI_BASE STM32_SPI1_BASE
#elif VCXO_DAC_SPI_BUS == 2
#define VCXO_DAC_SPI_BASE STM32_SPI2_BASE
#elif VCXO_DAC_SPI_BUS == 3
#define VCXO_DAC_SPI_BASE STM32_SPI3_BASE
#elif VCXO_DAC_SPI_BUS == 4
#define VCXO_DAC_SPI_BASE STM32_SPI4_BASE
#elif VCXO_DAC_SPI_BUS == 5
#define VCXO_DAC_SPI_BASE STM32_SPI5_BASE
#elif VCXO_DAC_SPI_BUS == 6
#define VCXO_DAC_SPI_BASE STM32_SPI6_BASE
#endif

#define STM32_TIM1_HPTC_RES   16
#if defined(CONFIG_STM32_STM32L15XX) || defined(CONFIG_STM32_STM32F10XX)
#  define STM32_TIM2_HPTC_RES 16
#else
#  define STM32_TIM2_HPTC_RES 32
#endif
#if defined(CONFIG_STM32_STM32L20XX) || defined(CONFIG_STM32_STM32F40XX)
#  define STM32_TIM3_HPTC_RES 32
#  define STM32_TIM4_HPTC_RES 32
#else
#  define STM32_TIM3_HPTC_RES 16
#  define STM32_TIM4_HPTC_RES 16
#endif
#if defined(CONFIG_STM32_STM32F10XX) || defined(CONFIG_STM32_STM32F30XX)
#  define STM32_TIM5_HPTC_RES 16
#else
#  define STM32_TIM5_HPTC_RES 32
#endif
#define STM32_TIM8_HPTC_RES   16
#define STM32_TIM9_HPTC_RES   16
#define STM32_TIM10_HPTC_RES  16
#define STM32_TIM11_HPTC_RES  16
#define STM32_TIM12_HPTC_RES  16
#define STM32_TIM13_HPTC_RES  16
#define STM32_TIM14_HPTC_RES  16



#  ifdef CONFIG_STM32_HPTC_TICKTIMER_TIM1

//possible initial sync master for TIM1
//	"ITR0: TIM5_TRGO"
//	"ITR1: TIM2_TRGO"
//	"ITR2: TIM3_TRGO"
//	"ITR3: TIM4_TRGO"
#   define HPTC_TICKTIMER_ISSLAVE  0
#   define HPTC_TICKTIMER_TYPE 1
#   define HPTC_TICKTIMER_OC_MASK 0x1E00

#   define HPTC_TICK_TIMER_CLKIN  STM32_APB2_TIM1_CLKIN
#   define HPTC_TICK_IRQ          STM32_IRQ_TIM1
#   define HPTC_TICKTIMER_BASE    STM32_TIM1_BASE
#   define HPTC_TICKTIMER_CNT     STM32_TIM1_CNT
#   define HPTC_TICKTIMER_SR      STM32_TIM1_SR
#   define HPTC_TICKTIMER_DIER    STM32_TIM1_DIER
#   define HPTC_TICKTIMER_CCMR1   STM32_TIM1_CCMR1
#   define HPTC_TICKTIMER_CCMR2   STM32_TIM1_CCMR2
#   define HPTC_TICKTIMER_CCER	  STM32_TIM1_CCER
#   define HPTC_TICKTIMER_RES     STM32_TIM1_HPTC_RES
#   define HPTC_TICKTIMER_CHANNELS  4

#    if defined(CONFIG_STM32_HPTC_TICKTIMER_TICKCHANNEL1)
#   define HPTC_TICKTIMER_CCR      STM32_TIM1_CCR1
#   define HPTC_TICKTIMER_SR_CCIF ATIM_SR_CC1IF
#   define HPTC_TICKTIMER_DIER_CCIE ATIM_DIER_CC1IE
#   define HPTC_TICKTIMER_CHANNEL_IDX   0

#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_TICKCHANNEL2)
#   define HPTC_TICKTIMER_CCR      STM32_TIM1_CCR2
#   define HPTC_TICKTIMER_SR_CCIF ATIM_SR_CC2IF
#   define HPTC_TICKTIMER_DIER_CCIE ATIM_DIER_CC2IE
#   define HPTC_TICKTIMER_CHANNEL_IDX   1

#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_TICKCHANNEL3)
#   define HPTC_TICKTIMER_CCR      STM32_TIM1_CCR3
#   define HPTC_TICKTIMER_SR_CCIF ATIM_SR_CC3IF
#   define HPTC_TICKTIMER_DIER_CCIE ATIM_DIER_CC3IE
#   define HPTC_TICKTIMER_CHANNEL_IDX   2

#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_TICKCHANNEL4)
#   define HPTC_TICKTIMER_CCR      STM32_TIM2_CCR4
#   define HPTC_TICKTIMER_SR_CCIF GTIM_SR_CC4IF
#   define HPTC_TICKTIMER_DIER_CCIE GTIM_DIER_CC4IE
#   define HPTC_TICKTIMER_CHANNEL_IDX   3
#    endif

//Channel 1 GPIO
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_DIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH1 GPIO_TIM1_CH1IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_INDIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH1 GPIO_TIM1_CH2IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_CH1_OC_PIN)
#    define GPIO_HPTC_TICKTIMER_CH1 GPIO_TIM1_CH1OUT
#    endif
//Channel 2 GPIO
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_DIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH2 GPIO_TIM1_CH2IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_INDIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH2 GPIO_TIM1_CH1IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_CH2_OC_PIN)
#    define GPIO_HPTC_TICKTIMER_CH2 GPIO_TIM1_CH2OUT
#    endif
//Channel 3 GPIO
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_DIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH3 GPIO_TIM1_CH3IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_INDIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH3 GPIO_TIM1_CH4IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_CH3_OC_PIN)
#    define GPIO_HPTC_TICKTIMER_CH3 GPIO_TIM1_CH3OUT
#    endif
//Channel 4 GPIO
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_DIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH4 GPIO_TIM1_CH4IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_INDIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH4 GPIO_TIM1_CH3IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_CH4_OC_PIN)
#    define GPIO_HPTC_TICKTIMER_CH4 GPIO_TIM1_CH4OUT
#    endif

#  endif


#  ifdef CONFIG_STM32_HPTC_TICKTIMER_TIM2

//possible initial sync master for TIM2
//	"ITR0: TIM1_TRGO"
//	"ITR1: TIM8_TRGO"
//	"ITR2: TIM3_TRGO"
//	"ITR3: TIM4_TRGO"
#   define HPTC_TICKTIMER_ISSLAVE  0
#   define HPTC_TICKTIMER_TYPE 2
#   define HPTC_TICKTIMER_OC_MASK 0x1E00

#   define HPTC_TICK_TIMER_CLKIN    STM32_APB1_TIM2_CLKIN
#   define HPTC_TICK_IRQ            STM32_IRQ_TIM2
#   define HPTC_TICKTIMER_BASE      STM32_TIM2_BASE
#   define HPTC_TICKTIMER_CNT       STM32_TIM2_CNT
#   define HPTC_TICKTIMER_SR        STM32_TIM2_SR
#   define HPTC_TICKTIMER_DIER      STM32_TIM2_DIER
#   define HPTC_TICKTIMER_CCMR1     STM32_TIM2_CCMR1
#   define HPTC_TICKTIMER_CCMR2     STM32_TIM2_CCMR2
#   define HPTC_TICKTIMER_CCER		STM32_TIM2_CCER
#   define HPTC_TICKTIMER_RES       STM32_TIM2_HPTC_RES
#   define HPTC_TICKTIMER_CHANNELS  4


#    if defined(CONFIG_STM32_HPTC_TICKTIMER_TICKCHANNEL1)
#   define HPTC_TICKTIMER_CCR       STM32_TIM2_CCR1
#   define HPTC_TICKTIMER_SR_CCIF   GTIM_SR_CC1IF
#   define HPTC_TICKTIMER_DIER_CCIE GTIM_DIER_CC1IE
#   define HPTC_TICKTIMER_CHANNEL_IDX   0

#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_TICKCHANNEL2)
#   define HPTC_TICKTIMER_CCR       STM32_TIM2_CCR2
#   define HPTC_TICKTIMER_SR_CCIF   GTIM_SR_CC2IF
#   define HPTC_TICKTIMER_DIER_CCIE GTIM_DIER_CC2IE
#   define HPTC_TICKTIMER_CHANNEL_IDX   1

#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_TICKCHANNEL3)
#   define HPTC_TICKTIMER_CCR       STM32_TIM2_CCR3
#   define HPTC_TICKTIMER_SR_CCIF   GTIM_SR_CC3IF
#   define HPTC_TICKTIMER_DIER_CCIE GTIM_DIER_CC3IE
#   define HPTC_TICKTIMER_CHANNEL_IDX   2

#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_TICKCHANNEL4)
#   define HPTC_TICKTIMER_CCR       STM32_TIM2_CCR4
#   define HPTC_TICKTIMER_SR_CCIF   GTIM_SR_CC4IF
#   define HPTC_TICKTIMER_DIER_CCIE GTIM_DIER_CC4IE
#   define HPTC_TICKTIMER_CHANNEL_IDX   3

#    endif

//TODO: add GPIO defines for all other channels

//Channel 1 GPIO
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_DIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH1 GPIO_TIM2_CH1IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_INDIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH1 GPIO_TIM2_CH2IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_CH1_OC_PIN)
#    define GPIO_HPTC_TICKTIMER_CH1 GPIO_TIM2_CH1OUT
#    endif
//Channel 2 GPIO
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_DIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH2 GPIO_TIM2_CH2IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_INDIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH2 GPIO_TIM2_CH1IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_CH2_OC_PIN)
#    define GPIO_HPTC_TICKTIMER_CH2 GPIO_TIM2_CH2OUT
#    endif
//Channel 3 GPIO
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_DIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH3 GPIO_TIM2_CH3IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_INDIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH3 GPIO_TIM2_CH4IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_CH3_OC_PIN)
#    define GPIO_HPTC_TICKTIMER_CH3 GPIO_TIM2_CH3OUT
#    endif
//Channel 4 GPIO
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_DIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH4 GPIO_TIM2_CH4IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_INDIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH4 GPIO_TIM2_CH3IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_CH4_OC_PIN)
#    define GPIO_HPTC_TICKTIMER_CH4 GPIO_TIM2_CH4OUT
#    endif

#  endif


#  ifdef CONFIG_STM32_HPTC_TICKTIMER_TIM5

//possible initial sync master for TIM5
//	"ITR0: TIM2_TRGO"
//	"ITR1: TIM3_TRGO"
//	"ITR2: TIM4_TRGO"
//	"ITR3: TIM5_TRGO"

// only combination TIM2+TIM5 implemented
#    ifdef CONFIG_STM32_HPTC_AUXTIMER_TIM2
#     define HPTC_TICKTIMER_ISSLAVE  1
#     define HPTC_TICKTIMER_TS GTIM_SMCR_ITR2
#    else
#     define HPTC_TICKTIMER_ISSLAVE  0
#    endif

#   define HPTC_TICKTIMER_TYPE 2
#   define HPTC_TICKTIMER_OC_MASK 0x1E00

#   define HPTC_TICK_TIMER_CLKIN    STM32_APB1_TIM5_CLKIN
#   define HPTC_TICK_IRQ            STM32_IRQ_TIM5
#   define HPTC_TICKTIMER_BASE      STM32_TIM5_BASE
#   define HPTC_TICKTIMER_CNT       STM32_TIM5_CNT
#   define HPTC_TICKTIMER_SR        STM32_TIM5_SR
#   define HPTC_TICKTIMER_DIER      STM32_TIM5_DIER
#   define HPTC_TICKTIMER_CCMR1     STM32_TIM5_CCMR1
#   define HPTC_TICKTIMER_CCMR2     STM32_TIM5_CCMR2
#   define HPTC_TICKTIMER_CCER		STM32_TIM5_CCER
#   define HPTC_TICKTIMER_RES       STM32_TIM5_HPTC_RES
#   define HPTC_TICKTIMER_CHANNELS  4

#    if defined(CONFIG_STM32_HPTC_TICKTIMER_TICKCHANNEL1)
#   define HPTC_TICKTIMER_CCR       STM32_TIM5_CCR1
#   define HPTC_TICKTIMER_SR_CCIF   GTIM_SR_CC1IF
#   define HPTC_TICKTIMER_DIER_CCIE GTIM_DIER_CC1IE
#   define HPTC_TICKTIMER_CHANNEL_IDX   0

#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_TICKCHANNEL2)
#   define HPTC_TICKTIMER_CCR       STM32_TIM5_CCR2
#   define HPTC_TICKTIMER_SR_CCIF   GTIM_SR_CC2IF
#   define HPTC_TICKTIMER_DIER_CCIE GTIM_DIER_CC2IE
#   define HPTC_TICKTIMER_CHANNEL_IDX   1

#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_TICKCHANNEL3)
#   define HPTC_TICKTIMER_CCR       STM32_TIM5_CCR3
#   define HPTC_TICKTIMER_SR_CCIF   GTIM_SR_CC3IF
#   define HPTC_TICKTIMER_DIER_CCIE GTIM_DIER_CC3IE
#   define HPTC_TICKTIMER_CHANNEL_IDX   2

#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_TICKCHANNEL4)
#   define HPTC_TICKTIMER_CCR       STM32_TIM5_CCR4
#   define HPTC_TICKTIMER_SR_CCIF   GTIM_SR_CC4IF
#   define HPTC_TICKTIMER_DIER_CCIE GTIM_DIER_CC4IE
#   define HPTC_TICKTIMER_CHANNEL_IDX   3

#    endif

//Channel 1 GPIO
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_DIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH1 GPIO_TIM5_CH1IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_INDIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH1 GPIO_TIM5_CH2IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_CH1_OC_PIN)
#    define GPIO_HPTC_TICKTIMER_CH1 GPIO_TIM5_CH1OUT
#    endif
//Channel 2 GPIO
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_DIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH2 GPIO_TIM5_CH2IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_INDIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH2 GPIO_TIM5_CH1IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_CH2_OC_PIN)
#    define GPIO_HPTC_TICKTIMER_CH2 GPIO_TIM5_CH2OUT
#    endif
//Channel 3 GPIO
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_DIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH3 GPIO_TIM5_CH3IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_INDIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH3 GPIO_TIM5_CH4IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_CH3_OC_PIN)
#    define GPIO_HPTC_TICKTIMER_CH3 GPIO_TIM5_CH3OUT
#    endif
//Channel 4 GPIO
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_DIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH4 GPIO_TIM5_CH4IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_INDIRECT_TI)
#    define GPIO_HPTC_TICKTIMER_CH4 GPIO_TIM5_CH3IN
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_CH4_OC_PIN)
#    define GPIO_HPTC_TICKTIMER_CH4 GPIO_TIM5_CH4OUT
#    endif

#  endif



#  ifdef CONFIG_STM32_HPTC_AUXTIMER_TIM1
#   define HPTC_AUXTIMER
#	define HPTC_AUXTIMER_ISSLAVE  0
#   define HPTC_AUXTIMER_TYPE 1
#   define HPTC_AUXTIMER_OC_MASK 0x1E00
#   define HPTC_AUX_IRQ          STM32_IRQ_TIM1
#   define HPTC_AUXTIMER_BASE    STM32_TIM1_BASE
#   define HPTC_AUXTIMER_CNT     STM32_TIM1_CNT
#   define HPTC_AUXTIMER_SR      STM32_TIM1_SR
#   define HPTC_AUXTIMER_DIER    STM32_TIM1_DIER
#   define HPTC_AUXTIMER_CCMR1   STM32_TIM1_CCMR1
#   define HPTC_AUXTIMER_CCMR2   STM32_TIM1_CCMR2
#   define HPTC_AUXTIMER_CCER	 STM32_TIM1_CCER
#   define HPTC_AUXTIMER_RES     STM32_TIM1_HPTC_RES
#   define HPTC_AUXTIMER_CHANNELS  4
#  endif


#  ifdef CONFIG_STM32_HPTC_AUXTIMER_TIM2
#   define HPTC_AUXTIMER
#	define HPTC_AUXTIMER_ISSLAVE  0
#   define HPTC_AUXTIMER_TYPE 2
#   define HPTC_AUXTIMER_OC_MASK 0x1E00
#   define HPTC_AUX_IRQ            STM32_IRQ_TIM2
#   define HPTC_AUXTIMER_BASE      STM32_TIM2_BASE
#   define HPTC_AUXTIMER_CNT       STM32_TIM2_CNT
#   define HPTC_AUXTIMER_SR        STM32_TIM2_SR
#   define HPTC_AUXTIMER_DIER      STM32_TIM2_DIER
#   define HPTC_AUXTIMER_CCMR1     STM32_TIM2_CCMR1
#   define HPTC_AUXTIMER_CCMR2     STM32_TIM2_CCMR2
#   define HPTC_AUXTIMER_CCER	   STM32_TIM2_CCER
#   define HPTC_AUXTIMER_RES       STM32_TIM2_HPTC_RES
#   define HPTC_AUXTIMER_CHANNELS  4
#  endif


#  ifdef CONFIG_STM32_HPTC_AUXTIMER_TIM5
#   define HPTC_AUXTIMER
// only combination TIM2+TIM5 implemented
#    ifdef CONFIG_STM32_HPTC_TICKTIMER_TIM2
#     define HPTC_AUXTIMER_ISSLAVE  1
#     define HPTC_AUXTIMER_TS GTIM_SMCR_ITR2
#    else
#	  define HPTC_AUXTIMER_ISSLAVE  0
#    endif

#   define HPTC_AUXTIMER_TYPE 2
#   define HPTC_AUXTIMER_OC_MASK 0x1E00
#   define HPTC_AUX_IRQ            STM32_IRQ_TIM5
#   define HPTC_AUXTIMER_BASE      STM32_TIM5_BASE
#   define HPTC_AUXTIMER_CNT       STM32_TIM5_CNT
#   define HPTC_AUXTIMER_SR        STM32_TIM5_SR
#   define HPTC_AUXTIMER_DIER      STM32_TIM5_DIER
#   define HPTC_AUXTIMER_CCMR1     STM32_TIM5_CCMR1
#   define HPTC_AUXTIMER_CCMR2     STM32_TIM5_CCMR2
#   define HPTC_AUXTIMER_CCER	   STM32_TIM5_CCER
#   define HPTC_AUXTIMER_RES       STM32_TIM5_HPTC_RES
#   define HPTC_AUXTIMER_CHANNELS  4


//TODO: add GPIO defines for all other channels

//Channel 1 GPIO
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC1_DIRECT_TI)
#    define GPIO_HPTC_AUXTIMER_CH1 GPIO_TIM5_CH1IN
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC1_INDIRECT_TI)
#    define GPIO_HPTC_AUXTIMER_CH1 GPIO_TIM5_CH2IN
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_CH1_OC_PIN)
#    define GPIO_HPTC_AUXTIMER_CH1 GPIO_TIM5_CH1OUT
#    endif
//Channel 2 GPIO
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC2_DIRECT_TI)
#    define GPIO_HPTC_AUXTIMER_CH2 GPIO_TIM5_CH2IN
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC2_INDIRECT_TI)
#    define GPIO_HPTC_AUXTIMER_CH2 GPIO_TIM5_CH1IN
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_CH2_OC_PIN)
#    define GPIO_HPTC_AUXTIMER_CH2 GPIO_TIM5_CH2OUT
#    endif
//Channel 3 GPIO
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC3_DIRECT_TI)
#    define GPIO_HPTC_AUXTIMER_CH3 GPIO_TIM5_CH3IN
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC3_INDIRECT_TI)
#    define GPIO_HPTC_AUXTIMER_CH3 GPIO_TIM5_CH4IN
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_CH3_OC_PIN)
#    define GPIO_HPTC_AUXTIMER_CH3 GPIO_TIM5_CH3OUT
#    endif
//Channel 4 GPIO
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC4_DIRECT_TI)
#    define GPIO_HPTC_AUXTIMER_CH4 GPIO_TIM5_CH4IN
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC4_INDIRECT_TI)
#    define GPIO_HPTC_AUXTIMER_CH4 GPIO_TIM5_CH3IN
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_CH4_OC_PIN)
#    define GPIO_HPTC_AUXTIMER_CH4 GPIO_TIM5_CH4OUT
#    endif

#  endif



#define HPTC_TIMER_CHANNEL_TYPE_UNUSED       0
#define HPTC_TIMER_CHANNEL_TYPE_ACTIVE       1
#define HPTC_TIMER_CHANNEL_TYPE_TICKTIMER    2
#define HPTC_TIMER_CHANNEL_TYPE_OC           4
#define HPTC_TIMER_CHANNEL_TYPE_IC           8

#define HPTC_JUMP_STATE_IDLE     0
#define HPTC_JUMP_STATE_TRIGGERD 1
#define HPTC_JUMP_STATE_SETUP    2
#define HPTC_JUMP_STATE_JUMP     3


#define HPTC_INITIAL_SYSTICK 0

/****************************************************************************
 * Private Types
 ****************************************************************************/
/* This structure provides the private representation of the "lower-half"
 * driver state structure.  This structure must be cast-compatible with the
 * hptc_lowerhalf_s structure.
 */

struct stm32_tim_phys_dev_s
{
	FAR struct stm32_tim_phys_dev_s *next;
	const uint8_t                   resolution;
	bool                            isSlave; /*master or slave to sync multiple hptc timers*/
	uint16_t						ts;
	uint32_t						minPeriod; /*minimum Counter overflow period*/
	uint32_t                        base;
	uint8_t							type;
	//	hptccb_t                          callback;   /* Current user interrupt callback */
};


struct stm32_dev_s
{
	//  FAR const struct hptc_ops_s *ops;        /* Lower half operations */
	FAR  struct stm32_tim_phys_dev_s *first;
	//TODO: linked list for devices
	//  FAR struct stm32_tim_dev_s   *tim;        /* stm32 timer driver */
	hptccb_t                        tick_cb;   /* Current user interrupt callback */
//	hptc_in_cb_t					hptc_in_cb;
	//  FAR void                     *arg;        /* Argument passed to upper half callback */
	//  const xcpt_t                  timhandler; /* Current timer interrupt handler */
	bool							started;
	//  bool                          tick_started;    /* True: Timer has been started */
	//  bool						  cnt_started; /* True: Counter has been started */
	//  uint8_t                       resolution; /* Number of bits in the timer (16 or 32 bits) */
	uint32_t						frequency;
};

struct stm32_tick_timer_dev_channel_s
{
	uint8_t type;
	uint32_t ccr;
	uint32_t sr_ccif;
	uint32_t sr_ccof;
	uint32_t  gpio;  /* TI1 input pin configuration (20-bit encoding) */
	int32_t delay_comp_ticks;
	struct hptc_oc_wf_s wf;
	struct hptc_io_dev_s *iodev;
};

#ifdef HPTC_AUXTIMER
struct stm32_aux_timer_dev_channel_s
{
	uint8_t type;
	uint32_t ccr;
	uint32_t sr_ccif;
	uint32_t sr_ccof;
	uint32_t  gpio;  /* TI1 input pin configuration (20-bit encoding) */
	int32_t delay_comp_ticks;
	struct hptc_oc_wf_s wf;
	struct hptc_io_dev_s *iodev;
    hptc_async_in_cb_t async_in_cb; /* Handle to the async callback of the input capture isr */
    FAR void *async_in_cb_arg; /* Argument for async_in_cb */
};
#endif

struct stm32_timdev_s
{
	uint32_t tick_timer_ccmr1_shadow;
	uint32_t tick_timer_ccmr2_shadow;
	uint32_t tick_timer_ccer_shadow;
	struct stm32_tick_timer_dev_channel_s tick_timer_ch[HPTC_TICKTIMER_CHANNELS];
#  ifdef HPTC_AUXTIMER
	uint32_t aux_timer_ccmr1_shadow;
	uint32_t aux_timer_ccmr2_shadow;
	uint32_t aux_timer_ccer_shadow;
	struct stm32_aux_timer_dev_channel_s aux_timer_ch[HPTC_AUXTIMER_CHANNELS];
#  endif
};

#define TIMER_CCIF_FLAG 1
#define TIMER_CCOF_FLAG 2
#define TIMER_SET_FLAG 4
#define TIMER_SET_RISING_EDGE 8
#define TIMER_SET_FALLING_EDGE 16

struct systime_table_s {
	uint32_t systick;
	int32_t  time_s;
	int32_t  time_ns;
	uint32_t ticks_per_systick;
	uint32_t ticks_at_systick;
};

struct channel_s {
	uint32_t flags;
	uint32_t  ticks;
};

struct stm32_systick_buffer_s {
	uint32_t table_index;
	uint32_t systick;
	int32_t  time_s;
	int32_t  time_ns;
	uint32_t ticks_per_systick;
	uint32_t ticks_per_systick_prev; //???
	uint32_t ticks_at_systick;
	uint32_t missed_timticks;
	uint32_t missed_systicks;

	uint32_t tick_timer_sr[CONFIG_HPTC_HP_OVERSAMPLE];
	struct channel_s tick_timer[HPTC_TICKTIMER_CHANNELS][CONFIG_HPTC_HP_OVERSAMPLE];
#  ifdef HPTC_AUXTIMER
	uint32_t aux_timer_sr[CONFIG_HPTC_HP_OVERSAMPLE];
	struct channel_s  aux_timer[HPTC_AUXTIMER_CHANNELS][CONFIG_HPTC_HP_OVERSAMPLE];
#  endif
};

struct channel_cmd_s {
	uint32_t flags;
	int32_t  ticks;
};

struct stm32_systick_buffer_cmd_s {
	uint32_t systick;
	struct channel_cmd_s tick_timer[HPTC_TICKTIMER_CHANNELS][CONFIG_HPTC_HP_OVERSAMPLE];
#  ifdef HPTC_AUXTIMER
	//uint32_t aux_timer_sr;
	struct channel_cmd_s  aux_timer[HPTC_AUXTIMER_CHANNELS][CONFIG_HPTC_HP_OVERSAMPLE];
#  endif
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* Interrupt handling *******************************************************/

static int stm32_systick_isr(int irq, void *context, void *arg);

#  ifdef HPTC_AUXTIMER
static int stm32_async_isr(int irq, void *context, void *arg);
#  endif

/* "Lower half" driver methods **********************************************/
static int stm32_cnt_start(FAR struct stm32_dev_s *priv);
static int stm32_cnt_stop(FAR struct stm32_dev_s *priv);
static int stm32_start(FAR struct hptc_dev_s *dev, sem_t* sem);
static int stm32_stop(FAR struct hptc_dev_s *dev);

static int stm32_getstatus(FAR struct hptc_dev_s *dev,
		FAR struct hptc_status_s *status);
static int stm32_getsemtime(FAR struct hptc_dev_s *dev,
		FAR struct timespec *ts);
static int stm32_tune(int32_t tune);
static int stm32_jump(struct timespec *jump);

static int stm32_settimeout(FAR struct hptc_dev_s *dev,
		uint32_t timeout);
//static int stm32_setinputcallback(FAR struct hptc_dev_s *dev,
//		hptc_in_cb_t callback);
static int stm32_setasynccallback(auxchannel_t channel, hptc_async_in_cb_t callback,FAR void* arg);
static int stm32_enableasynccallback(auxchannel_t channel);
static int stm32_disableasynccallback(auxchannel_t channel);

static void stm32_setcallback(FAR struct hptc_dev_s *dev,
		hptccb_t callback, FAR void *arg);

int up_io_initialize(struct hptc_io_dev_s *dev);

/****************************************************************************
 * Private Data
 ****************************************************************************/
/* "Lower half" driver methods */

static const struct hptc_ops_s g_hptc_ops =
{
		.start       = stm32_start,
		.stop        = stm32_stop,
		.getstatus   = stm32_getstatus,
		.getsemtime  = stm32_getsemtime,
		.tune        = stm32_tune,
		.jump        = stm32_jump,
		.settimeout  = stm32_settimeout,
		.setasyncinputcallback = stm32_setasynccallback,
		.enableasyncinputcallback = stm32_enableasynccallback,
		.disableasyncinputcallback = stm32_disableasynccallback,
		.setcallback = stm32_setcallback,
		.ioctl       = NULL,
};


static struct stm32_dev_s g_hptc_priv; /* set defaults? */

static struct hptc_dev_s g_hptc_dev = {
		.ops         = &g_hptc_ops,
		.priv        = &g_hptc_priv
		//		  .timhandler  = stm32_tim1_interrupt,
		//		  .resolution  = STM32_TIM1_HPTC_RES
};


static const struct hptc_io_ops_s g_hptc_io_ops= {
		.initialize = up_io_initialize,
		.ioctl = NULL
};

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH1
static struct hptc_io_dev_s g_hptc_io_dev_ticktimer_ch1;
#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH2
static struct hptc_io_dev_s g_hptc_io_dev_ticktimer_ch2;

#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH3
static struct hptc_io_dev_s g_hptc_io_dev_ticktimer_ch3;

#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH4
static struct hptc_io_dev_s g_hptc_io_dev_ticktimer_ch4;

#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH1
static struct hptc_io_dev_s g_hptc_io_dev_auxtimer_ch1;

#endif


#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH2

static struct hptc_io_dev_s g_hptc_io_dev_auxtimer_ch2;

#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH3

static struct hptc_io_dev_s g_hptc_io_dev_auxtimer_ch3;

#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH4

static struct hptc_io_dev_s g_hptc_io_dev_auxtimer_ch4;

#endif



#define HPTC_INITIAL_TICKS_PER_SYSTICK (HPTC_TICK_TIMER_CLKIN/HPTC_SYSTICK_FREQUENCY)

#define SYSTICK_IRQ_STATE_IDLE 0
#define SYSTICK_IRQ_STATE_ACTIVE 1
#define SYSTICK_IRQ_STATE_TRIGGERED 2

static volatile uint32_t g_systick_irq_state=SYSTICK_IRQ_STATE_TRIGGERED;

static volatile struct stm32_systick_buffer_cmd_s g_systick_buffer_cmd_A;
static volatile struct stm32_systick_buffer_cmd_s g_systick_buffer_cmd_B;
static volatile struct stm32_systick_buffer_cmd_s* volatile g_systick_buffer_cmd_active= &g_systick_buffer_cmd_B;

static volatile struct stm32_systick_buffer_s g_systick_buffer_A;
static volatile struct stm32_systick_buffer_s g_systick_buffer_B;
static volatile struct stm32_systick_buffer_s* volatile g_systick_buffer_active= &g_systick_buffer_B;

#define SYSTIME_TABLE_SIZE 100
static volatile struct systime_table_s g_systime_table[SYSTIME_TABLE_SIZE];
static volatile uint32_t g_systime_table_index=0;

static volatile uint32_t g_hptc_semtime_s=0;
static volatile uint32_t g_hptc_semtime_ns=0;

//static sem_t sem_coarse_time;
static volatile int32_t g_hptc_coarse_time_s=0;
static volatile int32_t g_hptc_coarse_time_ns=0;

static volatile int32_t g_tune=0;
static volatile int32_t g_clock_dev=0;

static volatile int32_t g_jump_state=HPTC_JUMP_STATE_IDLE;
static volatile struct timespec g_jump;


static struct stm32_tim_phys_dev_s g_ticktimer_dev =
{
		.next = NULL,
		.resolution = HPTC_TICKTIMER_RES,
		.base = HPTC_TICKTIMER_BASE,
		.isSlave = HPTC_TICKTIMER_ISSLAVE,
#  if HPTC_TICKTIMER_ISSLAVE
		.ts = HPTC_TICKTIMER_TS,
#  endif
		.type = HPTC_TICKTIMER_TYPE
};

#  ifdef HPTC_AUXTIMER
static struct stm32_tim_phys_dev_s g_auxtimer_dev =
{
		.next = NULL,
		.resolution = HPTC_AUXTIMER_RES,
		.base = HPTC_AUXTIMER_BASE,
		.isSlave = HPTC_AUXTIMER_ISSLAVE,
#    if HPTC_AUXTIMER_ISSLAVE
		.ts =HPTC_AUXTIMER_TS,
#    endif
		.type = HPTC_AUXTIMER_TYPE
};
#  endif

static volatile struct stm32_timdev_s g_tim_dev;

static sem_t* g_sem;
static volatile bool g_sem_armed = false;
//static volatile bool g_in_cb = false;


#ifdef VCXO_DAC_SPI_BUS
FAR struct spi_dev_s *spi;
#endif



/****************************************************************************
 * Private Functions
 ****************************************************************************/

static int timer_ticks_to_time(uint32_t timer_ticks, FAR struct timespec *tp);


/****************************************************************************
 * Name: recover_wf
 *
 * Description:
 *   try to recover a waveform by jumping forward about one second per slice
 *   called from stm32_systick_isr
 *
 * Input Parameters: FAR struct hptc_oc_wf_s
 *
 * Returned Values: 0 TODO:return error flags
 *
 ****************************************************************************/

static inline uint32_t recover_wf(FAR struct hptc_oc_wf_s *wf){
    //only continuous wf can be recovered
   if (wf->type < HPTC_IO_WF_CONTINUOUS){
           wf->type = HPTC_IO_WF_DISABLED;
   } else {
               wf->time_s+=wf->recovery_s;
               wf->time_ns+=wf->recovery_ns;

               //adjust fraction
               wf->residual+=wf->recovery_numerator;
               if(wf->residual >= wf->denominator){
                       wf->residual -= wf->denominator;
                       wf->time_ns++;
               }
               if(wf->time_ns >= (NS_PER_SEC+HPTC_NS_PER_TIMTICK-CONFIG_HPTC_OC_WINDOW_NS)){
                       wf->time_ns -= NS_PER_SEC;
                       wf->time_s++;
               }

   }


   //TODO: set error flags???
   return 0;

}


/****************************************************************************
 * Name: stm32_tick_tim_handler
 *
 * Description:
 *   hptc high priority interrupt handler
 *
 * Input Parameters:
 *
 * Returned Values:
 *
 ****************************************************************************/
void stm32_tick_tim_handler(void)
{

	static uint32_t hptc_oversample_slice=CONFIG_HPTC_HP_OVERSAMPLE-1; //XXX start at 0 after increment
	static uint32_t hptc_oversample_slice_last=CONFIG_HPTC_HP_OVERSAMPLE-1;

	static uint32_t systick=HPTC_INITIAL_SYSTICK; //FIXME: overflow?
//	static int32_t  systime_s=0; //uptime seconds
//	static int32_t  systime_ns=0;//uptime nanoseconds

	static uint32_t missed_timticks_sum=0;
	static uint32_t missed_systicks_sum=0;

	static uint32_t ticks_per_systick = HPTC_INITIAL_TICKS_PER_SYSTICK;
	static uint32_t ticks_at_systick=0;

#  if HPTC_TICKTIMER_RES == 16
	static uint16_t hptc_tim_ticks_next;
	uint16_t hptc_tim_ticks;
#  else
	static uint32_t hptc_tim_ticks_next;
	uint32_t hptc_tim_ticks;
#  endif

	volatile static struct stm32_systick_buffer_s* volatile systickbuffer = &g_systick_buffer_A;
	volatile static struct stm32_systick_buffer_cmd_s* volatile systickbuffer_cmd = &g_systick_buffer_cmd_A;

	static uint8_t  cmd_buffer_valid=0;
	uint8_t  set_tick_timer_ccmr1=0;
	uint8_t  set_tick_timer_ccmr2=0;
#  ifdef HPTC_AUXTIMER
	uint8_t  set_aux_timer_ccmr1=0;
	uint8_t  set_aux_timer_ccmr2=0;

#  endif

	uint8_t too_late=0;

	do {
		uint16_t ticktimer_sr=getreg16(HPTC_TICKTIMER_SR);
#  ifdef HPTC_AUXTIMER
		uint16_t auxtimer_sr=getreg16(HPTC_AUXTIMER_SR);
#  endif

		putreg16(~(ticktimer_sr & HPTC_TICKTIMER_OC_MASK),HPTC_TICKTIMER_SR);  //clear overcapture flags

#  ifdef HPTC_AUXTIMER
		putreg16(~(auxtimer_sr & HPTC_AUXTIMER_OC_MASK),HPTC_AUXTIMER_SR);   //clear overcapture flags
#  endif




#  if HPTC_TICKTIMER_RES == 16
		hptc_tim_ticks=getreg16(HPTC_TICKTIMER_CCR);   //nom ticks now
#  else
		hptc_tim_ticks=getreg32(HPTC_TICKTIMER_CCR);   //nom ticks now
#  endif

#  if (CONFIG_HPTC_HP_OVERSAMPLE > 1)
		hptc_oversample_slice_last=hptc_oversample_slice;
		hptc_oversample_slice++;
//toggle_LED_green();
		if (hptc_oversample_slice == CONFIG_HPTC_HP_OVERSAMPLE) {
			hptc_oversample_slice = 0;
//toggle_LED_red();
#  endif
			systick++;
#if 0
			systime_ns+=HPTC_NS_PER_SYSTICK;
			if(systime_ns > NS_PER_SEC){
				systime_ns -= NS_PER_SEC;
				systime_s++;
			}
#endif
			ticks_at_systick=hptc_tim_ticks;
			//TODO: if(hptc_tim_ticks != hptc_tim_ticks_next) ==> ERROR, wrong triggered interrupt

			//check for idle systick_isr to get consistent data
			if(g_systick_irq_state == SYSTICK_IRQ_STATE_IDLE) {
				//switch systickbuffer_cmd
				volatile struct stm32_systick_buffer_cmd_s* volatile systickbuffer_cmd_temp=systickbuffer_cmd;
				systickbuffer_cmd           = g_systick_buffer_cmd_active;
				g_systick_buffer_cmd_active = systickbuffer_cmd_temp;

				cmd_buffer_valid = (systickbuffer_cmd->systick == systick);
			} else {
				cmd_buffer_valid = 0;
				missed_systicks_sum++;
			}

			systickbuffer->systick=systick;
			systickbuffer->ticks_at_systick=ticks_at_systick;
			systickbuffer->ticks_per_systick_prev=ticks_per_systick;
			ticks_per_systick=systickbuffer_cmd->tick_timer[HPTC_TICKTIMER_CHANNEL_IDX][CONFIG_HPTC_HP_OVERSAMPLE-1].ticks;
			systickbuffer->ticks_per_systick=ticks_per_systick;
#  if (CONFIG_HPTC_HP_OVERSAMPLE > 1)
		}
#  endif


		for(int i=0;i<HPTC_TICKTIMER_CHANNELS;i++){
			uint8_t type = g_tim_dev.tick_timer_ch[i].type;

			if(type & HPTC_TIMER_CHANNEL_TYPE_TICKTIMER){

				hptc_tim_ticks_next=ticks_at_systick+systickbuffer_cmd->tick_timer[i][hptc_oversample_slice].ticks;

#  if (HPTC_TICKTIMER_RES ==16)
				putreg16(hptc_tim_ticks_next,HPTC_TICKTIMER_CCR); //set next irq trigger time
				//putreg16(hptc_tim_ticks_next,g_tim_dev.tick_timer_ch[i].ccr); //also possible
#  else
				putreg32(hptc_tim_ticks_next,HPTC_TICKTIMER_CCR); //set next irq trigger time
				//putreg32(hptc_tim_ticks_next,g_tim_dev.tick_timer_ch[i].ccr); //also possible
#  endif

				//int32_t hptc_tim_tick_delay=getreg32(HPTC_TICKTIMER_CNT)-g_hptc_tim_ticks;

				if  ( (type & HPTC_TIMER_CHANNEL_TYPE_OC) && (type & HPTC_TIMER_CHANNEL_TYPE_ACTIVE)){ //TICKTIMER_OC

					if(cmd_buffer_valid){

						if(systickbuffer_cmd->tick_timer[i][hptc_oversample_slice].flags & TIMER_SET_RISING_EDGE){
							if(i<2){
								g_tim_dev.tick_timer_ccmr1_shadow &=~(0x70<<(i*8));
								g_tim_dev.tick_timer_ccmr1_shadow |=0x10<<(i*8);
								set_tick_timer_ccmr1=1;
							} else {
								g_tim_dev.tick_timer_ccmr2_shadow &=~(0x70<<((i-2)*8));
								g_tim_dev.tick_timer_ccmr2_shadow |=0x10<<((i-2)*8);
								set_tick_timer_ccmr2=1;
							}
						} else if (systickbuffer_cmd->tick_timer[i][hptc_oversample_slice].flags & TIMER_SET_FALLING_EDGE){
							if(i<2){
								g_tim_dev.tick_timer_ccmr1_shadow &=~(0x70<<(i*8));
								g_tim_dev.tick_timer_ccmr1_shadow |=0x20<<(i*8);
								set_tick_timer_ccmr1=1;
							} else {
								g_tim_dev.tick_timer_ccmr2_shadow &=~(0x70<<((i-2)*8));
								g_tim_dev.tick_timer_ccmr2_shadow |=0x20<<((i-2)*8);
								set_tick_timer_ccmr2=1;
							}
						}
					}
				}
			} else if ( (type & HPTC_TIMER_CHANNEL_TYPE_OC) && (type & HPTC_TIMER_CHANNEL_TYPE_ACTIVE)){
				//*** NOT REQUIRED *** ticktimer_sr_write |= ticktimer_sr & g_tim_dev.tick_timer_ch[i].sr_ccif;  //prepare clear flag
//toggle_LED_green();
				if(cmd_buffer_valid && (systickbuffer_cmd->tick_timer[i][hptc_oversample_slice].flags & TIMER_SET_FLAG)){
#  if (HPTC_TICKTIMER_RES ==16)
					putreg16(ticks_at_systick + systickbuffer_cmd->tick_timer[i][hptc_oversample_slice].ticks ,g_tim_dev.tick_timer_ch[i].ccr);
#  else
					putreg32(ticks_at_systick + systickbuffer_cmd->tick_timer[i][hptc_oversample_slice].ticks ,g_tim_dev.tick_timer_ch[i].ccr);
#  endif
					if(systickbuffer_cmd->tick_timer[i][hptc_oversample_slice].flags & TIMER_SET_RISING_EDGE){
//toggle_LED_green();
						if(i<2){
							g_tim_dev.tick_timer_ccmr1_shadow &=~(0x70<<(i*8));
							g_tim_dev.tick_timer_ccmr1_shadow |=0x10<<(i*8);
							set_tick_timer_ccmr1=1;
						} else {
							g_tim_dev.tick_timer_ccmr2_shadow &=~(0x70<<((i-2)*8));
							g_tim_dev.tick_timer_ccmr2_shadow |=0x10<<((i-2)*8);
							set_tick_timer_ccmr2=1;
						}
					} else if (systickbuffer_cmd->tick_timer[i][hptc_oversample_slice].flags & TIMER_SET_FALLING_EDGE){
						if(i<2){
							g_tim_dev.tick_timer_ccmr1_shadow &=~(0x70<<(i*8));
							g_tim_dev.tick_timer_ccmr1_shadow |=0x20<<(i*8);
							set_tick_timer_ccmr1=1;
						} else {
							g_tim_dev.tick_timer_ccmr2_shadow &=~(0x70<<((i-2)*8));
							g_tim_dev.tick_timer_ccmr2_shadow |=0x20<<((i-2)*8);
							set_tick_timer_ccmr2=1;
						}
					}
				}

			} else if ( (type & HPTC_TIMER_CHANNEL_TYPE_IC) && (type & HPTC_TIMER_CHANNEL_TYPE_ACTIVE)){
				if (ticktimer_sr & g_tim_dev.tick_timer_ch[i].sr_ccif){

#  if (HPTC_TICKTIMER_RES ==16)
					systickbuffer->tick_timer[i][hptc_oversample_slice_last].ticks=getreg16(g_tim_dev.tick_timer_ch[i].ccr);
#  else
					systickbuffer->tick_timer[i][hptc_oversample_slice_last].ticks=getreg32(g_tim_dev.tick_timer_ch[i].ccr);
#  endif

				}
			}

		}

		if(set_tick_timer_ccmr1){
			putreg32(g_tim_dev.tick_timer_ccmr1_shadow,HPTC_TICKTIMER_CCMR1);
		}
		if(set_tick_timer_ccmr2){
			putreg32(g_tim_dev.tick_timer_ccmr2_shadow,HPTC_TICKTIMER_CCMR2);
		}

		ticktimer_sr |= getreg16(HPTC_TICKTIMER_SR) & HPTC_TICKTIMER_OC_MASK;  //update overcapture

		systickbuffer->tick_timer_sr[hptc_oversample_slice_last]=ticktimer_sr;


#  ifdef HPTC_AUXTIMER

		auxtimer_sr=getreg16(HPTC_AUXTIMER_SR);

		for(int i=0;i<HPTC_AUXTIMER_CHANNELS;i++){

			uint8_t type=g_tim_dev.aux_timer_ch[i].type;

			if ( (type & HPTC_TIMER_CHANNEL_TYPE_OC) && (type & HPTC_TIMER_CHANNEL_TYPE_ACTIVE)) {
				//not required*** auxtimer_sr_write |= auxtimer_sr & g_tim_dev.aux_timer_ch[i].sr_ccif;
				if(cmd_buffer_valid && (systickbuffer_cmd->aux_timer[i][hptc_oversample_slice].flags & TIMER_SET_FLAG)){
#    if HPTC_AUXTIMER_RES ==16
					putreg16(ticks_at_systick + systickbuffer_cmd->aux_timer[i][hptc_oversample_slice].ticks ,g_tim_dev.aux_timer_ch[i].ccr);
#    else
					putreg32(ticks_at_systick + systickbuffer_cmd->aux_timer[i][hptc_oversample_slice].ticks ,g_tim_dev.aux_timer_ch[i].ccr);
#    endif
					if(systickbuffer_cmd->aux_timer[i][hptc_oversample_slice].flags & TIMER_SET_RISING_EDGE){
//switch_LED_green_on();
						if(i<2){
							g_tim_dev.aux_timer_ccmr1_shadow &=~(0x70<<(i*8));
							g_tim_dev.aux_timer_ccmr1_shadow |=0x10<<(i*8);
							set_aux_timer_ccmr1=1;
						} else {
							g_tim_dev.aux_timer_ccmr2_shadow &=~(0x70<<((i-2)*8));
							g_tim_dev.aux_timer_ccmr2_shadow |=0x10<<((i-2)*8);
							set_aux_timer_ccmr2=1;
						}
					} else if (systickbuffer_cmd->aux_timer[i][hptc_oversample_slice].flags & TIMER_SET_FALLING_EDGE){
//switch_LED_green_off();
						if(i<2){
							g_tim_dev.aux_timer_ccmr1_shadow &=~(0x70<<(i*8));
							g_tim_dev.aux_timer_ccmr1_shadow |=0x20<<(i*8);
							set_aux_timer_ccmr1=1;
						} else {
							g_tim_dev.aux_timer_ccmr2_shadow &=~(0x70<<((i-2)*8));
							g_tim_dev.aux_timer_ccmr2_shadow |=0x20<<((i-2)*8);
							set_aux_timer_ccmr2=1;
						}
					}
				}
			} else if ((type & HPTC_TIMER_CHANNEL_TYPE_IC) && (type & HPTC_TIMER_CHANNEL_TYPE_ACTIVE)){
				if (auxtimer_sr & g_tim_dev.aux_timer_ch[i].sr_ccif){
#    if HPTC_AUXTIMER_RES ==16
					systickbuffer->aux_timer[i][hptc_oversample_slice_last].ticks=getreg16(g_tim_dev.aux_timer_ch[i].ccr);
#    else
					systickbuffer->aux_timer[i][hptc_oversample_slice_last].ticks=getreg32(g_tim_dev.aux_timer_ch[i].ccr);
#    endif
				}
			}
		}

		if(set_aux_timer_ccmr1){
			putreg32(g_tim_dev.aux_timer_ccmr1_shadow,HPTC_AUXTIMER_CCMR1);
		}
		if(set_aux_timer_ccmr2){
			putreg32(g_tim_dev.aux_timer_ccmr2_shadow,HPTC_AUXTIMER_CCMR2);
		}

		auxtimer_sr |= getreg16(HPTC_AUXTIMER_SR) & HPTC_AUXTIMER_OC_MASK; //update overcapture
		systickbuffer->aux_timer_sr[hptc_oversample_slice_last]=auxtimer_sr;

#  endif

#if 0
#  if (CONFIG_HPTC_HP_OVERSAMPLE > 1)
		if (!hptc_oversample_slice){
#  endif

			//update timetable
			int index=g_systime_table_index+1;
			if (index==SYSTIME_TABLE_SIZE){
				index=0;
			}
			g_systime_table[index].systick=systick;
			g_systime_table[index].ticks_at_systick=hptc_tim_ticks;
			g_systime_table[index].time_s=systime_s;
			g_systime_table[index].time_ns=systime_ns;
			g_systime_table[index].ticks_per_systick=ticks_per_systick;

			g_systime_table_index=index;

#  if (CONFIG_HPTC_HP_OVERSAMPLE > 1)
		}
#  endif
#endif
		putreg16(~(HPTC_TICKTIMER_SR_CCIF),HPTC_TICKTIMER_SR);  //clear pending CCIF bit to acknowledge interrupt

		// check timer after interrupt acknowledge
#  if HPTC_TICKTIMER_RES == 16
		uint16_t tim_cnt=getreg16(HPTC_TICKTIMER_CNT);
		int16_t delta_ticks;
		int16_t max_ticks;
#  else
		uint32_t tim_cnt=getreg32(HPTC_TICKTIMER_CNT);
		int32_t delta_ticks;
		int32_t max_ticks;
#  endif
		max_ticks=hptc_tim_ticks_next-hptc_tim_ticks;
		delta_ticks=tim_cnt-hptc_tim_ticks;

		if( (delta_ticks<0) || (delta_ticks >= max_ticks)){
			too_late=1;
			missed_timticks_sum++;
//toggle_LED_red();
		} else {
			too_late=0;
		}
	} while (too_late);


//toggle_LED_green();
	//here we are in time
#  if (CONFIG_HPTC_HP_OVERSAMPLE > 1)
	if (!hptc_oversample_slice){
#  endif
//toggle_LED_red();
		if(g_systick_irq_state != SYSTICK_IRQ_STATE_ACTIVE){
#ifdef VCXO_DAC_SPI_BUS
			stm32_gpiowrite(GPIO_VCXO,true);
#endif
			g_systick_irq_state = SYSTICK_IRQ_STATE_TRIGGERED;

			systickbuffer->missed_systicks=missed_systicks_sum;
			systickbuffer->missed_timticks=missed_timticks_sum;

			//change systickbuffer
			volatile struct stm32_systick_buffer_s* volatile systickbuffer_temp=g_systick_buffer_active;
			g_systick_buffer_active =systickbuffer;
			systickbuffer=systickbuffer_temp;


			//trigger systick
			putreg32( NVIC_INTCTRL_PENDSTSET,NVIC_INTCTRL); /*ICSR!!!*/
			//trigger pendsv
			//putreg32( NVIC_INTCTRL_PENDSVSET, NVIC_INTCTRL); /*ICSR!!!*/
		}
#  if (CONFIG_HPTC_HP_OVERSAMPLE > 1)
	}
#  endif

}





/****************************************************************************
 * Name: stm32_systick_isr
 *
 * Description:
 *   systick interrupt handler
 *
 * Input Parameters:
 *
 * Returned Values:
 *
 ****************************************************************************/

//static int stm32_hptc_pendsv_handler(FAR struct hptc_dev_s *dev)
static int stm32_systick_isr(int irq, void *context, void *arg)
{

	static uint32_t missed_systicks_sum=0;
	static uint32_t systick=1;
	uint32_t ticks_at_systick;
	static int32_t time_s;
	static int32_t time_ns = HPTC_NS_PER_SYSTICK;
	static int32_t ticks_per_second_equalize=0;

	//static int32_t time_s_next=0;
	//static int32_t time_ns_next=HPTC_NS_PER_SYSTICK;
	//static int32_t systick_next=1;

	uint32_t missed_systicks=0;

	//FAR struct hptc_dev_s *dev = (FAR struct hptc_dev_s *)arg;
	//FAR struct stm32_dev_s *priv = (FAR struct stm32_dev_s *)(dev->priv);

	g_systick_irq_state=SYSTICK_IRQ_STATE_ACTIVE;



	ticks_at_systick = g_systick_buffer_active->ticks_at_systick;
	//g_systick_buffer_active->systick;


#if 0
		//remove?
		missed_systicks=g_systick_buffer_active->missed_systicks;
#endif
//tmrinfo("t: %d m:%d\n",systick, g_systick_buffer_active->systick);
//tmrinfo("systicks: own:%d tim:%d  t: %d s, %d ms, ticks:%d  missedsys: %d, ticks_per_sys: %d  \n",systick,g_systick_buffer_active->systick, time_s,time_ns/1000000,ticks_at_systick, g_systick_buffer_active->missed_systicks,  g_systick_buffer_active->ticks_per_systick );
	missed_systicks=g_systick_buffer_active->systick-systick;
	if(missed_systicks){
		tmrerr("missed systicks: %d\n",missed_systicks);
		missed_systicks_sum+=missed_systicks;
		systick+=missed_systicks;
		int64_t missed_ns=(int64_t)missed_systicks * HPTC_NS_PER_SYSTICK;
		int32_t missed_s=missed_ns/NS_PER_SEC;
		missed_ns-=(int64_t)missed_s*NS_PER_SEC;

		time_s+=missed_s;
		time_ns+=missed_ns;
		if(time_ns>=NS_PER_SEC){
			time_ns-=NS_PER_SEC;
			time_s++;
		}
	}



	if(g_jump_state == HPTC_JUMP_STATE_JUMP){
		g_jump_state = HPTC_JUMP_STATE_IDLE;
        /* use same timescale for input capture */
		g_systick_buffer_active->ticks_per_systick_prev = g_systick_buffer_active->ticks_per_systick;
	}

//tmrerr("%d s, %d ms\n",time_s,time_ns/1000000);

	//remove?
	//time_s=g_systick_buffer_active->time_s;
	//time_ns=g_systick_buffer_active->time_ns;

	//get input data
	for(int i=0;i<HPTC_TICKTIMER_CHANNELS;i++){
		if( (g_tim_dev.tick_timer_ch[i].type & (HPTC_TIMER_CHANNEL_TYPE_IC | HPTC_TIMER_CHANNEL_TYPE_ACTIVE)) == (HPTC_TIMER_CHANNEL_TYPE_IC | HPTC_TIMER_CHANNEL_TYPE_ACTIVE)){
			for(int j=0; j< CONFIG_HPTC_HP_OVERSAMPLE; j++){

				if (g_systick_buffer_active->tick_timer_sr[j] & g_tim_dev.tick_timer_ch[i].sr_ccif){

					uint32_t flags=0;
					int32_t capture_s  = time_s;
					int32_t capture_ns = time_ns;
					int32_t capture = g_systick_buffer_active->tick_timer[i][j].ticks-ticks_at_systick;


					capture-=g_tim_dev.tick_timer_ch[i].delay_comp_ticks;

					int64_t offset_ns = (int64_t)capture*HPTC_NS_PER_SYSTICK;
					offset_ns/=(capture<0)?g_systick_buffer_active->ticks_per_systick_prev:g_systick_buffer_active->ticks_per_systick;
					capture_ns+=offset_ns;

					while(capture_ns < 0){
						capture_ns+=NS_PER_SEC;
						capture_s--;
					}

					while(capture_ns >= NS_PER_SEC){
						capture_ns-=NS_PER_SEC;
						capture_s++;
					}
					if (g_systick_buffer_active->tick_timer_sr[j] & g_tim_dev.tick_timer_ch[i].sr_ccof){
						//overcapture
						flags=HPTC_IO_HW_OC_FLAG;
					}
					//add capture_s and captuer_ns to read buffer
					hptc_ic_fifo_put(g_tim_dev.tick_timer_ch[i].iodev,i, flags, capture_s, capture_ns);
	//				toggle_LED_green();
//tmrinfo("ic:  %9d offset, time: %9d ns -> ic: %09d ns\n",g_systick_buffer_active->tick_timer[i][j].ticks,time_ns, capture_ns);
				}
			}
		}
	}


#  ifdef HPTC_AUXTIMER
	for(int i=0;i<HPTC_AUXTIMER_CHANNELS;i++){
		if((g_tim_dev.aux_timer_ch[i].type & (HPTC_TIMER_CHANNEL_TYPE_IC | HPTC_TIMER_CHANNEL_TYPE_ACTIVE)) == (HPTC_TIMER_CHANNEL_TYPE_IC | HPTC_TIMER_CHANNEL_TYPE_ACTIVE)){
			for(int j=0; j< CONFIG_HPTC_HP_OVERSAMPLE; j++){

				if (g_systick_buffer_active->aux_timer_sr[j] & g_tim_dev.aux_timer_ch[i].sr_ccif){

					uint32_t flags=0;
					int32_t capture_s  = time_s;
					int32_t capture_ns = time_ns;
					int32_t capture = g_systick_buffer_active->aux_timer[i][j].ticks-ticks_at_systick;

					capture-=g_tim_dev.aux_timer_ch[i].delay_comp_ticks;

					int64_t offset_ns = (int64_t)capture*HPTC_NS_PER_SYSTICK;
					offset_ns/=(capture<0)?g_systick_buffer_active->ticks_per_systick_prev:g_systick_buffer_active->ticks_per_systick;
					capture_ns+=offset_ns;

					while(capture_ns < 0){
						capture_ns+=NS_PER_SEC;
						capture_s--;
					}

					while(capture_ns >= NS_PER_SEC){
						capture_ns-=NS_PER_SEC;
						capture_s++;
					}
					if (g_systick_buffer_active->aux_timer_sr[j] & g_tim_dev.aux_timer_ch[i].sr_ccof){
						//overcapture
						flags=HPTC_IO_HW_OC_FLAG;
					}
					//add capture_s and captuer_ns to read buffer
					hptc_ic_fifo_put(g_tim_dev.aux_timer_ch[i].iodev,i, flags, capture_s, capture_ns);

				}
			}
		}
	}

#  endif //HPTC_AUXTIMER


	/* external time synchronisation
	 *
	 */

	int32_t tune=g_tune;
	int32_t ticks_per_systick_next;

#ifdef VCXO_DAC_SPI_BUS
#define CONFIG_HPTC_VCXO_VAL_MAX 32767
#define CONFIG_HPTC_VCXO_PPB_PER_BIT 5
#define HPTC_VCXO_PPB_MAX (CONFIG_HPTC_VCXO_VAL_MAX * CONFIG_HPTC_VCXO_PPB_PER_BIT)

	int32_t dac_val=0x8000;

	if(tune>HPTC_VCXO_PPB_MAX){
		dac_val-=CONFIG_HPTC_VCXO_VAL_MAX;
		tune-=HPTC_VCXO_PPB_MAX;
	} else if (tune<-HPTC_VCXO_PPB_MAX){
		dac_val+=CONFIG_HPTC_VCXO_VAL_MAX;
		tune+=HPTC_VCXO_PPB_MAX;
	} else {
		dac_val-=tune/CONFIG_HPTC_VCXO_PPB_PER_BIT;
		tune=0;
	}
#endif

	int32_t ticks_per_second_next=HPTC_TICK_TIMER_CLKIN + (int64_t)tune*HPTC_TICK_TIMER_CLKIN/1000000000;
	g_clock_dev=tune;

	ticks_per_second_equalize+=ticks_per_second_next;
	ticks_per_systick_next= ticks_per_second_equalize/HPTC_SYSTICK_FREQUENCY;
	ticks_per_second_equalize-=ticks_per_systick_next*HPTC_SYSTICK_FREQUENCY;








	uint32_t systick_next=systick+1;
	int32_t time_ns_next=time_ns + HPTC_NS_PER_SYSTICK;
	int32_t time_s_next=time_s;

	while (time_ns_next>= NS_PER_SEC){
		time_ns_next-=NS_PER_SEC;
		time_s_next++;
	}

	if (g_jump_state ==HPTC_JUMP_STATE_TRIGGERD){

		int32_t delta_systick_ns=g_jump.tv_nsec%HPTC_NS_PER_SYSTICK;

		g_jump_state=HPTC_JUMP_STATE_SETUP;

		if(delta_systick_ns>0){
			delta_systick_ns-=HPTC_NS_PER_SYSTICK;
		}
		g_jump.tv_nsec-=delta_systick_ns;

		ticks_per_systick_next-=(int64_t)delta_systick_ns*ticks_per_second_next/1000000000;

	} else if (g_jump_state == HPTC_JUMP_STATE_SETUP) {

		g_jump_state=HPTC_JUMP_STATE_JUMP;
		time_ns_next+=g_jump.tv_nsec;
		time_s_next+=g_jump.tv_sec;

		while (time_ns_next>= NS_PER_SEC){
			time_ns_next-=NS_PER_SEC;
			time_s_next++;
		}

		while (time_ns_next < 0){
			time_ns_next+=NS_PER_SEC;
			time_s_next--;
		}
	}


	//update systime_table
	int index=g_systime_table_index+1;
	if (index==SYSTIME_TABLE_SIZE){
		index=0;
	}

	g_systime_table[index].systick=systick_next;
	g_systime_table[index].ticks_at_systick=ticks_at_systick+g_systick_buffer_active->ticks_per_systick;
	g_systime_table[index].time_s=time_s_next;
	g_systime_table[index].time_ns=time_ns_next;
	g_systime_table[index].ticks_per_systick=ticks_per_systick_next;

	g_systime_table_index=index;


	//set g_systick_buffer_cmd_active


	int32_t time_ns_oc_min_0=time_ns_next+HPTC_NS_PER_TIMTICK-CONFIG_HPTC_OC_WINDOW_NS;
	int32_t time_ns_oc_min;
	int32_t time_ns_oc_max;



//tmrinfo("a\n");
	g_systick_buffer_cmd_active->systick=systick_next;
	for(int i=0;i<HPTC_TICKTIMER_CHANNELS;i++){

                if(g_tim_dev.tick_timer_ch[i].type & HPTC_TIMER_CHANNEL_TYPE_TICKTIMER){
                        int32_t ticks=0;
                        int32_t ticks_res=0;

                        int32_t ticks_per_timtick=ticks_per_systick_next/CONFIG_HPTC_HP_OVERSAMPLE;
                        int32_t tick_remainder=ticks_per_systick_next-ticks_per_timtick*CONFIG_HPTC_HP_OVERSAMPLE; // %

                        for(int j=0; j< CONFIG_HPTC_HP_OVERSAMPLE; j++){
                                ticks+=ticks_per_timtick;
                                ticks_res+=tick_remainder;
                                if(ticks_res>=CONFIG_HPTC_HP_OVERSAMPLE){
                                        ticks++;
                                        ticks_res-=CONFIG_HPTC_HP_OVERSAMPLE;
                                }
                                g_systick_buffer_cmd_active->tick_timer[i][j].ticks =ticks;
                                //g_systick_buffer_cmd_active->tick_timer[i][j].ticks = ticks_per_systick_next*(j+1)/CONFIG_HPTC_HP_OVERSAMPLE;
                        }

                } else if ((g_tim_dev.tick_timer_ch[i].type & (HPTC_TIMER_CHANNEL_TYPE_OC| HPTC_TIMER_CHANNEL_TYPE_ACTIVE)) == (HPTC_TIMER_CHANNEL_TYPE_OC| HPTC_TIMER_CHANNEL_TYPE_ACTIVE)){
                        volatile struct hptc_oc_wf_s *wf = &g_tim_dev.tick_timer_ch[i].wf;

                        time_ns_oc_min = time_ns_oc_min_0;
                        time_ns_oc_max = time_ns_oc_min_0 + HPTC_NS_PER_TIMTICK;
//tmrerr("%d,%09d, ch%d wf=%d,%09d,l=%d,%09d\n",time_s_next,time_ns_next, i, wf->time_s,wf->time_ns,wf->low_s,wf->low_ns);


//tmrinfo("%d s, %d ms, min: %d ms max: %d ms  edge:%d ms\n",time_s_next,time_ns_next/1000000, time_ns_oc_min_0/1000000, time_ns_oc_max/1000000, wf->time_ns/1000000);

                        for(int j=0; j< CONFIG_HPTC_HP_OVERSAMPLE; j++){

                                if(hptc_oc_fifo_get(g_tim_dev.tick_timer_ch[i].iodev,i,wf,time_s_next,time_ns_oc_max)){// && oc_msg.sec <= time_s_next && oc_msg.nsec<time_ns_oc_max){
                                //
                                }
                                if( (wf->type !=  HPTC_IO_WF_DISABLED) && (wf->time_s <= time_s_next)  ){
//tmrerr("%d,%09d, %d/%d wf=%d,%09d h:%d,%09d\n",time_s_next,time_ns_next, i, j, wf->time_s,wf->time_ns,wf->high_s,wf->high_ns);

                                    if(wf->time_s == time_s_next){
                                        if (wf->time_ns < time_ns_oc_max){
                                                if (wf->time_ns >= time_ns_oc_min){
                                                    //hit
                                                    int32_t time_ns_oc = wf->time_ns - time_ns_next;
                                                    g_systick_buffer_cmd_active->tick_timer[i][j].ticks = ((int64_t)time_ns_oc * (int64_t)ticks_per_systick_next) / HPTC_NS_PER_SYSTICK;
            //toggle_LED_green();
                                                    if (wf->state == HPTC_IO_RISING_EDGE){
            //switch_LED_green_on();
                                                            g_systick_buffer_cmd_active->tick_timer[i][j].flags = TIMER_SET_FLAG | TIMER_SET_RISING_EDGE;
                                                            wf->state=HPTC_IO_FALLING_EDGE;
                                                            wf->time_s+=wf->high_s;
                                                            wf->time_ns+=wf->high_ns;

                                                            //adjust fraction
                                                            wf->residual+=wf->numerator;
                                                            if(wf->residual >= wf->denominator){
                                                                    wf->residual -= wf->denominator;
                                                                    wf->time_ns++;
                                                            }


//tmrerr("%d rising wf=%d,%09d\n",j, wf->time_s,wf->time_ns);

                                                    } else {
            //switch_LED_green_off();
            //tmrinfo("tick %d:%d next:%9d offset, tps: %9d\n",i,j,g_systick_buffer_cmd_active->tick_timer[i][j].ticks,ticks_per_systick_next);
                                                            g_systick_buffer_cmd_active->tick_timer[i][j].flags = TIMER_SET_FLAG | TIMER_SET_FALLING_EDGE;
                                                            wf->state=HPTC_IO_RISING_EDGE;
                                                            wf->time_s+=wf->low_s;
                                                            wf->time_ns+=wf->low_ns;


//tmrerr("%d falling wf=%d,%09d\n",j, wf->time_s,wf->time_ns);
                                                    }


                                                    while(wf->time_ns >= (NS_PER_SEC+HPTC_NS_PER_TIMTICK-CONFIG_HPTC_OC_WINDOW_NS)){
                                                            wf->time_ns -= NS_PER_SEC;
                                                            wf->time_s++;
                                                    }



            //tmrinfo("next:%09dns\n",wf->time_ns);
                                                    if (wf->type < HPTC_IO_WF_CONTINUOUS) wf->type--;

                                                } else {
                                                        //missed
                                                        g_systick_buffer_cmd_active->tick_timer[i][j].flags = recover_wf(wf);
//tmrerr("%d recover1 wf=%d,%09d\n",j, wf->time_s,wf->time_ns);
                                                }


                                        } else {
                                                //idle
                                                g_systick_buffer_cmd_active->tick_timer[i][j].flags = 0;
                                        }


                                    }else{

                                            //missed
                                            g_systick_buffer_cmd_active->tick_timer[i][j].flags = recover_wf(wf);

//tmrerr("%d recover2 wf=%d,%09d\n",j, wf->time_s,wf->time_ns);

                                    }




                            } else {
                                    //idle
                                    g_systick_buffer_cmd_active->tick_timer[i][j].flags = 0;
                            }


                            time_ns_oc_min = time_ns_oc_max;
                            time_ns_oc_max += HPTC_NS_PER_TIMTICK;

                        }
//tmrinfo("y");
		}
	}

#  ifdef HPTC_AUXTIMER
	for(int i=0;i<HPTC_AUXTIMER_CHANNELS;i++){
		if ((g_tim_dev.aux_timer_ch[i].type & (HPTC_TIMER_CHANNEL_TYPE_OC| HPTC_TIMER_CHANNEL_TYPE_ACTIVE))== (HPTC_TIMER_CHANNEL_TYPE_OC| HPTC_TIMER_CHANNEL_TYPE_ACTIVE) ){
					volatile struct hptc_oc_wf_s *wf = &g_tim_dev.aux_timer_ch[i].wf;

					time_ns_oc_min = time_ns_oc_min_0;
					time_ns_oc_max = time_ns_oc_min_0 + HPTC_NS_PER_TIMTICK;

					for(int j=0; j< CONFIG_HPTC_HP_OVERSAMPLE; j++){

						if(hptc_oc_fifo_get(g_tim_dev.aux_timer_ch[i].iodev,i,wf,time_s_next,time_ns_oc_max)){// && oc_msg.sec <= time_s_next && oc_msg.nsec<time_ns_oc_max){
								//
							}

						//old if( (wf->type !=  HPTC_IO_WF_DISABLED) && (wf->time_ns >= time_ns_oc_min) && (wf->time_ns < time_ns_oc_max) ){
	                                        if( (wf->type !=  HPTC_IO_WF_DISABLED) && (wf->time_s <= time_s_next)  ){

						        if(wf->time_s == time_s_next){
						            if (wf->time_ns < time_ns_oc_max){
						                    if (wf->time_ns >= time_ns_oc_min){
						                        //hit
						                        int32_t time_ns_oc = wf->time_ns - time_ns_next;
                                                                        g_systick_buffer_cmd_active->aux_timer[i][j].ticks = ((int64_t)time_ns_oc * (int64_t)ticks_per_systick_next) / HPTC_NS_PER_SYSTICK;
                //toggle_LED_green();
                                                                        if (wf->state == HPTC_IO_RISING_EDGE){
                //switch_LED_green_on();
                                                                                g_systick_buffer_cmd_active->aux_timer[i][j].flags = TIMER_SET_FLAG | TIMER_SET_RISING_EDGE;
                                                                                wf->state=HPTC_IO_FALLING_EDGE;
                                                                                wf->time_s+=wf->high_s;
                                                                                wf->time_ns+=wf->high_ns;

                                                                                //adjust fraction
                                                                                wf->residual+=wf->numerator;
                                                                                if(wf->residual >= wf->denominator){
                                                                                        wf->residual -= wf->denominator;
                                                                                        wf->time_ns++;
                                                                                }



                                                                        } else {
                //switch_LED_green_off();
                //tmrinfo("aux %d:%d next:%9d offset, tps: %9d\n",i,j,g_systick_buffer_cmd_active->aux_timer[i][j].ticks,ticks_per_systick_next);
                                                                                g_systick_buffer_cmd_active->aux_timer[i][j].flags = TIMER_SET_FLAG | TIMER_SET_FALLING_EDGE;
                                                                                wf->state=HPTC_IO_RISING_EDGE;
                                                                                wf->time_ns+=wf->low_ns;
                                                                        }


                                                                        if(wf->time_ns >= (NS_PER_SEC+HPTC_NS_PER_TIMTICK-CONFIG_HPTC_OC_WINDOW_NS)){
                                                                                wf->time_ns -= NS_PER_SEC;
                                                                                wf->time_s++;
                                                                        }
                //tmrinfo("next:%09dns\n",wf->time_ns);
                                                                        if (wf->type < HPTC_IO_WF_CONTINUOUS) wf->type--;

						                    } else {
						                            //missed
						                            g_systick_buffer_cmd_active->aux_timer[i][j].flags = recover_wf(wf);

						                    }


						            } else {
						                    //idle
						                    g_systick_buffer_cmd_active->aux_timer[i][j].flags = 0;
						            }


						        }else{

                                                                //missed
						                g_systick_buffer_cmd_active->aux_timer[i][j].flags = recover_wf(wf);

						        }




						} else {
						        //idle
							g_systick_buffer_cmd_active->aux_timer[i][j].flags = 0;
						}


						time_ns_oc_min = time_ns_oc_max;
						time_ns_oc_max += HPTC_NS_PER_TIMTICK;

					}
				}
	}
#endif

#ifdef VCXO_DAC_SPI_BUS
	//dirty: GPIO_VCXO was set at the begin of isr. Hope that we have spent enough time high until here
	stm32_gpiowrite(GPIO_VCXO,false);
	//ST32F4 DAC has no buffer, so we have to wait for TXE
	//FIXME: use DMA or worker thread?
	while(!(getreg8(VCXO_DAC_SPI_BASE + STM32_SPI_SR_OFFSET) & SPI_SR_TXE));
	putreg8(0x00, VCXO_DAC_SPI_BASE + STM32_SPI_DR_OFFSET);
	while(!(getreg8(VCXO_DAC_SPI_BASE + STM32_SPI_SR_OFFSET) & SPI_SR_TXE));
	putreg8((uint8_t)(dac_val>>8), VCXO_DAC_SPI_BASE + STM32_SPI_DR_OFFSET);
	while(!(getreg8(VCXO_DAC_SPI_BASE + STM32_SPI_SR_OFFSET) & SPI_SR_TXE));
	putreg8((uint8_t)dac_val, VCXO_DAC_SPI_BASE + STM32_SPI_DR_OFFSET);

#endif

#if 0
	if(missed_systick){
		//ERROR MISSED TICKS for input capture
		//set flag, count errors, ...
	}
#endif

	//set semaphore to wake up related tasks.
	if (g_sem_armed) {
		int sem_val=0;
		sem_getvalue(g_sem,&sem_val);
		if(sem_val<1){
			g_hptc_semtime_s = time_s;
			g_hptc_semtime_ns = time_ns;
			sem_post(g_sem);
		}
	}

	//nxsem_wait(&sem_coarse_time);
	g_hptc_coarse_time_s=time_s;
	g_hptc_coarse_time_ns=time_ns;
	//nxsem_post(&sem_coarse_time);

/* Process timer interrupt */

	nxsched_process_timer();
//	tmrinfo("S\n");

	//setup vars for next slice
	time_s=time_s_next;
	time_ns=time_ns_next;
	systick=systick_next;

	g_systick_irq_state=SYSTICK_IRQ_STATE_IDLE;
	return OK;
}

#  ifdef HPTC_AUXTIMER

/****************************************************************************
 * Name: stm32_async_isr
 *
 * Description:
 *   asynchronous interrupt handler
 *
 * Input Parameters:
 *
 * Returned Values:
 *
 ****************************************************************************/

static int stm32_async_isr(int irq, void *context, void *arg)
{
	//TODO: use systime instead of hptctime

	int ret = OK;

	uint32_t auxtimer_sr     = getreg16(HPTC_AUXTIMER_SR);
	uint32_t aux_timer_ticks = 0;
	struct timespec capture_time;

#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC1_ASYNC_IRQ
	if (auxtimer_sr & GTIM_SR_CC1IF) {

		/* read capture compare register (that also resets the corresponding interrupt flag */

#    if HPTC_TICKTIMER_RES ==16
		aux_timer_ticks = getreg16(g_tim_dev.aux_timer_ch[0].ccr);
#    else
		aux_timer_ticks = getreg32(g_tim_dev.aux_timer_ch[0].ccr);
#    endif

		/* Convert timer ticks to timestamp */

		ret = timer_ticks_to_time(aux_timer_ticks, &capture_time);
		if(ret < 0) tmrerr("timer_ticks_to_time failed in async isr!\n");

		/* Now perform the callback */

		if(g_tim_dev.aux_timer_ch[0].async_in_cb != NULL){

			g_tim_dev.aux_timer_ch[0].async_in_cb(g_tim_dev.aux_timer_ch[0].async_in_cb_arg, ret, capture_time.tv_sec, capture_time.tv_nsec);
		}
	}
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC2_ASYNC_IRQ
	if (auxtimer_sr & GTIM_SR_CC2IF) {

		/* read capture compare register (that also resets the corresponding interrupt flag */

#    if HPTC_TICKTIMER_RES ==16
		aux_timer_ticks = getreg16(g_tim_dev.aux_timer_ch[1].ccr);
#    else
		aux_timer_ticks = getreg32(g_tim_dev.aux_timer_ch[1].ccr);
#    endif

		/* Convert timer ticks to timestamp */

		ret = timer_ticks_to_time(aux_timer_ticks, &capture_time);
		if(ret < 0) tmrerr("timer_ticks_to_time failed in async isr!\n");

		/* Now perform the callback */

		if(g_tim_dev.aux_timer_ch[1].async_in_cb != NULL){

			g_tim_dev.aux_timer_ch[1].async_in_cb(g_tim_dev.aux_timer_ch[1].async_in_cb_arg, ret, capture_time.tv_sec, capture_time.tv_nsec);
		}
	}
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC3_ASYNC_IRQ
	if (auxtimer_sr & GTIM_SR_CC3IF) {

		/* read capture compare register (that also resets the corresponding interrupt flag */

#    if HPTC_TICKTIMER_RES ==16
		aux_timer_ticks = getreg16(g_tim_dev.aux_timer_ch[2].ccr);
#    else
		aux_timer_ticks = getreg32(g_tim_dev.aux_timer_ch[2].ccr);
#    endif

		/* Convert timer ticks to timestamp */

		ret = timer_ticks_to_time(aux_timer_ticks, &capture_time);
		if(ret < 0) tmrerr("timer_ticks_to_time failed in async isr!\n");

		/* Now perform the callback */

		if(g_tim_dev.aux_timer_ch[2].async_in_cb != NULL){

			g_tim_dev.aux_timer_ch[2].async_in_cb(g_tim_dev.aux_timer_ch[2].async_in_cb_arg, ret, capture_time.tv_sec, capture_time.tv_nsec);
		}
	}
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC4_ASYNC_IRQ
	if (auxtimer_sr & GTIM_SR_CC4IF) {

		/* read capture compare register (that also resets the corresponding interrupt flag */

#    if HPTC_TICKTIMER_RES ==16
		aux_timer_ticks = getreg16(g_tim_dev.aux_timer_ch[3].ccr);
#    else
		aux_timer_ticks = getreg32(g_tim_dev.aux_timer_ch[3].ccr);
#    endif

		/* Convert timer ticks to timestamp */

		ret = timer_ticks_to_time(aux_timer_ticks, &capture_time);
		if(ret < 0) tmrerr("timer_ticks_to_time failed in async isr!\n");

		/* Now perform the callback */

		if(g_tim_dev.aux_timer_ch[3].async_in_cb != NULL){

			g_tim_dev.aux_timer_ch[3].async_in_cb((void*)g_tim_dev.aux_timer_ch[3].async_in_cb_arg, ret, capture_time.tv_sec, capture_time.tv_nsec);
		}
	}
#endif

	return ret;
}
#  endif


/************************************************************************************
 * Name: stm32_apb_tim_enable
 ************************************************************************************/

void stm32_apb_tim_enable()
{
	/* enable power */

#  ifdef CONFIG_STM32_TIM1_HPTC
	modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_TIM1EN);
#  endif
#  ifdef CONFIG_STM32_TIM2_HPTC
	modifyreg32(STM32_RCC_APB1ENR, 0, RCC_APB1ENR_TIM2EN);
#  endif
#  ifdef CONFIG_STM32_TIM3_HPTC
	modifyreg32(STM32_RCC_APB1ENR, 0, RCC_APB1ENR_TIM3EN);
#  endif
#  ifdef CONFIG_STM32_TIM4_HPTC
	modifyreg32(STM32_RCC_APB1ENR, 0, RCC_APB1ENR_TIM4EN);
#  endif
#  ifdef CONFIG_STM32_TIM5_HPTC
	modifyreg32(STM32_RCC_APB1ENR, 0, RCC_APB1ENR_TIM5EN);
#  endif

#  ifdef CONFIG_STM32_TIM8_HPTC
    modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_TIM8EN);
#  endif
#  ifdef CONFIG_STM32_TIM9_HPTC
    modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_TIM9EN);
#  endif
#  ifdef CONFIG_STM32_TIM10_HPTC
    modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_TIM10EN);
#  endif
#  ifdef CONFIG_STM32_TIM11_HPTC
    modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_TIM11EN);
#  endif
#  ifdef CONFIG_STM32_TIM12_HPTC
    modifyreg32(STM32_RCC_APB1ENR, 0, RCC_APB1ENR_TIM12EN);
#  endif
#  ifdef CONFIG_STM32_TIM13_HPTC
    modifyreg32(STM32_RCC_APB1ENR, 0, RCC_APB1ENR_TIM13EN);
#  endif
#  ifdef CONFIG_STM32_TIM14_HPTC
    modifyreg32(STM32_RCC_APB1ENR, 0, RCC_APB1ENR_TIM14EN);
#  endif
#  ifdef CONFIG_STM32_TIM15_HPTC
    modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_TIM15EN);
#  endif
#  ifdef CONFIG_STM32_TIM16_HPTC
    modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_TIM16EN);
#  endif
#  ifdef CONFIG_STM32_TIM17_HPTC
    modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_TIM17EN);
#  endif

}

/************************************************************************************
 * Name: stm32_apb_tim_disable
 ************************************************************************************/

void stm32_apb_tim_disable()
{

    /* Disable power */

#  ifdef CONFIG_STM32_TIM1_HPTC
    modifyreg32(STM32_RCC_APB2ENR, RCC_APB2ENR_TIM1EN, 0);
#  endif
#  ifdef CONFIG_STM32_TIM2_HPTC
    modifyreg32(STM32_RCC_APB1ENR, RCC_APB1ENR_TIM2EN, 0);
#  endif
#  ifdef CONFIG_STM32_TIM3_HPTC
    modifyreg32(STM32_RCC_APB1ENR, RCC_APB1ENR_TIM3EN, 0);
#  endif
#  ifdef CONFIG_STM32_TIM4_HPTC
    modifyreg32(STM32_RCC_APB1ENR, RCC_APB1ENR_TIM4EN, 0);
#  endif
#  ifdef CONFIG_STM32_TIM5_HPTC
    modifyreg32(STM32_RCC_APB1ENR, RCC_APB1ENR_TIM5EN, 0);
#  endif
#  ifdef CONFIG_STM32_TIM8_HPTC
    modifyreg32(STM32_RCC_APB2ENR, RCC_APB2ENR_TIM8EN, 0);
#  endif
#  ifdef CONFIG_STM32_TIM9_HPTC
    modifyreg32(STM32_RCC_APB2ENR, RCC_APB2ENR_TIM9EN, 0);
#  endif
#  ifdef CONFIG_STM32_TIM10_HPTC
    modifyreg32(STM32_RCC_APB2ENR, RCC_APB2ENR_TIM10EN, 0);
#  endif
#  ifdef CONFIG_STM32_TIM11_HPTC
    modifyreg32(STM32_RCC_APB2ENR, RCC_APB2ENR_TIM11EN, 0);
#  endif
#  ifdef CONFIG_STM32_TIM12_HPTC
    modifyreg32(STM32_RCC_APB1ENR, RCC_APB1ENR_TIM12EN, 0);
#  endif
#  ifdef CONFIG_STM32_TIM13_HPTC
    modifyreg32(STM32_RCC_APB1ENR, RCC_APB1ENR_TIM13EN, 0);
#  endif
#  ifdef CONFIG_STM32_TIM14_HPTC
    modifyreg32(STM32_RCC_APB1ENR, RCC_APB1ENR_TIM14EN, 0);
#  endif
#  ifdef CONFIG_STM32_TIM15_HPTC
    modifyreg32(STM32_RCC_APB2ENR, RCC_APB2ENR_TIM15EN, 0);
#  endif
#  ifdef CONFIG_STM32_TIM16_HPTC
    modifyreg32(STM32_RCC_APB2ENR, RCC_APB2ENR_TIM16EN, 0);
#  endif
#  ifdef CONFIG_STM32_TIM17_HPTC
    modifyreg32(STM32_RCC_APB2ENR, RCC_APB2ENR_TIM17EN, 0);
#  endif

}


/************************************************************************************
 * Name: stm32_init_timer_gpio
 ************************************************************************************/
void stm32_init_timer_gpio()
{

    putreg32(g_tim_dev.tick_timer_ccmr1_shadow,HPTC_TICKTIMER_CCMR1);

    putreg32(g_tim_dev.tick_timer_ccmr2_shadow,HPTC_TICKTIMER_CCMR2);

    putreg32(g_tim_dev.tick_timer_ccer_shadow,HPTC_TICKTIMER_CCER);


#  ifdef GPIO_HPTC_TICKTIMER_CH1
    stm32_configgpio(GPIO_HPTC_TICKTIMER_CH1);
#  endif
#  ifdef GPIO_HPTC_TICKTIMER_CH2
    stm32_configgpio(GPIO_HPTC_TICKTIMER_CH2);
#  endif
#  ifdef GPIO_HPTC_TICKTIMER_CH3
    stm32_configgpio(GPIO_HPTC_TICKTIMER_CH3);
#  endif
#  ifdef GPIO_HPTC_TICKTIMER_CH4
    stm32_configgpio(GPIO_HPTC_TICKTIMER_CH4);
#  endif

#  ifdef HPTC_AUXTIMER
	putreg32(g_tim_dev.aux_timer_ccmr1_shadow,HPTC_AUXTIMER_CCMR1);

	putreg32(g_tim_dev.aux_timer_ccmr2_shadow,HPTC_AUXTIMER_CCMR2);

	putreg32(g_tim_dev.aux_timer_ccer_shadow,HPTC_AUXTIMER_CCER);


#    ifdef GPIO_HPTC_AUXTIMER_CH1
	stm32_configgpio(GPIO_HPTC_AUXTIMER_CH1);
#    endif
#    ifdef GPIO_HPTC_AUXTIMER_CH2
	stm32_configgpio(GPIO_HPTC_AUXTIMER_CH2);
#    endif
#    ifdef GPIO_HPTC_AUXTIMER_CH3
	stm32_configgpio(GPIO_HPTC_AUXTIMER_CH3);
#    endif
#    ifdef GPIO_HPTC_AUXTIMER_CH4
	stm32_configgpio(GPIO_HPTC_AUXTIMER_CH4);
#    endif

#  endif //HPTC_AUXTIMER

}




/************************************************************************************
 * Name: stm32_init_timer
 ************************************************************************************/

void stm32_init_timer(FAR struct stm32_tim_phys_dev_s *first)
{
	struct stm32_tim_phys_dev_s *dev = first;
	while (dev!=NULL){
		uint16_t reg16;
		//uint32_t reg32;

		/* Set up the timer CR1 register:
		 *
		 * 1,8   CKD[1:0] ARPE CMS[1:0] DIR OPM URS UDIS CEN
		 * 2-5   CKD[1:0] ARPE CMS      DIR OPM URS UDIS CEN
		 * 9-14  CKD[1:0] ARPE                  URS UDIS CEN
		 * 15-17 CKD[1:0] ARPE              OPM URS UDIS CEN
		 */
		reg16 = ATIM_CR1_UDIS;

		putreg16 (reg16, dev->base + STM32_ATIM_CR1_OFFSET);

		/* Set up the timer CR2 register:
		 *
		 * 1,8   OIS4 OIS3N OIS3 OIS2N OIS2 OIS1N OIS1 TI1S MMS[2:0] CCDS CCUS CCPC
		 * 2-5                                         TI1S MMS[2:0] CCDS
		 */
		if(dev->type == 1 || dev->type == 2 ){
			reg16 = ATIM_CR2_MMS_ENABLE;
			putreg16 (reg16, dev->base + STM32_ATIM_CR2_OFFSET);
//tmrinfo("TRGO: MMS_ENABLE (0x%08x)\n",dev->base);
		}

		//slave: set trigger type; only start is controlled
		reg16= dev->ts | (dev->isSlave)? ATIM_SMCR_TRIGGER : 0x00;
		putreg16 (reg16, dev->base + STM32_ATIM_SMCR_OFFSET);

		if(dev->resolution == 16){
			putreg16(0xFFFF,dev->base + STM32_ATIM_ARR_OFFSET);
		}

		if(dev->resolution == 32){
			putreg32(0xFFFFFFFF,dev->base + STM32_ATIM_ARR_OFFSET);
		}

		//set prescaler
		putreg16(0x0, dev->base+STM32_ATIM_PSC_OFFSET); //TODO: HPTC: setup prescaler



		dev=dev->next;
	}

	//enable master -> also enables slave
	dev=first;
	while (dev!=NULL){
		if(!dev->isSlave){

//tmrinfo("enable master (0x%08x)\n",dev->base);
			modifyreg16(dev->base + STM32_ATIM_CR1_OFFSET, 0, ATIM_CR1_CEN);
		}

		dev=dev->next;
	}



}





/****************************************************************************
 * Name: stm32_cnt_start
 *
 * Description:
 *   Start synchronized counters
 *
 * Input Parameters:
 *   lower - A pointer the publicly visible representation of the "lower-half"
 *           driver state structure.
 *
 * Returned Values:
 *   Zero on success; a negated errno value on failure.
 *
 ****************************************************************************/

static int stm32_cnt_start(FAR struct stm32_dev_s *priv)
{
	//FAR struct stm32_lowerhalf_s *priv = (FAR struct stm32_lowerhalf_s *)lower;

	//  if (!priv->cnt_started)
	//    {
	stm32_apb_tim_enable();

	//configure gpios
	stm32_init_timer_gpio();

	//configure timer
	stm32_init_timer(priv->first);

	//      priv->cnt_started = true;
	return OK;
	//    }

	/* Return EBUSY to indicate that the timer was already running */

	//  return -EBUSY;
}



void stm32_tim_reset(){
	//TODO



}


/****************************************************************************
 * Name: stm32_cnt_stop
 *
 * Description:
 *   Stop and reset the timer resources
 *
 * Input parameters:
 *   dev - A reference to the lower half driver state structure
 *
 * Returned Value:
 *   Zero on success; a negated errno value on failure
 *
 * Assumptions:
 *   This function is called to stop the pulsed output at anytime.  This
 *   method is also called from the timer interrupt handler when a repetition
 *   count expires... automatically stopping the timer.
 *
 ****************************************************************************/

static int stm32_cnt_stop(FAR struct stm32_dev_s *priv)
{

	//    FAR struct stm32_lowerhalf_s *priv = (FAR struct stm32_lowerhalf_s *)lower;

#if 0
	if(priv->cnt_started){

		if(priv->tick_started){
			//TODO: stm32_tick_stop
		}

		priv->cnt_started=false;

		stm32_tim_reset();

		stm32_apb_tim_disable();

	}
#endif


	return OK;
}


/****************************************************************************
 * Name: stm32_tick_start
 *
 * Description:
 *   Start HPTC tick and interrupt
 *
 * Input Parameters:
 *   lower - A pointer the publicly visible representation of the "lower-half"
 *           driver state structure.
 *
 * Returned Values:
 *   Zero on success;
 *
 ****************************************************************************/

static int stm32_tick_start(FAR struct stm32_dev_s *priv)
{
	//FAR struct stm32_lowerhalf_s *priv = (FAR struct stm32_lowerhalf_s *)lower;

	uint32_t ticks;

	//setup initial like a virtual previos systick

	g_systime_table[0].systick=0;
	//g_systime_table[0].ticks_at_systick=0; //unknown here, initialized later
	g_systime_table[0].ticks_per_systick=HPTC_INITIAL_TICKS_PER_SYSTICK;
	g_systime_table[0].time_s=0;
	g_systime_table[0].time_ns=0;

	g_systime_table[1].systick=1;
	//g_systime_table[1].ticks_at_systick=0; //unknown here, initialized later
	g_systime_table[1].ticks_per_systick=HPTC_INITIAL_TICKS_PER_SYSTICK;
	g_systime_table[1].time_s=0;
	g_systime_table[1].time_ns=HPTC_NS_PER_SYSTICK;

	while(g_systime_table[1].time_ns>=NS_PER_SEC){
		g_systime_table[1].time_ns-=NS_PER_SEC;
		g_systime_table[1].time_s++;
	}
	g_systime_table_index=1;





	g_systick_irq_state=SYSTICK_IRQ_STATE_IDLE;

	//set max timeout to avoid match during setup
	putreg32(getreg32(HPTC_TICKTIMER_CNT),HPTC_TICKTIMER_CCR);
	// clear pending interrupt
	putreg16(~HPTC_TICKTIMER_SR_CCIF, HPTC_TICKTIMER_SR);
	// enable capture compare interrupt
	putreg16(HPTC_TICKTIMER_DIER_CCIE, HPTC_TICKTIMER_DIER);

	//set first tick_cnt
	//g_hptc_tim_ticks=getreg32(HPTC_TICKTIMER_CNT)+g_hptc_tim_ticks_per_hptc_tick;
	ticks=getreg32(HPTC_TICKTIMER_CNT);
	g_systime_table[0].ticks_at_systick=ticks;
	ticks+=HPTC_INITIAL_TICKS_PER_SYSTICK;
	putreg32(ticks,HPTC_TICKTIMER_CCR);
	g_systime_table[1].ticks_at_systick=ticks;

	up_enable_irq(HPTC_TICK_IRQ);


	return OK;

}





/****************************************************************************
 * Name: stm32_start
 *
 * Description:
 *   Start the timer, resetting the time to the current timeout,
 *
 * Input Parameters:
 *   dev - A pointer driver state structure.
 *
 *
 * Returned Values:
 *   Zero on success; a negated errno value on failure.
 *
 ****************************************************************************/

static int stm32_start(FAR struct hptc_dev_s *dev, sem_t *sem)
{

	FAR struct stm32_dev_s *priv = (FAR struct stm32_dev_s *)dev->priv;


	if (!priv->started)
	{
		g_sem = sem;
		g_sem_armed = true;
#if 0
		STM32_TIM_SETMODE(priv->tim, STM32_TIM_MODE_UP);

		if (priv->tick_cb != NULL)
		{
			STM32_TIM_SETISR(priv->tim, priv->timhandler, 0);
			STM32_TIM_ENABLEINT(priv->tim, 0);
		}
#endif
//    stm32_cnt_start(priv);
// 	  stm32_tick_start(priv);

#if CONFIG_ARCH_BOARD_MB_SERIALCOMM
//XXX gs_trigger test, quick and dirty

    {
		uint32_t scr;

		static uint8_t data=0xAA;
		/* Try to take timer's DMA stream in the OS. It will be locked with
		 * a semaphore so that no other driver can use the stream. If another
		 * driver already uses the stream stm32_dmachannel waits until the
		 * specified stream is released. This is a safety feature to make sure
		 * the driver has exclusive access to the DMA stream.
		 *
		 * Also initially configure the stream with the method stm32_dmasetup.
		 */
		DMA_HANDLE trig_dma;
		//typedef void (*dma_callback_t)(DMA_HANDLE handle, uint8_t status, void *arg);
		//dma_callback_t dma_cb;
		scr = (DMA_SCR_DIR_M2P | DMA_SCR_CIRC | DMA_SCR_MINC | DMA_SCR_PRIHI);
		trig_dma = stm32_dmachannel(DMAMAP_TIM2_CH1);

		stm32_dmasetup(trig_dma, STM32_USART3_DR, &data, 1, scr);
//		stm32_dmastart(trig_dma, NULL, NULL, false);
		stm32_dmaenable(trig_dma);
		// enable capture compare DMA
		putreg16(getreg16(HPTC_TICKTIMER_DIER) | GTIM_DIER_CC1DE, HPTC_TICKTIMER_DIER);

    }
//XXX end gs_trigger test
#endif







		priv->started = true;
		return OK;
	}

    /* Return EBUSY to indicate that the timer was already running */

    return -EBUSY;
}

/****************************************************************************
 * Name: stm32_stop
 *
 * Description:
 *   Stop the timer
 *
 * Input Parameters:
 *   dev - A pointer the publicly visible representation of the "lower-half"
 *           driver state structure.
 *
 * Returned Values:
 *   Zero on success; a negated errno value on failure.
 *
 ****************************************************************************/

static int stm32_stop(struct hptc_dev_s *dev)
{

	struct stm32_dev_s *priv = (struct stm32_dev_s *)dev->priv;

	if (priv->started)
	{
		//STM32_TIM_SETMODE(priv->tim, STM32_TIM_MODE_DISABLED);
		//STM32_TIM_DISABLEINT(priv->tim, 0);
		//STM32_TIM_SETISR(priv->tim, 0, 0);
		g_sem_armed = false;
		priv->started = false;
		return OK;
	}

	/* Return ENODEV to indicate that the timer was not running */

	return -ENODEV;
}


/************************************************************************************
 * Name: timer_ticks_to_time
 *
 * Description:
 *   Get the time for a clock/counter value.  This interface
 *
 * Input Parameters:
 *   ticks - clock/counter value
 *   tp    - The location to return the high resolution time value.
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ************************************************************************************/

static int timer_ticks_to_time(uint32_t timer_ticks, FAR struct timespec *tp)
{
	int32_t ticks_per_systick;
	uint32_t idx=g_systime_table_index;
	uint32_t systick=g_systime_table[idx].systick;

#  if HPTC_TICKTIMER_RES == 16
	int16_t delta_ticks;
# else
	int32_t delta_ticks;
#endif


	delta_ticks = timer_ticks- g_systime_table[idx].ticks_at_systick;

	while(delta_ticks<0){
		if(idx==0){
			idx=SYSTIME_TABLE_SIZE;
		}
		idx--;
		if(g_systime_table[idx].systick>=systick) { //buffer underflow
			tp->tv_sec=0;
			tp->tv_nsec=0;
			return -1; //error, value not in buffer
		}
		delta_ticks+=g_systime_table[idx].ticks_per_systick;
	}
	ticks_per_systick=g_systime_table[idx].ticks_per_systick;

	tp->tv_sec=g_systime_table[idx].time_s;
	tp->tv_nsec=g_systime_table[idx].time_ns;

	//check data integrity (or use mutex?!)
	if(g_systime_table[idx].systick>=systick) { //buffer underflow in background
		tp->tv_sec=0;
		tp->tv_nsec=0;
		return -1; //error, value not in buffer
	}

	tp->tv_nsec += (int64_t)delta_ticks*HPTC_NS_PER_SYSTICK/ticks_per_systick;//int64_t cast required to avoid overvlow in calculation

	while(tp->tv_nsec >= NS_PER_SEC){
		tp->tv_nsec-=NS_PER_SEC;
		tp->tv_sec++;
	}
	return OK;
}


/************************************************************************************
 * Name: up_hptc_gettime
 *
 * Description:
 *   Get the current time from the HPTC clock/counter.  This interface
 *   is used to replace the system timer.
 *
 * Input Parameters:
 *   tp - The location to return the high resolution time value.
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ************************************************************************************/

int up_hptc_gettime(FAR struct timespec *tp)
{
	uint32_t timer_ticks;

#  if HPTC_TICKTIMER_RES == 16
	timer_ticks=getreg16(HPTC_TICKTIMER_CNT);   //ticks now
#  else
	timer_ticks=getreg32(HPTC_TICKTIMER_CNT);   //ticks now
#  endif
    return timer_ticks_to_time(timer_ticks, tp);
}

/************************************************************************************
 * Name: up_hptc_gettime_coarse
 *
 * Description:
 *   Get the current coarse time from the HPTC clock/counter at systick_isr.  This interface
 *   is used to replace the system timer.
 *
 * Input Parameters:
 *   tp - The location to return the high resolution time value.
 *
 * Returned Value:
 *   Zero (OK) on success
 *   -1 on inconsistent data
 *
 ************************************************************************************/

int up_hptc_gettime_coarse(FAR struct timespec *tp)
{
	//nxsem_wait(&sem_coarse_time);
	do{
	tp->tv_sec=g_hptc_coarse_time_s;
	tp->tv_nsec=g_hptc_coarse_time_ns;
	/* recheck time_s for consistency instead of a semaphore */
	} while (tp->tv_sec!=g_hptc_coarse_time_s);

//	if(tp->tv_sec!=g_hptc_coarse_time_s){
//		return -1;
//	}
	//nxsem_post(&sem_coarse_time);
	return OK;
}

/************************************************************************************
 * Name: up_hptc_gettime_coarse_ns
 *
 * Description:
 *   Get the current coarse time_ns part from the HPTC clock/counter at systick_isr.
 *
 * Returned Value:
 *   time_ns
 *
 ************************************************************************************/

int32_t up_hptc_gettime_coarse_ns()
{
	return g_hptc_coarse_time_ns;
}

/****************************************************************************
 * Name: stm32_tune
 *
 * Description:
 *   Update clock tuning
 *
 * Input Parameters:
 *   tune - clock offset in nanoseconds per second (ppb)
 *
 * Returned Values:
 *   Zero on success; a negated errno value on failure.
 *
 ****************************************************************************/
static int stm32_tune(int32_t tune){
	g_tune=tune;
	return OK;
}

/****************************************************************************
 * Name: stm32_jump
 *
 * Description:
 *   Jump in time within one systick
 *
 * Input Parameters:
 *   jump - time offset in seconds and nanoseconds.
 *
 * Returned Values:
 *   Zero on success; a negated errno value on failure.
 *
 ****************************************************************************/
static int stm32_jump(struct timespec *jump){
	if(g_jump_state != HPTC_JUMP_STATE_IDLE) return -EBUSY;
	g_jump=*jump;
	g_jump_state = HPTC_JUMP_STATE_TRIGGERD;
	return OK;
}

/****************************************************************************
 * Name: stm32_settimeout
 *
 * Description:
 *   Set a new timeout value (and reset the timer)
 *
 * Input Parameters:
 *   dev     - A pointer the publicly visible representation of the "lower-half"
 *             driver state structure.
 *   timeout - The new timeout value in microseconds.
 *
 * Returned Values:
 *   Zero on success; a negated errno value on failure.
 *
 ****************************************************************************/

static int stm32_settimeout(FAR struct hptc_dev_s *dev, uint32_t timeout)
{
#if 0
	FAR struct stm32_dev_s *priv = (FAR struct stm32_dev_s *)dev->priv;
	uint64_t maxtimeout;

	if (priv->started)
	{
		return -EPERM;
	}

	maxtimeout = (1 << priv->resolution) - 1;
	if (timeout > maxtimeout)
	{
		uint64_t freq = (maxtimeout * 1000000) / timeout;
		STM32_TIM_SETCLOCK(priv->tim, freq);
		STM32_TIM_SETPERIOD(priv->tim, maxtimeout);
	}
	else
	{
		STM32_TIM_SETCLOCK(priv->tim, 1000000);
		STM32_TIM_SETPERIOD(priv->tim, timeout);
	}
#endif
  return OK;
}

static int stm32_getstatus(FAR struct hptc_dev_s *dev, FAR struct hptc_status_s *status){

	status->flags=getreg32(HPTC_TICKTIMER_CNT);
	status->cnt1=0;
	status->cnt2=0; //getreg32(HPTC_TICKTIMER_CCR); //

	return OK;
}

static int stm32_getsemtime(FAR struct hptc_dev_s *dev, FAR struct timespec *ts){
	ts->tv_sec=g_hptc_semtime_s;
	ts->tv_nsec=g_hptc_semtime_ns;
	return OK;
}

static int stm32_setasynccallback(auxchannel_t channel, hptc_async_in_cb_t callback, FAR void* arg)
{
	DEBUGASSERT(callback);
	if(callback == NULL) return -EINVAL;

#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC1_ASYNC_IRQ
	if(channel == ch1)
	{
		g_tim_dev.aux_timer_ch[0].async_in_cb_arg=arg;
		g_tim_dev.aux_timer_ch[0].async_in_cb = callback;
	}
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC2_ASYNC_IRQ
	if(channel == ch2)
	{
		g_tim_dev.aux_timer_ch[1].async_in_cb_arg=arg;
		g_tim_dev.aux_timer_ch[1].async_in_cb = callback;
	}
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC3_ASYNC_IRQ
	if (channel == ch3)
	{
		g_tim_dev.aux_timer_ch[2].async_in_cb_arg=arg;
		g_tim_dev.aux_timer_ch[2].async_in_cb = callback;
	}
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC4_ASYNC_IRQ
	if(channel == ch4)
	{
		g_tim_dev.aux_timer_ch[3].async_in_cb_arg=arg;
		g_tim_dev.aux_timer_ch[3].async_in_cb = callback;
	}
#endif


	return OK;
}

static int stm32_enableasynccallback(auxchannel_t channel)
{
	//FIXME: critical section?!

#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC1_ASYNC_IRQ
	if(channel == ch1)
	{
		/* clear ch1 pending bits */
		putreg16(~(GTIM_SR_CC1OF | GTIM_SR_CC1IF), HPTC_AUXTIMER_SR);
		/* enable ch1 interrupts */
		putreg16(GTIM_DIER_CC1IE | getreg16(HPTC_AUXTIMER_DIER), HPTC_AUXTIMER_DIER);
	}
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC2_ASYNC_IRQ
	if(channel == ch2)
	{
		/* clear ch2 pending bits */
		putreg16(~(GTIM_SR_CC2OF | GTIM_SR_CC2IF), HPTC_AUXTIMER_SR);
		/* enable ch2 interrupts */
		putreg16(GTIM_DIER_CC2IE | getreg16(HPTC_AUXTIMER_DIER), HPTC_AUXTIMER_DIER);
	}
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC3_ASYNC_IRQ
	if (channel == ch3)
	{
		/* clear ch3 pending bits */
		putreg16(~(GTIM_SR_CC3OF | GTIM_SR_CC3IF), HPTC_AUXTIMER_SR);
		/* enable ch3 interrupts */
		putreg16(GTIM_DIER_CC3IE | getreg16(HPTC_AUXTIMER_DIER), HPTC_AUXTIMER_DIER);
	}
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC4_ASYNC_IRQ
	if(channel == ch4)
	{
		/* clear ch4 pending bits */
		putreg16(~(GTIM_SR_CC4OF | GTIM_SR_CC4IF), HPTC_AUXTIMER_SR);
		/* enable ch4 interrupts */
		putreg16(GTIM_DIER_CC4IE | getreg16(HPTC_AUXTIMER_DIER), HPTC_AUXTIMER_DIER);
	}
#endif

	return OK;
}


static int stm32_disableasynccallback(auxchannel_t channel)
{
	//FIXME: critical section?!

#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC1_ASYNC_IRQ
	if(channel == ch1)
	{
		/* disable ch1 interrupts */
		putreg16(~GTIM_DIER_CC1IE & getreg16(HPTC_AUXTIMER_DIER), HPTC_AUXTIMER_DIER);
	}
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC2_ASYNC_IRQ
	if(channel == ch2)
	{
		/* disable ch2 interrupts */
		putreg16(~GTIM_DIER_CC2IE & getreg16(HPTC_AUXTIMER_DIER), HPTC_AUXTIMER_DIER);
	}
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC3_ASYNC_IRQ
	if (channel == ch3)
	{
		/* disable ch3 interrupts */
		putreg16(~GTIM_DIER_CC3IE & getreg16(HPTC_AUXTIMER_DIER), HPTC_AUXTIMER_DIER);
	}
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC4_ASYNC_IRQ
	if(channel == ch4)
	{
		/* disable ch4 interrupts */
		putreg16(~GTIM_DIER_CC4IE & getreg16(HPTC_AUXTIMER_DIER), HPTC_AUXTIMER_DIER);
	}
#endif

	return OK;
}



#if 0
static int stm32_setinputcallback(FAR struct hptc_dev_s *dev,
		hptc_in_cb_t callback)
{
	FAR struct stm32_dev_s *priv = (FAR struct stm32_dev_s *)dev->priv;
	priv->hptc_in_cb = callback;
	g_in_cb = true;
	return OK;
}
#endif

/****************************************************************************
 * Name: stm32_setcallback
 *
 * Description:
 *   Call this user provided timeout callback.
 *
 * Input Parameters:
 *   lower      - A pointer the publicly visible representation of the "lower-half"
 *                driver state structure.
 *   callback - The new timer expiration function pointer.  If this
 *                function pointer is NULL, then the reset-on-expiration
 *                behavior is restored,
 *  arg          - Argument that will be provided in the callback
 *
 * Returned Values:
 *   The previous timer expiration function pointer or NULL is there was
 *   no previous function pointer.
 *
 ****************************************************************************/

static void stm32_setcallback(FAR struct hptc_dev_s *dev,
		hptccb_t callback, FAR void *arg)
{
#if 0
	FAR struct stm32_dev_s *priv = (FAR struct stm32_dev_s *)dev->priv;

	irqstate_t flags = enter_critical_section();

	/* Save the new callback */

	priv->callback = callback;
	priv->arg      = arg;

	if (callback != NULL && priv->started)
	{
		STM32_TIM_SETISR(priv->tim, priv->timhandler, 0);
		STM32_TIM_ENABLEINT(priv->tim, 0);
	}
	else
	{
		STM32_TIM_DISABLEINT(priv->tim, 0);
		STM32_TIM_SETISR(priv->tim, 0, 0);
	}

    leave_critical_section(flags);
#endif
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Function:  arm_timer_initialize
 *
 * Description:
 *   This function is called during start-up to initialize the timer
 *   interrupt.
 *
 ****************************************************************************/

void arm_timer_initialize(void)
{

	//FAR struct stm32_lowerhalf_s *lower;
	FAR struct hptc_dev_s *dev;
	FAR struct stm32_dev_s *priv;

	dev = &g_hptc_dev;
	priv = (FAR struct stm32_dev_s *)dev->priv;
	//TODO  priv->cb = NULL;


#ifdef VCXO_DAC_SPI_BUS
	stm32_configgpio(GPIO_VCXO);

	stm32_gpiowrite(GPIO_VCXO,true);
#if 1
    spi = stm32_spibus_initialize(VCXO_DAC_SPI_BUS);
    if (!spi)
    {
    	PANIC();
    }

    SPI_LOCK(spi, true);
    SPI_SETFREQUENCY(spi,20000000);
    SPI_SETMODE(spi,SPIDEV_MODE2);
    SPI_SETBITS(spi,8);
#endif

	stm32_gpiowrite(GPIO_VCXO,false);
#if 1
	while(!(getreg8(VCXO_DAC_SPI_BASE + STM32_SPI_SR_OFFSET) & SPI_SR_TXE));
	putreg8(0x00, VCXO_DAC_SPI_BASE + STM32_SPI_DR_OFFSET);
	while(!(getreg8(VCXO_DAC_SPI_BASE + STM32_SPI_SR_OFFSET) & SPI_SR_TXE));
	putreg8(0x00, VCXO_DAC_SPI_BASE + STM32_SPI_DR_OFFSET);
	while(!(getreg8(VCXO_DAC_SPI_BASE + STM32_SPI_SR_OFFSET) & SPI_SR_TXE));
	putreg8(0x00, VCXO_DAC_SPI_BASE + STM32_SPI_DR_OFFSET);
#endif

#endif


	//nxsem_init(&sem_coarse_time, 0, 1);



	//  FAR  struct stm32_tim_phys_dev_s *dev=NULL;
	priv->started = false;

	priv->first = &g_ticktimer_dev;


#  ifdef CONFIG_STM32_HPTC_AUXTIMER_NONE
	g_ticktimer_dev.next=NULL;
#  else
	g_auxtimer_dev.next=NULL;
	g_ticktimer_dev.next=&g_auxtimer_dev;
#  endif


	g_tim_dev.tick_timer_ccmr1_shadow=0x0000;
	g_tim_dev.tick_timer_ccmr2_shadow=0x0000;

	g_tim_dev.tick_timer_ccer_shadow=0x0000;

	g_tim_dev.tick_timer_ch[0].ccr = HPTC_TICKTIMER_BASE + STM32_ATIM_CCR1_OFFSET;
	g_tim_dev.tick_timer_ch[1].ccr = HPTC_TICKTIMER_BASE + STM32_ATIM_CCR2_OFFSET;
	g_tim_dev.tick_timer_ch[2].ccr = HPTC_TICKTIMER_BASE + STM32_ATIM_CCR3_OFFSET;
	g_tim_dev.tick_timer_ch[3].ccr = HPTC_TICKTIMER_BASE + STM32_ATIM_CCR4_OFFSET;

	g_tim_dev.tick_timer_ch[0].sr_ccif = ATIM_SR_CC1IF;
	g_tim_dev.tick_timer_ch[1].sr_ccif = ATIM_SR_CC2IF;
	g_tim_dev.tick_timer_ch[2].sr_ccif = ATIM_SR_CC3IF;
	g_tim_dev.tick_timer_ch[3].sr_ccif = ATIM_SR_CC4IF;

	g_tim_dev.tick_timer_ch[0].sr_ccof = ATIM_SR_CC1OF;
	g_tim_dev.tick_timer_ch[1].sr_ccof = ATIM_SR_CC2OF;
	g_tim_dev.tick_timer_ch[2].sr_ccof = ATIM_SR_CC3OF;
	g_tim_dev.tick_timer_ch[3].sr_ccof = ATIM_SR_CC4OF;

	g_tim_dev.tick_timer_ch[0].type = HPTC_TIMER_CHANNEL_TYPE_UNUSED;
	g_tim_dev.tick_timer_ch[1].type = HPTC_TIMER_CHANNEL_TYPE_UNUSED;
	g_tim_dev.tick_timer_ch[2].type = HPTC_TIMER_CHANNEL_TYPE_UNUSED;
	g_tim_dev.tick_timer_ch[3].type = HPTC_TIMER_CHANNEL_TYPE_UNUSED;

	g_tim_dev.tick_timer_ch[HPTC_TICKTIMER_CHANNEL_IDX].type |= HPTC_TIMER_CHANNEL_TYPE_TICKTIMER;

	/* channel 1 *********************************************************************/
#ifdef 	CONFIG_STM32_HPTC_TICKTIMER_CH1
	g_tim_dev.tick_timer_ch[0].delay_comp_ticks = CONFIG_STM32_HPTC_TICKTIMER_CH1_DELAY_COMP;
#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH1_OC
	g_tim_dev.tick_timer_ch[0].type  |= HPTC_TIMER_CHANNEL_TYPE_OC;
	g_tim_dev.tick_timer_ccer_shadow |= ATIM_CCER_CC1E;

#if defined(CONFIG_STM32_HPTC_TICKTIMER_CH1_OC_HIGH) | defined(CONFIG_STM32_HPTC_TICKTIMER_CH1_OC_WF_FALLING_EDGE)
	g_tim_dev.tick_timer_ccmr1_shadow |= ATIM_CCMR_MODE_OCREFHI << ATIM_CCMR1_OC1M_SHIFT;
#else
	g_tim_dev.tick_timer_ccmr1_shadow |= ATIM_CCMR_MODE_OCREFLO << ATIM_CCMR1_OC1M_SHIFT;
#endif

#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH1_IC
	g_tim_dev.tick_timer_ch[0].type |= HPTC_TIMER_CHANNEL_TYPE_IC;
	g_tim_dev.tick_timer_ccer_shadow |= ATIM_CCER_CC1E;

#if defined (CONFIG_STM32_HPTC_TICKTIMER_IC1_FALLING)
	g_tim_dev.tick_timer_ccer_shadow |= ATIM_CCER_CC1P;
#elif defined (CONFIG_STM32_HPTC_TICKTIMER_IC1_BOTH)
	g_tim_dev.tick_timer_ccer_shadow |= ATIM_CCER_CC1NP | ATIM_CCER_CC1P;
#endif

#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_IC1_DIRECT_TI
	g_tim_dev.tick_timer_ccmr1_shadow |= ATIM_CCMR_CCS_CCIN1 << ATIM_CCMR1_CC1S_SHIFT;//0x1;
#endif
#ifdef CONFIG_STM32_HPTC_TICKTIMER_IC1_INDIRECT_TI
	g_tim_dev.tick_timer_ccmr1_shadow |= ATIM_CCMR_CCS_CCIN2 << ATIM_CCMR1_CC1S_SHIFT;// 0x2;
#endif
#ifdef CONFIG_STM32_HPTC_TICKTIMER_IC1_TRC
	g_tim_dev.tick_timer_ccmr1_shadow |= ATIM_CCMR_CCS_CCINTRC << ATIM_CCMR1_CC1S_SHIFT;// 0x3;
#endif

	/* Input filter *********************************************************************/

#ifdef CONFIG_STM32_HPTC_TICKTIMER_IC1_FILTER
#  if defined (CONFIG_STM32_HPTC_TICKTIMER_IC1_SAMPLE_CKINT)
#    if defined (CONFIG_STM32_HPTC_TICKTIMER_IC1_SAMPLE_EVENT_2)
	g_tim_dev.tick_timer_ccmr1_shadow |= ATIM_CCMR_ICF_FCKINT2 << ATIM_CCMR1_IC1F_SHIFT;
#    elif   defined (CONFIG_STM32_HPTC_TICKTIMER_IC1_SAMPLE_EVENT_4)
	g_tim_dev.tick_timer_ccmr1_shadow |= ATIM_CCMR_ICF_FCKINT4 << ATIM_CCMR1_IC1F_SHIFT;
#    elif   defined (CONFIG_STM32_HPTC_TICKTIMER_IC1_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr1_shadow |= ATIM_CCMR_ICF_FCKINT8 << ATIM_CCMR1_IC1F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_SAMPLE_FDTS_2)
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_SAMPLE_EVENT_6)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd26 << ATIM_CCMR1_IC1F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd28 << ATIM_CCMR1_IC1F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_SAMPLE_FDTS_4)
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_SAMPLE_EVENT_6)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd46 << ATIM_CCMR1_IC1F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd48 << ATIM_CCMR1_IC1F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_SAMPLE_FDTS_8)
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_SAMPLE_EVENT_6)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd86 << ATIM_CCMR1_IC1F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd88 << ATIM_CCMR1_IC1F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_SAMPLE_FDTS_16)
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_SAMPLE_EVENT_5)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd165 << ATIM_CCMR1_IC1F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_SAMPLE_EVENT_6)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd166 << ATIM_CCMR1_IC1F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd168 << ATIM_CCMR1_IC1F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_SAMPLE_FDTS_32)
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_SAMPLE_EVENT_5)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd325 << ATIM_CCMR1_IC1F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_SAMPLE_EVENT_6)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd326 << ATIM_CCMR1_IC1F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC1_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd328 << ATIM_CCMR1_IC1F_SHIFT;
#    endif
#  endif
#endif
#endif /*CONFIG_STM32_HPTC_TICKTIMER_CH1 */


	/* channel 2 *********************************************************************/
#ifdef 	CONFIG_STM32_HPTC_TICKTIMER_CH2
	g_tim_dev.tick_timer_ch[1].delay_comp_ticks = CONFIG_STM32_HPTC_TICKTIMER_CH2_DELAY_COMP;
#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH2_OC
	g_tim_dev.tick_timer_ch[1].type  |= HPTC_TIMER_CHANNEL_TYPE_OC;
	g_tim_dev.tick_timer_ccer_shadow |= ATIM_CCER_CC2E;
#if defined(CONFIG_STM32_HPTC_TICKTIMER_CH2_OC_HIGH) | defined(CONFIG_STM32_HPTC_TICKTIMER_CH2_OC_WF_FALLING_EDGE)
	g_tim_dev.tick_timer_ccmr1_shadow |= ATIM_CCMR_MODE_OCREFHI << ATIM_CCMR1_OC2M_SHIFT;
#else
	g_tim_dev.tick_timer_ccmr1_shadow |= ATIM_CCMR_MODE_OCREFLO << ATIM_CCMR1_OC2M_SHIFT;
#endif

#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH2_IC
	g_tim_dev.tick_timer_ch[1].type |= HPTC_TIMER_CHANNEL_TYPE_IC;
	g_tim_dev.tick_timer_ccer_shadow |= ATIM_CCER_CC2E;

#if defined (CONFIG_STM32_HPTC_TICKTIMER_IC2_FALLING)
	g_tim_dev.tick_timer_ccer_shadow |= ATIM_CCER_CC2P;
#elif defined (CONFIG_STM32_HPTC_TICKTIMER_IC2_BOTH)
	g_tim_dev.tick_timer_ccer_shadow |= ATIM_CCER_CC2NP | ATIM_CCER_CC2P;
#endif

#endif


#ifdef CONFIG_STM32_HPTC_TICKTIMER_IC2_DIRECT_TI
	g_tim_dev.tick_timer_ccmr1_shadow |= ATIM_CCMR_CCS_CCIN1 << ATIM_CCMR1_CC2S_SHIFT;// 0x1;
#endif
#ifdef CONFIG_STM32_HPTC_TICKTIMER_IC2_INDIRECT_TI
	g_tim_dev.tick_timer_ccmr1_shadow |= ATIM_CCMR_CCS_CCIN2 << ATIM_CCMR1_CC2S_SHIFT;// 0x2;
#endif
#ifdef CONFIG_STM32_HPTC_TICKTIMER_IC2_TRC
	g_tim_dev.tick_timer_ccmr1_shadow |= ATIM_CCMR_CCS_CCINTRC << ATIM_CCMR1_CC2S_SHIFT;// 0x3;
#endif

	/* Input filter *********************************************************************/

#ifdef CONFIG_STM32_HPTC_TICKTIMER_IC2_FILTER
#  if defined (CONFIG_STM32_HPTC_TICKTIMER_IC2_SAMPLE_CKINT)
#    if defined (CONFIG_STM32_HPTC_TICKTIMER_IC2_SAMPLE_EVENT_2)
	g_tim_dev.tick_timer_ccmr1_shadow |= ATIM_CCMR_ICF_FCKINT2 << ATIM_CCMR1_IC2F_SHIFT;
#    elif   defined (CONFIG_STM32_HPTC_TICKTIMER_IC2_SAMPLE_EVENT_4)
	g_tim_dev.tick_timer_ccmr1_shadow |= ATIM_CCMR_ICF_FCKINT4 << ATIM_CCMR1_IC2F_SHIFT;
#    elif   defined (CONFIG_STM32_HPTC_TICKTIMER_IC2_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr1_shadow |= ATIM_CCMR_ICF_FCKINT8 << ATIM_CCMR1_IC2F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_SAMPLE_FDTS_2)
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_SAMPLE_EVENT_6)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd26 << ATIM_CCMR1_IC2F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd28 << ATIM_CCMR1_IC2F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_SAMPLE_FDTS_4)
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_SAMPLE_EVENT_6)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd46 << ATIM_CCMR1_IC2F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd48 << ATIM_CCMR1_IC2F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_SAMPLE_FDTS_8)
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_SAMPLE_EVENT_6)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd86 << ATIM_CCMR1_IC2F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd88 << ATIM_CCMR1_IC2F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_SAMPLE_FDTS_16)
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_SAMPLE_EVENT_5)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd165 << ATIM_CCMR1_IC2F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_SAMPLE_EVENT_6)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd166 << ATIM_CCMR1_IC2F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd168 << ATIM_CCMR1_IC2F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_SAMPLE_FDTS_32)
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_SAMPLE_EVENT_5)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd325 << ATIM_CCMR1_IC2F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_SAMPLE_EVENT_6)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd326 << ATIM_CCMR1_IC2F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC2_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd328 << ATIM_CCMR1_IC2F_SHIFT;
#    endif
#  endif
#endif
#endif /*CONFIG_STM32_HPTC_TICKTIMER_CH2




	/* channel 3 *********************************************************************/
#ifdef 	CONFIG_STM32_HPTC_TICKTIMER_CH3
	g_tim_dev.tick_timer_ch[2].delay_comp_ticks = CONFIG_STM32_HPTC_TICKTIMER_CH3_DELAY_COMP;
#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH3_OC
	g_tim_dev.tick_timer_ch[2].type |= HPTC_TIMER_CHANNEL_TYPE_OC;
	g_tim_dev.tick_timer_ccer_shadow |= ATIM_CCER_CC3E;

#if defined(CONFIG_STM32_HPTC_TICKTIMER_CH3_OC_HIGH) | defined(CONFIG_STM32_HPTC_TICKTIMER_CH3_OC_WF_FALLING_EDGE)
	g_tim_dev.tick_timer_ccmr2_shadow |= ATIM_CCMR_MODE_OCREFHI << ATIM_CCMR2_OC3M_SHIFT;
#else
	g_tim_dev.tick_timer_ccmr2_shadow |= ATIM_CCMR_MODE_OCREFLO << ATIM_CCMR2_OC3M_SHIFT;
#endif

#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH3_IC
	g_tim_dev.tick_timer_ch[2].type |= HPTC_TIMER_CHANNEL_TYPE_IC;
	g_tim_dev.tick_timer_ccer_shadow |= ATIM_CCER_CC3E;

#if defined (CONFIG_STM32_HPTC_TICKTIMER_IC3_FALLING)
	g_tim_dev.tick_timer_ccer_shadow |= ATIM_CCER_CC3P;
#elif defined (CONFIG_STM32_HPTC_TICKTIMER_IC3_BOTH)
	g_tim_dev.tick_timer_ccer_shadow |= ATIM_CCER_CC3NP | ATIM_CCER_CC3P;
#endif

#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_IC3_DIRECT_TI
	g_tim_dev.tick_timer_ccmr2_shadow |= ATIM_CCMR_CCS_CCIN1 << ATIM_CCMR2_CC3S_SHIFT;// 0x1;
#endif
#ifdef CONFIG_STM32_HPTC_TICKTIMER_IC3_INDIRECT_TI
	g_tim_dev.tick_timer_ccmr2_shadow |= ATIM_CCMR_CCS_CCIN2 << ATIM_CCMR2_CC3S_SHIFT;// 0x2;
#endif
#ifdef CONFIG_STM32_HPTC_TICKTIMER_IC3_TRC
	g_tim_dev.tick_timer_ccmr2_shadow |= ATIM_CCMR_CCS_CCINTRC << ATIM_CCMR2_CC3S_SHIFT;// 0x3;
#endif

	/* Input filter *********************************************************************/

#ifdef CONFIG_STM32_HPTC_TICKTIMER_IC3_FILTER
#  if defined (CONFIG_STM32_HPTC_TICKTIMER_IC3_SAMPLE_CKINT)
#    if defined (CONFIG_STM32_HPTC_TICKTIMER_IC3_SAMPLE_EVENT_2)
	g_tim_dev.tick_timer_ccmr2_shadow |= ATIM_CCMR_ICF_FCKINT2 << ATIM_CCMR2_IC3F_SHIFT;
#    elif   defined (CONFIG_STM32_HPTC_TICKTIMER_IC3_SAMPLE_EVENT_4)
	g_tim_dev.tick_timer_ccmr2_shadow |= ATIM_CCMR_ICF_FCKINT4 << ATIM_CCMR2_IC3F_SHIFT;
#    elif   defined (CONFIG_STM32_HPTC_TICKTIMER_IC3_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr2_shadow |= ATIM_CCMR_ICF_FCKINT8 << ATIM_CCMR2_IC3F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_SAMPLE_FDTS_2)
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_SAMPLE_EVENT_6)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd26 << ATIM_CCMR2_IC3F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd28 << ATIM_CCMR2_IC3F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_SAMPLE_FDTS_4)
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_SAMPLE_EVENT_6)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd46 << ATIM_CCMR2_IC3F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd48 << ATIM_CCMR2_IC3F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_SAMPLE_FDTS_8)
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_SAMPLE_EVENT_6)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd86 << ATIM_CCMR2_IC3F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd88 << ATIM_CCMR2_IC3F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_SAMPLE_FDTS_16)
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_SAMPLE_EVENT_5)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd165 << ATIM_CCMR2_IC3F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_SAMPLE_EVENT_6)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd166 << ATIM_CCMR2_IC3F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd168 << ATIM_CCMR2_IC3F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_SAMPLE_FDTS_32)
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_SAMPLE_EVENT_5)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd325 << ATIM_CCMR2_IC3F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_SAMPLE_EVENT_6)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd326 << ATIM_CCMR2_IC3F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC3_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd328 << ATIM_CCMR2_IC3F_SHIFT;
#    endif
#  endif
#endif
#endif /* CONFIG_STM32_HPTC_TICKTIMER_CH3 */

	/* channel 4 *********************************************************************/
#ifdef 	CONFIG_STM32_HPTC_TICKTIMER_CH4
	g_tim_dev.tick_timer_ch[3].delay_comp_ticks = CONFIG_STM32_HPTC_TICKTIMER_CH4_DELAY_COMP;
#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH4_OC
	g_tim_dev.tick_timer_ch[3].type  |= HPTC_TIMER_CHANNEL_TYPE_OC;
	g_tim_dev.tick_timer_ccer_shadow |= ATIM_CCER_CC4E;

#if defined(CONFIG_STM32_HPTC_TICKTIMER_CH4_OC_HIGH) | defined(CONFIG_STM32_HPTC_TICKTIMER_CH4_OC_WF_FALLING_EDGE)
	g_tim_dev.tick_timer_ccmr2_shadow |= ATIM_CCMR_MODE_OCREFHI << ATIM_CCMR2_OC4M_SHIFT;
#else
	g_tim_dev.tick_timer_ccmr2_shadow |= ATIM_CCMR_MODE_OCREFLO << ATIM_CCMR2_OC4M_SHIFT;
#endif

#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH4_IC
	g_tim_dev.tick_timer_ch[3].type |= HPTC_TIMER_CHANNEL_TYPE_IC;
	g_tim_dev.tick_timer_ccer_shadow |= ATIM_CCER_CC4E;

#if defined (CONFIG_STM32_HPTC_TICKTIMER_IC4_FALLING)
	g_tim_dev.tick_timer_ccer_shadow |= ATIM_CCER_CC4P;
#elif defined (CONFIG_STM32_HPTC_TICKTIMER_IC4_BOTH)
	g_tim_dev.tick_timer_ccer_shadow |= ATIM_CCER_CC4NP | ATIM_CCER_CC4P;
#endif

#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_IC4_DIRECT_TI
	g_tim_dev.tick_timer_ccmr2_shadow |= ATIM_CCMR_CCS_CCIN1 << ATIM_CCMR2_CC4S_SHIFT;//  0x1;
#endif
#ifdef CONFIG_STM32_HPTC_TICKTIMER_IC4_INDIRECT_TI
	g_tim_dev.tick_timer_ccmr2_shadow |= ATIM_CCMR_CCS_CCIN2 << ATIM_CCMR2_CC4S_SHIFT;//  0x2;
#endif
#ifdef CONFIG_STM32_HPTC_TICKTIMER_IC4_TRC
	g_tim_dev.tick_timer_ccmr2_shadow |= ATIM_CCMR_CCS_CCINTRC << ATIM_CCMR2_CC4S_SHIFT;//  0x3;
#endif

	/* Input filter *********************************************************************/

#ifdef CONFIG_STM32_HPTC_TICKTIMER_IC4_FILTER
#  if defined (CONFIG_STM32_HPTC_TICKTIMER_IC4_SAMPLE_CKINT)
#    if defined (CONFIG_STM32_HPTC_TICKTIMER_IC4_SAMPLE_EVENT_2)
	g_tim_dev.tick_timer_ccmr2_shadow |= ATIM_CCMR_ICF_FCKINT2 << ATIM_CCMR2_IC4F_SHIFT;
#    elif   defined (CONFIG_STM32_HPTC_TICKTIMER_IC4_SAMPLE_EVENT_4)
	g_tim_dev.tick_timer_ccmr2_shadow |= ATIM_CCMR_ICF_FCKINT4 << ATIM_CCMR2_IC4F_SHIFT;
#    elif   defined (CONFIG_STM32_HPTC_TICKTIMER_IC4_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr2_shadow |= ATIM_CCMR_ICF_FCKINT8 << ATIM_CCMR2_IC4F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_SAMPLE_FDTS_2)
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_SAMPLE_EVENT_6)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd26 << ATIM_CCMR2_IC4F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd28 << ATIM_CCMR2_IC4F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_SAMPLE_FDTS_4)
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_SAMPLE_EVENT_6)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd46 << ATIM_CCMR2_IC4F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd48 << ATIM_CCMR2_IC4F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_SAMPLE_FDTS_8)
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_SAMPLE_EVENT_6)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd86 << ATIM_CCMR2_IC4F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd88 << ATIM_CCMR2_IC4F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_SAMPLE_FDTS_16)
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_SAMPLE_EVENT_5)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd165 << ATIM_CCMR2_IC4F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_SAMPLE_EVENT_6)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd166 << ATIM_CCMR2_IC4F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd168 << ATIM_CCMR2_IC4F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_SAMPLE_FDTS_32)
#    if defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_SAMPLE_EVENT_5)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd325 << ATIM_CCMR2_IC4F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_SAMPLE_EVENT_6)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd326 << ATIM_CCMR2_IC4F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_TICKTIMER_IC4_SAMPLE_EVENT_8)
	g_tim_dev.tick_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd328 << ATIM_CCMR2_IC4F_SHIFT;
#    endif
#  endif
#endif
#endif /*CONFIG_STM32_HPTC_TICKTIMER_CH3*/
#ifdef HPTC_AUXTIMER



	g_tim_dev.aux_timer_ccmr1_shadow=0x0000;
	g_tim_dev.aux_timer_ccmr2_shadow=0x0000;

	g_tim_dev.aux_timer_ccer_shadow=0x0000;

	g_tim_dev.aux_timer_ch[0].ccr = HPTC_AUXTIMER_BASE + STM32_ATIM_CCR1_OFFSET;
	g_tim_dev.aux_timer_ch[1].ccr = HPTC_AUXTIMER_BASE + STM32_ATIM_CCR2_OFFSET;
	g_tim_dev.aux_timer_ch[2].ccr = HPTC_AUXTIMER_BASE + STM32_ATIM_CCR3_OFFSET;
	g_tim_dev.aux_timer_ch[3].ccr = HPTC_AUXTIMER_BASE + STM32_ATIM_CCR4_OFFSET;

	g_tim_dev.aux_timer_ch[0].sr_ccif = ATIM_SR_CC1IF;
	g_tim_dev.aux_timer_ch[1].sr_ccif = ATIM_SR_CC2IF;
	g_tim_dev.aux_timer_ch[2].sr_ccif = ATIM_SR_CC3IF;
	g_tim_dev.aux_timer_ch[3].sr_ccif = ATIM_SR_CC4IF;

	g_tim_dev.aux_timer_ch[0].sr_ccof = ATIM_SR_CC1OF;
	g_tim_dev.aux_timer_ch[1].sr_ccof = ATIM_SR_CC2OF;
	g_tim_dev.aux_timer_ch[2].sr_ccof = ATIM_SR_CC3OF;
	g_tim_dev.aux_timer_ch[3].sr_ccof = ATIM_SR_CC4OF;


	g_tim_dev.aux_timer_ch[0].type = HPTC_TIMER_CHANNEL_TYPE_UNUSED;
	g_tim_dev.aux_timer_ch[1].type = HPTC_TIMER_CHANNEL_TYPE_UNUSED;
	g_tim_dev.aux_timer_ch[2].type = HPTC_TIMER_CHANNEL_TYPE_UNUSED;
	g_tim_dev.aux_timer_ch[3].type = HPTC_TIMER_CHANNEL_TYPE_UNUSED;


	/* channel 1 *********************************************************************/
#ifdef 	CONFIG_STM32_HPTC_AUXTIMER_CH1
	g_tim_dev.aux_timer_ch[0].delay_comp_ticks = CONFIG_STM32_HPTC_AUXTIMER_CH1_DELAY_COMP;
	g_tim_dev.aux_timer_ch[0].async_in_cb      = NULL;
#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH1_OC
	g_tim_dev.aux_timer_ch[0].type  |= HPTC_TIMER_CHANNEL_TYPE_OC;
	g_tim_dev.aux_timer_ccer_shadow |= ATIM_CCER_CC1E;

#if defined(CONFIG_STM32_HPTC_AUXTIMER_CH1_OC_HIGH) | defined(CONFIG_STM32_HPTC_AUXTIMER_CH1_OC_WF_FALLING_EDGE)
	g_tim_dev.aux_timer_ccmr1_shadow |= ATIM_CCMR_MODE_OCREFHI << ATIM_CCMR1_OC1M_SHIFT;
#else
	g_tim_dev.aux_timer_ccmr1_shadow |= ATIM_CCMR_MODE_OCREFLO << ATIM_CCMR1_OC1M_SHIFT;
#endif

#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH1_IC
	g_tim_dev.aux_timer_ch[0].type = HPTC_TIMER_CHANNEL_TYPE_IC;
	g_tim_dev.aux_timer_ccer_shadow |= ATIM_CCER_CC1E;

#if defined (CONFIG_STM32_HPTC_AUXTIMER_IC1_FALLING)
	g_tim_dev.aux_timer_ccer_shadow |= ATIM_CCER_CC1P;
#elif defined (CONFIG_STM32_HPTC_AUXTIMER_IC1_BOTH)
	g_tim_dev.aux_timer_ccer_shadow |= ATIM_CCER_CC1NP | ATIM_CCER_CC1P;
#endif

#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC1_DIRECT_TI
	g_tim_dev.aux_timer_ccmr1_shadow |= ATIM_CCMR_CCS_CCIN1 << ATIM_CCMR1_CC1S_SHIFT;//0x1;
#endif
#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC1_INDIRECT_TI
	g_tim_dev.aux_timer_ccmr1_shadow |= ATIM_CCMR_CCS_CCIN2 << ATIM_CCMR1_CC1S_SHIFT;// 0x2;
#endif
#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC1_TRC
	g_tim_dev.aux_timer_ccmr1_shadow |= ATIM_CCMR_CCS_CCINTRC << ATIM_CCMR1_CC1S_SHIFT;// 0x3;
#endif

	/* Input filter *********************************************************************/

#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC1_FILTER
#  if defined (CONFIG_STM32_HPTC_AUXTIMER_IC1_SAMPLE_CKINT)
#    if defined (CONFIG_STM32_HPTC_AUXTIMER_IC1_SAMPLE_EVENT_2)
	g_tim_dev.aux_timer_ccmr1_shadow |= ATIM_CCMR_ICF_FCKINT2 << ATIM_CCMR1_IC1F_SHIFT;
#    elif   defined (CONFIG_STM32_HPTC_AUXTIMER_IC1_SAMPLE_EVENT_4)
	g_tim_dev.aux_timer_ccmr1_shadow |= ATIM_CCMR_ICF_FCKINT4 << ATIM_CCMR1_IC1F_SHIFT;
#    elif   defined (CONFIG_STM32_HPTC_AUXTIMER_IC1_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr1_shadow |= ATIM_CCMR_ICF_FCKINT8 << ATIM_CCMR1_IC1F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC1_SAMPLE_FDTS_2)
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC1_SAMPLE_EVENT_6)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd26 << ATIM_CCMR1_IC1F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC1_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd28 << ATIM_CCMR1_IC1F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC1_SAMPLE_FDTS_4)
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC1_SAMPLE_EVENT_6)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd46 << ATIM_CCMR1_IC1F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC1_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd48 << ATIM_CCMR1_IC1F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC1_SAMPLE_FDTS_8)
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC1_SAMPLE_EVENT_6)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd86 << ATIM_CCMR1_IC1F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC1_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd88 << ATIM_CCMR1_IC1F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC1_SAMPLE_FDTS_16)
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC1_SAMPLE_EVENT_5)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd165 << ATIM_CCMR1_IC1F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC1_SAMPLE_EVENT_6)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd166 << ATIM_CCMR1_IC1F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC1_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd168 << ATIM_CCMR1_IC1F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC1_SAMPLE_FDTS_32)
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC1_SAMPLE_EVENT_5)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd325 << ATIM_CCMR1_IC1F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC1_SAMPLE_EVENT_6)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd326 << ATIM_CCMR1_IC1F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC1_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd328 << ATIM_CCMR1_IC1F_SHIFT;
#    endif
#  endif
#endif
#endif /* 	CONFIG_STM32_HPTC_AUXTIMER_CH1*/

	/* channel 2 *********************************************************************/
#ifdef 	CONFIG_STM32_HPTC_AUXTIMER_CH2
	g_tim_dev.aux_timer_ch[1].delay_comp_ticks = CONFIG_STM32_HPTC_AUXTIMER_CH2_DELAY_COMP;
	g_tim_dev.aux_timer_ch[1].async_in_cb      = NULL;
#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH2_OC
	g_tim_dev.aux_timer_ch[1].type  |= HPTC_TIMER_CHANNEL_TYPE_OC;
	g_tim_dev.aux_timer_ccer_shadow |= ATIM_CCER_CC2E;

#if defined(CONFIG_STM32_HPTC_AUXTIMER_CH2_OC_HIGH) | defined(CONFIG_STM32_HPTC_AUXTIMER_CH2_OC_WF_FALLING_EDGE)
	g_tim_dev.aux_timer_ccmr1_shadow |= ATIM_CCMR_MODE_OCREFHI << ATIM_CCMR1_OC2M_SHIFT;
#else
	g_tim_dev.aux_timer_ccmr1_shadow |= ATIM_CCMR_MODE_OCREFLO << ATIM_CCMR1_OC2M_SHIFT;
#endif

#endif
#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH2_IC
	g_tim_dev.aux_timer_ch[1].type = HPTC_TIMER_CHANNEL_TYPE_IC;
	g_tim_dev.aux_timer_ccer_shadow |= ATIM_CCER_CC2E;

#if defined (CONFIG_STM32_HPTC_AUXTIMER_IC2_FALLING)
	g_tim_dev.aux_timer_ccer_shadow |= ATIM_CCER_CC2P;
#elif defined (CONFIG_STM32_HPTC_AUXTIMER_IC2_BOTH)
	g_tim_dev.aux_timer_ccer_shadow |= ATIM_CCER_CC2NP | ATIM_CCER_CC2P;
#endif

#endif



#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC2_DIRECT_TI
	g_tim_dev.aux_timer_ccmr1_shadow |= ATIM_CCMR_CCS_CCIN1 << ATIM_CCMR1_CC2S_SHIFT;// 0x1;
#endif
#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC2_INDIRECT_TI
	g_tim_dev.aux_timer_ccmr1_shadow |= ATIM_CCMR_CCS_CCIN2 << ATIM_CCMR1_CC2S_SHIFT;// 0x2;
#endif
#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC2_TRC
	g_tim_dev.aux_timer_ccmr1_shadow |= ATIM_CCMR_CCS_CCINTRC << ATIM_CCMR1_CC2S_SHIFT;// 0x3;
#endif

	/* Input filter *********************************************************************/

#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC2_FILTER
#  if defined (CONFIG_STM32_HPTC_AUXTIMER_IC2_SAMPLE_CKINT)
#    if defined (CONFIG_STM32_HPTC_AUXTIMER_IC2_SAMPLE_EVENT_2)
	g_tim_dev.aux_timer_ccmr1_shadow |= ATIM_CCMR_ICF_FCKINT2 << ATIM_CCMR1_IC2F_SHIFT;
#    elif   defined (CONFIG_STM32_HPTC_AUXTIMER_IC2_SAMPLE_EVENT_4)
	g_tim_dev.aux_timer_ccmr1_shadow |= ATIM_CCMR_ICF_FCKINT4 << ATIM_CCMR1_IC2F_SHIFT;
#    elif   defined (CONFIG_STM32_HPTC_AUXTIMER_IC2_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr1_shadow |= ATIM_CCMR_ICF_FCKINT8 << ATIM_CCMR1_IC2F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC2_SAMPLE_FDTS_2)
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC2_SAMPLE_EVENT_6)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd26 << ATIM_CCMR1_IC2F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC2_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd28 << ATIM_CCMR1_IC2F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC2_SAMPLE_FDTS_4)
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC2_SAMPLE_EVENT_6)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd46 << ATIM_CCMR1_IC2F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC2_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd48 << ATIM_CCMR1_IC2F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC2_SAMPLE_FDTS_8)
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC2_SAMPLE_EVENT_6)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd86 << ATIM_CCMR1_IC2F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC2_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd88 << ATIM_CCMR1_IC2F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC2_SAMPLE_FDTS_16)
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC2_SAMPLE_EVENT_5)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd165 << ATIM_CCMR1_IC2F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC2_SAMPLE_EVENT_6)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd166 << ATIM_CCMR1_IC2F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC2_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd168 << ATIM_CCMR1_IC2F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC2_SAMPLE_FDTS_32)
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC2_SAMPLE_EVENT_5)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd325 << ATIM_CCMR1_IC2F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC2_SAMPLE_EVENT_6)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd326 << ATIM_CCMR1_IC2F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC2_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr1_shadow  |= ATIM_CCMR_ICF_FDTSd328 << ATIM_CCMR1_IC2F_SHIFT;
#    endif
#  endif
#endif
#endif /* CONFIG_STM32_HPTC_AUXTIMER_CH2 */


	/* channel 3 *********************************************************************/
#ifdef 	CONFIG_STM32_HPTC_AUXTIMER_CH3
	g_tim_dev.aux_timer_ch[2].delay_comp_ticks = CONFIG_STM32_HPTC_AUXTIMER_CH3_DELAY_COMP;
	g_tim_dev.aux_timer_ch[2].async_in_cb      = NULL;
#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH3_OC
	g_tim_dev.aux_timer_ch[2].type  |= HPTC_TIMER_CHANNEL_TYPE_OC;
	g_tim_dev.aux_timer_ccer_shadow |= ATIM_CCER_CC3E;

#if defined(CONFIG_STM32_HPTC_AUXTIMER_CH3_OC_HIGH) | defined(CONFIG_STM32_HPTC_AUXTIMER_CH3_OC_WF_FALLING_EDGE)
	g_tim_dev.aux_timer_ccmr2_shadow |= ATIM_CCMR_MODE_OCREFHI << ATIM_CCMR2_OC3M_SHIFT;
#else
	g_tim_dev.aux_timer_ccmr2_shadow |= ATIM_CCMR_MODE_OCREFLO << ATIM_CCMR2_OC3M_SHIFT;
#endif

#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH3_IC
	g_tim_dev.aux_timer_ch[2].type = HPTC_TIMER_CHANNEL_TYPE_IC;
	g_tim_dev.aux_timer_ccer_shadow |= ATIM_CCER_CC3E;

#if defined (CONFIG_STM32_HPTC_AUXTIMER_IC3_FALLING)
	g_tim_dev.aux_timer_ccer_shadow |= ATIM_CCER_CC3P;
#elif defined (CONFIG_STM32_HPTC_AUXTIMER_IC3_BOTH)
	g_tim_dev.aux_timer_ccer_shadow |= ATIM_CCER_CC3NP | ATIM_CCER_CC3P;
#endif

#endif


#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC3_DIRECT_TI
	g_tim_dev.aux_timer_ccmr2_shadow |= ATIM_CCMR_CCS_CCIN1 << ATIM_CCMR2_CC3S_SHIFT;//  0x1;
#endif
#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC3_INDIRECT_TI
	g_tim_dev.aux_timer_ccmr2_shadow |= ATIM_CCMR_CCS_CCIN2 << ATIM_CCMR2_CC3S_SHIFT;//  0x2;
#endif
#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC3_TRC
	g_tim_dev.aux_timer_ccmr2_shadow |= ATIM_CCMR_CCS_CCINTRC << ATIM_CCMR2_CC3S_SHIFT;//  0x3;
#endif

	/* Input filter *********************************************************************/

#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC3_FILTER
#  if defined (CONFIG_STM32_HPTC_AUXTIMER_IC3_SAMPLE_CKINT)
#    if defined (CONFIG_STM32_HPTC_AUXTIMER_IC3_SAMPLE_EVENT_2)
	g_tim_dev.aux_timer_ccmr2_shadow |= ATIM_CCMR_ICF_FCKINT2 << ATIM_CCMR2_IC3F_SHIFT;
#    elif   defined (CONFIG_STM32_HPTC_AUXTIMER_IC3_SAMPLE_EVENT_4)
	g_tim_dev.aux_timer_ccmr2_shadow |= ATIM_CCMR_ICF_FCKINT4 << ATIM_CCMR2_IC3F_SHIFT;
#    elif   defined (CONFIG_STM32_HPTC_AUXTIMER_IC3_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr2_shadow |= ATIM_CCMR_ICF_FCKINT8 << ATIM_CCMR2_IC3F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC3_SAMPLE_FDTS_2)
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC3_SAMPLE_EVENT_6)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd26 << ATIM_CCMR2_IC3F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC3_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd28 << ATIM_CCMR2_IC3F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC3_SAMPLE_FDTS_4)
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC3_SAMPLE_EVENT_6)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd46 << ATIM_CCMR2_IC3F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC3_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd48 << ATIM_CCMR2_IC3F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC3_SAMPLE_FDTS_8)
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC3_SAMPLE_EVENT_6)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd86 << ATIM_CCMR2_IC3F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC3_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd88 << ATIM_CCMR2_IC3F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC3_SAMPLE_FDTS_16)
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC3_SAMPLE_EVENT_5)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd165 << ATIM_CCMR2_IC3F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC3_SAMPLE_EVENT_6)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd166 << ATIM_CCMR2_IC3F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC3_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd168 << ATIM_CCMR2_IC3F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC3_SAMPLE_FDTS_32)
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC3_SAMPLE_EVENT_5)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd325 << ATIM_CCMR2_IC3F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC3_SAMPLE_EVENT_6)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd326 << ATIM_CCMR2_IC3F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC3_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd328 << ATIM_CCMR2_IC3F_SHIFT;
#    endif
#  endif
#endif
#endif /*CONFIG_STM32_HPTC_AUXTIMER_CH3*/

	/* channel 4 *********************************************************************/
#ifdef 	CONFIG_STM32_HPTC_AUXTIMER_CH4
	g_tim_dev.aux_timer_ch[3].delay_comp_ticks = CONFIG_STM32_HPTC_AUXTIMER_CH4_DELAY_COMP;
	g_tim_dev.aux_timer_ch[3].async_in_cb      = NULL;
#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH4_OC
	g_tim_dev.aux_timer_ch[3].type  |= HPTC_TIMER_CHANNEL_TYPE_OC;
	g_tim_dev.aux_timer_ccer_shadow |= ATIM_CCER_CC4E;

#if defined(CONFIG_STM32_HPTC_AUXTIMER_CH4_OC_HIGH) | defined(CONFIG_STM32_HPTC_AUXTIMER_CH4_OC_WF_FALLING_EDGE)
	g_tim_dev.aux_timer_ccmr2_shadow |= ATIM_CCMR_MODE_OCREFHI << ATIM_CCMR2_OC4M_SHIFT;
#else
	g_tim_dev.aux_timer_ccmr2_shadow |= ATIM_CCMR_MODE_OCREFLO << ATIM_CCMR2_OC4M_SHIFT;
#endif

#endif
#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH4_IC
#  ifndef  CONFIG_STM32_HPTC_AUXTIMER_IC4_ASYNC_IRQ
	g_tim_dev.aux_timer_ch[3].type = HPTC_TIMER_CHANNEL_TYPE_IC;
#  endif
	g_tim_dev.aux_timer_ccer_shadow |= ATIM_CCER_CC4E;

#if defined (CONFIG_STM32_HPTC_AUXTIMER_IC4_FALLING)
	g_tim_dev.aux_timer_ccer_shadow |= ATIM_CCER_CC4P;
#elif defined (CONFIG_STM32_HPTC_AUXTIMER_IC4_BOTH)
	g_tim_dev.aux_timer_ccer_shadow |= ATIM_CCER_CC4NP | ATIM_CCER_CC4P;
#endif

#endif


#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC4_DIRECT_TI
	g_tim_dev.aux_timer_ccmr2_shadow |= ATIM_CCMR_CCS_CCIN1 << ATIM_CCMR2_CC4S_SHIFT;//0x1;
#endif
#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC4_INDIRECT_TI
	g_tim_dev.aux_timer_ccmr2_shadow |= ATIM_CCMR_CCS_CCIN2 << ATIM_CCMR2_CC4S_SHIFT;// 0x2;
#endif
#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC4_TRC
	g_tim_dev.aux_timer_ccmr2_shadow |= ATIM_CCMR_CCS_CCINTRC << ATIM_CCMR2_CC4S_SHIFT;// 0x3;
#endif

	/* Input filter *********************************************************************/

#ifdef CONFIG_STM32_HPTC_AUXTIMER_IC4_FILTER
#  if defined (CONFIG_STM32_HPTC_AUXTIMER_IC4_SAMPLE_CKINT)
#    if defined (CONFIG_STM32_HPTC_AUXTIMER_IC4_SAMPLE_EVENT_2)
	g_tim_dev.aux_timer_ccmr2_shadow |= ATIM_CCMR_ICF_FCKINT2 << ATIM_CCMR2_IC4F_SHIFT;
#    elif   defined (CONFIG_STM32_HPTC_AUXTIMER_IC4_SAMPLE_EVENT_4)
	g_tim_dev.aux_timer_ccmr2_shadow |= ATIM_CCMR_ICF_FCKINT4 << ATIM_CCMR2_IC4F_SHIFT;
#    elif   defined (CONFIG_STM32_HPTC_AUXTIMER_IC4_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr2_shadow |= ATIM_CCMR_ICF_FCKINT8 << ATIM_CCMR2_IC4F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC4_SAMPLE_FDTS_2)
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC4_SAMPLE_EVENT_6)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd26 << ATIM_CCMR2_IC4F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC4_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd28 << ATIM_CCMR2_IC4F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC4_SAMPLE_FDTS_4)
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC4_SAMPLE_EVENT_6)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd46 << ATIM_CCMR2_IC4F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC4_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd48 << ATIM_CCMR2_IC4F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC4_SAMPLE_FDTS_8)
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC4_SAMPLE_EVENT_6)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd86 << ATIM_CCMR2_IC4F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC4_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd88 << ATIM_CCMR2_IC4F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC4_SAMPLE_FDTS_16)
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC4_SAMPLE_EVENT_5)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd165 << ATIM_CCMR2_IC4F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC4_SAMPLE_EVENT_6)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd166 << ATIM_CCMR2_IC4F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC4_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd168 << ATIM_CCMR2_IC4F_SHIFT;
#    endif
#  elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC4_SAMPLE_FDTS_32)
#    if defined(CONFIG_STM32_HPTC_AUXTIMER_IC4_SAMPLE_EVENT_5)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd325 << ATIM_CCMR2_IC4F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC4_SAMPLE_EVENT_6)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd326 << ATIM_CCMR2_IC4F_SHIFT;
#    elif defined(CONFIG_STM32_HPTC_AUXTIMER_IC4_SAMPLE_EVENT_8)
	g_tim_dev.aux_timer_ccmr2_shadow  |= ATIM_CCMR_ICF_FDTSd328 << ATIM_CCMR2_IC4F_SHIFT;
#    endif
#  endif
#endif
#endif /*CONFIG_STM32_HPTC_AUXTIMER_CH1*/



#endif //HPTC_AUXTIMER


	//initialize systime table
	for(int i=0;i<SYSTIME_TABLE_SIZE;i++){
		g_systime_table[i].systick=~0; //max value
	}

	g_systick_buffer_cmd_active->systick=1;
	//g_systick_buffer_active->ticks_per_systick=HPTC_INITIAL_TICKS_PER_SYSTICK;
	for(int i=0;i<HPTC_TICKTIMER_CHANNELS;i++){
		if(g_tim_dev.tick_timer_ch[i].type & HPTC_TIMER_CHANNEL_TYPE_TICKTIMER){
			for(int j=0; j< CONFIG_HPTC_HP_OVERSAMPLE; j++){
				g_systick_buffer_cmd_active->tick_timer[i][j].ticks = HPTC_INITIAL_TICKS_PER_SYSTICK*(j+1)/CONFIG_HPTC_HP_OVERSAMPLE;
			}
		}
	}

	//attach and prioritize high priority hptc tick handler

	int ret;


	ret = up_ramvec_attach(HPTC_TICK_IRQ, stm32_tick_tim_handler);
	if (ret < 0)
	{
		fprintf(stderr, "highpri_main: ERROR: up_ramvec_attach failed: %d\n", ret);
		return; // -EPERM;
	}

	/* Set the priority of the HPTC interrupt vector */

	ret = up_prioritize_irq(HPTC_TICK_IRQ, NVIC_SYSH_HIGH_PRIORITY);
	if (ret < 0)
	{
		//todo: detach vector
		fprintf(stderr, "highpri_main: ERROR: up_prioritize_irq failed: %d\n", ret);
		return; // -EPERM;
	}


	/* Leave SysTick Timer disabled, will be triggered from HPTC Timer ISR:
	 *
	 *   NVIC_SYSTICK_CTRL_CLKSOURCE=0 : Use the implementation defined clock
	 *                                   source which, for the STM32, will be
	 *                                   HCLK/8
	 *   NVIC_SYSTICK_CTRL_TICKINT=1   : Generate interrupts
	 *   NVIC_SYSTICK_CTRL_ENABLE      : Enable the counter
	 */

	putreg32(0,NVIC_SYSTICK_CTRL);

	/* Attach the timer interrupt vector */

	//XXX replaced  irq_attach(STM32_IRQ_PENDSV,(xcpt_t )stm32_hptc_pendsv_handler, (FAR void *)dev);
	//(void)irq_attach(STM32_IRQ_SYSTICK, (xcpt_t)stm32_systick_isr, NULL);
	(void)irq_attach(STM32_IRQ_SYSTICK, (xcpt_t)stm32_systick_isr, (FAR void *)dev);
#ifdef HPTC_AUXTIMER
	(void)irq_attach(HPTC_AUX_IRQ, (xcpt_t)stm32_async_isr, (FAR void *)dev);
#endif //HPTC_AUXTIMER
	/* And enable the timer interrupt */

	up_enable_irq(STM32_IRQ_SYSTICK);

#ifdef HPTC_AUXTIMER
	up_enable_irq(HPTC_AUX_IRQ);
#endif //HPTC_AUXTIMER
	/* Start and enable the HPTC timer and high priority interrupt */
	stm32_cnt_start(priv);

	/* reconfigure TRC after synchronized start */

#ifdef CONFIG_STM32_HPTC_TICKTIMER_TRC_ITR0
	modifyreg16(HPTC_TICKTIMER_BASE + STM32_ATIM_SMCR_OFFSET,ATIM_SMCR_TS_MASK,ATIM_SMCR_ITR0);
#endif
#ifdef CONFIG_STM32_HPTC_TICKTIMER_TRC_ITR1
	modifyreg16(HPTC_TICKTIMER_BASE + STM32_ATIM_SMCR_OFFSET,ATIM_SMCR_TS_MASK,ATIM_SMCR_ITR1);
#endif
#ifdef CONFIG_STM32_HPTC_TICKTIMER_TRC_ITR2
	modifyreg16(HPTC_TICKTIMER_BASE + STM32_ATIM_SMCR_OFFSET,ATIM_SMCR_TS_MASK,ATIM_SMCR_ITR2);
#endif
#ifdef CONFIG_STM32_HPTC_TICKTIMER_TRC_ITR3
	modifyreg16(HPTC_TICKTIMER_BASE + STM32_ATIM_SMCR_OFFSET,ATIM_SMCR_TS_MASK,ATIM_SMCR_ITR3);
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_TRC_ITR0
	modifyreg16(HPTC_AUXTIMER_BASE + STM32_ATIM_SMCR_OFFSET,ATIM_SMCR_TS_MASK,ATIM_SMCR_ITR0);
#endif
#ifdef CONFIG_STM32_HPTC_AUXTIMER_TRC_ITR1
	modifyreg16(HPTC_AUXTIMER_BASE + STM32_ATIM_SMCR_OFFSET,ATIM_SMCR_TS_MASK,ATIM_SMCR_ITR1);
#endif
#ifdef CONFIG_STM32_HPTC_AUXTIMER_TRC_ITR2
	modifyreg16(HPTC_AUXTIMER_BASE + STM32_ATIM_SMCR_OFFSET,ATIM_SMCR_TS_MASK,ATIM_SMCR_ITR2);
#endif
#ifdef CONFIG_STM32_HPTC_AUXTIMER_TRC_ITR3
	modifyreg16(HPTC_AUXTIMER_BASE + STM32_ATIM_SMCR_OFFSET,ATIM_SMCR_TS_MASK,ATIM_SMCR_ITR3);
#endif




	/* reconfigure TRGO after synchronized start of timer */

#ifdef CONFIG_STM32_HPTC_TICKTIMER_TRGO_CC1IF
	modifyreg16(HPTC_TICKTIMER_BASE + STM32_ATIM_CR2_OFFSET,ATIM_CR2_MMS_MASK,ATIM_CR2_MMS_COMPP);
#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_TRGO_OC1REF
	modifyreg16(HPTC_TICKTIMER_BASE + STM32_ATIM_CR2_OFFSET,ATIM_CR2_MMS_MASK,ATIM_CR2_MMS_OC1REF);
#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_TRGO_OC2REF
	modifyreg16(HPTC_TICKTIMER_BASE + STM32_ATIM_CR2_OFFSET,ATIM_CR2_MMS_MASK,ATIM_CR2_MMS_OC2REF);
#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_TRGO_OC3REF
	modifyreg16(HPTC_TICKTIMER_BASE + STM32_ATIM_CR2_OFFSET,ATIM_CR2_MMS_MASK,ATIM_CR2_MMS_OC3REF);
#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_TRGO_OC4REF
	modifyreg16(HPTC_TICKTIMER_BASE + STM32_ATIM_CR2_OFFSET,ATIM_CR2_MMS_MASK,ATIM_CR2_MMS_OC4REF);
#endif


#ifdef CONFIG_STM32_HPTC_AUXTIMER_TRGO_CC1IF
	modifyreg16(HPTC_AUXTIMER_BASE + STM32_ATIM_CR2_OFFSET,ATIM_CR2_MMS_MASK,ATIM_CR2_MMS_COMPP);
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_TRGO_OC1REF
	modifyreg16(HPTC_AUXTIMER_BASE + STM32_ATIM_CR2_OFFSET,ATIM_CR2_MMS_MASK,ATIM_CR2_MMS_OC1REF);
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_TRGO_OC2REF
	modifyreg16(HPTC_AUXTIMER_BASE + STM32_ATIM_CR2_OFFSET,ATIM_CR2_MMS_MASK,ATIM_CR2_MMS_OC2REF);
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_TRGO_OC3REF
	modifyreg16(HPTC_AUXTIMER_BASE + STM32_ATIM_CR2_OFFSET,ATIM_CR2_MMS_MASK,ATIM_CR2_MMS_OC3REF);
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_TRGO_OC4REF
	modifyreg16(HPTC_AUXTIMER_BASE + STM32_ATIM_CR2_OFFSET,ATIM_CR2_MMS_MASK,ATIM_CR2_MMS_OC4REF);
#endif

	stm32_tick_start(priv);


}



int up_io_initialize(struct hptc_io_dev_s* dev){
	struct stm32_tick_timer_dev_channel_s *chan = (struct stm32_tick_timer_dev_channel_s*)dev->priv;
//tmrinfo("io_i\n");
	chan->iodev=dev;

	if (chan->type & HPTC_TIMER_CHANNEL_TYPE_OC){
		//condition wf->time
		while (chan->wf.time_ns < (HPTC_NS_PER_TIMTICK-CONFIG_HPTC_OC_WINDOW_NS)){
			chan->wf.time_ns += NS_PER_SEC;
		}
		while (chan->wf.time_ns >= (NS_PER_SEC+HPTC_NS_PER_TIMTICK-CONFIG_HPTC_OC_WINDOW_NS)){
			chan->wf.time_ns -= NS_PER_SEC;
		}
	}

	if (chan->type &HPTC_TIMER_CHANNEL_TYPE_IC){

	}
	chan->type |= HPTC_TIMER_CHANNEL_TYPE_ACTIVE;

    return 0;
}

void stm32_hptc_io_dev_initialize(){
//tmrinfo("iodev_i\n");
	struct hptc_oc_wf_s *wf;
	int16_t ic_bufsize;
	int16_t oc_bufsize;

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH1
	g_hptc_io_dev_ticktimer_ch1.priv = &g_tim_dev.tick_timer_ch[0];
	g_hptc_io_dev_ticktimer_ch1.ops = &g_hptc_io_ops;

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH1_IC_BUFSIZE
	ic_bufsize=CONFIG_STM32_HPTC_TICKTIMER_CH1_IC_BUFSIZE;
#else
    ic_bufsize=0;
#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH1_OC_BUFSIZE
	oc_bufsize=CONFIG_STM32_HPTC_TICKTIMER_CH1_OC_BUFSIZE;
#else
    oc_bufsize=0;
#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH1_OC_WF

	wf = &g_tim_dev.tick_timer_ch[0].wf;

	hptc_oc_wf_condition(wf,CONFIG_STM32_HPTC_TICKTIMER_CH1_OC_WF_RISING_EDGE,
	             CONFIG_STM32_HPTC_TICKTIMER_CH1_OC_WF_START_S,
	             CONFIG_STM32_HPTC_TICKTIMER_CH1_OC_WF_START_NS,
	             CONFIG_STM32_HPTC_TICKTIMER_CH1_OC_WF_PULSE_WIDTH_S,
	             CONFIG_STM32_HPTC_TICKTIMER_CH1_OC_WF_PULSE_WIDTH_NS,
	             CONFIG_STM32_HPTC_TICKTIMER_CH1_OC_WF_PERIOD_S,
	             CONFIG_STM32_HPTC_TICKTIMER_CH1_OC_WF_PERIOD_NS,
	             CONFIG_STM32_HPTC_TICKTIMER_CH1_OC_WF_PERIOD_NUMERATOR_NS,
	             CONFIG_STM32_HPTC_TICKTIMER_CH1_OC_WF_PERIOD_DENOMINATOR_NS);

#endif  //CONFIG_STM32_HPTC_TICKTIMER_CH1_OC_WF

	hptc_io_register(CONFIG_STM32_HPTC_TICKTIMER_CH1_DEVPATH,&g_hptc_io_dev_ticktimer_ch1, ic_bufsize, oc_bufsize);
#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH2

	g_hptc_io_dev_ticktimer_ch2.priv = &g_tim_dev.tick_timer_ch[1];
	g_hptc_io_dev_ticktimer_ch2.ops = &g_hptc_io_ops;

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH2_IC_BUFSIZE
	ic_bufsize=CONFIG_STM32_HPTC_TICKTIMER_CH2_IC_BUFSIZE;
#else
    ic_bufsize=0;
#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH2_OC_BUFSIZE
	oc_bufsize=CONFIG_STM32_HPTC_TICKTIMER_CH2_OC_BUFSIZE;
#else
    oc_bufsize=0;
#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH2_OC_WF

    hptc_oc_wf_condition(wf,CONFIG_STM32_HPTC_TICKTIMER_CH2_OC_WF_RISING_EDGE,
                 CONFIG_STM32_HPTC_TICKTIMER_CH2_OC_WF_START_S,
                 CONFIG_STM32_HPTC_TICKTIMER_CH2_OC_WF_START_NS,
                 CONFIG_STM32_HPTC_TICKTIMER_CH2_OC_WF_PULSE_WIDTH_S,
                 CONFIG_STM32_HPTC_TICKTIMER_CH2_OC_WF_PULSE_WIDTH_NS,
                 CONFIG_STM32_HPTC_TICKTIMER_CH2_OC_WF_PERIOD_S,
                 CONFIG_STM32_HPTC_TICKTIMER_CH2_OC_WF_PERIOD_NS,
                 CONFIG_STM32_HPTC_TICKTIMER_CH2_OC_WF_PERIOD_NUMERATOR_NS,
                 CONFIG_STM32_HPTC_TICKTIMER_CH2_OC_WF_PERIOD_DENOMINATOR_NS);

#endif  //CONFIG_STM32F7_HPTC_TICKTIMER_CH2_OC_WF

	hptc_io_register(CONFIG_STM32_HPTC_TICKTIMER_CH2_DEVPATH,&g_hptc_io_dev_ticktimer_ch2, ic_bufsize, oc_bufsize);

#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH3

	g_hptc_io_dev_ticktimer_ch3.priv = &g_tim_dev.tick_timer_ch[2];
	g_hptc_io_dev_ticktimer_ch3.ops = &g_hptc_io_ops;

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH3_IC_BUFSIZE
	ic_bufsize=CONFIG_STM32_HPTC_TICKTIMER_CH3_IC_BUFSIZE;
#else
    ic_bufsize=0;
#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH3_OC_BUFSIZE
	oc_bufsize=CONFIG_STM32_HPTC_TICKTIMER_CH3_OC_BUFSIZE;
#else
    oc_bufsize=0;
#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH3_OC_WF

	wf = &g_tim_dev.tick_timer_ch[2].wf;
        hptc_oc_wf_condition(wf,CONFIG_STM32_HPTC_TICKTIMER_CH3_OC_WF_RISING_EDGE,
                     CONFIG_STM32_HPTC_TICKTIMER_CH3_OC_WF_START_S,
                     CONFIG_STM32_HPTC_TICKTIMER_CH3_OC_WF_START_NS,
                     CONFIG_STM32_HPTC_TICKTIMER_CH3_OC_WF_PULSE_WIDTH_S,
                     CONFIG_STM32_HPTC_TICKTIMER_CH3_OC_WF_PULSE_WIDTH_NS,
                     CONFIG_STM32_HPTC_TICKTIMER_CH3_OC_WF_PERIOD_S,
                     CONFIG_STM32_HPTC_TICKTIMER_CH3_OC_WF_PERIOD_NS,
                     CONFIG_STM32_HPTC_TICKTIMER_CH3_OC_WF_PERIOD_NUMERATOR_NS,
                     CONFIG_STM32_HPTC_TICKTIMER_CH3_OC_WF_PERIOD_DENOMINATOR_NS);

#endif  //CONFIG_STM32_HPTC_TICKTIMER_CH3_OC_WF

	hptc_io_register(CONFIG_STM32_HPTC_TICKTIMER_CH3_DEVPATH,&g_hptc_io_dev_ticktimer_ch3, ic_bufsize, oc_bufsize);

#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH4
	g_hptc_io_dev_ticktimer_ch4.priv = &g_tim_dev.tick_timer_ch[3];
	g_hptc_io_dev_ticktimer_ch4.ops = &g_hptc_io_ops;

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH4_IC_BUFSIZE
	ic_bufsize=CONFIG_STM32_HPTC_TICKTIMER_CH4_IC_BUFSIZE;
#else
    ic_bufsize=0;
#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH4_OC_BUFSIZE
	oc_bufsize=CONFIG_STM32_HPTC_TICKTIMER_CH4_OC_BUFSIZE;
#else
    oc_bufsize=0;
#endif

#ifdef CONFIG_STM32_HPTC_TICKTIMER_CH4_OC_WF

	wf = &g_tim_dev.tick_timer_ch[3].wf;
        hptc_oc_wf_condition(wf,CONFIG_STM32_HPTC_TICKTIMER_CH4_OC_WF_RISING_EDGE,
                     CONFIG_STM32_HPTC_TICKTIMER_CH4_OC_WF_START_S,
                     CONFIG_STM32_HPTC_TICKTIMER_CH4_OC_WF_START_NS,
                     CONFIG_STM32_HPTC_TICKTIMER_CH4_OC_WF_PULSE_WIDTH_S,
                     CONFIG_STM32_HPTC_TICKTIMER_CH4_OC_WF_PULSE_WIDTH_NS,
                     CONFIG_STM32_HPTC_TICKTIMER_CH4_OC_WF_PERIOD_S,
                     CONFIG_STM32_HPTC_TICKTIMER_CH4_OC_WF_PERIOD_NS,
                     CONFIG_STM32_HPTC_TICKTIMER_CH4_OC_WF_PERIOD_NUMERATOR_NS,
                     CONFIG_STM32_HPTC_TICKTIMER_CH4_OC_WF_PERIOD_DENOMINATOR_NS);

#endif  //CONFIG_STM32_HPTC_TICKTIMER_CH4_OC_WF

	hptc_io_register(CONFIG_STM32_HPTC_TICKTIMER_CH4_DEVPATH,&g_hptc_io_dev_ticktimer_ch4, ic_bufsize, oc_bufsize);

#endif

/* AUX TIMER */

#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH1
	g_hptc_io_dev_auxtimer_ch1.priv = &g_tim_dev.aux_timer_ch[0];
	g_hptc_io_dev_auxtimer_ch1.ops = &g_hptc_io_ops;

#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH1_IC_BUFSIZE
	ic_bufsize=CONFIG_STM32_HPTC_AUXTIMER_CH1_IC_BUFSIZE;
#else
    ic_bufsize=0;
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH1_OC_BUFSIZE
	oc_bufsize=CONFIG_STM32_HPTC_AUXTIMER_CH1_OC_BUFSIZE;
#else
    oc_bufsize=0;
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH1_OC_WF

	wf = &g_tim_dev.aux_timer_ch[0].wf;
        hptc_oc_wf_condition(wf,CONFIG_STM32_HPTC_AUXTIMER_CH1_OC_WF_RISING_EDGE,
                     CONFIG_STM32_HPTC_AUXTIMER_CH1_OC_WF_START_S,
                     CONFIG_STM32_HPTC_AUXTIMER_CH1_OC_WF_START_NS,
                     CONFIG_STM32_HPTC_AUXTIMER_CH1_OC_WF_PULSE_WIDTH_S,
                     CONFIG_STM32_HPTC_AUXTIMER_CH1_OC_WF_PULSE_WIDTH_NS,
                     CONFIG_STM32_HPTC_AUXTIMER_CH1_OC_WF_PERIOD_S,
                     CONFIG_STM32_HPTC_AUXTIMER_CH1_OC_WF_PERIOD_NS,
                     CONFIG_STM32_HPTC_AUXTIMER_CH1_OC_WF_PERIOD_NUMERATOR_NS,
                     CONFIG_STM32_HPTC_AUXTIMER_CH1_OC_WF_PERIOD_DENOMINATOR_NS);

	wf->type = HPTC_IO_WF_CONTINUOUS;

#endif  //CONFIG_STM32_HPTC_AUXTIMER_CH1_OC_WF

	hptc_io_register(CONFIG_STM32_HPTC_AUXTIMER_CH1_DEVPATH,&g_hptc_io_dev_auxtimer_ch1, ic_bufsize, oc_bufsize);
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH2

	g_hptc_io_dev_auxtimer_ch2.priv = &g_tim_dev.aux_timer_ch[1];
	g_hptc_io_dev_auxtimer_ch2.ops = &g_hptc_io_ops;

#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH2_IC_BUFSIZE
	ic_bufsize=CONFIG_STM32_HPTC_AUXTIMER_CH2_IC_BUFSIZE;
#else
    ic_bufsize=0;
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH2_OC_BUFSIZE
	oc_bufsize=CONFIG_STM32_HPTC_AUXTIMER_CH2_OC_BUFSIZE;
#else
    oc_bufsize=0;
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH2_OC_WF

	wf = &g_tim_dev.aux_timer_ch[1].wf;
        hptc_oc_wf_condition(wf,CONFIG_STM32_HPTC_AUXTIMER_CH2_OC_WF_RISING_EDGE,
                     CONFIG_STM32_HPTC_AUXTIMER_CH2_OC_WF_START_S,
                     CONFIG_STM32_HPTC_AUXTIMER_CH2_OC_WF_START_NS,
                     CONFIG_STM32_HPTC_AUXTIMER_CH2_OC_WF_PULSE_WIDTH_S,
                     CONFIG_STM32_HPTC_AUXTIMER_CH2_OC_WF_PULSE_WIDTH_NS,
                     CONFIG_STM32_HPTC_AUXTIMER_CH2_OC_WF_PERIOD_S,
                     CONFIG_STM32_HPTC_AUXTIMER_CH2_OC_WF_PERIOD_NS,
                     CONFIG_STM32_HPTC_AUXTIMER_CH2_OC_WF_PERIOD_NUMERATOR_NS,
                     CONFIG_STM32_HPTC_AUXTIMER_CH2_OC_WF_PERIOD_DENOMINATOR_NS);

	wf->type = HPTC_IO_WF_CONTINUOUS;

#endif  //CONFIG_STM32_HPTC_AUXTIMER_CH2_OC_WF

	hptc_io_register(CONFIG_STM32_HPTC_AUXTIMER_CH2_DEVPATH,&g_hptc_io_dev_auxtimer_ch2, ic_bufsize, oc_bufsize);

#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH3

	g_hptc_io_dev_auxtimer_ch3.priv = &g_tim_dev.aux_timer_ch[2];
	g_hptc_io_dev_auxtimer_ch3.ops = &g_hptc_io_ops;

#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH3_IC_BUFSIZE
	ic_bufsize=CONFIG_STM32_HPTC_AUXTIMER_CH3_IC_BUFSIZE;
#else
    ic_bufsize=0;
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH3_OC_BUFSIZE
	oc_bufsize=CONFIG_STM32_HPTC_AUXTIMER_CH3_OC_BUFSIZE;
#else
    oc_bufsize=0;
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH3_OC_WF

	wf = &g_tim_dev.aux_timer_ch[2].wf;
        hptc_oc_wf_condition(wf,CONFIG_STM32_HPTC_AUXTIMER_CH3_OC_WF_RISING_EDGE,
                     CONFIG_STM32_HPTC_AUXTIMER_CH3_OC_WF_START_S,
                     CONFIG_STM32_HPTC_AUXTIMER_CH3_OC_WF_START_NS,
                     CONFIG_STM32_HPTC_AUXTIMER_CH3_OC_WF_PULSE_WIDTH_S,
                     CONFIG_STM32_HPTC_AUXTIMER_CH3_OC_WF_PULSE_WIDTH_NS,
                     CONFIG_STM32_HPTC_AUXTIMER_CH3_OC_WF_PERIOD_S,
                     CONFIG_STM32_HPTC_AUXTIMER_CH3_OC_WF_PERIOD_NS,
                     CONFIG_STM32_HPTC_AUXTIMER_CH3_OC_WF_PERIOD_NUMERATOR_NS,
                     CONFIG_STM32_HPTC_AUXTIMER_CH3_OC_WF_PERIOD_DENOMINATOR_NS);

#endif  //CONFIG_STM32_HPTC_AUXTIMER_CH3_OC_WF

	hptc_io_register(CONFIG_STM32_HPTC_AUXTIMER_CH3_DEVPATH,&g_hptc_io_dev_auxtimer_ch3, ic_bufsize, oc_bufsize);

#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH4
	g_hptc_io_dev_auxtimer_ch4.priv = &g_tim_dev.aux_timer_ch[3];
	g_hptc_io_dev_auxtimer_ch4.ops = &g_hptc_io_ops;

#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH4_IC_BUFSIZE
	ic_bufsize=CONFIG_STM32_HPTC_AUXTIMER_CH4_IC_BUFSIZE;
#else
    ic_bufsize=0;
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH4_OC_BUFSIZE
	oc_bufsize=CONFIG_STM32_HPTC_AUXTIMER_CH4_OC_BUFSIZE;
#else
    oc_bufsize=0;
#endif

#ifdef CONFIG_STM32_HPTC_AUXTIMER_CH4_OC_WF

	wf = &g_tim_dev.aux_timer_ch[3].wf;
        hptc_oc_wf_condition(wf,CONFIG_STM32_HPTC_AUXTIMER_CH4_OC_WF_RISING_EDGE,
                     CONFIG_STM32_HPTC_AUXTIMER_CH4_OC_WF_START_S,
                     CONFIG_STM32_HPTC_AUXTIMER_CH4_OC_WF_START_NS,
                     CONFIG_STM32_HPTC_AUXTIMER_CH4_OC_WF_PULSE_WIDTH_S,
                     CONFIG_STM32_HPTC_AUXTIMER_CH4_OC_WF_PULSE_WIDTH_NS,
                     CONFIG_STM32_HPTC_AUXTIMER_CH4_OC_WF_PERIOD_S,
                     CONFIG_STM32_HPTC_AUXTIMER_CH4_OC_WF_PERIOD_NS,
                     CONFIG_STM32_HPTC_AUXTIMER_CH4_OC_WF_PERIOD_NUMERATOR_NS,
                     CONFIG_STM32_HPTC_AUXTIMER_CH4_OC_WF_PERIOD_DENOMINATOR_NS);

#endif  //CONFIG_STM32_HPTC_AUXTIMER_CH4_OC_WF

	hptc_io_register(CONFIG_STM32_HPTC_AUXTIMER_CH4_DEVPATH,&g_hptc_io_dev_auxtimer_ch4, ic_bufsize, oc_bufsize);

#endif

}




/****************************************************************************
 * Name: stm32_clock_get_deviation
 *
 * Description:
 *   provides clock deviation for other low level drivers using a timer
 *
 * Returned Values:
 *   deviation in ppb from nominal clock
 *
 ****************************************************************************/

int32_t stm32_clock_get_deviation(void){
	return g_clock_dev;
}


/****************************************************************************
 * Name: stm32_htpc_initialize
 *
 * Description:
 *   Bind the configuration hptc to a hptc lower half instance and
 *   register the device
 *
 * Returned Values:
 *   Zero (OK) is returned on success; A negated errno value is returned
 *   to indicate the nature of any failure.
 *
 ****************************************************************************/

int stm32_hptc_initialize()
{

//	switch_LED_red_on();

	/* Register the hptc driver.  The returned value from
	 * timer_register is a handle that could be used with timer_unregister().
	 * REVISIT: The returned handle is discard here.
	 */

	FAR void *drvr = hptc_register(CONFIG_HPTC_DEV_PATH,&g_hptc_dev);
	if (drvr == NULL)
	{
		/* The actual cause of the failure may have been a failure to allocate
		 * perhaps a failure to register the timer driver (such as if the
		 * 'depath' were not unique).  We know here but we return EEXIST to
		 * indicate the failure (implying the non-unique devpath).
		 */

		return -EEXIST;
	}

	stm32_hptc_io_dev_initialize();


	return OK;
}





#endif /* CONFIG_HPTC */

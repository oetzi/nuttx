/****************************************************************************
 * include/nuttx/sensors/smt172.h
 *
 *   Copyright 2020 Leichtwerk Research GmbH
 *   Author: Stefan Nowak
 *
 ****************************************************************************/

#ifndef __INCLUDE_NUTTX_SENSORS_SMT172_H
#define __INCLUDE_NUTTX_SENSORS_SMT172_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>
#include <time.h>
#include <nuttx/fs/ioctl.h>

#if defined(CONFIG_SENSORS_SMT172)

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#ifndef NS_PER_SEC
#define NS_PER_SEC 1000000000
#endif

#define CONFIG_SMT172_CHANNELS 2 //FIXME: required?

#define SMT172_OUTPUT_FREQ_MIN 500
#define SMT172_OUTPUT_FREQ_MAX 7000
#define SMT172_TIMER_FREQ_DESIRED (0x10000 * SMT172_OUTPUT_FREQ_MIN /2)



/****************************************************************************
 * Public Types
 ****************************************************************************/

struct smt172_config_s;

/* A reference to a structure of this type must be passed to the smt172
 * driver. This structure provides information about the configuration
 * of the sensor and provides some board-specific hooks.
 *
 * Memory for this structure is provided by the caller.  It is not copied
 * by the driver and is presumed to persist while the driver is active.
 */

struct smt172_config_s
{
  uint16_t period_cnt_min;
  uint16_t period_cnt_max;
  volatile uint16_t* fifo;
  int32_t fifosize;
  uint8_t channel;
  int (*update)(FAR struct smt172_config_s *conf);
};

struct smt172_msg_s
{
  int32_t   time_s;
  int32_t   time_ns;
  int32_t   channel;
  float     value;
};


/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/* IOCTL Commands ***********************************************************/

/* Enable SMT172 readout
 *
 * Arg: none
 */

//#define SNIOC_SMT172_ENABLE           _SNIOC(0x0001)


/* Update buffer
 *
 * Arg: none
 */

#define SNIOC_SMT172_UPDATE           _SNIOC(0x0003)


/****************************************************************************
 * Name: smt172_instantiate
 *
 * Description:
 *   Instantiate an optional register the SMT172 character device as 'devpath'
 *
 * Input Parameters:
 *   devpath - The full path to the driver to register. E.g., "/dev/smt172"
 *   config  - Persistent board configuration data
 *             SMT172
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/
int smt172_instantiate(FAR const char *devpath, FAR struct smt172_config_s *config);

#undef EXTERN
#ifdef __cplusplus
}
#endif

#endif /* CONFIG_SENSORS_SMT172 */
#endif /* __INCLUDE_NUTTX_SMT172_H */

/************************************************************************************
 * arch/arm/src/stm32f7/stm32_messBUSMaster.h
 *
 *   Author: Peer Ulbig <p.ulbig@tu-braunschweig.de>
 *
 ************************************************************************************/

#ifndef _ARCH_ARM_SRC_STM32_STM32_MESSBUSMASTER_H_
#define _ARCH_ARM_SRC_STM32_STM32_MESSBUSMASTER_H_

/****************************************************************************
 * Includes
 ****************************************************************************/

#include <nuttx/config.h>
#include <sys/types.h>
#include <arch/board/board.h>
#include <nuttx/messBUS/messBUSMaster.h>
#include "stm32_dma.h"
#include "stm32_gpio.h"

/****************************************************************************
 * Debug
 ****************************************************************************/

/* Output TIM2's trigger on PA15 */
#define mB_TIM2_OC_PA15		0

/* Mark the beginnings end endings of the ISRs */
#define mB_DEBUG_VISUALIZE_INTERRUPTS	1

/****************************************************************************
 * Helper #defines
 ****************************************************************************/

#define mB_SEC_2_USEC		1000000ul

/*
 * messBUS node need to begin offset microseconds before specified rx slot begin with
 * its preparation to ensure that it is really ready to read data at slot begin.
 */
#define mB_RX_OFFSET_US     -1

/****************************************************************************
 * messBUS
 ****************************************************************************/

/* Messbus frequency (100 Hz) */
#define mB_SLICE_PERIOD_NS  10000000                                                // messBUS slice period -> 10ms

/* SYNC related timing */
#define mB_SYNC				5														// messBUS SYNC duration -> 5us
#define mB_FULLSEC_SYNC     4                                                       // messBUS SYNC duration @ full second -> 4us
#define mB_SYNC_DELAY		2														// messBUS SYNC delay after trigger -> 2us
#define mB_ISR1_DELAY		(mB_SYNC + mB_SYNC_DELAY + 2)							// messBUS interrupt 1 delay after trigger -> 9us
#define mB_ISR2_DELAY		(10000 - 2)												// messBUS interrupt 2 delay after trigger -> 9998us

/* Normal operation modes of timer 3 interrupt handler */
#define mB_CH1_RX_START		10
#define mB_CH1_RX_END		11
#define mB_CH1_TX_DE		12
#define mB_CH1_TX_START		13
#define mB_CH1_TX_END		14
#define mB_CH1_WAKE_UP		15

#define mB_CH2_RX_START		20
#define mB_CH2_RX_END		21
#define mB_CH2_TX_DE		22
#define mB_CH2_TX_START		23
#define mB_CH2_TX_END		24
#define mB_CH2_WAKE_UP		25

#define mB_CH3_RX_START		30
#define mB_CH3_RX_END		31
#define mB_CH3_TX_DE		32
#define mB_CH3_TX_START		33
#define mB_CH3_TX_END		34
#define mB_CH3_WAKE_UP		35

#define mB_CH4_RX_START		40
#define mB_CH4_RX_END		41
#define mB_CH4_TX_DE		42
#define mB_CH4_TX_START		43
#define mB_CH4_TX_END		44
#define mB_CH4_WAKE_UP		45

/* Channel selection if CONFIG_MESSBUS_NCHANNELS == 1 */
#if CONFIG_MESSBUS_NCHANNELS == 1
	#if CONFIG_MESSBUS_USE_CH1
		#define mB_CHx_RX_START		mB_CH1_RX_START
		#define mB_CHx_RX_END		mB_CH1_RX_END
		#define mB_CHx_TX_DE		mB_CH1_TX_DE
		#define mB_CHx_TX_START		mB_CH1_TX_START
		#define mB_CHx_TX_END		mB_CH1_TX_END
		#define mB_CHx_WAKE_UP		mB_CH1_WAKE_UP
		#define mB_CHx_SLOTLIST		ch1_slotlist
		#define mB_CHx_INFOTABLE	info1
		#define mB_CHx_WAKE_UP_INFO	ch1_wake_up_callback_info
	#elif CONFIG_MESSBUS_USE_CH2
		#define mB_CHx_RX_START		mB_CH2_RX_START
		#define mB_CHx_RX_END		mB_CH2_RX_END
		#define mB_CHx_TX_DE		mB_CH2_TX_DE
		#define mB_CHx_TX_START		mB_CH2_TX_START
		#define mB_CHx_TX_END		mB_CH2_TX_END
		#define mB_CHx_WAKE_UP		mB_CH2_WAKE_UP
		#define mB_CHx_SLOTLIST		ch2_slotlist
		#define mB_CHx_INFOTABLE	info2
		#define mB_CHx_WAKE_UP_INFO	ch2_wake_up_callback_info
	#elif CONFIG_MESSBUS_USE_CH3
		#define mB_CHx_RX_START		mB_CH3_RX_START
		#define mB_CHx_RX_END		mB_CH3_RX_END
		#define mB_CHx_TX_DE		mB_CH3_TX_DE
		#define mB_CHx_TX_START		mB_CH3_TX_START
		#define mB_CHx_TX_END		mB_CH3_TX_END
		#define mB_CHx_WAKE_UP		mB_CH3_WAKE_UP
		#define mB_CHx_SLOTLIST		ch3_slotlist
		#define mB_CHx_INFOTABLE	info3
		#define mB_CHx_WAKE_UP_INFO	ch3_wake_up_callback_info
	#elif CONFIG_MESSBUS_USE_CH4
		#define mB_CHx_RX_START		mB_CH4_RX_START
		#define mB_CHx_RX_END		mB_CH4_RX_END
		#define mB_CHx_TX_DE		mB_CH4_TX_DE
		#define mB_CHx_TX_START		mB_CH4_TX_START
		#define mB_CHx_TX_END		mB_CH4_TX_END
		#define mB_CHx_WAKE_UP		mB_CH4_WAKE_UP
		#define mB_CHx_SLOTLIST		ch4_slotlist
		#define mB_CHx_INFOTABLE	info4
		#define mB_CHx_WAKE_UP_INFO	ch4_wake_up_callback_info
	#endif
#endif //CONFIG_MESSBUS_NCHANNELS == 1

/* How many slots and actions do we actually need? */
#if CONFIG_MESSBUS_USE_CH1
	#define mB_CH1_NRXSLOTS		CONFIG_MESSBUS_CH1_MAX_RX_SLOTS
	#define mB_CH1_NTXSLOTS		CONFIG_MESSBUS_CH1_MAX_TX_SLOTS
#else
	#define mB_CH1_NRXSLOTS		0
	#define mB_CH1_NTXSLOTS		0
#endif
#if CONFIG_MESSBUS_USE_CH2
	#define mB_CH2_NRXSLOTS		CONFIG_MESSBUS_CH2_MAX_RX_SLOTS
	#define mB_CH2_NTXSLOTS		CONFIG_MESSBUS_CH2_MAX_TX_SLOTS
#else
	#define mB_CH2_NRXSLOTS		0
	#define mB_CH2_NTXSLOTS		0
#endif
#if CONFIG_MESSBUS_USE_CH3
	#define mB_CH3_NRXSLOTS		CONFIG_MESSBUS_CH3_MAX_RX_SLOTS
	#define mB_CH3_NTXSLOTS		CONFIG_MESSBUS_CH3_MAX_TX_SLOTS
#else
	#define mB_CH3_NRXSLOTS		0
	#define mB_CH3_NTXSLOTS		0
#endif
#if CONFIG_MESSBUS_USE_CH4
	#define mB_CH4_NRXSLOTS		CONFIG_MESSBUS_CH4_MAX_RX_SLOTS
	#define mB_CH4_NTXSLOTS		CONFIG_MESSBUS_CH4_MAX_TX_SLOTS
#else
	#define mB_CH4_NRXSLOTS		0
	#define mB_CH4_NTXSLOTS		0
#endif

#define mB_CH1_NSLOTS		mB_CH1_NRXSLOTS + mB_CH1_NTXSLOTS
#define mB_CH1_NACTIONS		((2 * mB_CH1_NRXSLOTS) + (3 * mB_CH1_NTXSLOTS))

#define mB_CH2_NSLOTS		mB_CH2_NRXSLOTS + mB_CH2_NTXSLOTS
#define mB_CH2_NACTIONS		((2 * mB_CH2_NRXSLOTS) + (3 * mB_CH2_NTXSLOTS))

#define mB_CH3_NSLOTS		mB_CH3_NRXSLOTS + mB_CH3_NTXSLOTS
#define mB_CH3_NACTIONS		((2 * mB_CH3_NRXSLOTS) + (3 * mB_CH3_NTXSLOTS))

#define mB_CH4_NSLOTS		mB_CH4_NRXSLOTS + mB_CH4_NTXSLOTS
#define mB_CH4_NACTIONS		((2 * mB_CH4_NRXSLOTS) + (3 * mB_CH4_NTXSLOTS))

#define mB_NSLOTS			mB_CH1_NSLOTS + mB_CH2_NSLOTS + mB_CH3_NSLOTS + mB_CH4_NSLOTS
#define mB_NACTIONS			mB_CH1_NACTIONS + mB_CH2_NACTIONS + mB_CH3_NACTIONS + mB_CH4_NACTIONS

/****************************************************************************
 * Timer 2 (Master)
 ****************************************************************************/

#define mB_TIM2_FREQ 		4000000ul  												// Desired frequency of Timer 2 -> 250ns resolution
#define mB_TIM2_PSC 		(uint16_t) ((STM32_APB1_TIM2_CLKIN / mB_TIM2_FREQ)-1)	// Required prescaler setting according to clocking defined in board.h
#define mB_TIM2_PERIOD 		(40000 - 1)												// ARR for one timeslice, with respect to known freq devation of used board
#define mB_TIM2_PULSE		(mB_SYNC*3*4)											// Tim 2 debug pulse width = 3*4*mB_SYNC = 60 ticks = 15us (Output Compare PA15)

/****************************************************************************
 * Timer 1 (Sync)
 ****************************************************************************/

/* Timing */
#define mB_TIM1_FREQ 		1000000ul  												// Desired frequency of Timer 1 -> 1us resolution
#define mB_TIM1_PSC 		(uint16_t) ((STM32_APB2_TIM1_CLKIN / mB_TIM1_FREQ)-1)	// Required prescaler setting according to clocking defined in board.h
#define mB_TIM1_PERIOD 		((mB_SYNC + mB_SYNC_DELAY) - 1)							// Tim 1 pulse width = mB_SYNC + 2 = 7us (because of 2us delay)
#define mB_TIM1_FULLSEC_PERIOD ((mB_FULLSEC_SYNC + mB_SYNC_DELAY) - 1)              //       full sec pulse width = mB_FULLSEC_SYNC +2 = 6us
#define mB_TIM1_PULSE		mB_SYNC_DELAY											// Tim 1 delay = 2 = 2us

/* Output pin configurations */
#if CONFIG_MESSBUS_USE_CH1
#if (GPIO_TIM1_CH1OUT == GPIO_TIM1_CH1OUT_1)										// Config option1 -> Tim1 Ch1 at PA8
	#define mB_TIM1_CH1OUT_MODER	STM32_GPIOA_MODER
	#define mB_TIM1_CH1OUT_ACTIVE	(2 << GPIO_MODER8_SHIFT)
	#define mB_TIM1_CH1OUT_HI_Z		GPIO_MODER8_MASK
#else																				// Config option2 -> Tim1 Ch1 at PE9
	#define mB_TIM1_CH1OUT_MODER	STM32_GPIOE_MODER
	#define mB_TIM1_CH1OUT_ACTIVE	(2 << GPIO_MODER9_SHIFT)
	#define mB_TIM1_CH1OUT_HI_Z		GPIO_MODER9_MASK
#endif
#endif

#if CONFIG_MESSBUS_USE_CH2
#if (GPIO_TIM1_CH2OUT == GPIO_TIM1_CH2OUT_1)										// Config option1 -> Tim1 Ch2 at PA9
	#define mB_TIM1_CH2OUT_MODER	STM32_GPIOA_MODER
	#define mB_TIM1_CH2OUT_ACTIVE	(2 << GPIO_MODER9_SHIFT)
	#define mB_TIM1_CH2OUT_HI_Z		GPIO_MODER9_MASK
#else																				// Config option2 -> Tim2 Ch1 at PE11
	#define mB_TIM1_CH2OUT_MODER	STM32_GPIOE_MODER
	#define mB_TIM1_CH2OUT_ACTIVE	(2 << GPIO_MODER11_SHIFT)
	#define mB_TIM1_CH2OUT_HI_Z		GPIO_MODER11_MASK
#endif
#endif

#if CONFIG_MESSBUS_USE_CH3
#if (GPIO_TIM1_CH3OUT == GPIO_TIM1_CH3OUT_1)										// Config option1 -> Tim1 Ch3 at PA10
	#define mB_TIM1_CH3OUT_MODER	STM32_GPIOA_MODER
	#define mB_TIM1_CH3OUT_ACTIVE	(2 << GPIO_MODER10_SHIFT)
	#define mB_TIM1_CH3OUT_HI_Z		GPIO_MODER10_MASK
#else																				// Config option2 -> Tim1 Ch3 at PE13
	#define mB_TIM1_CH3OUT_MODER	STM32_GPIOE_MODER
	#define mB_TIM1_CH3OUT_ACTIVE	(2 << GPIO_MODER13_SHIFT)
	#define mB_TIM1_CH3OUT_HI_Z		GPIO_MODER13_MASK
#endif
#endif

#if CONFIG_MESSBUS_USE_CH4
#if (GPIO_TIM1_CH4OUT == GPIO_TIM1_CH4OUT_1)										// Config option1 -> Tim1 Ch4 at PA11
	#define mB_TIM1_CH4OUT_MODER	STM32_GPIOA_MODER
	#define mB_TIM1_CH4OUT_ACTIVE	(2 << GPIO_MODER11_SHIFT)
	#define mB_TIM1_CH4OUT_HI_Z		GPIO_MODER11_MASK
#else																				// Config option2 -> Tim1 Ch4 at PE14
	#define mB_TIM1_CH4OUT_MODER	STM32_GPIOE_MODER
	#define mB_TIM1_CH4OUT_ACTIVE	(2 << GPIO_MODER14_SHIFT)
	#define mB_TIM1_CH4OUT_HI_Z		GPIO_MODER14_MASK
#endif
#endif


/****************************************************************************
 * Timer 3 (ISR)
 ****************************************************************************/

#define mB_TIM3_FREQ 		4000000ul  												// Desired frequency of Timer 3 -> 0.25us resolution
#define mB_TIM3_PSC 		(uint16_t) ((STM32_APB1_TIM3_CLKIN / mB_TIM3_FREQ)-1)	// Required prescaler setting according to clocking defined in board.h
#define mB_TIM3_1US_TICKS	(mB_TIM3_FREQ / mB_SEC_2_USEC)							// Ticks per us @ 4Mhz
#define mB_TIM3_CCR3_1		(mB_TIM3_1US_TICKS * mB_ISR1_DELAY)						// CCR3 value to trigger interrupt 1 (first after SYNC)
#define mB_TIM3_CCR3_2		(mB_TIM3_1US_TICKS * mB_ISR2_DELAY)						// CCR3 value to trigger interrupt 2 (last before SYNC)
#define mB_OFFSET_TICKS		(mB_SYNC_DELAY*mB_TIM3_1US_TICKS)						// Offset because of delayed SYNC

/****************************************************************************
 * Channel 1 configuration
 ****************************************************************************/

#if CONFIG_MESSBUS_USE_CH1
/* USART output pin configurations */
#if (GPIO_USART1_TX == GPIO_USART1_TX_1)
	#define mB_USART1TX_MODER			STM32_GPIOA_MODER
	#define mB_USART1TX_ACTIVE			(2 << GPIO_MODER9_SHIFT)
	#define mB_USART1TX_HI_Z			GPIO_MODER9_MASK
#else
	#define mB_USART1TX_MODER			STM32_GPIOB_MODER
	#define mB_USART1TX_ACTIVE			(2 << GPIO_MODER6_SHIFT)
	#define mB_USART1TX_HI_Z			GPIO_MODER6_MASK
#endif

/* DMA RX configuration */
#if (DMAMAP_USART1_RX == DMAMAP_USART1_RX_1)
	#define mB_USART1RX_DMA_SxNDTR		STM32_DMA2_S2NDTR
	#define mB_USART1RX_DMA_SxCR		STM32_DMA2_S2CR
	#define mB_USART1RX_DMA_xIFCR		STM32_DMA2_LIFCR
	#define mB_USART1RX_DMA_SxMOAR		STM32_DMA2_S2M0AR
	#define mB_USART1RX_DMA_INT_SxMASK	DMA_INT_STREAM2_MASK
#elif (DMAMAP_USART1_RX == DMAMAP_USART1_RX_2)
	#define mB_USART1RX_DMA_SxNDTR		STM32_DMA2_S5NDTR
	#define mB_USART1RX_DMA_SxCR		STM32_DMA2_S5CR
	#define mB_USART1RX_DMA_xIFCR		STM32_DMA2_HIFCR
	#define mB_USART1RX_DMA_SxMOAR		STM32_DMA2_S5M0AR
	#define mB_USART1RX_DMA_INT_SxMASK	DMA_INT_STREAM5_MASK
#endif

/* DMA TX configuration */
#define mB_USART1TX_DMA_SxNDTR		STM32_DMA2_S7NDTR
#define mB_USART1TX_DMA_SxCR		STM32_DMA2_S7CR
#define mB_USART1TX_DMA_xIFCR		STM32_DMA2_HIFCR
#define mB_USART1TX_DMA_SxMOAR		STM32_DMA2_S7M0AR
#define mB_USART1TX_DMA_INT_SxMASK	DMA_INT_STREAM7_MASK

#endif //CONFIG_MESSBUS_USE_CH1

/****************************************************************************
 * Channel 2 configuration
 ****************************************************************************/

#if CONFIG_MESSBUS_USE_CH2
/* USART output pin configurations */
#if (GPIO_USART2_TX == GPIO_USART2_TX_1)
	#define mB_USART2TX_MODER			STM32_GPIOA_MODER
	#define mB_USART2TX_ACTIVE			(2 << GPIO_MODER2_SHIFT)
	#define mB_USART2TX_HI_Z			GPIO_MODER2_MASK
#else
	#define mB_USART2TX_MODER			STM32_GPIOD_MODER
	#define mB_USART2TX_ACTIVE			(2 << GPIO_MODER5_SHIFT)
	#define mB_USART2TX_HI_Z			GPIO_MODER5_MASK
#endif

/* DMA RX configuration */
#define mB_USART2RX_DMA_SxNDTR		STM32_DMA1_S5NDTR
#define mB_USART2RX_DMA_SxCR		STM32_DMA1_S5CR
#define mB_USART2RX_DMA_xIFCR		STM32_DMA1_HIFCR
#define mB_USART2RX_DMA_SxMOAR		STM32_DMA1_S5M0AR
#define mB_USART2RX_DMA_INT_SxMASK	DMA_INT_STREAM5_MASK

/* DMA TX configuration */
#define mB_USART2TX_DMA_SxNDTR		STM32_DMA1_S6NDTR
#define mB_USART2TX_DMA_SxCR		STM32_DMA1_S6CR
#define mB_USART2TX_DMA_xIFCR		STM32_DMA1_HIFCR
#define mB_USART2TX_DMA_SxMOAR		STM32_DMA1_S6M0AR
#define mB_USART2TX_DMA_INT_SxMASK	DMA_INT_STREAM6_MASK

#endif //#if CONFIG_MESSBUS_USE_CH2

/****************************************************************************
 * Channel 3 configuration
 ****************************************************************************/

#if CONFIG_MESSBUS_USE_CH3
/* USART output pin configurations */
#if (GPIO_USART3_TX == GPIO_USART3_TX_1)
	#define mB_USART3TX_MODER			STM32_GPIOB_MODER
	#define mB_USART3TX_ACTIVE			(2 << GPIO_MODER10_SHIFT)
	#define mB_USART3TX_HI_Z			GPIO_MODER10_MASK
#elif (GPIO_USART3_TX == GPIO_USART3_TX_2)
	#define mB_USART3TX_MODER			STM32_GPIOC_MODER
	#define mB_USART3TX_ACTIVE			(2 << GPIO_MODER10_SHIFT)
	#define mB_USART3TX_HI_Z			GPIO_MODER10_MASK
#else
	#define mB_USART3TX_MODER			STM32_GPIOD_MODER
	#define mB_USART3TX_ACTIVE			(2 << GPIO_MODER8_SHIFT)
	#define mB_USART3TX_HI_Z			GPIO_MODER8_MASK
#endif

/* DMA RX configuration */
#define mB_USART3RX_DMA_SxNDTR		STM32_DMA1_S1NDTR
#define mB_USART3RX_DMA_SxCR		STM32_DMA1_S1CR
#define mB_USART3RX_DMA_xIFCR		STM32_DMA1_LIFCR
#define mB_USART3RX_DMA_SxMOAR		STM32_DMA1_S1M0AR
#define mB_USART3RX_DMA_INT_SxMASK	DMA_INT_STREAM1_MASK

/* DMA TX configuration */
#if (DMAMAP_USART3_TX == DMAMAP_USART3_TX_1)
	#define mB_USART3TX_DMA_SxNDTR		STM32_DMA1_S3NDTR
	#define mB_USART3TX_DMA_SxCR		STM32_DMA1_S3CR
	#define mB_USART3TX_DMA_xIFCR		STM32_DMA1_LIFCR
	#define mB_USART3TX_DMA_SxMOAR		STM32_DMA1_S3M0AR
	#define mB_USART3TX_DMA_INT_SxMASK	DMA_INT_STREAM3_MASK
#else
	#define mB_USART3TX_DMA_SxNDTR		STM32_DMA1_S4NDTR
	#define mB_USART3TX_DMA_SxCR		STM32_DMA1_S4CR
	#define mB_USART3TX_DMA_xIFCR		STM32_DMA1_HIFCR
	#define mB_USART3TX_DMA_SxMOAR		STM32_DMA1_S4M0AR
	#define mB_USART3TX_DMA_INT_SxMASK	DMA_INT_STREAM4_MASK
#endif

#endif //#if CONFIG_MESSBUS_USE_CH3

/****************************************************************************
 * Channel 4 configuration
 ****************************************************************************/

#if CONFIG_MESSBUS_USE_CH4
/* USART output pin configurations */
#if (GPIO_UART4_TX == GPIO_UART4_TX_1)
	#define mB_USART4TX_MODER			STM32_GPIOA_MODER
	#define mB_USART4TX_ACTIVE			(2 << GPIO_MODER0_SHIFT)
	#define mB_USART4TX_HI_Z			GPIO_MODER0_MASK
#else
	#define mB_USART4TX_MODER			STM32_GPIOC_MODER
	#define mB_USART4TX_ACTIVE			(2 << GPIO_MODER10_SHIFT)
	#define mB_USART4TX_HI_Z			GPIO_MODER10_MASK
#endif

/* DMA RX configuration */
#define mB_USART4RX_DMA_SxNDTR		STM32_DMA1_S2NDTR
#define mB_USART4RX_DMA_SxCR		STM32_DMA1_S2CR
#define mB_USART4RX_DMA_xIFCR		STM32_DMA1_LIFCR
#define mB_USART4RX_DMA_SxMOAR		STM32_DMA1_S2M0AR
#define mB_USART4RX_DMA_INT_SxMASK	DMA_INT_STREAM2_MASK

/* DMA TX configuration */
#define mB_USART4TX_DMA_SxNDTR		STM32_DMA1_S4NDTR
#define mB_USART4TX_DMA_SxCR		STM32_DMA1_S4CR
#define mB_USART4TX_DMA_xIFCR		STM32_DMA1_HIFCR
#define mB_USART4TX_DMA_SxMOAR		STM32_DMA1_S4M0AR
#define mB_USART4TX_DMA_INT_SxMASK	DMA_INT_STREAM4_MASK

#endif //#if CONFIG_MESSBUS_USE_CH4

/****************************************************************************
 * Ch1 Data Enable pin
 ****************************************************************************/

/* Check for F4 or F7 architecture */
#if defined(CONFIG_ARCH_CHIP_STM32F7)
#define mB_NGPIO_PORTS	STM32F7_NGPIO
#else
#define mB_NGPIO_PORTS	STM32_NGPIO_PORTS
#endif

#if CONFIG_MESSBUS_USE_CH1
/* Extract the pin number */
#define mB_CH1_DE_ON			(1 << (GPIO_CH1_DE & GPIO_PIN_MASK))
#define mB_CH1_DE_OFF			(1 << ((GPIO_CH1_DE & GPIO_PIN_MASK)+16))

/* Get the right BSRR address according to port configuration */
#if mB_NGPIO_PORTS > 0
#if ((GPIO_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTA)
#define mB_CH1_DE_BSRR		STM32_GPIOA_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 1
#if ((GPIO_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTB)
#define mB_CH1_DE_BSRR		STM32_GPIOB_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 2
#if ((GPIO_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTC)
#define mB_CH1_DE_BSRR		STM32_GPIOC_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 3
#if ((GPIO_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTD)
#define mB_CH1_DE_BSRR		STM32_GPIOD_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 4
#if ((GPIO_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTE)
#define mB_CH1_DE_BSRR		STM32_GPIOE_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 5
#if ((GPIO_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTF)
#define mB_CH1_DE_BSRR		STM32_GPIOF_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 6
#if ((GPIO_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTG)
#define mB_CH1_DE_BSRR		STM32_GPIOG_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 7
#if ((GPIO_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTH)
#define mB_CH1_DE_BSRR		STM32_GPIOH_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 8
#if ((GPIO_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTI)
#define mB_CH1_DE_BSRR		STM32_GPIOI_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 9
#if ((GPIO_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTJ)
#define mB_CH1_DE_BSRR		STM32_GPIOJ_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 10
#if ((GPIO_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTK)
#define mB_CH1_DE_BSRR		STM32_GPIOK_BSRR
#endif
#endif
#endif //CONFIG_MESSBUS_USE_CH1

/****************************************************************************
 * Ch2 Data Enable pin
 ****************************************************************************/

#if CONFIG_MESSBUS_USE_CH2
/* Extract the pin number */
#define mB_CH2_DE_ON			(1 << (GPIO_CH2_DE & GPIO_PIN_MASK))
#define mB_CH2_DE_OFF			(1 << ((GPIO_CH2_DE & GPIO_PIN_MASK)+16))

/* Get the right BSRR address according to port configuration */
#if mB_NGPIO_PORTS > 0
#if ((GPIO_CH2_DE & GPIO_PORT_MASK) == GPIO_PORTA)
#define mB_CH2_DE_BSRR		STM32_GPIOA_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 1
#if ((GPIO_CH2_DE & GPIO_PORT_MASK) == GPIO_PORTB)
#define mB_CH2_DE_BSRR		STM32_GPIOB_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 2
#if ((GPIO_CH2_DE & GPIO_PORT_MASK) == GPIO_PORTC)
#define mB_CH2_DE_BSRR		STM32_GPIOC_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 3
#if ((GPIO_CH2_DE & GPIO_PORT_MASK) == GPIO_PORTD)
#define mB_CH2_DE_BSRR		STM32_GPIOD_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 4
#if ((GPIO_CH2_DE & GPIO_PORT_MASK) == GPIO_PORTE)
#define mB_CH2_DE_BSRR		STM32_GPIOE_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 5
#if ((GPIO_CH2_DE & GPIO_PORT_MASK) == GPIO_PORTF)
#define mB_CH2_DE_BSRR		STM32_GPIOF_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 6
#if ((GPIO_CH2_DE & GPIO_PORT_MASK) == GPIO_PORTG)
#define mB_CH2_DE_BSRR		STM32_GPIOG_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 7
#if ((GPIO_CH2_DE & GPIO_PORT_MASK) == GPIO_PORTH)
#define mB_CH2_DE_BSRR		STM32_GPIOH_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 8
#if ((GPIO_CH2_DE & GPIO_PORT_MASK) == GPIO_PORTI)
#define mB_CH2_DE_BSRR		STM32_GPIOI_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 9
#if ((GPIO_CH2_DE & GPIO_PORT_MASK) == GPIO_PORTJ)
#define mB_CH2_DE_BSRR		STM32_GPIOJ_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 10
#if ((GPIO_CH2_DE & GPIO_PORT_MASK) == GPIO_PORTK)
#define mB_CH2_DE_BSRR		STM32_GPIOK_BSRR
#endif
#endif
#endif //CONFIG_MESSBUS_USE_CH2

/****************************************************************************
 * Ch3 Data Enable pin
 ****************************************************************************/

#if CONFIG_MESSBUS_USE_CH3
/* Extract the pin number */
#define mB_CH3_DE_ON			(1 << (GPIO_CH3_DE & GPIO_PIN_MASK))
#define mB_CH3_DE_OFF			(1 << ((GPIO_CH3_DE & GPIO_PIN_MASK)+16))

/* Get the right BSRR address according to port configuration */
#if mB_NGPIO_PORTS > 0
#if ((GPIO_CH3_DE & GPIO_PORT_MASK) == GPIO_PORTA)
#define mB_CH3_DE_BSRR		STM32_GPIOA_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 1
#if ((GPIO_CH3_DE & GPIO_PORT_MASK) == GPIO_PORTB)
#define mB_CH3_DE_BSRR		STM32_GPIOB_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 2
#if ((GPIO_CH3_DE & GPIO_PORT_MASK) == GPIO_PORTC)
#define mB_CH3_DE_BSRR		STM32_GPIOC_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 3
#if ((GPIO_CH3_DE & GPIO_PORT_MASK) == GPIO_PORTD)
#define mB_CH3_DE_BSRR		STM32_GPIOD_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 4
#if ((GPIO_CH3_DE & GPIO_PORT_MASK) == GPIO_PORTE)
#define mB_CH3_DE_BSRR		STM32_GPIOE_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 5
#if ((GPIO_CH3_DE & GPIO_PORT_MASK) == GPIO_PORTF)
#define mB_CH3_DE_BSRR		STM32_GPIOF_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 6
#if ((GPIO_CH3_DE & GPIO_PORT_MASK) == GPIO_PORTG)
#define mB_CH3_DE_BSRR		STM32_GPIOG_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 7
#if ((GPIO_CH3_DE & GPIO_PORT_MASK) == GPIO_PORTH)
#define mB_CH3_DE_BSRR		STM32_GPIOH_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 8
#if ((GPIO_CH3_DE & GPIO_PORT_MASK) == GPIO_PORTI)
#define mB_CH3_DE_BSRR		STM32_GPIOI_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 9
#if ((GPIO_CH3_DE & GPIO_PORT_MASK) == GPIO_PORTJ)
#define mB_CH3_DE_BSRR		STM32_GPIOJ_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 10
#if ((GPIO_CH3_DE & GPIO_PORT_MASK) == GPIO_PORTK)
#define mB_CH3_DE_BSRR		STM32_GPIOK_BSRR
#endif
#endif
#endif //CONFIG_MESSBUS_USE_CH3

/****************************************************************************
 * Ch4 Data Enable pin
 ****************************************************************************/

#if CONFIG_MESSBUS_USE_CH4
/* Extract the pin number */
#define mB_CH4_DE_ON			(1 << (GPIO_CH4_DE & GPIO_PIN_MASK))
#define mB_CH4_DE_OFF			(1 << ((GPIO_CH4_DE & GPIO_PIN_MASK)+16))

/* Get the right BSRR address according to port configuration */
#if mB_NGPIO_PORTS > 0
#if ((GPIO_CH4_DE & GPIO_PORT_MASK) == GPIO_PORTA)
#define mB_CH4_DE_BSRR		STM32_GPIOA_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 1
#if ((GPIO_CH4_DE & GPIO_PORT_MASK) == GPIO_PORTB)
#define mB_CH4_DE_BSRR		STM32_GPIOB_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 2
#if ((GPIO_CH4_DE & GPIO_PORT_MASK) == GPIO_PORTC)
#define mB_CH4_DE_BSRR		STM32_GPIOC_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 3
#if ((GPIO_CH4_DE & GPIO_PORT_MASK) == GPIO_PORTD)
#define mB_CH4_DE_BSRR		STM32_GPIOD_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 4
#if ((GPIO_CH4_DE & GPIO_PORT_MASK) == GPIO_PORTE)
#define mB_CH4_DE_BSRR		STM32_GPIOE_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 5
#if ((GPIO_CH4_DE & GPIO_PORT_MASK) == GPIO_PORTF)
#define mB_CH4_DE_BSRR		STM32_GPIOF_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 6
#if ((GPIO_CH4_DE & GPIO_PORT_MASK) == GPIO_PORTG)
#define mB_CH4_DE_BSRR		STM32_GPIOG_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 7
#if ((GPIO_CH4_DE & GPIO_PORT_MASK) == GPIO_PORTH)
#define mB_CH4_DE_BSRR		STM32_GPIOH_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 8
#if ((GPIO_CH4_DE & GPIO_PORT_MASK) == GPIO_PORTI)
#define mB_CH4_DE_BSRR		STM32_GPIOI_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 9
#if ((GPIO_CH4_DE & GPIO_PORT_MASK) == GPIO_PORTJ)
#define mB_CH4_DE_BSRR		STM32_GPIOJ_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 10
#if ((GPIO_CH4_DE & GPIO_PORT_MASK) == GPIO_PORTK)
#define mB_CH4_DE_BSRR		STM32_GPIOK_BSRR
#endif
#endif
#endif //CONFIG_MESSBUS_USE_CH4

/****************************************************************************
 * Debug GPIOs
 ****************************************************************************/

/* This pin has to be belong to Port D */
#define GPIO_DEBUG_ISR     (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
							GPIO_OUTPUT_CLEAR|GPIO_PORTD|GPIO_PIN7)
#define GPIO_DEBUG_ISR_ON	(1 << 7)
#define GPIO_DEBUG_ISR_OFF	(1 << (7+16))

#endif /* _ARCH_ARM_SRC_STM32_STM32_MESSBUSMASTER_H_ */

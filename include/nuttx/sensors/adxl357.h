/********************************************************************************************
 * include/nuttx/input/adxl357.h
 *
 *   Author: Björn Brandt
 *   Based on ADXL345 driver
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 ********************************************************************************************/

#ifndef __INCLUDE_NUTTX_INPUT_ADXL357_H
#define __INCLUDE_NUTTX_INPUT_ADXL357_H

/********************************************************************************************
 * Included Files
 ********************************************************************************************/

#include <nuttx/config.h>
#include <nuttx/irq.h>
#include <time.h>
#include <stdbool.h>

#if defined(CONFIG_SENSORS_ADXL357)

/********************************************************************************************
 * Pre-processor Definitions
 ********************************************************************************************/
/* Configuration ****************************************************************************/
/* Prerequisites:
 *
 * CONFIG_SCHED_HPWORK - High priority work queue support is required
 *
 * CONFIG_SPI _ SPI must be enabled since I2C is not supported yet
 *
 * CONFIG_SENSORS_ADXL357
 *   Enables support for the ADXL357 driver
 * CONFIG_ADXL357_SPI
 *   Enables support for the SPI interface
 * CONFIG_ADXL357_I2C
 *   Enables support for the I2C interface (not currently supported)
 * CONFIG_ADXL357_ACTIVELOW
 *    The ADXL357 interrupt will be inverted. Instead starting low and
 *    going high, it will start high and will go low when an interrupt
 *    is fired. Default:  Active high/rising edge.
 * CONFIG_ADXL357_REGDEBUG
 *   Enable very low register-level debug output.  Requires CONFIG_DEBUG_FEATURES.
 *   (Not yet supported.)
 */

#ifdef CONFIG_DISABLE_SIGNALS
#  error "Signals are required.  CONFIG_DISABLE_SIGNALS must not be selected."
#endif

#ifndef CONFIG_SCHED_HPWORK
#  error "High priority work queue support required.  CONFIG_SCHED_HPWORK must be selected."
#endif

/* The ADXL357 interfaces with the target CPU via a I2C or SPI interface. The pin IN_1
 * allows the selection of interface protocol at reset state.
 */

#if !defined(CONFIG_ADXL357_SPI) && !defined(CONFIG_ADXL357_I2C)
#  error "One of CONFIG_ADXL357_SPI or CONFIG_ADXL357_I2C must be defined"
#endif

#if defined(CONFIG_ADXL357_SPI) && defined(CONFIG_ADXL357_I2C)
#  error "Only one of CONFIG_ADXL357_SPI or CONFIG_ADXL357_I2C can be defined"
#endif

/* Check for some required settings.  This can save the user a lot of time
 * in getting the right configuration.
 */

#ifdef CONFIG_ADXL357_I2C
#  error "I2C interface of ADXL357 not yet supported!"
#  ifndef CONFIG_I2C
#    error "CONFIG_I2C is required in the I2C support"
#  endif
#endif


/* SPI **************************************************************************************/
#ifdef CONFIG_ADXL357_SPI
/* The device always operates in mode 0 */

#define ADXL357_SPI_MODE                    SPIDEV_MODE0 /* Mode 0 */

/* SPI frequency */

#define ADXL357_SPI_MAXFREQUENCY            10000000       /* 10 MHz */
#define ADXL357_SPI_MINFREQUENCY            100000         /* 100 kHz */

/* SPI Bits per word */

#define ADXL357_SPI_NBITS                   8

#endif /* CONFIG_ADXL357_SPI_MODE */

/* ADXL357 Registers ************************************************************************/
/* Register Addresses */

#define ADXL357_DEVID_AD                    0x00  /* Contains the Analog Devices ID, 0xAD */
#define ADXL357_DEVID_MST                   0x01  /* Contains the Analog Devices MEMS ID, 0x1D */
#define ADXL357_PARTID                      0x02  /* Contains the device ID (355 octal), 0xED */
#define ADXL357_REVID                       0x03  /* Contains the product revion ID (8 bit) */
#define ADXL357_STATUS                      0x04  /* Status register describing the device conditions (8 bit) */
#define ADXL357_FIFO_ENTRIES                0x05  /* This register indicates the number of valid data samples present
                                                     in the FIFO buffer. This number ranges from 0 to 96.*/
#define ADXL357_TEMP2                       0x06  /* Contains the 4 most significant bits of the 12 bit temperature value */
#define ADXL357_TEMP1                       0x07  /* Contains the 8 least significant bits of the 12 bit temperature value */
#define ADXL357_XDATA3                      0x08  /* X-axis Data 3 */
#define ADXL357_XDATA2                      0x09  /* X-axis Data 2 */
#define ADXL357_XDATA1                      0x0A  /* X-axis Data 1 */
#define ADXL357_YDATA3                      0x0B  /* Y-axis Data 3 */
#define ADXL357_YDATA2                      0x0C  /* Y-axis Data 2 */
#define ADXL357_YDATA1                      0x0D  /* Y-axis Data 1 */
#define ADXL357_ZDATA3                      0x0E  /* Z-axis Data 3 */
#define ADXL357_ZDATA2                      0x0F  /* Z-axis Data 2 */
#define ADXL357_ZDATA1                      0x10  /* Z-axis Data 1 */
#define ADXL357_FIFO_DATA                   0x11  /* Read this register to access data stored in the FIFO */

#define ADXL357_OFFSET_X_H                  0x1E  /* X-axis offset trim register: MSBs (8 bit) */
#define ADXL357_OFFSET_X_L                  0x1F  /* X-axis offset trim register: LSBs (8 bit) */
#define ADXL357_OFFSET_Y_H                  0x20  /* Y-axis offset trim register: MSBs (8 bit) */
#define ADXL357_OFFSET_Y_L                  0x21  /* Y-axis offset trim register: LSBs (8 bit) */
#define ADXL357_OFFSET_Z_H                  0x22  /* Z-axis offset trim register: MSBs (8 bit) */
#define ADXL357_OFFSET_Z_L                  0x23  /* Z-axis offset trim register: LSBs (8 bit) */

#define ADXL357_ACT_EN                      0x24  /* Activity enable register */
#define ADXL357_ACT_THRESH_H                0x25  /* Activity threshold: MSBs (8 bit) */
#define ADXL357_ACT_THRESH_L                0x26  /* Activity threshold: LSBs (8 bit) */
#define ADXL357_ACT_COUNT                   0x27  /* Activity count */

#define ADXL357_FILTER                      0x28  /* Parameters for internal high-pass and low-pass filters */
#define ADXL357_FIFO_SAMPLES                0x29  /* Number of samples to store in the FIFO (default: 0x60) */
#define ADXL357_INT_MAP                     0x2A  /* Interrupt mapping control */
#define ADXL357_SYNC                        0x2B  /* Control of external timing triggers */

#define ADXL357_RANGE                       0x2C  /* I2C Speed, interrupt polarity and range register */
#define ADXL357_POWER_CTL                   0x2D  /* Power-saving features control */
#define ADXL357_SELF_TEST                   0x2E  /* Self test */
#define ADXL357_RESET                       0x2F  /* Reset */



/* Register bit definitions */


/* Register 0x04 - STATUS */

#define ADXL357_STATUS_DATA_RDY             (1 << 0)  /* Bit 0: A complete measurement was made and results can be read */
#define ADXL357_STATUS_FIFO_FULL            (1 << 1)  /* Bit 1: FIFO watermark is reached */
#define ADXL357_STATUS_FIFO_OVR             (1 << 2)  /* Bit 2: FIFO has overrun, and the oldest data is lost */
#define ADXL357_STATUS_ACTIVITY             (1 << 3)  /* Bit 3: Activity, as defined in the ACT_THRESH_x and ACT_COUNT registers, is detected */
#define ADXL357_STATUS_NVM_BUSY             (1 << 4)  /* Bit 4: NVM controller is busy */
/* Bit 5-7: reserved */

/* Register 0x05 - FIFO_ENTRIES */
#define ADXL357_FIFO_ENTRIES_MASK           (0x7F)

/* Register 0x06 - TEMP2 */
#define ADXL357_TEMP2_MASK                  (0x0F)

/* Register 0x0A, 0x0D, 0x10 - XDATA1, YDATA1, ZDATA1) */
#define ADXL357_XYZDATA1_MASK               (0xE0) 

/* Register 0x24 - ACT_EN */
#define ADXL357_ACT_X_ENABLE                (1 << 0)  /* Bit 0: Enable x-axis activity detection */
#define ADXL357_ACT_Y_ENABLE                (1 << 1)  /* Bit 1: Enable y-axis activity detection */
#define ADXL357_ACT_Z_ENABLE                (1 << 2)  /* Bit 2: Enable z-axis activity detection */

/* Register 0x28 - FILTER */
#define ADXL357_HPF_CORNER_MASK             (0x70)    /* -3dB filter corner for the 1. order high-pass filter. 
                                                 All values realtive to output data rate (ODR) */
#   define ADXL357_HPF_CORNER_NO            (0 << 4)  /* high-pass filter disabled */
#   define ADXL357_HPF_CORNER_247           (1 << 4)  /* 247 x 10^-3 x ODR */
#   define ADXL357_HPF_CORNER_062           (2 << 4)  /* 62.084 x 10^-3 x ODR */
#   define ADXL357_HPF_CORNER_015           (3 << 4)  /* 15.545 x 10^-3 x ODR */
#   define ADXL357_HPF_CORNER_004           (4 << 4)  /* 3.862 x 10^-3 x ODR */
#   define ADXL357_HPF_CORNER_001           (5 << 4)  /* 0.954 x 10^-3 x ODR */
#   define ADXL357_HPF_CORNER_000           (6 << 4)  /* 0.238 x 10^-3 x ODR */

#define ADXL357_ODR_LPF_CORNER_MASK         (0x0F)    /* ODR and low-pass filter corner */
#   define ADXL357_ODR_4000                 (0 << 0)  /* ODR: 4000     Hz, lp-filter corner: 1000     Hz */
#   define ADXL357_ODR_2000                 (1 << 0)  /* ODR: 2000     Hz, lp-filter corner:  500     Hz */
#   define ADXL357_ODR_1000                 (2 << 0)  /* ODR: 1000     Hz, lp-filter corner:  250     Hz */
#   define ADXL357_ODR_0500                 (3 << 0)  /* ODR:  500     Hz, lp-filter corner:  125     Hz */
#   define ADXL357_ODR_0250                 (4 << 0)  /* ODR:  250     Hz, lp-filter corner:   62.5   Hz */
#   define ADXL357_ODR_0125                 (5 << 0)  /* ODR:  125     Hz, lp-filter corner:   31.25  Hz */
#   define ADXL357_ODR_0063                 (6 << 0)  /* ODR:   62.5   Hz, lp-filter corner:   15.625 Hz */
#   define ADXL357_ODR_0031                 (7 << 0)  /* ODR:   31.25  Hz, lp-filter corner:    7.813 Hz */
#   define ADXL357_ODR_0016                 (8 << 0)  /* ODR:   15.625 Hz, lp-filter corner:    3.906 Hz */
#   define ADXL357_ODR_0008                 (9 << 0)  /* ODR:    7.813 Hz, lp-filter corner:    1.953 Hz */
#   define ADXL357_ODR_0004                 (10 << 0) /* ODR:    3.906 Hz, lp-filter corner:    0.977 Hz */


/* Register 0x29 - FIFO_SAMPLES */
#define ADXL357_FIFO_ENTRIES_MASK           (0x7F)


/* Register 0x2A - INT_MAP */

#define ADXL357_INT_MAP_RDY_EN1             (1 << 0)  /* Bit 0: Map DATA_RDY interrupt enable on INT1 */
#define ADXL357_INT_MAP_FULL_EN1            (1 << 1)  /* Bit 1: Map Fifo Watermark interrupt on INT1 */
#define ADXL357_INT_MAP_OVR_EN1             (1 << 2)  /* Bit 2: Map Fifo overrun interrupt on  INT1 */
#define ADXL357_INT_MAP_ACT_EN1             (1 << 3)  /* Bit 3: Map Activity interrupt on INT1 */
#define ADXL357_INT_MAP_RDY_EN2             (1 << 4)  /* Bit 0: Map DATA_RDY interrupt enable on INT2 */
#define ADXL357_INT_MAP_FULL_EN2            (1 << 5)  /* Bit 1: Map Fifo Watermark interrupt on INT2 */
#define ADXL357_INT_MAP_OVR_EN2             (1 << 6)  /* Bit 2: Map Fifo overrun interrupt on  INT2 */
#define ADXL357_INT_MAP_ACT_EN2             (1 << 7)  /* Bit 3: Map Activity interrupt on INT2 */


/* Register 0x2B - SYNC */
#define ADXL357_SYNC_MASK                   (0x03)
#   define ADXL357_SYNC_INT                 (0 << 0)  /* Internal sync */
#   define ADXL357_EXT_SYNC_INTERPOL        (1 << 1)  /* External sync with interpolation */
#   define ADXL357_EXT_SYNC_NO_INTERPOL     (1 << 0)  /* External sync and external clock without interpolation */
#define ADXL357_EXT_CLK                     (1 << 2)  /* Bit 2: Enable external clock */


/* Register 0x2C - Range */ 
#define ADXL357_I2C_SPEED_MASK              (0x80)    /* I2C speed */
#define ADXL357_I2C_SPEED HS_SHIFT          7
#   define ADXL357_I2C_SPEED HS             (1 << ADXL357_I2C_SPEED HS_SHIFT)   /* High speed */
#   define ADXL357_I2C_SPEED FM             (0 << ADXL357_I2C_SPEED HS_SHIFT)   /* Fast mode */
#define ADXL357_INT_POL_SHIFT               6
#define ADXL357_INT_POL_MASK                (1 << ADXL357_INT_POL_SHIFT)   /* I2C input polarity */
#   define ADXL357_INT_ACTIVE_LOW           (0 << ADXL357_INT_POL_SHIFT)
#   define ADXL357_INT_ACTIVE_HIGH          (1 << ADXL357_INT_POL_SHIFT)
#define ADXL357_RANGE_SHIFT                 0
#define ADXL357_RANGE_MASK                  (0x3)     /* G-force Range */
#   define ADXL357_RANGE_10G                (1 << ADXL357_RANGE_SHIFT)
#   define ADXL357_RANGE_20G                (2 << ADXL357_RANGE_SHIFT)
#   define ADXL357_RANGE_40G                (3 << ADXL357_RANGE_SHIFT)


/* Register 0x2D - POWER_CTL */
#define ADXL357_STANDBY_ON                  (1 << 0)  /* Bit 0: Enable Standby Mode */
#define ADXL357_MEASUREMENT_ON              (0 << 0)  /* Bit 0: Enable Measurement Mode */
#define ADXL357_MEASUREMENT_OFF             STANDBY_ON
#define ADXL357_TEMP_OFF                    (1 << 1)  /* Bit 1: Disable temperature processing */
#define ADXL357_DRDY_OFF                    (1 << 2)  /* Bit 2: Force DRDY output to always 0 (low) */


/* Register 0x2E - SELF_TEST */
#define ADXL357_SELF_TEST_MASK              0x03
#define ADXL357_ST1                         (1 << 0)  /* Bit 0: Enable self test mode */
#define ADXL357_ST2                         (1 << 1)  /* Bit 1: Enable self test force */


/* Register 0x2F - RESET */
#define ADXL357_RESET_VALUE                 0x52      /* Write this value to reset the device, similar to a power-on reset */


#define ADXL357_DEVID_AD_VALUE              0xAD
#define ADXL357_DEVID_MST_VALUE             0x1D
#define ADXL357_DEVID_PARTID_VALUE          0xED


/********************************************************************************************
 * Public Types
 ********************************************************************************************/

/* Enum describing the 3 different ADXL357 interrupts */

typedef enum adxl357_IRQ_e
{
    ADXL357_IRQ_DRDY,
    ADXL357_IRQ_INT1,
    ADXL357_IRQ_INT2,
}adxl357_IRQ_e;

typedef struct timespec adxl357_timestamp;

/* This structure describes the results of one ADXL357 sample */

typedef struct adxl357_sample_s
{
    adxl357_timestamp timestamp;
    int32_t acc_x;                     /* Measured X-axis acceleration */
    int32_t acc_y;                     /* Measured Y-axis acceleration */
    int32_t acc_z;                     /* Measured Z-axis acceleration */
}adxl357_sample_t;

typedef struct adxl357_temperature_sample_s
{
    adxl357_timestamp timestamp;
    uint16_t temperature;              /* Measured sensor temperature. 12 bit value. */
} adxl357_temperature_sample_t;


/********************* Configuration ********************/

/* Trim offsets. Values from -262,141 - 262,140 are valid */

typedef struct adxl357_offset_s
{
    int32_t x;                         /* X-axis trim value added to x-axis data. Interval = [0x08 - 0x7FFF8]*/
    int32_t y;                         /* Y-axis trim value added to y-axis data. Interval = [0x08 - 0x7FFF8]*/
    int32_t z;                         /* Z-axis trim value added to z-axis data. Interval = [0x08 - 0x7FFF8]*/
}adxl357_offset_t;

/* -3 dB filter corner for the first-order, high-pass filter relative to the ODR */

typedef enum adxl357_hpfcorner_e
{
    off,                               /* filter disabled */
    hpf_247,                           /* 247 x 10^-3 x ODR */   
    hpf_62,                            /* 62.084 x 10^-3 x ODR */
    hpf_15,                            /* 15.545 x 10^-3 x ODR */
    hpf_3,                             /* 3.862 x 10^-3 x ODR */ 
    hpf_0_9,                           /* 0.954 x 10^-3 x ODR */ 
    hpf_0_2                            /* 0.238 x 10^-3 x ODR */ 
}adxl357_hpfcorner_t;

/* ODR and low-pass filter corner */

typedef enum adxl357_odr_lpf_e
{
    odr_4000Hz = 4000,                  /* ODR: 4000     Hz, lp-filter corner: 1000     Hz */
    odr_2000Hz = 2000,                  /* ODR: 2000     Hz, lp-filter corner:  500     Hz */
    odr_1000Hz = 1000,                  /* ODR: 1000     Hz, lp-filter corner:  250     Hz */
    odr_500Hz  =  500,                  /* ODR:  500     Hz, lp-filter corner:  125     Hz */
    odr_250Hz  =  250,                  /* ODR:  250     Hz, lp-filter corner:   62.5   Hz */
    odr_125Hz  =  125,                  /* ODR:  125     Hz, lp-filter corner:   31.25  Hz */
    odr_62_5Hz =   63,                  /* ODR:   62.5   Hz, lp-filter corner:   15.625 Hz */
    odr_31_2Hz =   31,                  /* ODR:   31.25  Hz, lp-filter corner:    7.813 Hz */
    odr_15_6Hz =   16,                  /* ODR:   15.625 Hz, lp-filter corner:    3.906 Hz */
    odr_7_81Hz =    8,                  /* ODR:    7.813 Hz, lp-filter corner:    1.953 Hz */
    odr_3_91Hz =    4,                  /* ODR:    3.906 Hz, lp-filter corner:    0.977 Hz */
}adxl357_odr_lpf_t;

/* Sensor range */

typedef enum adxl357_range_e
{
    range_10g,                          /* Range: +- 10 g */
    range_20g,                          /* Range: +- 20 g */
    range_40g,                          /* Range: +- 40 g */
}adxl357_range_t;

typedef struct adxl357_sensor_config_s
{
  adxl357_range_t     range;             /* Sensor measurement range */
  adxl357_odr_lpf_t   odr;               /* Output data rate that also defines low-pass filter corner */
  uint16_t            acc_read_divider;  /* Specifies frequency to read acceleration data as bulk
                                          * from fifo: Acc read freq = ODR/divider.
                                          * Values = [1,32] are allowed. */
  uint16_t            temp_read_divider; /* Frequency divider for polling temperature.
                                          * Temperature read freq = Acc read freq/divider.
                                          */
  adxl357_hpfcorner_t hpfcorner;         /* High-pass filter corner */
  adxl357_offset_t    offsets;           /* Trim values for x-data, y-data, z-data */

}adxl357_sensor_config_t;


/* A reference to a structure of this type must be passed to the ADXL357 driver when the
 * driver is instantiated. This structure provides information about the configuration of the
 * ADXL357 and provides some board-specific hooks.
 *
 * Memory for this structure is provided by the caller.  It is not copied by the driver
 * and is presumed to persist while the driver is active. The memory must be writeable
 * because, under certain circumstances, the driver may modify the frequency.
 */

struct adxl357_config_s
{
  /* Device characterization */

#ifdef CONFIG_ADXL357_I2C
  uint8_t address;                       /* 7-bit I2C address (only bits 0-6 used) */
#endif
  adxl357_sensor_config_t sensor;       /* All config data specifing ADXL357 hardware */

  /* IRQ/GPIO access callbacks. These operations all hidden behind
   * callbacks to isolate the ADXL357 driver from differences in GPIO
   * interrupt handling by varying boards and MCUs.
   *
   * attachIRQ  - Attach the ADXL357 interrupt handlers to the GPIO interrupt
   * enableIRQ  - Enable or disable the GPIO interrupt
   */

  int  (*attachIRQ)(FAR struct adxl357_config_s *state, xcpt_t handlerDRDY, FAR void *argDRDY,
                          xcpt_t handlerInt1, FAR void* argInt1,
                          xcpt_t handlerInt2, FAR void* argInt2);
  void (*enableIRQ)(FAR struct adxl357_config_s *state, adxl357_IRQ_e irq, bool enable);
  void (*triggerSync)(void);
  void (*triggerPhaseShift)(const int8_t);
};

typedef FAR void *ADXL357_HANDLE;

#ifdef CONFIG_ADXL357_SPI
   struct spi_dev_s;
#else
   struct i2c_master_s;
#endif


/********************************************************************************************
 * Public Function Prototypes
 ********************************************************************************************/

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/********************************************************************************************
 * Name: adxl357_instantiate
 *
 * Description:
 *   Instantiate and configure the ADXL357 device driver to use the provided I2C or SPI
 *   device instance.
 *
 * Input Parameters:
 *   dev     - An I2C or SPI driver instance
 *   config  - Persistent board configuration data
 *
 * Returned Value:
 *   A non-zero handle is returned on success.  This handle may then be used to configure
 *   the ADXL357 driver as necessary.  A NULL handle value is returned on failure.
 *
 ********************************************************************************************/

#ifdef CONFIG_ADXL357_SPI
ADXL357_HANDLE adxl357_instantiate(FAR struct spi_dev_s *dev,
                                   FAR struct adxl357_config_s *config);
#else
ADXL357_HANDLE adxl357_instantiate(FAR struct i2c_master_s *dev,
                                   FAR struct adxl357_config_s *config);
#endif

/********************************************************************************************
 * Name: adxl357_register
 *
 * Description:
 *  This function will register the accelerometer driver as /dev/accelN
 *  where N is the minor device number
 *
 * Input Parameters:
 *   handle    - The handle previously returned by adxl357_instantiate
 *   minor     - The input device minor number
 *
 * Returned Value:
 *   Zero is returned on success.  Otherwise, a negated errno value is returned to indicate
 *   the nature of the failure.
 *
 ********************************************************************************************/

int adxl357_register(ADXL357_HANDLE handle, int minor);

#undef EXTERN
#ifdef __cplusplus
}
#endif

#endif /* CONFIG_SENSORS_ADXL357 */
#endif /* __INCLUDE_NUTTX_INPUT_ADXL357_H */

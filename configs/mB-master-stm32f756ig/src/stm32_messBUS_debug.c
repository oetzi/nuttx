/*
 * stm32_messBUS_debug.c
 *
 *  Created on: 27.02.2019
 *      Author: bbrandt
 */


/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/board.h>
#include <arch/board/board.h>
#include <debug.h>

#include "stm32_gpio.h"

#include <nuttx/messBUS/messBUS_Debug.h>

static inline void setbit32(unsigned int target, uint32_t bit) __attribute__((always_inline));
static inline void setbit32(unsigned int target, uint32_t bit)
{
    *(volatile uint32_t *) (target) |= (bit);
}
;

void switchDebugPinOn(void)
{
    setbit32(STM32_GPIOH_BSRR, GPIO_MB_DEBUG_ON);
}

void switchDebugPinOff(void)
{
    setbit32(STM32_GPIOH_BSRR, GPIO_MB_DEBUG_OFF);
}

void toggleDebugPin(void)
{
    switchDebugPinOn();
    switchDebugPinOff();
}

void switchDebugPinOn2(void)
{
    setbit32(STM32_GPIOH_BSRR, GPIO_MB_DEBUG_ON2);
}

void switchDebugPinOff2(void)
{
    setbit32(STM32_GPIOH_BSRR, GPIO_MB_DEBUG_OFF2);
}

void toggleDebugPin2(void)
{
    switchDebugPinOn2();
    switchDebugPinOff2();
}

void switchDebugPinOn3(void)
{
    setbit32(STM32_GPIOI_BSRR, GPIO_MB_DEBUG_ON3);
}

void switchDebugPinOff3(void)
{
    setbit32(STM32_GPIOI_BSRR, GPIO_MB_DEBUG_OFF3);
}

void toggleDebugPin3(void)
{
    switchDebugPinOn3();
    switchDebugPinOff3();
}

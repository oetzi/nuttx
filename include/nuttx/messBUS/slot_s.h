/*
 * slot_s.h
 *
 *  Created on: 06.03.2018
 *      Author: bbrandt
 */

#ifndef NUTTX_INCLUDE_NUTTX_MESSBUS_SLOT_S_H_
#define NUTTX_INCLUDE_NUTTX_MESSBUS_SLOT_S_H_

#if defined(__cplusplus)
namespace PhysicalLayer
{
#endif

/* This struct defines a single slot */
struct slot_s
{
    uint16_t ID;
    uint16_t t_start;
    uint16_t t_end;
    uint8_t read_write;
    uint32_t baud;
    char *buffer;
    uint16_t buf_length;
};

#if defined(__cplusplus)
}
#endif

#endif /* NUTTX_INCLUDE_NUTTX_MESSBUS_SLOT_S_H_ */

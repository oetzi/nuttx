/****************************************************************************
 * config/mB_I2C/src/stm32_ams5812.c
 *
 * Character driver for Analog Microelectronics AMS5812 Pressure sensors
 *
 * Based on MS58XX driver.
 *
 *   Copyright (C) 2015 Omni Hoverboards Inc. All rights reserved.
 *   Author: Paul Alexander Patience <paul-a.patience@polymtl.ca>
 *   Updated by: Karim Keddam <karim.keddam@polymtl.ca>
 *
 *   Copyright (C) 2016 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <errno.h>
#include <debug.h>
#include <stdlib.h>

#include <nuttx/kmalloc.h>
#include <nuttx/fs/fs.h>
#include <nuttx/arch.h>
#include <nuttx/i2c/i2c_master.h>
#include <nuttx/sensors/ams5812.h>

#include "mB_I2C.h"

#if defined(CONFIG_I2C) && defined(CONFIG_SENSORS_AMS5812)

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#ifndef CONFIG_AMS5812_I2C_FREQUENCY
/* Since 400 kHz produces data spikes, set I2C frequency to 100 kHz
 * (even if datasheet says 400 kHz).
 */
#  define CONFIG_AMS5812_I2C_FREQUENCY I2C_SPEED_STANDARD
#endif

#define AMS5812_RAW_SAMPLE_SIZE        4   /* 2 Byte raw pressure
                                            * +2 Byte raw temperature */

/****************************************************************************
 * Private Types
 ****************************************************************************/

struct ams5812_dev_s
{
  FAR struct i2c_master_s *i2c; /* I2C interface */
  uint8_t                 addr; /* I2C address */

  enum ams5812_model_e   model;

  uint16_t               temp;   /* raw digital 15-bit temperature data word */
  uint16_t               press;  /* raw digital 15-bit pressure data word */
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* I2C Helpers */

static int ams5812_i2c_read(FAR struct ams5812_dev_s *priv,
                            FAR uint8_t *buffer, int buflen);
static int ams5812_measure(FAR struct ams5812_dev_s *priv);

static float ams5812_convertTemperature(enum ams5812_model_e sensorType,
                                        uint16_t rawValue);
static float ams5812_convertPressure(enum ams5812_model_e sensorType,
                                     uint16_t rawValue);

/* Character Driver Methods */

static int     ams5812_open(FAR struct file *filep);
static int     ams5812_close(FAR struct file *filep);
static ssize_t ams5812_read(FAR struct file *filep, FAR char *buffer,
                           size_t buflen);
static ssize_t ams5812_write(FAR struct file *filep, FAR const char *buffer,
                            size_t buflen);
static int     ams5812_ioctl(FAR struct file *filep, int cmd,
                            unsigned long arg);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct file_operations g_fops =
{
  ams5812_open,
  ams5812_close,
  ams5812_read,
  ams5812_write,
  NULL,
  ams5812_ioctl,
  NULL
#ifndef CONFIG_DISABLE_PSEUDOFS_OPERATIONS
  , NULL
#endif
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: ams5812_i2c_read
 *
 * Description:
 *   Read from the I2C device.
 *
 ****************************************************************************/

static int ams5812_i2c_read(FAR struct ams5812_dev_s *priv,
                            FAR uint8_t *buffer, int buflen)
{
  struct i2c_msg_s msg;
  int ret;

  /* Setup for the transfer */

  msg.frequency = CONFIG_AMS5812_I2C_FREQUENCY,
  msg.addr      = priv->addr,
  msg.flags     = I2C_M_READ;
  msg.buffer    = buffer;
  msg.length    = buflen;

  /* Then perform the transfer. */

  ret = I2C_TRANSFER(priv->i2c, &msg, 1);
  return (ret >= 0) ? OK : ret;
}

/****************************************************************************
 * Name: ams5812_convertPressure
 *
 * Description:
 *       Converts the 16 bit 'rawValue' to a float value (unit: milibar)
 *
 *       Conversion depends on the sensorType.
 *
 ****************************************************************************/

static float ams5812_convertPressure(enum ams5812_model_e sensorType, uint16_t rawValue)
{
  switch (sensorType)
    {
        case AMS5812_MODEL_0001_D_B:
            /* Differential, minVal = -10,34 mbar, maxVal = 10,34 mbar */
            return (rawValue - 3277.0) / (29491.0 - 3277.0) * 20.68 - 10.34;
        case AMS5812_MODEL_0150_A:
            //Barometric, minVal = 0 mbar, maxVal 1034 mbar
            return (rawValue - 3277.0) / (29491.0 - 3277.0) * 1034.0 + 0;
        case AMS5812_MODEL_0150_B:
            /* Barometric, minVal = 758,4 mbar, maxVal 1206 mbar */
            return (rawValue - 3277.0) / (29491.0 - 3277.0) * 447.6 + 758.4;
        case AMS5812_MODEL_0003_D:
            /* Low pressure, Differential. minVal = 0 mbar, maxVal = 20.68 mBar */
            return (rawValue - 3277.0) / (29491.0 - 3277.0) * 20.68 + 0;
        case AMS5812_MODEL_0015_D_B:
            //Differential, minVal = -103,4 mbar, maxVal = 103,4 mbar
            return (rawValue - 3277.0) / (29491.0 - 3277.0) * 206.8 - 103.4;
        case AMS5812_MODEL_0300_A:
            //Barometric, minVal = 0 mbar, maxVal 2068 mbar
            return (rawValue - 3277.0) / (29491.0 - 3277.0) * 2068.0;
        default:
            return -99998; // error: unreachable Value
    }
}

/****************************************************************************
 * Name: ams5812_convertTemperature
 *
 * Description:
 *       Converts the 16 bit 'rawValue' to a float value
 *       (unit: degree Celsius).
 *
 *       Note: Parameter 'sensorType' is currently not used since the
 *             conversion is indepentend from the supported sensor types.
 *
 ****************************************************************************/

static float ams5812_convertTemperature(enum ams5812_model_e sensorType,
                                        uint16_t rawValue)
{
  UNUSED(sensorType);

  return (rawValue - 3277.0) / (29491.0 - 3277.0) * 110.0 - 25.0;
}

/****************************************************************************
 * Name: ams5812_measure
 *
 * Description:
 *   Measure the raw temperature and the raw pressure.
 *
 ****************************************************************************/

static int ams5812_measure(FAR struct ams5812_dev_s *priv)
{
  DEBUGASSERT(priv != NULL);

  if(priv == NULL) return -EINVAL;

  uint8_t buf_raw[AMS5812_RAW_SAMPLE_SIZE];

  const int i2c_read = ams5812_i2c_read(priv, buf_raw, AMS5812_RAW_SAMPLE_SIZE);

  if (i2c_read != OK) return i2c_read;

  /* Shift the pressure / temperature data from the raw data words to Little Endian */

  priv->press = (((uint16_t) (buf_raw[0] & 0x7F)) << 8) + (((uint16_t) buf_raw[1]));
  priv->temp  = (((uint16_t) (buf_raw[2] & 0x7F)) << 8) + (((uint16_t) buf_raw[3]));

  return OK;
}

/****************************************************************************
 * Name: ams5812_open
 *
 * Description:
 *   This method is called when the device is opened.
 *
 ****************************************************************************/

static int ams5812_open(FAR struct file *filep)
{
  return OK;
}

/****************************************************************************
 * Name: ams5812_close
 *
 * Description:
 *   This method is called when the device is closed.
 *
 ****************************************************************************/

static int ams5812_close(FAR struct file *filep)
{
  return OK;
}

/****************************************************************************
 * Name: ams5812_read
 *
 * Description:
 *   A dummy read method.
 *
 ****************************************************************************/

static ssize_t ams5812_read(FAR struct file *filep, FAR char *buffer,
                           size_t buflen)
{
  ssize_t size;
  FAR struct inode *inode        = filep->f_inode;
  FAR struct ams5812_dev_s *priv  = inode->i_private;
  FAR struct ams5812_measure_s *p = (FAR struct ams5812_measure_s *)buffer;

  size = buflen;
  while (size >= sizeof(*p))
    {
      if (ams5812_measure(priv) < 0)
        {
          return -1;
        }

      p->temperature  = ams5812_convertTemperature(priv->model, priv->temp);
      p->pressure     = ams5812_convertPressure(priv->model, priv->press);

      p++;
      size -= sizeof(*p);
    }

  return size;
}

/****************************************************************************
 * Name: ams5812_write
 *
 * Description:
 *   A dummy write method.
 *
 ****************************************************************************/

static ssize_t ams5812_write(FAR struct file *filep, FAR const char *buffer,
                            size_t buflen)
{
  return -ENOSYS;
}

/****************************************************************************
 * Name: ams5812_ioctl
 *
 * Description:
 *   The standard ioctl method.
 *
 ****************************************************************************/

static int ams5812_ioctl(FAR struct file *filep, int cmd, unsigned long arg)
{
  FAR struct inode        *inode = filep->f_inode;
  FAR struct ams5812_dev_s *priv  = inode->i_private;
  int                      ret   = OK;

  /* Handle ioctl commands */

  switch (cmd)
    {
      /* Measure the temperature and the pressure. Arg: None. */

      case SNIOC_MEASURE:
        DEBUGASSERT(arg == 0);
        ret = ams5812_measure(priv);
        break;

      /* Return the temperature last measured. Arg: float* pointer. */

      case SNIOC_TEMPERATURE:
        {
          FAR float *ptr = (FAR float *)((uintptr_t)arg);
          DEBUGASSERT(ptr != NULL);
          *ptr = ams5812_convertTemperature(priv->model, priv->temp);
          sninfo("temp: %3.3f\n", *ptr);
        }
        break;

      /* Return the pressure last measured. Arg: float* pointer. */

      case SNIOC_PRESSURE:
        {
          FAR float *ptr = (FAR float *)((uintptr_t)arg);
          DEBUGASSERT(ptr != NULL);
          *ptr = ams5812_convertPressure(priv->model, priv->press);
          sninfo("press: %3.3f\n", *ptr);
        }
        break;

      /* Unrecognized commands */

      default:
        snerr("ERROR: Unrecognized cmd: %d arg: %ld\n", cmd, arg);
        ret = -ENOTTY;
        break;
    }

  return ret;
}



/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: ams5812_readSample
 *
 * Description:
 *       Read a single sample from the specified AMS5812 sensor device.
 *
 *       This functions works without the nuttx VFS. That means that one
 *       does not have to register a driver instance before.
 *
 * Input Parameters:
 *   addr    - The I2C address of the AMS5812.
 *   model   - The AMS5812 model.
 *   sample  - Pointer to the target sample struct.
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/

int ams5812_readSample(uint8_t addr, enum ams5812_model_e model,
                       FAR struct ams5812_measure_s *sample)
{
    DEBUGASSERT(sample != NULL);
    DEBUGASSERT(g_i2c  != NULL);

    if(sample == NULL) return -EINVAL;
    if(g_i2c  == NULL) return -EACCES;

    /* When using this function directly, there is no registered
     * private driver instance allocated.
     *
     * So we are going to simulate one.
     */

    struct ams5812_dev_s dummy_priv =
      {
           .i2c   = g_i2c,
           .addr  = addr,
           .model = model,
           .temp  = 0,
           .press = 0
      };

    /* Read sensor data */

    const int ret = ams5812_measure(&dummy_priv);

    if(ret != OK) return ret;

    /* Convert the measurement data to physical units */

    sample->pressure    = ams5812_convertPressure(model, dummy_priv.press);
    sample->temperature = ams5812_convertTemperature(model, dummy_priv.temp);

    return OK;
}

/****************************************************************************
 * Name: ams5812_register
 *
 * Description:
 *   Register the AMS5812 character device as 'devpath'.
 *
 * Input Parameters:
 *   devpath - The full path to the driver to register, e.g., "/dev/press0".
 *   i2c     - An I2C driver instance.
 *   addr    - The I2C address of the AMS5812.
 *   model   - The AMS5812 model.
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/

int ams5812_register(FAR const char *devpath, FAR struct i2c_master_s *i2c,
                    uint8_t addr, enum ams5812_model_e model)
{
  FAR struct ams5812_dev_s *priv;
  int ret;

  /* Sanity check */

  DEBUGASSERT(i2c != NULL);
  DEBUGASSERT(model == AMS5812_MODEL_0001_D_B || model == AMS5812_MODEL_0150_B);

  /* Initialize the device's structure */

  priv = (FAR struct ams5812_dev_s *)kmm_malloc(sizeof(*priv));
  if (priv == NULL)
    {
      snerr("ERROR: Failed to allocate instance\n");
      return -ENOMEM;
    }

  priv->i2c   = i2c;
  priv->addr  = addr;
  priv->model = model;
  priv->temp  = 0;
  priv->press = 0;

  /* Register the character driver */

  ret = register_driver(devpath, &g_fops, 0666, priv);
  if (ret < 0)
    {
      snerr("ERROR: Failed to register driver: %d\n", ret);
      goto errout;
    }

  return ret;

errout:
  kmm_free(priv);
  return ret;
}

#endif /* CONFIG_I2C && CONFIG_SENSORS_AMS5812 */

/*
 * messBUS_led.h
 *
 *  Created on: 28.05.2018
 *      Author: bbrandt
 */

#ifndef NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_LED_H_
#define NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_LED_H_

#include <nuttx/config.h>
#include <nuttx/board.h>
#include <arch/board/board.h>
#include <stdbool.h>

#ifdef CONFIG_MESSBUS_USERLED

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/
void switch_LED(int led, bool state);
void toggle_LED(int led);

void switch_LED_red_on(void);
void switch_LED_red_off(void);
void toggle_LED_red(void);

void switch_LED_green_on(void);
void switch_LED_green_off(void);
void toggle_LED_green(void);

#ifdef CONFIG_ARCH_HAVE_SPI_LEDS
void switch_SPI_LED(uint16_t led, bool state);
void set_SPI_LEDS(uint16_t ledval);
void enable_SPI_LED(void);
void disable_SPI_LED(void);
#endif

#endif

#endif /* NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_LED_H_ */

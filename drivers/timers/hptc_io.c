/************************************************************************************
 * drivers/timers/hptc_io.c
 *
 *   Copyright (C) 2007-2009, 2011-2013, 2016-2018 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>,
 *           Stefan Nowak <stefan.nowak@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <poll.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/irq.h>
#include <nuttx/arch.h>
#include <nuttx/clock.h>
#include <nuttx/sched.h>
#include <nuttx/signal.h>
#include <nuttx/semaphore.h>
#include <nuttx/fs/fs.h>
#include <nuttx/timers/hptc.h>
#include <nuttx/timers/hptc_io.h>
#include <nuttx/fs/ioctl.h>
#include <nuttx/power/pm.h>

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

#if 0
/* Timing */

#define POLL_DELAY_MSEC 1
#define POLL_DELAY_USEC 1000

#endif


/************************************************************************************
 * Private Types
 ************************************************************************************/

/************************************************************************************
 * Private Function Prototypes
 ************************************************************************************/

static int     hptc_io_takesem(FAR sem_t *sem, bool errout);
#ifndef CONFIG_DISABLE_POLL
static void    hptc_io_pollnotify(FAR hptc_io_dev_t *dev, pollevent_t eventset);
#endif

/* Write support */

static int     hptc_io_putocmsg(FAR hptc_io_dev_t *dev, struct hptc_oc_msg_s *msg, bool oktoblock);
//static inline ssize_t hptc_io_irqwrite(FAR hptc_io_dev_t *dev, FAR const char *buffer, size_t buflen);
//static int     hptc_io_tcdrain(FAR hptc_io_dev_t *dev, clock_t timeout);

/* Character driver methods */

static int     hptc_io_open(FAR struct file *filep);
static int     hptc_io_close(FAR struct file *filep);
static ssize_t hptc_io_read(FAR struct file *filep, FAR char *buffer, size_t buflen);
static ssize_t hptc_io_write(FAR struct file *filep, FAR const char *buffer, size_t buflen);
static int     hptc_io_ioctl(FAR struct file *filep, int cmd, unsigned long arg);
#ifndef CONFIG_DISABLE_POLL
static int     hptc_io_poll(FAR struct file *filep, FAR struct pollfd *fds, bool setup);
#endif

/************************************************************************************
 * Private Data
 ************************************************************************************/

static const struct file_operations g_hptcioops =
{
  hptc_io_open,  /* open */
  hptc_io_close, /* close */
  hptc_io_read,  /* read */
  hptc_io_write, /* write */
  0,          /* seek */
  hptc_io_ioctl  /* ioctl */
#ifndef CONFIG_DISABLE_POLL
  , hptc_io_poll /* poll */
#endif
#ifndef CONFIG_DISABLE_PSEUDOFS_OPERATIONS
  , NULL      /* unlink */
#endif
};

/************************************************************************************
 * Private Functions
 ************************************************************************************/

/************************************************************************************
 * Name: hptc_io_takesem
 ************************************************************************************/

static int hptc_io_takesem(FAR sem_t *sem, bool errout)
{
  int ret;

  do
    {
      /* Take the semaphore (perhaps waiting) */

      ret = nxsem_wait(sem);
      if (ret < 0)
        {
          /* The only case that an error should occur here is if the wait was
           * awakened by a signal.
           */

          DEBUGASSERT(ret == -EINTR || ret == -ECANCELED);

          /* When the signal is received, should we errout? Or should we just
           * continue waiting until we have the semaphore?
           */

          if (errout)
            {
              return ret;
            }
        }
    }
  while (ret == -EINTR);

  return ret;
}

/************************************************************************************
 * Name: hptc_io_givesem
 ************************************************************************************/

#define hptc_io_givesem(sem) (void)nxsem_post(sem)

/****************************************************************************
 * Name: hptc_io_pollnotify
 ****************************************************************************/

#ifndef CONFIG_DISABLE_POLL
static void hptc_io_pollnotify(FAR hptc_io_dev_t *dev, pollevent_t eventset)
{
  int i;

  for (i = 0; i < CONFIG_HPTC_IO_NPOLLWAITERS; i++)
    {
      struct pollfd *fds = dev->fds[i];
      if (fds)
        {
          fds->revents |= (fds->events & eventset);
          if (fds->revents != 0)
            {
              finfo("Report events: %02x\n", fds->revents);
              nxsem_post(fds->sem);
            }
        }
    }
}
#endif


/****************************************************************************
 * Name: hptc_oc_wf_condition
 *
 * Description:
 *   condition a waveform for usage in systick_isr
 *
 * Input Parameters: FAR struct hptc_oc_wf_s
 *
 ****************************************************************************/

void hptc_oc_wf_condition(FAR struct hptc_oc_wf_s *wf,
             uint8_t rising,
             int32_t time_s,
             int32_t time_ns,
             uint32_t pulsewidth_s,
             uint32_t pulsewidth_ns,
             uint32_t period_s,
             uint32_t period_ns,
             uint32_t numerator,
             uint32_t denominator){

//    printf("hptc_oc_wf_condition: rising=%hhu time: %d.%09d\npulse: %u.%09u period: %u.%09u+%u/%u\n",rising,time_s, time_ns,pulsewidth_s,pulsewidth_ns,period_s,period_ns,numerator,denominator);

    if(denominator==0){
            //TODO: error feedback
            numerator=0;
            denominator=1;
    }
    if(numerator>=denominator){
            //TODO: eror feedback
            pulsewidth_ns+=numerator/denominator;
            numerator%=denominator;
    }

    wf->time_s  = time_s;
    wf->time_ns = time_ns;

    while (wf->time_ns < (HPTC_NS_PER_TIMTICK-CONFIG_HPTC_OC_WINDOW_NS)){
        wf->time_ns += NS_PER_SEC;
        wf->time_s--;
    }
    while (wf->time_ns >= (NS_PER_SEC+HPTC_NS_PER_TIMTICK-CONFIG_HPTC_OC_WINDOW_NS)){
        wf->time_ns -= NS_PER_SEC;
        wf->time_s++;
    }

    if(pulsewidth_s || pulsewidth_ns){
             while(pulsewidth_ns >= NS_PER_SEC){
                     pulsewidth_ns-=NS_PER_SEC;
                     pulsewidth_s++;
             }

             if(period_s || period_ns){
                     while(period_ns >= NS_PER_SEC){
                             period_ns-=NS_PER_SEC;
                             period_s++;
                     }
                     wf->type = HPTC_IO_WF_CONTINUOUS;

                     if(rising){
                         wf->state   = HPTC_IO_RISING_EDGE;
                         wf->high_s  = pulsewidth_s;
                         wf->high_ns = pulsewidth_ns;
                         wf->low_s   = period_s-pulsewidth_s;
                         if(period_ns<pulsewidth_ns){
                                 wf->low_ns=NS_PER_SEC+period_ns-pulsewidth_ns;
                                 wf->low_s--;
                         } else {
                                 wf->low_ns=period_ns-pulsewidth_ns;
                         }

                     } else {
                         wf->state = HPTC_IO_FALLING_EDGE;
                         wf->low_s=pulsewidth_s;
                         wf->low_ns=pulsewidth_ns;
                         wf->high_s=period_s-pulsewidth_s;
                         if(period_ns<pulsewidth_ns){
                                 wf->high_ns=NS_PER_SEC+period_ns-pulsewidth_ns;
                                 wf->high_s--;
                         } else {
                                 wf->high_ns=period_ns-pulsewidth_ns;
                         }
                     }

                     wf->residual = 0;
                     wf->numerator = numerator;
                     wf->denominator = denominator;
                     //signal recovery in >1s steps if
                     if(period_s){
                             wf->recovery_s=period_s;
                             wf->recovery_ns=period_ns;
                             wf->recovery_numerator=wf->numerator;
                     }else{
                        uint32_t n=1+NS_PER_SEC/period_ns;
                        uint32_t rec_num=n*wf->numerator;
                        wf->recovery_ns=rec_num/wf->denominator;
                        wf->recovery_numerator=rec_num-wf->recovery_ns*wf->denominator;

                        wf->recovery_ns+=n*period_ns;
                        while(wf->recovery_ns>=NS_PER_SEC){
                                wf->recovery_ns-=NS_PER_SEC;
                                wf->recovery_s++;
                        }

                     }

             } else {
                     wf->type = HPTC_IO_WF_PULSE;

                     if(rising){
                         wf->state   = HPTC_IO_RISING_EDGE;
                         wf->high_s  = pulsewidth_s;
                         wf->high_ns = pulsewidth_ns;
                         wf->low_s   = 0;
                         wf->low_ns  = 0;
                     } else {
                         wf->state = HPTC_IO_FALLING_EDGE;
                         wf->low_s=pulsewidth_s;
                         wf->low_ns=pulsewidth_ns;
                         wf->high_s=0;
                         wf->high_ns=0;
                     }
             }
     } else {
             wf->type = HPTC_IO_WF_SINGLE;
             if(rising){
                 wf->state   = HPTC_IO_RISING_EDGE;
                 wf->high_s  = 0;
                 wf->high_ns = 0;
                 wf->low_s   = 0;
                 wf->low_ns  = 0;
             } else {
                 wf->state = HPTC_IO_FALLING_EDGE;
                 wf->low_s=0;
                 wf->low_ns=0;
                 wf->high_s=0;
                 wf->high_ns=0;
             }
     }

//    printf("wf: %d.%09d,h:%u.%09u,l:%u.%09u\n\n",wf->time_s,wf->time_ns,wf->high_s,wf->high_ns,wf->low_s,wf->low_ns);

}

/************************************************************************************
 * Name: hptc_io_putocmsg
 ************************************************************************************/

static int hptc_io_putocmsg(FAR hptc_io_dev_t *dev, struct hptc_oc_msg_s *msg, bool oktoblock)
{
  irqstate_t flags;
  int nexthead;
  int ret;


  /* check for valid fifo buffer and return if we don't have one*/
  if (dev->ocbuf.size == 0) return -EAGAIN;


  /* Increment to see what the next head pointer will be.  We need to use the "next"
   * head pointer to determine when the circular buffer would overrun
   */

  nexthead = dev->ocbuf.head + 1;
  if (nexthead >= dev->ocbuf.size)
    {
      nexthead = 0;
    }

  /* Loop until we are able to add the character to the TX buffer. */

  for (; ; )
    {
      /* Check if the TX buffer is full */

      if (nexthead != dev->ocbuf.tail)
        {
            /* No.. not full.*/

            /*get pointer for next entry*/
            FAR struct hptc_oc_wf_s *wf=&dev->ocbuf.buffer[dev->ocbuf.head];


            /*condition wf*/
            hptc_oc_wf_condition(wf,
                                 msg->edge == HPTC_IO_RISING_EDGE,
                                 msg->sec,
                                 msg->nsec,
                                 msg->pulse_width_sec,
                                 msg->pulse_width_nsec,
                                 msg->period_sec,
                                 msg->period_nsec,
                                 msg->period_nsec_num,
                                 msg->period_nsec_denom);

            /*adjust head*/
            dev->ocbuf.head = nexthead;
            return OK;
        }

      /* The TX buffer is full.  Should be block, waiting for the hardware
       * to remove some data from the TX buffer?
       */

      else if (oktoblock)
        {
          /* The following steps must be atomic with respect to HPTC_IO
           * interrupt handling.
           */

          flags = enter_critical_section();

          /* Check again...  In certain race conditions an interrupt may
           * have occurred between the test at the top of the loop and
           * entering the critical section and the TX buffer may no longer
           * be full.
           *
           * NOTE: On certain devices, such as USB CDC/ACM, the entire TX
           * buffer may have been emptied in this race condition.  In that
           * case, the logic would hang below waiting for space in the TX
           * buffer without this test.
           */

          if (nexthead != dev->ocbuf.tail)
            {
              ret = OK;
            }
          else
            {
              /* Inform the interrupt level logic that we are waiting. */

              dev->ocbufwaiting = true;

              /* Wait for some characters to be sent from the buffer
               *
               * NOTE that interrupts will be re-enabled while we wait for
               * the semaphore.
               */

              ret = hptc_io_takesem(&dev->ocbufsem, true);
            }

          leave_critical_section(flags);

          /* Check if we were awakened by signal. */

          if (ret < 0)
            {
              /* A signal received while waiting for the ocbuf buffer to become
               * non-full will abort the transfer.
               */

              return -EINTR;
            }
        }

      /* The caller has request that we not block for data.  So return the
       * EAGAIN error to signal this situation.
       */

      else
        {
          return -EAGAIN;
        }
    }

  /* We won't get here.  Some compilers may complain that this code is
   * unreachable.
   */

  return OK;
}

#if 0
/************************************************************************************
 * Name: hptc_io_tcdrain
 *
 * Description:
 *   Block further TX input.  Wait until all data has been transferred from the TX
 *   buffer and until the hardware TX FIFOs are empty.
 *
 ************************************************************************************/

static int hptc_io_tcdrain(FAR hptc_io_dev_t *dev, clock_t timeout)
{
  int ret;

  /* Get exclusive access to the to dev->tmit.  We cannot permit new data to be
   * written while we are trying to flush the old data.
   *
   * A signal received while waiting for access to the ocbuf.head will abort the
   * operation with EINTR.
   */

  ret = (ssize_t)hptc_io_takesem(&dev->ocbuf.sem, true);
  if (ret >= 0)
    {
      irqstate_t flags;
      clock_t start;

      /* Trigger emission to flush the contents of the tx buffer */

      flags = enter_critical_section();

        {
          /* Continue waiting while the TX buffer is not empty.
           *
           * NOTE: There is no timeout on the following loop.  In
           * situations were this loop could hang (with hardware flow
           * control, as an example),  the caller should call
           * tcflush() first to discard this buffered Tx data.
           */

          ret = OK;
          while (ret >= 0 && dev->ocbuf.head != dev->ocbuf.tail)
            {
              /* Inform the interrupt level logic that we are waiting. */

              dev->ocbufwaiting = true;

              /* Wait for some characters to be sent from the buffer with
               * the TX interrupt enabled.  When the TX interrupt is
               * enabled, hptc_io_xmitchars() should execute and remove some
               * of the data from the TX buffer.  We may have to wait several
               * times for the TX buffer to be entirely emptied.
               *
               * NOTE that interrupts will be re-enabled while we wait for
               * the semaphore.
               */

              hptc_io_enabletxint(dev);
              ret = hptc_io_takesem(&dev->ocbufsem, true);
              hptc_io_disabletxint(dev);
            }
        }

      leave_critical_section(flags);

      /* The TX buffer is empty (or an error occurred).  But there still may
       * be data in the HPTC_IO TX FIFO.  We get no asynchronous indication of
       * this event, so we have to do a busy wait poll.
       */

      /* Set up for the timeout
       *
       * REVISIT:  This is a kludge.  The correct fix would be add an
       * interface to the lower half driver so that the tcflush() operation
       * all also cause the lower half driver to clear and reset the Tx FIFO.
       */

      start = clock_systimer();

      if (ret >= 0)
        {
          while (!hptc_io_txempty(dev))
            {
              clock_t elapsed;

#ifndef CONFIG_DISABLE_SIGNALS
              nxsig_usleep(POLL_DELAY_USEC);
#else
              up_mdelay(POLL_DELAY_MSEC);
#endif

              /* Check for a timeout */

              elapsed = clock_systimer() - start;
              if (elapsed >= timeout)
                {
                  return -ETIMEDOUT;
                }
            }
         }

      hptc_io_givesem(&dev->ocbuf.sem);
    }

  return ret;
}
#endif


/************************************************************************************
 * Name: hptc_io_open
 *
 * Description:
 *   This routine imallocs called whenever a HPTC_IO port is opened.
 *
 ************************************************************************************/

static int hptc_io_open(FAR struct file *filep)
{
  FAR struct inode *inode = filep->f_inode;
  FAR hptc_io_dev_t   *dev   = inode->i_private;
  uint8_t           tmp;
  int               ret;

  /* If the port is the middle of closing, wait until the close is finished.
   * If a signal is received while we are waiting, then return EINTR.
   */

  ret = hptc_io_takesem(&dev->closesem, true);
  if (ret < 0)
    {
      /* A signal received while waiting for the last close operation. */

      return ret;
    }

  /* Start up HPTC_IO port */
  /* Increment the count of references to the device. */

  tmp = dev->open_count + 1;
  if (tmp == 0)
    {
      /* More than 255 opens; uint8_t overflows to zero */

      ret = -EMFILE;
      goto errout_with_sem;
    }

  /* Check if this is the first time that the driver has been opened. */

  if (tmp == 1)
    {
      irqstate_t flags = enter_critical_section();

#if 0
          ret = hptc_io_setup(dev);
          if (ret < 0)
            {
              leave_critical_section(flags);
              goto errout_with_sem;
            }
#endif

#if 0
      /* In any event, we do have to configure for interrupt driven mode of
       * operation.  Attach the hardware IRQ(s). Hmm.. should shutdown() the
       * the device in the rare case that hptc_io_attach() fails, tmp==1, and
       * this is not the console.
       */

      ret = hptc_io_attach(dev);
      if (ret < 0)
        {
           hptc_io_shutdown(dev);
           leave_critical_section(flags);
           goto errout_with_sem;
        }

      /* Enable the RX interrupt */

      hptc_io_enablerxint(dev);
#endif
      leave_critical_section(flags);
    }

  /* Save the new open count on success */

  dev->open_count = tmp;

errout_with_sem:
  hptc_io_givesem(&dev->closesem);
  return ret;
}

/************************************************************************************
 * Name: hptc_io_close
 *
 * Description:
 *   This routine is called when the HPTC_IO port gets closed.
 *   It waits for the last remaining data to be sent.
 *
 ************************************************************************************/

static int hptc_io_close(FAR struct file *filep)
{
  FAR struct inode *inode = filep->f_inode;
  FAR hptc_io_dev_t   *dev   = inode->i_private;
  //irqstate_t        flags;

  /* Get exclusive access to the close semaphore (to synchronize open/close operations.
   * NOTE: that we do not let this wait be interrupted by a signal.  Technically, we
   * should, but almost no one every checks the return value from close() so we avoid
   * a potential memory leak by ignoring signals in this case.
   */

  (void)hptc_io_takesem(&dev->closesem, false);
  if (dev->open_count > 1)
    {
      dev->open_count--;
      hptc_io_givesem(&dev->closesem);
      return OK;
    }

  /* There are no more references to the port */

  dev->open_count = 0;

  /* Stop accepting input */

 // hptc_io_disablerxint(dev);
#if 0
  /* Prevent blocking if the device is opened with O_NONBLOCK */

  if ((filep->f_oflags & O_NONBLOCK) == 0)
    {
      /* Now we wait for the transmit buffer(s) to clear */

      (void)hptc_io_tcdrain(dev, 4 * TICK_PER_SEC);
    }
#endif

#if 0
  /* Free the IRQ and disable the HPTC_IO */

  flags = enter_critical_section();  /* Disable interrupts */
//  hptc_io_detach(dev);                  /* Detach interrupts */
//  if (!dev->isconsole)               /* Check for the HPTC_IO console HPTC_IO */
//    {
      hptc_io_shutdown(dev);            /* Disable the HPTC_IO */
//    }

  leave_critical_section(flags);
#endif
  /* We need to re-initialize the semaphores if this is the last close
   * of the device, as the close might be caused by pthread_cancel() of
   * a thread currently blocking on any of them.
   */

  hptc_io_reset_sem(dev);
  hptc_io_givesem(&dev->closesem);
  return OK;
}

/************************************************************************************
 * Name: hptc_io_read
 ************************************************************************************/

static ssize_t hptc_io_read(FAR struct file *filep, FAR char *buffer, size_t buflen)
{
  FAR struct inode *inode = filep->f_inode;
  FAR hptc_io_dev_t *dev = inode->i_private;
  FAR struct hptc_ic_buffer_s *rxbuf = &dev->icbuf;

  struct hptc_ic_msg_s *msg_ptr = (struct hptc_ic_msg_s *) buffer;

  irqstate_t flags;
  ssize_t recvd = 0;
  int16_t tail;
  //char ch;
  int ret;

  /* Do we have a read buffer? */
  if(rxbuf->size == 0) return -EAGAIN;

  /* Only one user can access rxbuf->tail at a time */


  ret = hptc_io_takesem(&rxbuf->sem, true);
  if (ret < 0)
    {
      /* A signal received while waiting for access to the icbuf.tail will abort
       * the transfer.  After the transfer has started, we are committed and
       * signals will be ignored.
       */

      return ret;
    }

  /* Loop while we still have data to copy to the receive buffer.
   * we add data to the head of the buffer; hptc_io_xmitchars takes the
   * data from the end of the buffer.
   */

  while (((size_t)recvd + sizeof(struct hptc_ic_msg_s)) <= buflen)
    {

      /* Check if there is more data to return in the circular buffer.
       * NOTE: Rx interrupt handling logic may asynchronously increment
       * the head index but must not modify the tail index.  The tail
       * index is only modified in this function.  Therefore, no
       * special handshaking is required here.
       *
       * The head and tail pointers are 16-bit values.  The only time that
       * the following could be unsafe is if the CPU made two non-atomic
       * 8-bit accesses to obtain the 16-bit head index.
       */

      tail = rxbuf->tail;
      if (rxbuf->head != tail)
        {
          /* Take the next character from the tail of the buffer */

          memcpy(msg_ptr,&rxbuf->buffer[tail],sizeof(struct hptc_ic_msg_s));

          /* Increment the tail index.  Most operations are done using the
           * local variable 'tail' so that the final rxbuf->tail update
           * is atomic.
           */

          if (++tail >= rxbuf->size)
            {
              tail = 0;
            }

          rxbuf->tail = tail;

          /* update the pointer */
          msg_ptr++;

          recvd+=sizeof(struct hptc_ic_msg_s);
        }


      /* No... the circular buffer is empty.  Have we returned anything
       * to the caller?
       */

      else if (recvd > 0)
       {
          /* Yes.. break out of the loop and return the number of bytes
           * received up to the wait condition.
           */

          break;
       }

      /* No... then we would have to wait to get receive some data.
       * If the user has specified the O_NONBLOCK option, then do not
       * wait.
       */

      else if ((filep->f_oflags & O_NONBLOCK) != 0)
        {
          /* Break out of the loop returning -EAGAIN */

          recvd = -EAGAIN;
          break;
        }

      /* Otherwise we are going to have to wait for data to arrive */

      else
        {




          /* All interrupts are
           * disabled briefly to assure that the following operations
           * are atomic.
           */

          flags = enter_critical_section();

          /* If the Rx ring buffer still empty?  Bytes may have been added
           * between the last time that we checked and when we disabled
           * interrupts.
           */

          if (rxbuf->head == rxbuf->tail)
            {
              /* Yes.. the buffer is still empty.  We will need to wait for
               * additional data to be received.
               */

			  /* Now wait.  NuttX will
			   * automatically re-enable global interrupts when this
			   * thread goes to sleep.
			   */


              dev->icbufwaiting = true;
              ret = hptc_io_takesem(&dev->icbufsem, true);


              leave_critical_section(flags);

              /* Was a signal received while waiting for data to be
               * received?  Was a removable device disconnected while
               * we were waiting?
               */

              if (ret < 0)

                {
                  /* POSIX requires that we return after a signal is received.
                   * If some bytes were read, we need to return the number of bytes
                   * read; if no bytes were read, we need to return -1 with the
                   * errno set correctly.
                   */

                  if (recvd == 0)
                    {
                      /* No bytes were read, return -EINTR (the VFS layer will
                       * set the errno value appropriately.
                       */

                      recvd = -EINTR;

                    }

                  break;
                }
            }
          else
            {
              /* No... the ring buffer is no longer empty.  Just re-enable
               * interrupts and accept the new data on the next time through
               * the loop.
               */
        	  leave_critical_section(flags);
            }
        }
    }

  hptc_io_givesem(&dev->icbuf.sem);
  return recvd;
}

/************************************************************************************
 * Name: hptc_io_write
 ************************************************************************************/

static ssize_t hptc_io_write(FAR struct file *filep, FAR const char *buffer,
                          size_t buflen)
{
  FAR struct inode *inode    = filep->f_inode;
  FAR hptc_io_dev_t   *dev      = inode->i_private;
  ssize_t           nwritten = 0;
  bool              oktoblock;
  int               ret;
  //char              ch;
  struct hptc_oc_msg_s *msg_ptr = (struct hptc_oc_msg_s *) buffer;

  /* Only one user can access dev->ocbuf.head at a time */

  ret = (ssize_t)hptc_io_takesem(&dev->ocbuf.sem, true);
  if (ret < 0)
    {
      /* A signal received while waiting for access to the ocbuf.head will
       * abort the transfer.  After the transfer has started, we are committed
       * and signals will be ignored.
       */

      return ret;
    }


  /* Can the following loop block, waiting for space in the TX
   * buffer?
   */

  oktoblock = ((filep->f_oflags & O_NONBLOCK) == 0);

  /* Loop while we still have data to copy to the transmit buffer.
   * we add data to the head of the buffer; hptc_io_xmitchars takes the
   * data from the end of the buffer.
   */

  for (; nwritten + sizeof(struct hptc_oc_msg_s) <= buflen; nwritten+=sizeof(struct hptc_oc_msg_s))
    {

      /* Put the character into the transmit buffer */

      ret = hptc_io_putocmsg(dev, msg_ptr, oktoblock);

      /* update the message pointer */

      msg_ptr++;

      /* hptc_io_putocmsg() might return an error under one of two
       * conditions:  (1) The wait for buffer space might have been
       * interrupted by a signal (ret should be -EINTR) or
       * (2) if O_NONBLOCK is specified, then hptc_io_putocmsg()
       * might return -EAGAIN if the output TX buffer is full.
       */

      if (ret < 0)
        {
          /* POSIX requires that we return -1 and errno set if no data was
           * transferred.  Otherwise, we return the number of bytes in the
           * interrupted transfer.
           */

          if ((size_t)nwritten == 0)
            {
              /* No data was transferred. Return the negated errno value.
               * The VFS layer will set the errno value appropriately).
               */

              nwritten = ret;
            }

          break;
        }
    }

  hptc_io_givesem(&dev->ocbuf.sem);
  return nwritten;
}

/************************************************************************************
 * Name: hptc_io_ioctl
 ************************************************************************************/

static int hptc_io_ioctl(FAR struct file *filep, int cmd, unsigned long arg)
{
  FAR struct inode *inode = filep->f_inode;
  FAR hptc_io_dev_t   *dev   = inode->i_private;

  /* Handle TTY-level IOCTLs here */
  /* Let low-level driver handle the call first */

  int ret = dev->ops->ioctl(filep, cmd, arg);

  /* The device ioctl() handler returns -ENOTTY when it doesn't know
   * how to handle the command. Check if we can handle it here.
   */

  if (ret == -ENOTTY)
    {
      switch (cmd)
        {
          /* Get the number of bytes that may be read from the RX buffer
           * (without waiting)
           */
#if 0
          case FIONREAD:
            {
              int count;
              irqstate_t flags = enter_critical_section();

              /* fetch remaining data from input/DMA buffer */
              hptc_io_recvchars(dev);

              /* Determine the number of bytes available in the RX buffer */

              if (dev->icbuf.tail <= dev->icbuf.head)
                {
                  count = dev->icbuf.head - dev->icbuf.tail;
                }
              else
                {
                  count = dev->icbuf.size - (dev->icbuf.tail - dev->icbuf.head);
                }

              leave_critical_section(flags);

              *(FAR int *)((uintptr_t)arg) = count;
              ret = 0;
            }
            break;
#endif
          /* Get the number of bytes that have been written to the TX buffer. */
#if 0
          case FIONWRITE:
            {
              int count;
              irqstate_t flags = enter_critical_section();

              /* Determine the number of bytes waiting in the TX buffer */

              if (dev->ocbuf.tail <= dev->ocbuf.head)
                {
                  count = dev->ocbuf.head - dev->ocbuf.tail;
                }
              else
                {
                  count = dev->ocbuf.size - (dev->ocbuf.tail - dev->ocbuf.head);
                }

              leave_critical_section(flags);

              *(FAR int *)((uintptr_t)arg) = count;
              ret = 0;
            }
            break;
#endif
          /* Get the number of free bytes in the TX buffer */
#if 0
          case FIONSPACE:
            {
              int count;
              irqstate_t flags = enter_critical_section();

              /* Determine the number of bytes free in the TX buffer */

              if (dev->ocbuf.head < dev->ocbuf.tail)
                {
                  count = dev->ocbuf.tail - dev->ocbuf.head - 1;
                }
              else
                {
                  count = dev->ocbuf.size - (dev->ocbuf.head - dev->ocbuf.tail) - 1;
                }

              leave_critical_section(flags);

              *(FAR int *)((uintptr_t)arg) = count;
              ret = 0;
            }
            break;
#endif
        }
    }

  return ret;
}

/****************************************************************************
 * Name: hptc_io_poll
 ****************************************************************************/

#ifndef CONFIG_DISABLE_POLL
static int hptc_io_poll(FAR struct file *filep, FAR struct pollfd *fds, bool setup)
{
  FAR struct inode *inode = filep->f_inode;
  FAR hptc_io_dev_t   *dev   = inode->i_private;
  pollevent_t       eventset;
  int               ndx;
  int               ret;
  int               i;

  /* Some sanity checking */

#ifdef CONFIG_DEBUG_FEATURES
  if (!dev || !fds)
    {
      return -ENODEV;
    }
#endif

  /* Are we setting up the poll?  Or tearing it down? */

  ret = hptc_io_takesem(&dev->pollsem, true);
  if (ret < 0)
    {
      /* A signal received while waiting for access to the poll data
       * will abort the operation.
       */

      return ret;
    }

  if (setup)
    {
      /* This is a request to set up the poll.  Find an available
       * slot for the poll structure reference
       */

      for (i = 0; i < CONFIG_HPTC_IO_NPOLLWAITERS; i++)
        {
          /* Find an available slot */

          if (!dev->fds[i])
            {
              /* Bind the poll structure and this slot */

              dev->fds[i]  = fds;
              fds->priv    = &dev->fds[i];
              break;
            }
        }

      if (i >= CONFIG_HPTC_IO_NPOLLWAITERS)
        {
          fds->priv    = NULL;
          ret          = -EBUSY;
          goto errout;
        }

      /* Should we immediately notify on any of the requested events?
       * First, check if the ocbuf buffer is full.
       *
       * Get exclusive access to the ocbuf buffer indices.  NOTE: that we do not
       * let this wait be interrupted by a signal (we probably should, but that
       * would be a little awkward).
       */

      eventset = 0;
      (void)hptc_io_takesem(&dev->ocbuf.sem, false);

      ndx = dev->ocbuf.head + 1;
      if (ndx >= dev->ocbuf.size)
        {
          ndx = 0;
        }

      if (ndx != dev->ocbuf.tail)
       {
         eventset |= (fds->events & POLLOUT);
       }

      hptc_io_givesem(&dev->ocbuf.sem);

      /* Check if the receive buffer is empty.
       *
       * Get exclusive access to the icbuf buffer indices.  NOTE: that we do not
       * let this wait be interrupted by a signal (we probably should, but that
       * would be a little awkward).
       */

      (void)hptc_io_takesem(&dev->icbuf.sem, false);
      if (dev->icbuf.head != dev->icbuf.tail)
       {
         eventset |= (fds->events & POLLIN);
       }

      hptc_io_givesem(&dev->icbuf.sem);

      if (eventset)
        {
          hptc_io_pollnotify(dev, eventset);
        }

    }
  else if (fds->priv)
    {
      /* This is a request to tear down the poll. */

      struct pollfd **slot = (struct pollfd **)fds->priv;

#ifdef CONFIG_DEBUG_FEATURES
      if (!slot)
        {
          ret              = -EIO;
          goto errout;
        }
#endif

      /* Remove all memory of the poll setup */

      *slot                = NULL;
      fds->priv            = NULL;
    }

errout:
  hptc_io_givesem(&dev->pollsem);
  return ret;
}
#endif

/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: hptc_io_register
 *
 * Description:
 *   Register HPTC_IO console and HPTC_IO ports.
 *
 ************************************************************************************/

int hptc_io_register(FAR const char *path, FAR hptc_io_dev_t *dev, int16_t ic_bufsize, int16_t oc_bufsize)
{

  /* Initialize semaphores */

  nxsem_init(&dev->ocbuf.sem, 0, 1);
  nxsem_init(&dev->icbuf.sem, 0, 1);
  nxsem_init(&dev->closesem, 0, 1);
  nxsem_init(&dev->ocbufsem,  0, 0);
  nxsem_init(&dev->icbufsem,  0, 0);
//  nxsem_init(&dev->iobufsem, 0, 0);
#ifndef CONFIG_DISABLE_POLL
  nxsem_init(&dev->pollsem,  0, 1);
#endif

  /* The icbufsem and ocbufsem are used for signaling and, hence, should not have
   * priority inheritance enabled.
   */

  nxsem_setprotocol(&dev->ocbufsem, SEM_PRIO_NONE);
  nxsem_setprotocol(&dev->icbufsem, SEM_PRIO_NONE);
//  nxsem_setprotocol(&dev->iobufsem, SEM_PRIO_NONE);

  if(ic_bufsize>0){
	  dev->icbuf.buffer = malloc(ic_bufsize*sizeof(struct hptc_ic_msg_s));
	  if(dev->icbuf.buffer != NULL){
		  dev->icbuf.size = ic_bufsize;
	  } else {
		  tmrerr("Could not allocate buffer for input capture\n");
	  }
  }

  if(oc_bufsize>0){
	  dev->ocbuf.buffer = malloc(oc_bufsize*sizeof(struct hptc_oc_msg_s));
	  if(dev->ocbuf.buffer != NULL){
		  dev->ocbuf.size = oc_bufsize;
	  } else {
		  tmrerr("Could not allocate buffer for output compare\n");
	  }
  }

  /* Initialize low level */
  dev->ops->initialize(dev);



  /* Register the HPTC_IO driver */

  tmrinfo("Registering %s\n", path);
  return register_driver(path, &g_hptcioops, 0666, dev);
}


/************************************************************************************
 * Name: hptc_oc_fifo_get
 *
 * Description:
 *   This function is called from the HPTC interrupt handler to get output compare
 *   waveform data.  It will get data from the tail of the oc buffer while the driver
 *   write() logic adds data to the head of the oc buffer.
 *   returns 0 when fifo was empty, otherwise 1 when data was taken from fifo
 *
 ************************************************************************************/
int hptc_oc_fifo_get(FAR hptc_io_dev_t *dev, uint8_t channel, struct hptc_oc_wf_s *oc_wf, int32_t time_s, int32_t time_ns)
//int hptc_oc_fifo_get(FAR hptc_io_dev_t *dev, uint8_t channel, struct hptc_oc_msg_s *oc_wf, int32_t time_s, int32_t time_ns)
{

	struct hptc_oc_wf_s *fifo_oc_wf;

  /* Check for data in buffer */
  if (dev->ocbuf.head == dev->ocbuf.tail) return 0;

  /* get waveform */
  fifo_oc_wf=&dev->ocbuf.buffer[dev->ocbuf.tail];

  /* Check Time */
  if((fifo_oc_wf->time_s >time_s) || (fifo_oc_wf->time_ns >= time_ns)){
     /* too early, return and try later */
	  return 0;
  }


  memcpy(oc_wf,fifo_oc_wf,sizeof(struct hptc_oc_wf_s));

  /* Increment the tail index */

  if (++(dev->ocbuf.tail) >= dev->ocbuf.size)
	{
	  dev->ocbuf.tail = 0;
	}


	/* Is there a thread waiting for space in ocbuf.buffer?  */

	if (dev->ocbufwaiting)
	  {
		/* Yes... wake it up */

		dev->ocbufwaiting = false;
		(void)nxsem_post(&dev->ocbufsem);
	  }

#ifndef CONFIG_DISABLE_POLL
	/* Notify all poll/select waiters that they can write to ocbuf buffer */

	hptc_io_pollnotify(dev, POLLOUT);
#endif


  return 1;
}

/************************************************************************************
 * Name: hptc_ic_fifo_put
 *
 * Description:
 *   This function is called from the HPTC interrupt handler when input capture data
 *   is available  This function will add data to head of ic buffer.  Driver read()
 *   logic will take data from the tail of the buffer.
 *
 ************************************************************************************/

void hptc_ic_fifo_put(FAR hptc_io_dev_t *dev, uint8_t ch, uint8_t flags, int32_t capture_s, int32_t capture_ns)
{
  FAR struct hptc_ic_buffer_s *rxbuf = &dev->icbuf;

  /* Increment the head index */
  int nexthead = rxbuf->head + 1;

  if (nexthead >= rxbuf->size)
    {
      nexthead = 0;
    }


  /* If the fifo buffer becomes full, then the IC data is discarded.
   */
  if(nexthead == rxbuf->tail){
	  rxbuf->overflow=true;
      tmrwarn("rxbuf overflow\n");
	  return;
  }

  /* Add the character to the buffer */
  struct hptc_ic_msg_s *msg = &rxbuf->buffer[rxbuf->head];

  msg->sec=capture_s;
  msg->nsec=capture_ns;
  msg->flags = flags;


  /* was der buffer overflow before? */
  if (rxbuf->overflow){
	  msg->flags |= HPTC_IO_SW_OC_FLAG;
	  rxbuf->overflow = false;
  }


  /* Update the head index */
  rxbuf->head = nexthead;

  /* Is there a thread waiting for read data?  */

  if (dev->icbufwaiting)
    {
      /* Yes... wake it up */

      dev->icbufwaiting = false;
      (void)nxsem_post(&dev->icbufsem);
    }

#ifndef CONFIG_DISABLE_POLL
  /* Notify all poll/select waiters that they can read from the icbuf buffer */

  hptc_io_pollnotify(dev, POLLIN);
#endif

}

/************************************************************************************
 * Name: hptc_io_reset_sem
 *
 * Description:
 *   This function is called when need reset hptc_io semphore, this may used in kill one
 *   process, but this process was reading/writing with the semphore.
 *
 ************************************************************************************/

void hptc_io_reset_sem(FAR hptc_io_dev_t *dev)
{
  nxsem_reset(&dev->ocbufsem,  0);
  nxsem_reset(&dev->icbufsem,  0);
  nxsem_reset(&dev->ocbuf.sem, 1);
  nxsem_reset(&dev->icbuf.sem, 1);
#ifndef CONFIG_DISABLE_POLL
  nxsem_reset(&dev->pollsem,  1);
#endif
}

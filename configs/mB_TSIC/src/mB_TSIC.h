/************************************************************************************
 * configs/mB_TSIC/src/mB_TSIC.h
 *
 ************************************************************************************/

#ifndef __CONFIGS_TSIC_SRC_TSIC_H
#define __CONFIGS_TSIC_SRC_TSIC_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>
#include <nuttx/board.h>
#include <nuttx/compiler.h>
#include <stdint.h>
#include "stm32_gpio.h"

#ifdef CONFIG_SENSORS_TSIC
#include <nuttx/sensors/tsic.h>
#endif


/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/* Configuration ********************************************************************/

#define HAVE_PROGMEM_CHARDEV  1

/* mB_trig GPIO Pin Definitions **************************************************/

/* LED ***************************************************************************/
#define GPIO_LED1            (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_100MHz|\
                              GPIO_OUTPUT_CLEAR|GPIO_PORTA|GPIO_PIN9)           /* PA9: green */

#define GPIO_LED2            (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_100MHz|\
                              GPIO_OUTPUT_CLEAR|GPIO_PORTA|GPIO_PIN8)           /* PA8: red */

/* messBUS ***********************************************************************/

/************************************************************************************
 * Public data
 ************************************************************************************/

#ifndef __ASSEMBLY__

/************************************************************************************
 * Public Functions
 ************************************************************************************/

 /************************************************************************************
  * Name: stm32_adxl357_initialize
  *
  * Description:
  *   Called to configure and initialize TSIC driver.
  *
  ************************************************************************************/
#if defined(CONFIG_SENSORS_TSIC)
void stm32_tsic_initialize(void);
#endif

/****************************************************************************
 * Name: stm32_hptc_setup
 *
 * Description:
 *   Initialize HPTC and register the HPTC device.
 *
 ****************************************************************************/

#ifdef CONFIG_HPTC
int stm32_hptc_setup(void);
#endif

#endif /* __ASSEMBLY__ */
#endif /* __CONFIGS_TSIC_SRC_TSIC_H */

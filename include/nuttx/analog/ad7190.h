/****************************************************************************
 * include/nuttx/analog/ad7190.h
 *
 * Copyright (C) 2015, 2017 Gregory Nutt. All rights reserved.
 * Based on stm32_ducspi from Alan Carvalho de Assis <acassis@gmail.com>>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef __INCLUDE_NUTTX_ANALOG_AD7190_H
#define __INCLUDE_NUTTX_ANALOG_AD7190_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/irq.h>
#include <nuttx/analog/ioctl.h>

#ifdef CONFIG_SPI && CONFIG_ADC_AD7190

#define AD7190_CONF_GAIN_1       0
#define AD7190_CONF_GAIN_8       3
#define AD7190_CONF_GAIN_16      4
#define AD7190_CONF_GAIN_32      5
#define AD7190_CONF_GAIN_64      6
#define AD7190_CONF_GAIN_128     7
#define AD7190_CONF_POLARITY_UNIPOLAR 1
#define AD7190_CONF_POLARITY_BIPOLAR  0
#define AD7190_CONF_AINBUF_ENABLE 1
#define AD7190_CONF_AINBUF_DISABLE 0
#define AD7190_CONF_FILTER_SINC3 1
#define AD7190_CONF_FILTER_SINC4 0
#define AD7190_CONF_CHANNEL_CH0  1
#define AD7190_CONF_CHANNEL_CH2  4
#define AD7190_CONF_CHANNEL_CH3  8


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/


/****************************************************************************
 * Public Types
 ****************************************************************************/

struct spi_dev_s;

struct ad7190_msg_s{
	int32_t time_s;
	int32_t time_ns;
	int32_t status;
	int32_t data[CONFIG_AD7190_NUMBEROFDEVICES];
};

struct ad7190_conf_s{
	uint8_t gain; /*1,8,16,32,64,128*/
	uint8_t polarity;
	uint8_t ain_buf;
	uint8_t filter; /*sinc4 default*/
	uint8_t channel;
};

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/* IOCTL Commands ***********************************************************/

#define ANIOC_AD7190_RESET           _ANIOC (0x0001)
#define ANIOC_AD7190_READ_STATUS_REG _ANIOC (0x0002)
#define ANIOC_AD7190_WRITE_MODE_REG  _ANIOC (0x0003)
#define ANIOC_AD7190_WRITE_CONF_REG  _ANIOC (0x0004)
#define ANIOC_AD7190_READ_DATA_REG   _ANIOC (0x0005)
#define ANIOC_AD7190_READ_ID_REG     _ANIOC (0x0006)
#define ANIOC_AD7190_CHIP_SELECT     _ANIOC (0x0007)
#define ANIOC_AD7190_CONTINUOUS_MODE _ANIOC (0x0008)
#define ANIOC_AD7190_SET_FS          _ANIOC (0x0009)
#define ANIOC_AD7190_SET_CONFIG      _ANIOC (0x0010)

/****************************************************************************
 * Name: ad7190_register
 *
 * Description:
 *  This function will register the max31855 driver as /dev/tempN
 *  where N is the minor device number
 *
 * Input Parameters:
 *   devpath - The full path to the driver to register. E.g., "/dev/temp0"
 *   spi     - An instance of the SPI interface to use to communicate with
 *             ad7190
 *	 num_dev - number of device
 *
 * Returned Value:
 *   Zero is returned on success.  Otherwise, a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 ****************************************************************************/

int ad7190_register(FAR const char *devpath,FAR struct spi_dev_s *spi, int num_dev);

#undef EXTERN
#ifdef __cplusplus
}
#endif

#endif /* CONFIG_AD7190 */
#endif /* __INCLUDE_NUTTX_analog_AD7190_H */

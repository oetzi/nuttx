/****************************************************************************
 * drivers/sensors/AD7190.c
 * Character driver for the AD7190
 *
 * Copyright (C) 2015, 2017 Gregory Nutt. All rights reserved.
 * Based on driver MAX31855 from Alan Carvalho de Assis <acassis@gmail.com>>
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/


/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdlib.h>
#include <fixedmath.h>
#include <errno.h>
#include <debug.h>
#include <semaphore.h>

#include <nuttx/kmalloc.h>
#include <nuttx/wqueue.h>
#include <nuttx/fs/fs.h>
#include <nuttx/fs/ioctl.h>
#include <fcntl.h>

#include <nuttx/spi/spi.h>
#include <nuttx/analog/ad7190.h>
#include <nuttx/timers/hptc.h>

#if defined(CONFIG_SPI) && defined(CONFIG_ADC_AD7190)

#define CONFIG_AD7190_FIFOSIZE 10

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private
 ****************************************************************************/

/* This describes a FIFO of AD7190 messages */

struct ad7190_fifo_s
{
  sem_t         af_sem;                   /* Counting semaphore */
  volatile uint16_t      af_head;                  /* Index to the head [IN] index in the circular buffer */
  volatile uint16_t      af_tail;                  /* Index to the tail [OUT] index in the circular buffer */
  struct ad7190_msg_s af_buffer[CONFIG_AD7190_FIFOSIZE];
};


struct ad7190_dev_s
{
  FAR struct spi_dev_s *spi;	/* SPI struct vor SPI Communication*/
//  FAR int devicid;				/* deviced ID for SPI_SELECT*/
  FAR int select;               /* select device*/
  FAR uint16_t fs;              /*frequency selection 1...1023*/
  FAR int time_s;
  FAR int time_ns;

  sem_t recvsem;

  volatile bool recvwaiting; /* read is waiting to enqueue a message */
  volatile bool recvoverflow; /* read is waiting to enqueue a message */

  struct ad7190_fifo_s     recv;       /* Describes receive FIFO */
  FAR int mode;                 /* mode: continuous-mode */
  FAR int num_of_devices;       /*number of devices*/
//  sem_t exclsem;           		/* exclusion *//* Saved SPI driver instance */
  FAR struct work_s work;                 /* The work queue is responsible for
                                       * retrieving the data from the sensor
                                       * after the arrival of new data was
                                       * signalled in an interrupt */
};


/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

static void ad7190_worker(FAR void *arg);
static int  ad7190_dataready_callback(FAR void *arg, uint8_t flags, int32_t capture_s, int32_t capture_ns);


/* Character driver methods */

static int     ad7190_open(FAR struct file *filep);
static int     ad7190_close(FAR struct file *filep);
static ssize_t ad7190_read(FAR struct file *filep, FAR char *buffer,
                             size_t buflen);
static ssize_t ad7190_write(FAR struct file *filep, FAR const char *buffer, size_t buflen);

static int     ad7190_ioctl(FAR struct file *filep, int cmd, unsigned long arg);





/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct file_operations g_ad7190fops =
{
  ad7190_open,
  ad7190_close,
  ad7190_read,
  ad7190_write,
  NULL,
  ad7190_ioctl,    /* ioctl */
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/





static int ad7190_dataready_callback(FAR void *arg, uint8_t flags, int32_t capture_s, int32_t capture_ns){
	FAR struct ad7190_dev_s *priv = arg;
	int ret;

	DEBUGASSERT(priv != NULL);

	/*disable interrupt to avoid MISO line triggering an interrupt*/
	hptc_disable_async_input_callback(ch4);

	priv->time_s=capture_s;
	priv->time_ns=capture_ns;
//	ainfo("ns:%09u\n",capture_ns);


	DEBUGASSERT(priv->work.worker == NULL);

	ret = work_queue(HPWORK, &priv->work, ad7190_worker, priv, 0);
	if (ret < 0){
	    aerr("ERROR: Failed to queue work: %d\n", ret);
		return ret;
	}

	return OK;
}


/****************************************************************************
 * Name: adis16xxx_worker
 ****************************************************************************/

static void ad7190_worker(FAR void *arg)
{
	static uint8_t dummy[4]={0,0,0,0};
	uint8_t buf[4];

  //ainfo("worker\n");
  FAR struct ad7190_dev_s *priv = (FAR struct adis16xxx_dev_s *)(arg);
  DEBUGASSERT(priv != NULL);

	FAR struct ad7190_fifo_s *fifo = &priv->recv;
	int nexthead;
	int head;

	 head= fifo->af_head;
	  nexthead = head + 1;
	  if (nexthead >= CONFIG_AD7190_FIFOSIZE){
		  nexthead = 0;
	  }

	  //if the fifo becomes full, then adjust the tail to make place for
	   //new data
	   int tail=fifo->af_tail;
	   if (nexthead == tail){
	 	  tail++;
	 	  if (tail>=CONFIG_AD7190_FIFOSIZE){
	 		  tail=0;
	 	  }
	      priv->recvoverflow=true;
	 	  fifo->af_tail=tail;
	      priv->recvoverflow=true;
	   }


		  /* Copy timestamp into fifo */

		  fifo->af_buffer[head].time_s=priv->time_s;
		  fifo->af_buffer[head].time_ns = priv-> time_ns;

		  fifo->af_buffer[head].status = priv->select;
  /* Read out the latest sensor data */

		  SPI_SELECT(priv->spi, priv->select, false);

		  for(int j=priv->num_of_devices;j>0;){
              SPI_SELECT(priv->spi, j, true);

              SPI_EXCHANGE(priv->spi,dummy,buf,4);

              SPI_SELECT(priv->spi, j, false);
			  /* Copy data into fifo */
              j--;
              fifo->af_buffer[head].data[j]=(buf[0]<<16) + (buf[1]<<8) + (buf[2]);

              /*TODO: check status and parity in buf[3]*/

			}

		  fifo->af_head = nexthead;

		  /*select next device for data ready interrupt*/
		  priv->select++;
		  if(priv->select > priv->num_of_devices){
			  priv->select =1;
		  }
		  SPI_SELECT(priv->spi, priv->select, true);
			/*enable data ready interrupt*/
			hptc_enable_async_input_callback(ch4);


		  /* Is there a thread waiting for read data?  */

		   if (priv->recvwaiting)
		     {
		       /* Yes... wake it up */

		       priv->recvwaiting = false;
		       (void)nxsem_post(&priv->recvsem);
		     }
		   //TODO: notify for blocking read
		   //  ad7190_notify(dev);

		     /* Feed sensor data to entropy pool */

		   //  add_sensor_randomness((x_gyr << 16) ^ (y_gyr << 8) ^ z_gyr);

}





/****************************************************************************
 * Name: ad7190_open
 *
 * Description:
 *   This function is called whenever the AD5662 device is opened.
 *
 ****************************************************************************/

static int ad7190_open(FAR struct file *filep)
{
	return OK;
}

/****************************************************************************
 * Name: ad7190_close
 *
 * Description:
 *   This routine is called when the AD5662 device is closed.
 *
 ****************************************************************************/

static int ad7190_close(FAR struct file *filep)
{
	return OK;
}

/****************************************************************************
 * Name: ad7190_read
 ****************************************************************************/

static ssize_t ad7190_read(FAR struct file *filep, FAR char *buffer, size_t buflen)
{

	FAR struct inode *inode = filep->f_inode;
	FAR struct ad7190_dev_s *dev = inode->i_private;
	int ret=0;
    ssize_t received=0;

	DEBUGASSERT(dev != NULL);

    size_t                nread=0;
    irqstate_t            flags;
    int                   msglen;
    int tail;

    //sninfo("buflen: %d\n", (int)buflen);

    msglen=sizeof(FAR struct ad7190_msg_s);

  /* Check if enough memory was provided for the read call */

  if (buflen < msglen)
    {
      aerr("ERROR: Not enough memory for reading out a sensor data sample\n");
      return -ENOSYS;
    }

#if 0
  /* Aquire the semaphore before the data is copied */
  ret = nxsem_wait(&dev->datasem);


  if (ret < 0)
    {
      aerr("ERROR: Could not aquire dev->datasem: %d\n", ret);
      DEBUGASSERT(ret == -EINTR);
      return ret;
    }
#endif

  do //while (buflen >= nread+msglen)
    {

	  tail=dev->recv.af_tail;

	  if (dev->recv.af_head != tail){
	    /* The ad_recv FIFO is not empty.  Copy buffered data in the user buffer.*/

		memcpy(&buffer[nread],&dev->recv.af_buffer[tail],msglen);

		/* Feed  data to entropy pool */

		//add_sensor_randomness(msg->am_data);

		/* Increment the head of the circular message buffer */

		tail++;
		if (tail >= CONFIG_AD7190_FIFOSIZE)
		{
			tail = 0;
		}
		if (dev->recvoverflow == false){
		  dev->recv.af_tail=tail;
		  nread+=msglen;
		} else {
		//TODO: overflow error
			//reset flag
			dev->recvoverflow =false;
			aerr("input buffer overflow\n");
			//maybe inconsistent data, reread data in next loop
		}

	  } else {

          /* The receive FIFO is empty -- was non-blocking mode selected? */
          if (filep->f_oflags & O_NONBLOCK)
          {
        	  if(nread<=0){
        		  nread = -EAGAIN;
        	  }
        	  break;
          }

          flags = enter_critical_section();
          //buffer still empty?
          if (dev->recv.af_head == tail){

			  /* Wait for a message to be received */
			  dev->recvwaiting=true;

              /* Now wait with the Rx interrupt enabled.  NuttX will
               * automatically re-enable global interrupts when this
               * thread goes to sleep.
               */

			  ret = nxsem_wait(&dev->recvsem);
			  //dev->recvwaiting=false;
			  if (ret < 0)
			  {
				  leave_critical_section(flags);
		       	  if(nread<=0){
		       		nread = -EINTR;
		          }
		          break;
			  }
          }
          leave_critical_section(flags);
        }



    } while (buflen >= nread+msglen);



  //sninfo("Returning: %d\n", nread);
  return nread;
}

/****************************************************************************
 * Name: ad7190_write
 ****************************************************************************/

static ssize_t ad7190_write(FAR struct file *filep, FAR const char *buffer, size_t buflen)
{
	return -ENOSYS;
}

/****************************************************************************
 * Name: ad7190_ioctl
 *
 * Description:
 *
 *
 * Input Parameters:
 *   filep -
 *   cmd - IOCTL command
 *   arg - must be empty
 *
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/


static int ad7190_ioctl (FAR struct file *filep, int cmd, unsigned long arg)
{

	FAR struct inode *inode = filep->f_inode;
	FAR struct ad7190_dev_s *priv;
	int ret=0;
	int i = 0;
	int selected;

//	uint32_t data;
	uint8_t buf3[3];
//printf("arg: 0x%08X\n",arg);
	/* Get exclusive access to the SPI driver state structure */

	DEBUGASSERT(filep != NULL && filep->f_inode != NULL);
	inode = filep->f_inode;

	priv = (FAR struct ad7190_dev_s*)inode->i_private;
	DEBUGASSERT(priv);

#if 0
	ret = sem_wait(&priv->exclsem);

	if (ret < 0)
	{
		int errcode = errno;
		DEBUGASSERT(errcode < 0);
		return -errcode;
	}
#endif

	switch(cmd){
		/*SPI Initialization
		 *set all settings
		 */
		case ANIOC_AD7190_RESET:
//			sem_post(&priv->exclsem);
			SPI_LOCK(priv->spi, true);
			SPI_SETFREQUENCY(priv->spi,CONFIG_AD7190_SPIFREQUENCY);
			SPI_SETMODE(priv->spi,SPIDEV_MODE3);
			SPI_SETBITS(priv->spi,8);

		/*AD7190 Reset
		 *
		 */
			for(int j=priv->num_of_devices;j>0;j--){
                SPI_SELECT(priv->spi, j, true);

			    for(i=0;i<10;i++){
				    SPI_SEND(priv->spi,0xFF);
			    }

			    SPI_SELECT(priv->spi, j, false);
			}


			SPI_LOCK(priv->spi, false);

//			sem_post(&priv->exclsem);
		break;
		/*AD7190 Read Status Register*/
		case ANIOC_AD7190_READ_STATUS_REG:

			selected=priv->select;
			SPI_LOCK(priv->spi, true);
			SPI_SELECT(priv->spi, selected, true);

			SPI_SEND(priv->spi,0x40);

			//SPI_RECVBLOCK(priv->spi,(char*)arg,1);
			SPI_RECVBLOCK(priv->spi,(FAR char *)((uintptr_t)arg),1);

			SPI_SELECT(priv->spi, selected, false);
			SPI_LOCK(priv->spi, false);


		break;

		/*AD7190 Write Mode Register*/
		case ANIOC_AD7190_WRITE_MODE_REG:

			selected=priv->select;
			SPI_LOCK(priv->spi, true);
			SPI_SELECT(priv->spi, selected, true);

			SPI_SEND(priv->spi,0x08);

			SPI_SEND(priv->spi,(char)(arg>>16));
			SPI_SEND(priv->spi,(char)(arg>>8));
			SPI_SEND(priv->spi,(char)(arg));
#if 0
			SPI_SEND(priv->spi,0x48);
			SPI_RECVBLOCK(priv->spi,buf3,3);
			printf("mode read:: 0:%02hhX, 1:%02hhX, 2:%02hhX\n",buf3[0],buf3[1],buf3[2]);
#endif


			SPI_SELECT(priv->spi, selected, false);
			SPI_LOCK(priv->spi, false);


		break;

		/*AD7190 Write Config Register*/
		case ANIOC_AD7190_WRITE_CONF_REG:

			selected=priv->select;
			SPI_LOCK(priv->spi, true);
			SPI_SELECT(priv->spi, selected, true);

			SPI_SEND(priv->spi,0x10);

			SPI_SEND(priv->spi,(char)(arg>>16));
			SPI_SEND(priv->spi,(char)(arg>>8));
			SPI_SEND(priv->spi,(char)(arg));
#if 0
			SPI_SEND(priv->spi,0x50);
			SPI_RECVBLOCK(priv->spi,buf3,3);
			printf("config read:: 0:%02hhX, 1:%02hhX, 2:%02hhX\n",buf3[0],buf3[1],buf3[2]);
#endif

			SPI_SELECT(priv->spi, selected, false);
			SPI_LOCK(priv->spi, false);


		break;

		/*read data*/
		case ANIOC_AD7190_READ_DATA_REG:
			//buffer =(char*)arg;

			selected=priv->select;
			SPI_LOCK(priv->spi, true);
			SPI_SELECT(priv->spi, selected, true);

			SPI_SEND(priv->spi,0x58);

			SPI_RECVBLOCK(priv->spi,buf3,3);

			SPI_SELECT(priv->spi, selected, false);
			SPI_LOCK(priv->spi, false);

//			data=(buf3[0]<<16) + (buf3[1]<<8) + (buf3[2]);
//			printf("0: %02hhx, 1: %02hhx, 2: %02hhx, data: %08x\n",buf3[0],buf3[1],buf3[2],data);

			 *(FAR uint32_t *)((uintptr_t)arg) =(buf3[0]<<16) + (buf3[1]<<8) + (buf3[2]);
			//*data=0x01234567;//((int32_t)(buf3[0])<<16) + ((int32_t)(buf3[1])<<8) + ((int32_t)(buf3[2]));

		break;
		/*AD7190 Read ID Register*/
		case ANIOC_AD7190_READ_ID_REG:

			selected=priv->select;
			SPI_LOCK(priv->spi, true);
			SPI_SELECT(priv->spi, selected, true);

			SPI_SEND(priv->spi,0x60);

			//SPI_RECVBLOCK(priv->spi,(char*)arg,1);
			SPI_RECVBLOCK(priv->spi,(FAR char *)((uintptr_t)arg),1);

			SPI_SELECT(priv->spi, selected, false);
			SPI_LOCK(priv->spi, false);


		break;
		/* select device*/
		case ANIOC_AD7190_CHIP_SELECT:
			if ( (arg>0) && (arg <=priv->num_of_devices)){
				priv->select=arg;
			}


		break;
		/* continuous mode*/
		case ANIOC_AD7190_CONTINUOUS_MODE:
			if (arg){
#ifdef CONFIG_HPTC
				/* set callback */
		    	ainfo("set async callback\n");

		    	hptc_set_async_input_callback(ch4, &ad7190_dataready_callback,priv);

				/* enable continuous mode and leave SPI locked*/

		    	SPI_LOCK(priv->spi, true);

				for(int j=priv->num_of_devices;j>0;j--){
	                SPI_SELECT(priv->spi, j, true);

				    for(i=0;i<10;i++){
					    SPI_SEND(priv->spi,0x5C);
				    }

				    SPI_SELECT(priv->spi, j, false);
				}

				/* select first device*/
				priv->select=1;
				SPI_SELECT(priv->spi, priv->select , true);

                /* enable callback */
				hptc_enable_async_input_callback(ch4);


#endif

			} else {
                /* disable callback */
				hptc_disable_async_input_callback(ch4);
				/*disable continuous mode*/
				SPI_SELECT(priv->spi, priv->select , false);

				SPI_LOCK(priv->spi, false);
			}


		break;

		case ANIOC_AD7190_SET_FS:

			if ( (arg>0) && (arg <1024)){
				priv->fs=arg;
			}


			break;

		case ANIOC_AD7190_SET_CONFIG:
		{
			uint32_t mode_reg=0x142000; /*CONT_CONV_MODE, DAT_STA, EXT_CLK*/
			uint32_t conf_reg=0x000000;

			struct ad7190_conf_s* config=(struct ad7190_conf_s*)arg;

			mode_reg |= (uint32_t)(config->filter) << 15;
			mode_reg |= priv->fs;

			conf_reg |= (uint32_t)(config->channel) <<8;
			conf_reg |= (uint32_t)(config->ain_buf) <<4;
			conf_reg |= (uint32_t)(config->polarity)<<3;
			conf_reg |= (uint32_t)(config->gain);
			ainfo("set mode_reg: 0x%06x\n",mode_reg);
			ainfo("set conf_reg: 0x%06x\n",conf_reg);
			selected=priv->select;
			SPI_LOCK(priv->spi, true);
			SPI_SELECT(priv->spi, selected, true);

			SPI_SEND(priv->spi,0x08);
			SPI_SEND(priv->spi,(char)(mode_reg>>16));
			SPI_SEND(priv->spi,(char)(mode_reg>>8));
			SPI_SEND(priv->spi,(char)(mode_reg));


//			SPI_SELECT(priv->spi, selected, false);
//			SPI_SELECT(priv->spi, selected, true);


			SPI_SEND(priv->spi,0x10);
			SPI_SEND(priv->spi,(char)(conf_reg>>16));
			SPI_SEND(priv->spi,(char)(conf_reg>>8));
			SPI_SEND(priv->spi,(char)(conf_reg));

			/* read back mode register */
                        SPI_SEND(priv->spi,0x48);
                        SPI_RECVBLOCK(priv->spi,buf3,3);
                        ainfo("read mode_reg: 0x%02hhX%02hhX%02hhX\n",buf3[0],buf3[1],buf3[2]);

                        /* read back config register */
                        SPI_SEND(priv->spi,0x50);
                        SPI_RECVBLOCK(priv->spi,buf3,3);
                        ainfo("read conf_reg: 0x%02hhX%02hhX%02hhX\n",buf3[0],buf3[1],buf3[2]);


			SPI_SELECT(priv->spi, selected, false);
			SPI_LOCK(priv->spi, false);







		}

		  break;

	}
	return ret;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: ad5662_register
 *
 * Description:
 *   Register the ad5662 character device as 'devpath'
 *
 * Input Parameters:
 *   devpath - The full path to the driver to register. E.g., "/dev/temp0"
 *   spi - An instance of the SPI interface to use to communicate wit
 *   bus - the bus adress of the connected SPI Device
 *
 *
 * Returned Value:
 *   Zero (OK) on success;
 *
 ****************************************************************************/

int ad7190_register(FAR const char *devpath,FAR struct spi_dev_s *spi, int num_dev)
{
	FAR struct ad7190_dev_s *priv;
	int ret = 0;

	priv = (FAR struct ad7190_dev_s *)kmm_zalloc(sizeof(struct ad7190_dev_s));

	/*check*/
	priv->spi = spi;
//	priv->exclsem.semcount = 1;
//	priv->devicid = 1;
	priv->select=0;
	priv->fs=40;
	priv->num_of_devices=num_dev;

	ret = register_driver(devpath, &g_ad7190fops, 0666, priv);

	return ret;
}
#endif /* CONFIG_SPI && CONFIG_AD5662 */

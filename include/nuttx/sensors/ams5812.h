/****************************************************************************
 * include/nuttx/sensors/ams5812.h
 *
 *   Copyright (C) 2015 Omni Hoverboards Inc. All rights reserved.
 *   Author: Paul Alexander Patience <paul-a.patience@polymtl.ca>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef __INCLUDE_NUTTX_SENSORS_AMS5812
#define __INCLUDE_NUTTX_SENSORS_AMS5812

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/sensors/ioctl.h>

#if defined(CONFIG_I2C) && defined(CONFIG_SENSORS_AMS5812)

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
/* Configuration ************************************************************
 * Prerequisites:
 *
 * CONFIG_I2C
 *   Enables support for I2C drivers
 * CONFIG_SENSORS_AMS5812
 *   Enables support for the AMS5812 driver
 */

/* I2C Address **************************************************************/

/* This is the factory-programmed 7-bit slave address of all pressure sensors
 * of the AMS 5812 series.
 *
 * If more than one AMS 5812 should be connected to the same I2C-bus, each
 * pressure sensor requires an individual slave address.
 * This can be programmed individually by factory or the AMS starter kit and
 * its value should be defined in board.h or another board specific place.
 */

#define AMS5812_ADDR_DEFAULT       0x78

/****************************************************************************
 * Public Types
 ****************************************************************************/

enum ams5812_model_e
{
  AMS5812_MODEL_0001_D_B,
  AMS5812_MODEL_0015_D_B,
  AMS5812_MODEL_0150_A,
  AMS5812_MODEL_0150_B,
  AMS5812_MODEL_0003_D,
  AMS5812_MODEL_0300_A
};

struct ams5812_measure_s
{
  float temperature;  /* in Degree   x100    */
  float pressure;     /* in mBar     x10     */
};

struct i2c_master_s;

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Name: ams5812_readSample
 *
 * Description:
 *       Read a single sample from the specified AMS5812 sensor device.
 *
 *       This functions works without the nuttx VFS. That means that one
 *       does not have to register a driver instance before.
 *
 * Input Parameters:
 *   addr    - The I2C address of the AMS5812.
 *   model   - The AMS5812 model.
 *   sample  - Pointer to the target sample struct.
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/

int ams5812_readSample(uint8_t addr, enum ams5812_model_e model,
                       FAR struct ams5812_measure_s *sample);

/****************************************************************************
 * Name: ams5812_register
 *
 * Description:
 *   Register the AMS5812 character device as 'devpath'.
 *
 * Input Parameters:
 *   devpath - The full path to the driver to register, e.g., "/dev/press0".
 *   i2c     - An I2C driver instance.
 *   addr    - The I2C address of the AMS5812.
 *   model   - The AMS5812 model.
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/

int ams5812_register(FAR const char *devpath, FAR struct i2c_master_s *i2c,
                     uint8_t addr, enum ams5812_model_e model);


#undef EXTERN
#ifdef __cplusplus
}
#endif

#endif /* CONFIG_I2C && CONFIG_SENSORS_AMS5812 */
#endif /* __INCLUDE_NUTTX_SENSORS_AMS5812 */

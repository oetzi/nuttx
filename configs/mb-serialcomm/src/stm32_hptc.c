/************************************************************************************
 * configs/mb-serialcomm/src/stm32_hptc.c
 *
 *   Copyright (C) 2011 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *           Stefan Nowak
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>
#include <nuttx/timers/hptc.h>

#include <errno.h>
#include <debug.h>


//#include <arch/board/board.h>

//#include "chip.h"
//#include "up_arch.h"
#include "stm32_hptc.h"
#include "mb-serialcomm.h"

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/
/* Configuration *******************************************************************/
/* HPTC
 *
 * The mB_imu board allows TIM2 and TIM5 to be used as HPTC
 */

#define HAVE_HPTC 1

#ifndef CONFIG_HPTC
#  undef HAVE_HPTC
#endif

//#ifndef CONFIG_STM32_TIM4
//#  undef HAVE_PWM
//#endif

//#ifndef CONFIG_STM32_TIM4_PWM
//#  undef HAVE_PWM
//#endif

//#if CONFIG_STM32_TIM4_CHANNEL != STM32F4_VAMEX_IMU_PWMCHANNEL
//#  undef HAVE_PWM
//#endif

/************************************************************************************
 * Private Functions
 ************************************************************************************/

/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_hptc_setup
 *
 * Description:
 *   Initialize HPTC and register the HPTC device.
 *
 ************************************************************************************/

int stm32_hptc_setup(void)
{
#ifdef HAVE_HPTC
  static bool initialized = false;
//  struct hptc_lowerhalf_s *hptc;
  int ret;

  /* Have we already initialized? */

  if (!initialized)
    {
      /* Call stm32_hptcinitialize() to get an instance of the HPTC interface */
	  tmrinfo("initialize hptc...\n");
	  ret=stm32_hptc_initialize();


      if (ret < 0)
        {
          tmrerr("ERROR: stm32_hptc_initialize failed: %d\n", ret);
          return ret;
        }

      /* Now we are initialized */

      initialized = true;
    }

  return OK;
#else
  return -ENODEV;
#endif /* HAVE_HPTC */
}



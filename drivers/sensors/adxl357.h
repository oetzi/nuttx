/********************************************************************************************
 * drivers/sensors/adxl357.h
 *
 *   Copyright (C) 2014 Alan Carvalho de Assis
 *   Author: Alan Carvalho de Assis <acassis@gmail.com>
 *   using ADXL357 driver as template reference
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ********************************************************************************************/

#ifndef __DRIVERS_SENSORS_ADXL357_H
#define __DRIVERS_SENSORS_ADXL357_H

/********************************************************************************************
 * Included Files
 ********************************************************************************************/

#include <nuttx/config.h>

#include <semaphore.h>

#include <nuttx/wdog.h>
#include <nuttx/clock.h>
#include <nuttx/wqueue.h>
#include <nuttx/sensors/adxl357.h>

#if defined(CONFIG_SENSORS_ADXL357)

/********************************************************************************************
 * Pre-processor Definitions
 ********************************************************************************************/

#ifdef CONFIG_ADXL357_I2C
#  error "Only the ADXL357 SPI interface is supported by this driver"
#endif

#define ADXL357_BYTE_PER_SAMPLE_ELEMENT   3    // 20 bit values -> stored in 3 byte each
#define ADXL357_ELEMENTS_PER_SAMPLE       3    // 1 sample = 3 sample elements (acc_x, acc_y, acc_z)

/* Driver support ***************************************************************************/
/* This format is used to construct the /dev/accel[n] device driver path.  It defined here
 * so that it will be used consistently in all places.
 */

#define DEV_FORMAT   "/dev/accSensor%d"
#define DEV_NAMELEN  16

#ifndef OK
#  define OK 0
#endif

/* Driver flags */

/* Everything is fine... */
#define ADXL357_STAT_NORMAL                      0

/* Working thread too slow and did not handeled all sensor data until next samples were available,
 * so we missed new data THIS SHOULD NEVER HAPPEN!!
 */

#define ADXL357_STAT_PROCESSING_DELAY_DETECTED   (1 << 1)

/* Internal driver buffer was full and had to be overwritten. Application layer should read and
 * clear buffer continuously
 */
#define ADXL357_STAT_BUFFER_OVERWRITTEN          (1 << 2)

/********************************************************************************************
 * Public Data
 ********************************************************************************************/
struct adxl357_workParam_s g_worker_param;

/********************************************************************************************
 * Public Types
 ********************************************************************************************/
/* This structure defines one serial I/O buffer.  */

struct adxl357_buffer_s
{
	sem_t sem;             /* Used to control exclusive access to the buffer */
	volatile int16_t head; /* Index to the head [IN] index in the buffer */
	volatile int16_t tail; /* Index to the tail [OUT] index in the buffer */
	int16_t size;          /* The allocated size of the buffer */
	FAR char *buffer;      /* Pointer to the allocated buffer memory */
};

/* This structure represents the state of the ADXL357 driver */

struct adxl357_dev_s
{
  /* Common fields */

  FAR struct adxl357_config_s *config; /* Board configuration data */
  sem_t exclsem;                       /* Manages exclusive access to this structure */
  sem_t recvsem;                       /* Wakeup user waiting for data in recv.buffer */
  uint16_t readBulkCnt;                /* This counter is incremented each time a bulk is read from
                                        * sensor.
                                        */

#ifdef CONFIG_ADXL357_SPI
  FAR struct spi_dev_s *spi;           /* Saved SPI driver instance */
  bool    fastMode;                    /* In fast mode the (spi) bus is configured and locked  onlay
                                        * once. So we do not need to reconfigure Bus for each
                                        * communication sequence which should be a lot faster
                                        */
#else
  FAR struct i2c_master_s *i2c;        /* Saved I2C driver instance */
#endif

  volatile bool recvwaiting;           /* true: User waiting for data in recv.buffer */
  int status;                          /* See ADXL357_STAT_* definitions */
  struct work_s work;                  /* Supports the interrupt handling "bottom half" */

  FAR struct adxl357_buffer_s  rxfifo; /* Rx Buffer management */
  uint16_t bulksize;                   /* Number of Bytes that is collected until internal sensor
                                        * fifo watermark is reached.
                                        * These bytes have to be read at a read event
                                        */
  adxl357_temperature_sample_t sensorTemperature; /* Last measured sensor temperature. (12 bit) */
};


/* This structure represents the parameter for the worker function */
struct adxl357_workParam_s
{
	FAR struct adxl357_dev_s *priv;
	adxl357_timestamp timestamp;                   /* Timestamp of the trigger */
};

/********************************************************************************************
 * Public Function Prototypes
 ********************************************************************************************/

/********************************************************************************************
 * Name: adxl357_getreg8
 *
 * Description:
 *   Read from an 8-bit ADXL357 register
 *
 ********************************************************************************************/

uint8_t adxl357_getreg8(FAR struct adxl357_dev_s *priv, uint8_t regaddr);

/********************************************************************************************
 * Name: adxl357_putreg8
 *
 * Description:
 *   Write a value to an 8-bit ADXL357 register
 *
 ********************************************************************************************/

void adxl357_putreg8(FAR struct adxl357_dev_s *priv, uint8_t regaddr, uint8_t regval);

/********************************************************************************************
 * Name: adxl357_readBulk
 *
 * Description:
 *   Read n bytes in stream mode from an ADXL357 FIFO into buffer.
 *
 ********************************************************************************************/

void adxl357_readBulk(FAR struct adxl357_dev_s *priv, FAR struct adxl357_buffer_s* rxbuffer, uint8_t nBytes);


/****************************************************************************
 * Name: adxl357_switchFastMode
 *
 * Description:
 *
 ****************************************************************************/

void adxl357_switchFastMode(FAR struct adxl357_dev_s *priv, bool fastMode);


#endif /* CONFIG_SENSORS_ADXL357 */
#endif /* __DRIVERS_SENSORS_ADXL357_H */

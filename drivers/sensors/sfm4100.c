/****************************************************************************
 * drivers/sensors/sfm4100.c
 * Character driver for Senserion SFM4100 Mass Flow Meter
 *
 * Based on ms58xx driver.
 *
 *   Copyright (C) 2015 Omni Hoverboards Inc. All rights reserved.
 *   Author: Paul Alexander Patience <paul-a.patience@polymtl.ca>
 *   Updated by: Karim Keddam <karim.keddam@polymtl.ca>
 *
 *   Copyright (C) 2016 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <errno.h>
#include <debug.h>
#include <stdlib.h>

#include <nuttx/kmalloc.h>
#include <nuttx/fs/fs.h>
#include <nuttx/arch.h>
#include <nuttx/i2c/i2c_master.h>
#include <nuttx/sensors/sfm4100.h>
#include <nuttx/random.h>

#if defined(CONFIG_I2C) && defined(CONFIG_SENSORS_SFM4100)

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#ifndef CONFIG_SFM4100_I2C_FREQUENCY
#  define CONFIG_SFM4100_I2C_FREQUENCY I2C_SPEED_STANDARD
#endif

#ifndef OK
#  define OK 0
#endif

/* Register Definitions *****************************************************/

/* CRC */

#define CRC_POLYNOMIAL 0x131 /* P(x)=x^8+x^5+x^4+1 = 100110001 */

/* SF04 eeprom map */

#define  EE_ADR_SN_CHIP        0x02E4
#define  EE_ADR_SN_PRODUCT     0x02F8
#define  EE_ADR_SCALE_FACTOR   0x02B6
#define  EE_ADR_FLOW_UNIT      0x02B7

/* sensor command */

typedef enum
{
     USER_REG_W               = 0xE2, /* command writing user register */
     USER_REG_R               = 0xE3, /* command reading user register */
     ADV_USER_REG_W           = 0xE4, /* command writing advanced user register */
     ADV_USER_REG_R           = 0xE5, /* command reading advanced user register */
     READ_ONLY_REG1_R         = 0xE7, /* command reading read-only register 1 */
     READ_ONLY_REG2_R         = 0xE9, /* command reading read-only register 2 */
     TRIGGER_FLOW_MEASUREMENT = 0xF1, /* command trig. a flow measurement */
     TRIGGER_TEMP_MEASUREMENT = 0xF3, /* command trig. a temperature measurement */
     TRIGGER_VDD_MEASUREMENT  = 0xF5, /* command trig. a supply voltage measurement */
     EEPROM_W                 = 0xFA, /* command writing eeprom */
     EEPROM_R                 = 0xFA, /* command reading eeprom */
     SOFT_RESET               = 0xFE  /* command soft reset */
}etCommand;

/* sensor register */

typedef enum
{
    USER_REG       = USER_REG_R,
    ADV_USER_REG   = ADV_USER_REG_R,
    READ_ONLY_REG1 = READ_ONLY_REG1_R,
    READ_ONLY_REG2 = READ_ONLY_REG2_R
}etSF04Register;

/* measurement signal selection */

typedef enum
{
    FLOW = TRIGGER_FLOW_MEASUREMENT,
    TEMP = TRIGGER_TEMP_MEASUREMENT,
    VDD  = TRIGGER_VDD_MEASUREMENT,
}etSF04MeasureType;

/* This enum lists all available flow resolution (Advanced User Register [11:9]) */

typedef enum 
{
    eSF04_RES_9BIT  = (0 << 9),
    eSF04_RES_10BIT = (1 << 9),
    eSF04_RES_11BIT = (2 << 9),
    eSF04_RES_12BIT = (3 << 9),
    eSF04_RES_13BIT = (4 << 9),
    eSF04_RES_14BIT = (5 << 9),
    eSF04_RES_15BIT = (6 << 9),
    eSF04_RES_16BIT = (7 << 9),
    eSF04_RES_MASK  = (7 << 9)
} etSF04Resolution;


/****************************************************************************
 * Private Types
 ****************************************************************************/

struct sfm4100_dev_s
{
  FAR struct i2c_master_s   *i2c;    /* I2C interface */
  uint8_t                   addr;    /* I2C address */

  struct sfm4100_measure_s  measure;
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/
/* CRC Calculation */

static uint8_t sfm4100_crc(FAR uint8_t *data, uint8_t nBytes, uint8_t checksum);

/* I2C Helpers */

static int   sfm4100_i2c_write(FAR struct sfm4100_dev_s *priv,
                               FAR const uint8_t *buffer, int buflen);
static int   sfm4100_i2c_read(FAR struct sfm4100_dev_s *priv,
                              FAR uint8_t *buffer, int buflen);
static int   sfm4100_readu16(FAR struct sfm4100_dev_s *priv, uint8_t regaddr,
                             FAR uint16_t *regval);
static int   sfm4100_reset(FAR struct sfm4100_dev_s *priv);
#if 0
static int   sfm4100_readprom(FAR struct sfm4100_dev_s *priv);
#endif
static int   sfm4100_triggerFlowMeasurement(FAR struct sfm4100_dev_s *priv);
static int   sfm4100_measure(FAR struct sfm4100_dev_s *priv);
static float sfm4100_convert(FAR struct sfm4100_dev_s *priv, uint8_t* sfmdata);

/* Character Driver Methods */

static int     sfm4100_open(FAR struct file *filep);
static int     sfm4100_close(FAR struct file *filep);
static ssize_t sfm4100_read(FAR struct file *filep, FAR char *buffer,
                           size_t buflen);
static ssize_t sfm4100_write(FAR struct file *filep, FAR const char *buffer,
                            size_t buflen);
static int     sfm4100_ioctl(FAR struct file *filep, int cmd,
                            unsigned long arg);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct file_operations g_fops =
{
  sfm4100_open,
  sfm4100_close,
  sfm4100_read,
  sfm4100_write,
  NULL,
  sfm4100_ioctl,
  NULL
#ifndef CONFIG_DISABLE_PSEUDOFS_OPERATIONS
  , NULL
#endif
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: sfm4100_crc
 *
 * Description:
 *   Calculate the CRC.
 *
 ****************************************************************************/

static uint8_t sfm4100_crc(FAR uint8_t *data, uint8_t nBytes, uint8_t checksum)
{
  uint8_t crc = 0;

  for (int byteCount = 0; byteCount < nBytes; byteCount++) 
  {
      crc ^= (data[byteCount]);

      for (uint8_t bit = 8; bit > 0; --bit) 
      {
          if (crc & 0x80) 
          {
              crc = (crc << 1) ^ CRC_POLYNOMIAL;
          }
          else 
          {
              crc = (crc << 1);
          }
      }
  }

  if (crc != checksum)
  {
      return false;
  }

  return true;
}

/****************************************************************************
 * Name: sfm4100_i2c_write
 *
 * Description:
 *   Write to the I2C device.
 *
 ****************************************************************************/

static int sfm4100_i2c_write(FAR struct sfm4100_dev_s *priv,
                             FAR const uint8_t *buffer, int buflen)
{
  struct i2c_msg_s msg;
  int ret;

  /* Setup for the transfer */

  msg.frequency = CONFIG_SFM4100_I2C_FREQUENCY,
  msg.addr      = priv->addr;
  msg.flags     = 0;
  msg.buffer    = (FAR uint8_t *)buffer;  /* Override const */
  msg.length    = buflen;

  /* Then perform the transfer. */

  ret = I2C_TRANSFER(priv->i2c, &msg, 1);
  return (ret >= 0) ? OK : ret;
}

/****************************************************************************
 * Name: sfm4100_i2c_read
 *
 * Description:
 *   Read from the I2C device.
 *
 ****************************************************************************/

static int sfm4100_i2c_read(FAR struct sfm4100_dev_s *priv,
                            FAR uint8_t *buffer, int buflen)
{
  struct i2c_msg_s msg;
  int ret;

  /* Setup for the transfer */

  msg.frequency = CONFIG_SFM4100_I2C_FREQUENCY,
  msg.addr      = priv->addr,
  msg.flags     = I2C_M_READ;
  msg.buffer    = buffer;
  msg.length    = buflen;

  /* Then perform the transfer. */

  ret = I2C_TRANSFER(priv->i2c, &msg, 1);
  return (ret >= 0) ? OK : ret;
}

/****************************************************************************
 * Name: sfm4100_readu16
 *
 * Description:
 *   Read from a 16-bit register.
 *
 ****************************************************************************/

static int sfm4100_readu16(FAR struct sfm4100_dev_s *priv, uint8_t regaddr,
                          FAR uint16_t *regval)
{
  uint8_t buffer[2];
  int ret;

  sninfo("addr: %02x\n", regaddr);

  /* Write the register address */

  ret = sfm4100_i2c_write(priv, &regaddr, sizeof(regaddr));
  if (ret < 0)
    {
      snerr("ERROR: i2c_write failed: %d\n", ret);
      return ret;
    }

  /* Restart and read 16 bits from the register */

  ret = sfm4100_i2c_read(priv, buffer, sizeof(buffer));
  if (ret < 0)
    {
      snerr("ERROR: i2c_read failed: %d\n", ret);
      return ret;
    }

  *regval = (uint16_t)buffer[0] << 8 | (uint16_t)buffer[1];
  sninfo("value: %04x ret: %d\n", *regval, ret);
  return ret;
}

/****************************************************************************
 * Name: sfm4100_readprom
 *
 * Description:
 *   Read from the PROM.
 *
 ****************************************************************************/
#if 0
static int sfm4100_readprom(FAR struct sfm4100_dev_s *priv)
{
    //TODO
}
#endif

/****************************************************************************
 * Name: sfm4100_changeResolution
 *
 * Description:
 *      Change the sensor resolution to the provided value.
 *
 *      9 to 16 Bit resolution - max. readout time:  0.9 ms @  9 bit, 
 *                                                  73.2 ms @ 16 bit
 *
 ****************************************************************************/

static int sfm4100_changeResolution(FAR struct sfm4100_dev_s *priv, 
                                    etSF04Resolution sensor_resolution)
{
  uint8_t buf_i2c_tx[7] = {0};

  /* Read Advanced user register content */

  uint16_t adv_user_reg_original  = 0;
  int ret = sfm4100_readu16(priv, ADV_USER_REG_R, &adv_user_reg_original);

  if(ret < 0) return ret;
           
  sleep(1);

  /* calculate new resolution mask */

  uint16_t resolution_mask = 0xF1FF | sensor_resolution;

  /* update new register bits - ONLY CHANGE RESOLUTION BITS (Bit 11-9)!!! */
  
  uint16_t adv_user_reg_new = (adv_user_reg_original | 0x0E00) & resolution_mask;

  /* Apply resolution changes:
   * Change mode to write to adv user register - NEVER CHANGE OTHER SENSOR BITS!!!
   */
  
  buf_i2c_tx[0] = ADV_USER_REG_W; 
  buf_i2c_tx[1] = (uint8_t)(adv_user_reg_new >> 8);   // MSB
  buf_i2c_tx[2] = (uint8_t)(adv_user_reg_new & 0xFF); // LSB

  return sfm4100_i2c_write(priv, buf_i2c_tx, 3);
}

/****************************************************************************
 * Name: sfm4100_reset
 *
 * Description:
 *   Reset the device.
 *
 ****************************************************************************/

static int sfm4100_reset(FAR struct sfm4100_dev_s *priv)
{
  /* Change resolution from default to 16 bit */

  return sfm4100_changeResolution(priv, eSF04_RES_16BIT);
}

/****************************************************************************
 * Name: sfm4100_convert
 *
 * Description:
 *      Convert the raw data to a physical flow value.
 *      Target unit is SCCM (standard cubic centimeters per minute).
 *
 ****************************************************************************/

static float sfm4100_convert(FAR struct sfm4100_dev_s *priv, uint8_t* sfmdata)
{
  if (sfm4100_crc(sfmdata, 0x02, sfmdata[2])) 
  {
      int16_t rawValue = sfmdata[1] | (sfmdata[0] << 8);

      return 1.0f * rawValue;
  }
  else 
  {
      return -99999;
  }
}

/****************************************************************************
 * Name: sfm4100_measure
 *
 * Description:
 *   Measure the mass flow.
 *   
 *   NOTE: Temperature and VDD measurement currently not supported.
 *
 ****************************************************************************/

static int sfm4100_measure(FAR struct sfm4100_dev_s *priv)
{
  uint8_t buffer[3];

  /* Read data */

  sfm4100_i2c_read(priv, buffer, 3);

  /* Convert raw data */

  priv->measure.massFlow = sfm4100_convert(priv, buffer);

  return OK;
}

/****************************************************************************
 * Name: sfm4100_triggerFlowMeasurement
 *
 * Description:
 *
 ****************************************************************************/

static int sfm4100_triggerFlowMeasurement(FAR struct sfm4100_dev_s *priv)
{
  uint8_t regaddr;
  int ret;

  regaddr = TRIGGER_FLOW_MEASUREMENT;
  sninfo("addr: %02x\n", regaddr);

  /* Write the register address */

  ret = sfm4100_i2c_write(priv, &regaddr, sizeof(regaddr));
  if (ret < 0)
    {
      snerr("ERROR: i2c_write failed: %d\n", ret);
      return ret;
    }

  /* Update Timestamp */

  clock_gettime(CLOCK_MONOTONIC, &priv->measure.timeStamp);

  return ret;
}

/****************************************************************************
 * Name: sfm4100_open
 *
 * Description:
 *   This method is called when the device is opened.
 *
 ****************************************************************************/

static int sfm4100_open(FAR struct file *filep)
{
  return OK;
}

/****************************************************************************
 * Name: sfm4100_close
 *
 * Description:
 *   This method is called when the device is closed.
 *
 ****************************************************************************/

static int sfm4100_close(FAR struct file *filep)
{
  return OK;
}

/****************************************************************************
 * Name: sfm4100_read
 *
 * Description:
 *   A dummy read method.
 *
 ****************************************************************************/

static ssize_t sfm4100_read(FAR struct file *filep, FAR char *buffer,
                           size_t buflen)
{
  ssize_t size;
  FAR struct inode *inode        = filep->f_inode;
  FAR struct sfm4100_dev_s *priv  = inode->i_private;
  FAR struct sfm4100_measure_s *p = (FAR struct sfm4100_measure_s *)buffer;

  size = buflen;
  while (size >= sizeof(*p))
    {
      if (sfm4100_measure(priv) < 0)
        {
          return -1;
        }

      *p = priv->measure;

      p++;
      size -= sizeof(*p);
    }

  return buflen;
}

/****************************************************************************
 * Name: sfm4100_write
 *
 * Description:
 *   A dummy write method.
 *
 ****************************************************************************/

static ssize_t sfm4100_write(FAR struct file *filep, FAR const char *buffer,
                            size_t buflen)
{
  return -ENOSYS;
}

/****************************************************************************
 * Name: sfm4100_ioctl
 *
 * Description:
 *   The standard ioctl method.
 *
 ****************************************************************************/

static int sfm4100_ioctl(FAR struct file *filep, int cmd, unsigned long arg)
{
  FAR struct inode        *inode = filep->f_inode;
  FAR struct sfm4100_dev_s *priv  = inode->i_private;
  int                      ret   = OK;

  /* Handle ioctl commands */

  switch (cmd)
    {

      /* Starts the conversion sequence. Arg: None. */

      case SNIOC_START_CONVERSION:
        ret = sfm4100_triggerFlowMeasurement(priv);
        break;

      /* Measure the mass flow. Arg: None. */

      case SNIOC_MEASURE:
        DEBUGASSERT(arg == 0);
        ret = sfm4100_measure(priv);
        break;

      /* Reset the device. Arg: None. */

      case SNIOC_RESET:
        DEBUGASSERT(arg == 0);
        ret = sfm4100_reset(priv);
        break;

      /* Unrecognized commands */

      default:
        snerr("ERROR: Unrecognized cmd: %d arg: %ld\n", cmd, arg);
        ret = -ENOTTY;
        break;
    }

  return ret;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: sfm4100_register
 *
 * Description:
 *   Register the SFM4100 character device as 'devpath'.
 *
 * Input Parameters:
 *   devpath - The full path to the driver to register, e.g., "/dev/press0".
 *   i2c     - An I2C driver instance.
 *   addr    - The I2C address of the SFM4100.
 *   osr     - The oversampling ratio.
 *   model   - The SFM4100 model.
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/

int sfm4100_register(FAR const char *devpath, FAR struct i2c_master_s *i2c,
                    uint8_t addr)
{
  FAR struct sfm4100_dev_s *priv;
  int ret;

  /* Sanity check */

  DEBUGASSERT(i2c != NULL);

  /* Initialize the device's structure */

  priv = (FAR struct sfm4100_dev_s *)kmm_malloc(sizeof(*priv));
  if (priv == NULL)
    {
      snerr("ERROR: Failed to allocate instance\n");
      return -ENOMEM;
    }

  priv->i2c   = i2c;
  priv->addr  = addr;

  priv->measure.massFlow          = 0;
  priv->measure.timeStamp.tv_sec  = 0;
  priv->measure.timeStamp.tv_nsec = 0;

  ret = sfm4100_reset(priv);
  if (ret < 0)
    {
      snerr("ERROR: sfm4100_reset failed: %d\n", ret);
      goto errout;
    }

  usleep(1000);

  /* Already trigger initial measurement */

  ret = sfm4100_triggerFlowMeasurement(priv);
  if (ret < 0)
    {
      snerr("ERROR: triggerFlowMeasurement failed: %d\n", ret);
      goto errout;
    }

  /* Register the character driver */

  ret = register_driver(devpath, &g_fops, 0666, priv);
  if (ret < 0)
    {
      snerr("ERROR: Failed to register driver: %d\n", ret);
      goto errout;
    }

  return ret;

errout:
  kmm_free(priv);
  return ret;
}

#endif /* CONFIG_I2C && CONFIG_SENSORS_SFM4100 */

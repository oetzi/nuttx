/*
 * slotlists_containter_s.h
 *
 *  Created on: 05.03.2018
 *      Author: bbrandt
 */

#ifndef NUTTX_INCLUDE_NUTTX_MESSBUS_SLOTLISTS_CONTAINER_S_H_
#define NUTTX_INCLUDE_NUTTX_MESSBUS_SLOTLISTS_CONTAINER_S_H_

#include "slotlist_s.h"


#if defined(__cplusplus)
namespace PhysicalLayer
{
#endif

/* This struct contains pointers to our slotlists. For the client we only
 * support the use of one channel anyway. But to achieve highest similarity
 * to the master we stuck to this nomenclature.
 */
struct slotlists_container_s
{
#ifdef CONFIG_MESSBUS_MASTER
    # if CONFIG_MESSBUS_USE_CH1
        ch1_slotlist_s *ch1_slotlist;
    #endif
    # if CONFIG_MESSBUS_USE_CH2
        ch2_slotlist_s *ch2_slotlist;
    #endif
    # if CONFIG_MESSBUS_USE_CH3
        ch3_slotlist_s *ch3_slotlist;
    #endif
    # if CONFIG_MESSBUS_USE_CH4
        ch4_slotlist_s *ch4_slotlist;
    # endif
#endif

#ifdef CONFIG_MESSBUS_CLIENT
        ch1_slotlist_s *ch1_slotlist;
#endif
};

#if defined(__cplusplus)
}
#endif

#endif /* NUTTX_INCLUDE_NUTTX_MESSBUS_SLOTLISTS_CONTAINER_S_H_ */

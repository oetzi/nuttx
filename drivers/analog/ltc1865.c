/****************************************************************************
 * arch/drivers/analog/ltc1865.c
 *
 *   Copyright (C) 2010, 2016 Gregory Nutt. All rights reserved.
 *   Copyright (C) 2011 Li Zhuoyi. All rights reserved.
 *   Author: Li Zhuoyi <lzyy.cn@gmail.com>
 *           Gregory Nutt <gnutt@nuttx.org>
 *
 * This file is a part of NuttX:
 *
 *   Copyright (C) 2010 Gregory Nutt. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdlib.h>
#include <errno.h>
#include <debug.h>
#include <semaphore.h>

#include <nuttx/kmalloc.h>
#include <nuttx/fs/fs.h>
#include <nuttx/fs/ioctl.h>

//#include <stdio.h>
//#include <sys/types.h>
//#include <stdint.h>
//#include <stdbool.h>



//#include <arch/board/board.h>
#include <nuttx/arch.h>
#include <nuttx/analog/ltc1865.h>

#include <time.h>

#if defined(CONFIG_ADC_LTC1865)

#define LTC1865_EXCLUSIVE_SPI

#define LTC1865_SAMPLE_CH0 0x8000
#define LTC1865_SAMPLE_CH1 0xC000

/****************************************************************************
 * ad_private Types
 ****************************************************************************/

struct ltc1865_dev_s
{
//  int devno;
  FAR struct spi_dev_s  *spi1;      /* Cached SPI device reference */
  unsigned int devno1;
  FAR struct spi_dev_s  *spi2;      /* Cached SPI device reference */
  unsigned int devno2;
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* Character driver methods */

static int     ltc1865_open(FAR struct file *filep);
static int     ltc1865_close(FAR struct file *filep);
static ssize_t ltc1865_read(FAR struct file *filep, FAR char *buffer,
                             size_t buflen);
static ssize_t ltc1865_write(FAR struct file *filep, FAR const char *buffer, size_t buflen);

static int     ltc1865_ioctl(FAR struct file *filep, int cmd, unsigned long arg);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct file_operations g_ltc1865fops =
{
  ltc1865_open,
  ltc1865_close,
  ltc1865_read,
  ltc1865_write,
  NULL,
  ltc1865_ioctl,    /* ioctl */
};


/****************************************************************************
 * Private Functions
 ****************************************************************************/
/****************************************************************************
 * Name: ltc1865_capture
 *
 * Description:
 *   capture a sample from all four channels
 *
 ****************************************************************************/
static ssize_t ltc1865_capture(struct ltc1865_msg_s *buffer, FAR struct ltc1865_dev_s *priv){

    struct timespec ts;
    uint16_t data[4];


#ifndef LTC1865_EXCLUSIVE_SPI
    SPI_LOCK(priv->spi1, true);
    SPI_SETMODE(spi1, SPIDEV_MODE0);
    SPI_SETBITS(spi1, 16);
    SPI_SETFREQUENCY(spi1, CONFIG_LTC1865_SPIFREQUENCY);

    SPI_LOCK(priv->spi2, true);
    SPI_SETMODE(spi2, SPIDEV_MODE0);
    SPI_SETBITS(spi2, 16);
    SPI_SETFREQUENCY(spi2, CONFIG_LTC1865_SPIFREQUENCY);
#endif

    clock_gettime(CLOCK_MONOTONIC,&ts);

    /*set first channel/dummy read*/

    SPI_SELECT(priv->spi1, priv->devno1, true);
    SPI_SEND(priv->spi1,(uint16_t)(LTC1865_SAMPLE_CH0));
    SPI_SELECT(priv->spi1, priv->devno1, false);

    SPI_SELECT(priv->spi2, priv->devno2, true);
    SPI_SEND(priv->spi2,(uint16_t)(LTC1865_SAMPLE_CH0));
    SPI_SELECT(priv->spi2, priv->devno2, false);

    SPI_SELECT(priv->spi1, priv->devno1, true);
    data[0]=SPI_SEND(priv->spi1,(uint16_t)(LTC1865_SAMPLE_CH1));
    SPI_SELECT(priv->spi1, priv->devno1, false);

    SPI_SELECT(priv->spi2, priv->devno2, true);
    data[1]=SPI_SEND(priv->spi2,(uint16_t)(LTC1865_SAMPLE_CH1));
    SPI_SELECT(priv->spi2, priv->devno2, false);

    SPI_SELECT(priv->spi1, priv->devno1, true);
    data[2]=SPI_SEND(priv->spi1,(uint16_t)(LTC1865_SAMPLE_CH0));
    SPI_SELECT(priv->spi1, priv->devno1, false);

    SPI_SELECT(priv->spi2, priv->devno2, true);
    data[3]=SPI_SEND(priv->spi2,(uint16_t)(LTC1865_SAMPLE_CH0));
    SPI_SELECT(priv->spi2, priv->devno2, false);

#ifndef LTC1865_EXCLUSIVE_SPI
    SPI_LOCK(priv->spi1, false);
    SPI_LOCK(priv->spi2, false);
#endif

    buffer->time_s   =  ts.tv_sec;
    buffer->time_ns  =  ts.tv_nsec;
    for (int i=0;i<4;i++){
        buffer->value[i] =  4.096*(float)data[i]/(float)0xFFFF;
    }

    return sizeof(struct ltc1865_msg_s);
}



/****************************************************************************
 * Name: ltc1865_open
 *
 * Description:
 *   This function is called whenever the AD5662 device is opened.
 *
 ****************************************************************************/

static int ltc1865_open(FAR struct file *filep)
{
	return OK;
}

/****************************************************************************
 * Name: ltc1865_close
 *
 * Description:
 *   This routine is called when the AD5662 device is closed.
 *
 ****************************************************************************/

static int ltc1865_close(FAR struct file *filep)
{
	return OK;
}


/****************************************************************************
 * Name: ltc1865_read
 ****************************************************************************/

static ssize_t ltc1865_read(FAR struct file *filep, FAR char *buffer, size_t buflen)
{
  FAR struct inode        *inode = filep->f_inode;
  FAR struct tsic_dev_s   *priv  = inode->i_private;

  if (!buffer)
    {
      snerr("Buffer is null\n");
      return -1;
    }

  if (buflen < sizeof(struct ltc1865_msg_s))
    {
      snerr("You can't read something other than 24 bytes\n");
      return -1;
    }

  /* Get the temperature */

  return ltc1865_capture((struct ltc1865_msg_s*)buffer, priv);

}




/****************************************************************************
 * Name: ltc1865_write
 ****************************************************************************/

static ssize_t ltc1865_write(FAR struct file *filep, FAR const char *buffer, size_t buflen)
{
	return -ENOSYS;
}


/****************************************************************************
 * Name: ltc1865_ioctl
 *
 * Description:
 *
 *
 * Input Parameters:
 *   filep -
 *   cmd - IOCTL command
 *   arg - must be empty
 *
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/


static int ltc1865_ioctl (FAR struct file *filep, int cmd, unsigned long arg)
{

    return -ENOSYS;
}





/****************************************************************************
 * Name: dac_lock
 *
 * Description:
 *   Lock and configure the SPI bus.
 *
 ****************************************************************************/

static void dac_lock(FAR struct spi_dev_s *spi)
{
  (void)SPI_LOCK(spi, true);
  SPI_SETMODE(spi, SPIDEV_MODE0);
  SPI_SETBITS(spi, 16);
  SPI_SETFREQUENCY(spi, CONFIG_LTC1865_SPIFREQUENCY);
}

/****************************************************************************
 * Name: dac_unlock
 *
 * Description:
 *   Unlock the SPI bus.
 *
 ****************************************************************************/

static void dac_unlock(FAR struct spi_dev_s *spi)
{
  (void)SPI_LOCK(spi, false);
}



/****************************************************************************
 * Public Functions
 ****************************************************************************/


/****************************************************************************
 * Name: ltc1865_register
 *
 * Description:
 *   Register the LTC1865 character device as 'devpath'
 *
 * Input Parameters:
 *   devpath - The full path to the driver to register. E.g., "/dev/adc0"
 *   spi - An instance of the SPI interface to use to communicate with
 *     LTC1865
 *   devno - SPI device number
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/

int ltc1865_register(FAR const char *devpath, FAR struct spi_dev_s *spi1,
                      unsigned int devno1, FAR struct spi_dev_s *spi2, unsigned int devno2)
{
  FAR struct ltc1865_dev_s *priv;
  int ret=0;

  /* Sanity check */

  DEBUGASSERT(spi1 != NULL);
  DEBUGASSERT(spi2 != NULL);
  /* Initialize the LTC1865 device structure */

  priv = (FAR struct ltc1865_dev_s *)kmm_malloc(sizeof(struct ltc1865_dev_s));
  if (priv == NULL)
    {
      aerr("ERROR: Failed to allocate ltc1865_dev_s instance\n");
      return -ENOMEM;
    }

  priv->spi1 = spi1;
  priv->devno1 = devno1;
  priv->spi2 = spi2;
  priv->devno2 = devno2;
//  ret = nxsem_init(&priv->sem, 1, 1);
//  if (ret < 0)
//    {
//      kmm_free(dacpriv);
//      return ret;
//    }


#ifdef LTC1865_EXCLUSIVE_SPI
			SPI_LOCK(priv->spi1, true);
			SPI_SETMODE(spi1, SPIDEV_MODE0);
			SPI_SETBITS(spi1, 16);
			SPI_SETFREQUENCY(spi1, CONFIG_LTC1865_SPIFREQUENCY);

                        SPI_LOCK(priv->spi2, true);
                        SPI_SETMODE(spi2, SPIDEV_MODE0);
                        SPI_SETBITS(spi2, 16);
                        SPI_SETFREQUENCY(spi2, CONFIG_LTC1865_SPIFREQUENCY);


#endif
  /* Register the character driver */
  ret = register_driver(devpath, &g_ltc1865fops, 0666, priv);
  if (ret < 0)
    {
      aerr("ERROR: Failed to register adc driver: %d\n", ret);
//      nxsem_destroy(&priv->sem);
      kmm_free(priv);
    }

  return ret;
}


#endif

/****************************************************************************
 * drivers/sensors/smt172.c
 *
 *   Copyright 2020 Leichtwerk Research GmbH
 *   Author: Stefan Nowak
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdlib.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/kmalloc.h>
#include <nuttx/fs/fs.h>
#include <nuttx/sensors/smt172.h>

#if defined(CONFIG_SENSORS_SMT172)

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define SMT172_IGNORE_SAMPLES 1
#define SMT172_IGNORE_ENTRIES (2*SMT172_IGNORE_SAMPLES)

/****************************************************************************
 * Private Type Definitions
 ****************************************************************************/

struct smt172_dev_s
{
    FAR struct smt172_config_s *config; /* Board configuration data */
    struct timespec timestamp;
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* Character driver methods */

static int smt172_open(FAR struct file *filep);
static int smt172_close(FAR struct file *filep);
static ssize_t smt172_read(FAR struct file *filep, FAR char *buffer,
                           size_t buflen);
static int smt172_ioctl(FAR struct file *filep, int cmd, unsigned long arg);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct file_operations g_smt172fops =
{
  smt172_open,                  /* open */
  smt172_close,                 /* close */
  smt172_read,                  /* read */
  0,                            /* write */
  0,                            /* seek */
  smt172_ioctl,                 /* ioctl */
#ifndef CONFIG_DISABLE_POLL
  0,                            /* poll */
#endif
  0                             /* unlink */
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: smt172_initialize
 *
 * Description:
 *   Initialize SMT172 device
 *
 ****************************************************************************/

static int smt172_initialize(FAR struct smt172_dev_s *priv)
{
//    priv->msg_valid = false;
    return OK;
}

/****************************************************************************
 * Name: smt172_update
 *
 * Description:
 *   update buffer ******call with 20Hz *******
 *
 ****************************************************************************/

static int smt172_update(FAR struct smt172_dev_s *priv)
{
    priv->config->update(priv->config);
    clock_gettime(CLOCK_MONOTONIC,&priv->timestamp);
    return OK;
}

/****************************************************************************
 * Name: smt172_readtemperature
 *
 * Description:
 *   get one temperature sample, if available
 *
 ****************************************************************************/
static ssize_t smt172_readtemperature(struct smt172_msg_s *buffer, FAR struct smt172_dev_s *priv)
{

    float dc=0;
    int duty_cnt_offset;
    int period_cnt_offset;


    /* cut SMT172_IGNORE_ENTRIES from begin and end of fifo */
    int entries=(priv->config->fifosize - 2*SMT172_IGNORE_ENTRIES) / 16;

    if (entries<1) return 0;

    entries *=16;

    //printf("samples: %d entries: %d\n",entries/2,entries);

    if(priv->config->channel == 0){
        duty_cnt_offset=1;
        period_cnt_offset=0;
    } else {
        duty_cnt_offset=0;
        period_cnt_offset=1;
    }

    for(int i=SMT172_IGNORE_ENTRIES;i<(entries+SMT172_IGNORE_ENTRIES);i+=2){ /* ignore first sample */
            uint16_t duty_cnt=priv->config->fifo[i+duty_cnt_offset];
            uint16_t period_cnt=priv->config->fifo[i+period_cnt_offset];

            if((duty_cnt> period_cnt) || period_cnt < priv->config->period_cnt_min || period_cnt > priv->config->period_cnt_max){
                    /* error wrong period in sampled date */
                    return -1;
            }

            dc+=(float)(period_cnt-duty_cnt)/(float)(period_cnt);


            //printf("%d: ccr1: %6hu, ccr2: %6hu\n",i/2,priv->config->fifo[i],priv->config->fifo[i+1]);

    }

    dc/=(float)(entries/2);
    float temperature = -1.43*dc*dc+214.56*dc-68.6;
    //printf("T=%6.3fdegC\n",temperature);


    buffer->channel = priv->config->channel+1;
    buffer->time_s  = priv->timestamp.tv_sec;
    buffer->time_ns = priv->timestamp.tv_nsec;
    buffer->value   = temperature;

    return sizeof(struct smt172_msg_s);
}

/****************************************************************************
 * Name: smt172_open
 *
 * Description:
 *   This function is called whenever the SMT172 device is opened.
 *
 ****************************************************************************/

static int smt172_open(FAR struct file *filep)
{
//  FAR struct inode      *inode = filep->f_inode;
//  FAR struct smt172_dev_s *priv  = inode->i_private;

  return OK;
}

/****************************************************************************
 * Name: smt172_close
 *
 * Description:
 *   This routine is called when the SMT172 device is closed.
 *
 ****************************************************************************/

static int smt172_close(FAR struct file *filep)
{
//  FAR struct inode        *inode = filep->f_inode;
//  FAR struct smt172_dev_s   *priv  = inode->i_private;

  return OK;
}

/****************************************************************************
 * Name: smt172_read
 ****************************************************************************/

static ssize_t smt172_read(FAR struct file *filep, FAR char *buffer,
                           size_t buflen)
{
  FAR struct inode        *inode = filep->f_inode;
  FAR struct smt172_dev_s   *priv  = inode->i_private;

  if (!buffer)
    {
      snerr("Buffer is null\n");
      return -1;
    }

  if (buflen < sizeof(struct smt172_msg_s))
    {
      snerr("You can't read something other than 32 bits (4 bytes)\n");
      return -1;
    }

  /* Get the temperature */

  return smt172_readtemperature((struct smt172_msg_s *)buffer, priv);

}

/****************************************************************************
 * Name: smt172_ioctl
 ****************************************************************************/

static int smt172_ioctl(FAR struct file *filep, int cmd, unsigned long arg)
{
  FAR struct inode *inode = filep->f_inode;
  FAR struct smt172_dev_s *priv  = inode->i_private;
  int ret = OK;

  switch (cmd)
    {
      case SNIOC_SMT172_UPDATE:
        ret = smt172_update(priv);
        break;

      default:
        snerr("Unrecognized cmd: %d\n", cmd);
        ret = - ENOTTY;
        break;
    }

  return ret;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: smt172_instantiate
 *
 * Description:
 *   Instantiate an optional register the SMT172 character device as 'devpath'
 *
 * Input Parameters:
 *   devpath - The full path to the driver to register. E.g., "/dev/smt172"
 *   config  - Persistent board configuration data
 *             SMT172
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/

int smt172_instantiate(FAR const char *devpath, FAR struct smt172_config_s *config)
{
  FAR struct smt172_dev_s *priv;
  int ret;

  /* Initialize the SMT172 device structure */

  priv = (FAR struct smt172_dev_s *)kmm_malloc(sizeof(struct smt172_dev_s));
  if (!priv)
    {
      snerr("Failed to allocate instance\n");
      return -ENOMEM;
    }

  priv->config = config;



  ret = smt172_initialize(priv);
  if (ret < 0)
    {
      snerr("Failed to initialize physical device smt172:%d\n", ret);
      kmm_free(priv);
      return ret;
    }

  /* Register the character driver */
  sninfo("register %s\n",devpath);
  ret = register_driver(devpath, &g_smt172fops, 0666, priv);
  if (ret < 0)
    {
      snerr("Failed to register driver: %d\n", ret);
      kmm_free(priv);
    }

  sninfo("SMT172 driver loaded successfully!\n");
  return ret;
}

#endif

/************************************************************************************
 * arch/arm/src/stm32f7/stm32_hptc.h
 *
 *   Copyright (C) 2011, 2015 Gregory Nutt. All rights reserved.
 *   Copyright (C) 2015 Omni Hoverboards Inc. All rights reserved.
 *   Authors: Gregory Nutt <gnutt@nuttx.org>
 *            Paul Alexander Patience <paul-a.patience@polymtl.ca>
 *            Stefan Nowak
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

#ifndef __ARCH_ARM_SRC_STM32F7_STM32_HPTC_H
#define __ARCH_ARM_SRC_STM32F7_STM32_HPTC_H

/* The STM32F7 does not have dedicated HTPC hardware.  Rather, hptc
 * is a capabilitiy of the STM32F7 timers.  The logic in this file implements the
 * lower half of the standard, NuttX HTPC interface using the STM32F7 timers.  That
 * interface is described in include/nuttx/drivers/htpc.h.
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include "chip.h"


#include <arch/board/board.h>
#include "hardware/stm32_tim.h"  //timer registers


/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/
/* Configuration ********************************************************************/
/* Timer devices may be used for different purposes.  One special purpose is
 * the HPTC.  If CONFIG_STM32F7_TIMn is defined then the CONFIG_STM32F7_TIMn_HPTC
 * must not be defined to avoid conflicts
 */

#ifdef CONFIG_STM32F7_TIM1
#  undef CONFIG_STM32F7_TIM1_HPTC
#endif
#ifdef CONFIG_STM32F7_TIM2
#  undef CONFIG_STM32F7_TIM2_HPTC
#endif
#ifdef CONFIG_STM32F7_TIM3
#  undef CONFIG_STM32F7_TIM3_HPTC
#endif
#ifdef CONFIG_STM32F7_TIM4
#  undef CONFIG_STM32F7_TIM4_HPTC
#endif
#ifdef CONFIG_STM32F7_TIM5
#  undef CONFIG_STM32F7_TIM5_HPTC
#endif
#ifdef CONFIG_STM32F7_TIM8
#  undef CONFIG_STM32F7_TIM8_HPTC
#endif
#ifdef CONFIG_STM32F7_TIM9
#  undef CONFIG_STM32F7_TIM9_HPTC
#endif
#ifdef CONFIG_STM32F7_TIM10
#  undef CONFIG_STM32F7_TIM10_HPTC
#endif
#ifdef CONFIG_STM32F7_TIM11
#  undef CONFIG_STM32F7_TIM11_HPTC
#endif
#ifdef CONFIG_STM32F7_TIM12
#  undef CONFIG_STM32F7_TIM12_HPTC
#endif
#ifdef CONFIG_STM32F7_TIM13
#  undef CONFIG_STM32F7_TIM13_HPTC
#endif
#ifdef CONFIG_STM32F7_TIM14
#  undef CONFIG_STM32F7_TIM14_HPTC
#endif
#ifdef CONFIG_STM32F7_TIM15
#  undef CONFIG_STM32F7_TIM15_HPTC
#endif
#ifdef CONFIG_STM32F7_TIM16
#  undef CONFIG_STM32F7_TIM16_HPTC
#endif
#ifdef CONFIG_STM32F7_TIM17
#  undef CONFIG_STM32F7_TIM17_HPTC
#endif

/* The basic timers (timer 6 and 7) are not capable of generating output pulses */

#undef CONFIG_STM32F7_TIM6_HPTC
#undef CONFIG_STM32F7_TIM7_HPTC


#if defined(CONFIG_HPTC)
/* Check if HPTC support for any channel is enabled. */
/*
//#if defined(CONFIG_HPTC) && \
//    (defined(CONFIG_STM32F7_TIM1_HPTC)  || defined(CONFIG_STM32F7_TIM2_HPTC)  || \
//     defined(CONFIG_STM32F7_TIM3_HPTC)  || defined(CONFIG_STM32F7_TIM4_HPTC)  || \
//     defined(CONFIG_STM32F7_TIM5_HPTC)  || defined(CONFIG_STM32F7_TIM6_HPTC)  || \
//     defined(CONFIG_STM32F7_TIM7_HPTC)  || defined(CONFIG_STM32F7_TIM8_HPTC)  || \
//     defined(CONFIG_STM32F7_TIM9_HPTC)  || defined(CONFIG_STM32F7_TIM10_HPTC) || \
//     defined(CONFIG_STM32F7_TIM11_HPTC) || defined(CONFIG_STM32F7_TIM12_HPTC) || \
//     defined(CONFIG_STM32F7_TIM13_HPTC) || defined(CONFIG_STM32F7_TIM14_HPTC))
*/


/************************************************************************************
 * Public Types
 ************************************************************************************/

/************************************************************************************
 * Public Data
 ************************************************************************************/

#ifndef __ASSEMBLY__

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/************************************************************************************
 * Public Functions
 ************************************************************************************/


/****************************************************************************
 * Name: stm32_htpc_initialize
 *
 * Description:
 *   Bind the configuration hptc to a hptc lower half instance and
 *   register the device
 *
 * Returned Values:
 *   Zero (OK) is returned on success; A negated errno value is returned
 *   to indicate the nature of any failure.
 *
 ****************************************************************************/

int stm32_hptc_initialize(void);


/****************************************************************************
 * Name: stm32_clock_get_deviation
 *
 * Description:
 *   provides clock deviation for other low level drivers using a timer
 *
 * Returned Values:
 *   deviation in ppb from nominal clock
 *
 ****************************************************************************/

int32_t stm32_clock_get_deviation(void);



#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* __ASSEMBLY__ */
#endif /* CONFIG_HTPC */
#endif /* __ARCH_ARM_SRC_STM32F7_STM32_HPTC_H */

/*
 * messBUS_led.h
 *
 *  Created on: 28.05.2018
 *      Author: bbrandt
 */

#ifndef NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_DEBUG_H_
#define NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_DEBUG_H_

#include <nuttx/config.h>
#include <nuttx/board.h>

#ifdef CONFIG_MESSBUS_DEBUG


/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/
#define GPIO_MESSBUS_DEBUG_PIN    (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
							GPIO_OUTPUT_CLEAR|GPIO_PORTH|GPIO_PIN11)
#define GPIO_MB_DEBUG_ON	(1 << 11)
#define GPIO_MB_DEBUG_OFF	(1 << (11+16))

#define GPIO_MESSBUS_DEBUG_PIN2    (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
                            GPIO_OUTPUT_CLEAR|GPIO_PORTH|GPIO_PIN12)
#define GPIO_MB_DEBUG_ON2    (1 << 12)
#define GPIO_MB_DEBUG_OFF2   (1 << (12+16))

#define GPIO_MESSBUS_DEBUG_PIN3    (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
                            GPIO_OUTPUT_CLEAR|GPIO_PORTI|GPIO_PIN0)
#define GPIO_MB_DEBUG_ON3    (1 << 0)
#define GPIO_MB_DEBUG_OFF3   (1 << (0+16))

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/
void switchDebugPinOn(void);
void switchDebugPinOff(void);
void toggleDebugPin(void);

void switchDebugPinOn2(void);
void switchDebugPinOff2(void);
void toggleDebugPin2(void);

void switchDebugPinOn3(void);
void switchDebugPinOff3(void);
void toggleDebugPin3(void);

#else

# define switchDebugPinOn()
# define switchDebugPinOff()
# define toggleDebugPin()
# define switchDebugPinOn2()
# define switchDebugPinOff2()
# define toggleDebugPin2()
# define switchDebugPinOn3()
# define switchDebugPinOff3()
# define toggleDebugPin3()

#endif

#endif /* NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_DEBUG_H_ */

/****************************************************************************
 * drivers/sensors/tsic.c
 *
 *   Copyright 2020 Leichtwerk Research GmbH
 *   Author: Stefan Nowak
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdlib.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/kmalloc.h>
#include <nuttx/fs/fs.h>
#include <nuttx/sensors/tsic.h>

#if defined(CONFIG_SENSORS_TSIC)

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define TSIC_DIFF_IDLE1       100
#define TSIC_DIFF_IDLE2         7
#define TSIC_DIFF_BIT_0         5
#define TSIC_DIFF_BIT_1         3
#define TSIC_DIFF_BIT_EQUAL     4
#define TSIC_DIFF_BIT_CLEAR     6
#define TSIC_DIFF_BIT_SET       2

#define TSIC_STATE_ERROR        0
#define TSIC_STATE_IDLE1        1
#define TSIC_STATE_START1       2
#define TSIC_STATE_BYTE1        3
#define TSIC_STATE_PARITY1      4
#define TSIC_STATE_IDLE2        5
#define TSIC_STATE_START2       6
#define TSIC_STATE_BYTE2        7
#define TSIC_STATE_PARITY2      8


/****************************************************************************
 * Private Type Definitions
 ****************************************************************************/

struct tsic_dev_s
{
    FAR struct tsic_config_s *config; /* Board configuration data */
    struct timespec reftime;
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* Character driver methods */

static int tsic_open(FAR struct file *filep);
static int tsic_close(FAR struct file *filep);
static ssize_t tsic_read(FAR struct file *filep, FAR char *buffer,
                           size_t buflen);
static int tsic_ioctl(FAR struct file *filep, int cmd, unsigned long arg);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct file_operations g_tsicfops =
{
  tsic_open,                  /* open */
  tsic_close,                 /* close */
  tsic_read,                  /* read */
  0,                            /* write */
  0,                            /* seek */
  tsic_ioctl,                 /* ioctl */
#ifndef CONFIG_DISABLE_POLL
  0,                            /* poll */
#endif
  0                             /* unlink */
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: tsic_initialize
 *
 * Description:
 *   Initialize TSIC device
 *
 ****************************************************************************/

static int tsic_initialize(FAR struct tsic_dev_s *priv)
{

    for(int i=0; i< CONFIG_TSIC_CHANNELS; i++){
        /* default: TSIC 306 */
        priv->config->ch[i].scale  =  ((TSIC_HT)-(TSIC_LT))/(TSIC_MAXVAL);
        priv->config->ch[i].offset =  TSIC_LT;
    }

    /*enable all channels*/
    priv->config->enable(priv->config, 0);


    return OK;
}

/****************************************************************************
 * Name: tsic_update
 *
 * Description:
 *   update reference counter
 *
 ****************************************************************************/

static int tsic_update(FAR struct tsic_dev_s *priv)
{
    clock_gettime(CLOCK_MONOTONIC,&priv->reftime);
    priv->config->update_refcnt(priv->config);
    return OK;
}



/****************************************************************************
 * Name: tsic_flush
 *
 * Description:
 *   flush buffer
 *
 ****************************************************************************/

static int tsic_flush(FAR struct tsic_dev_s *priv)
{
    for (int i=0; i< CONFIG_TSIC_CHANNELS; i++)
    {
        priv->config->ch[i].readpos=priv->config->writepos(priv->config,i);
    }
    return OK;
}


/****************************************************************************
 * Name: tsic_buildbuffer
 *
 * Description:
 *   build buffer
 *
 ****************************************************************************/
static ssize_t tsic_buildbuffer(struct tsic_msg_s *buffer, FAR struct tsic_dev_s *priv, int channel){


    int16_t cnt=priv->config->ch[channel].first_capture - *(priv->config->ch[channel].refcnt);

    buffer->time_s  = priv->reftime.tv_sec;
    buffer->time_ns = priv->reftime.tv_nsec+TSIC_SAMPLE_PERIOD_NS*(int32_t)cnt;
    if(buffer->time_ns >= NS_PER_SEC){
        buffer->time_ns -= NS_PER_SEC;
        buffer->time_s++;
    }
    else if(buffer->time_ns < 0){
        buffer->time_ns += NS_PER_SEC;
        buffer->time_s--;
    }

    buffer->channel=channel+1;

    //printf("data=0x%08x\n",priv->config->ch[channel].data);

    buffer->value=(float)(priv->config->ch[channel].data) * priv->config->ch[channel].scale + priv->config->ch[channel].offset;


    return sizeof(struct tsic_msg_s);
}





/****************************************************************************
 * Name: tsic_readtemperature
 *
 * Description:
 *   get one temperature sample, if available
 *
 ****************************************************************************/
static ssize_t tsic_readtemperature(struct tsic_msg_s *buffer, FAR struct tsic_dev_s *priv)
{
    /* rotate one time over all channels until a valid temperature was received */
    static int channel=0;
    for(int i=0;i<CONFIG_TSIC_CHANNELS;i++)
    {


         int32_t writepos=priv->config->writepos(priv->config,channel);
         while(writepos != priv->config->ch[channel].readpos){
                 uint16_t capture;
                 uint32_t diff;

                 capture=priv->config->ch[channel].fifo[priv->config->ch[channel].readpos];

                 //printf("w: %d r: %d\n",writepos,priv->config->ch[channel].readpos);


                 priv->config->ch[channel].readpos++;
                 if(priv->config->ch[channel].readpos==priv->config->ch[channel].fifosize)
                 {
                     priv->config->ch[channel].readpos=0;
                 }

                 diff=(capture-priv->config->ch[channel].last_capture);
                 priv->config->ch[channel].last_capture = capture;

                 /* shift 16 bit for better fixedpoint arithmetic accuracy */
                 diff<<=16;
                 /* add 1/8 bit for rounding */
                 diff+=((TSIC_COUNT_PER_BIT)<<16)/8;
                 /* divide by 4 to get length in quater bits */
                 diff/=((TSIC_COUNT_PER_BIT)<<16)/4;

                 //printf("cnt: %hu diff: %hd state:%d - %d\n",capture,diff,priv->config->ch[channel].state,priv->config->ch[channel].bitcnt);

                 switch (priv->config->ch[channel].state){

                     case TSIC_STATE_ERROR:
                         snerr("TSIC decoding error\n");
                         priv->config->ch[channel].state = TSIC_STATE_IDLE1;

                     case TSIC_STATE_IDLE1:
                         if(diff>TSIC_DIFF_IDLE1){
                             priv->config->ch[channel].first_capture=capture;
                             priv->config->ch[channel].state = TSIC_STATE_START1;
                         }
                         break;
                     case TSIC_STATE_START1:
                         if(diff==TSIC_DIFF_BIT_0)
                         {
                             priv->config->ch[channel].data=0;
                             priv->config->ch[channel].parity=0;
                             priv->config->ch[channel].prevbit=0;
                         }
                         else if(diff==TSIC_DIFF_BIT_1)
                         {
                             priv->config->ch[channel].data=1;
                             priv->config->ch[channel].parity=1;
                             priv->config->ch[channel].prevbit=1;
                         }
                         else
                         {
                             priv->config->ch[channel].state = TSIC_STATE_ERROR;
                             break;
                         }
                         priv->config->ch[channel].state  = TSIC_STATE_BYTE1;
                         priv->config->ch[channel].bitcnt = 0;

                         break;
                     case TSIC_STATE_BYTE1:
                         priv->config->ch[channel].data<<=1;
                         priv->config->ch[channel].bitcnt++;


                         if(diff==(priv->config->ch[channel].prevbit ? TSIC_DIFF_BIT_EQUAL : TSIC_DIFF_BIT_SET )){
                             /* one */
                             priv->config->ch[channel].data+=1;
                             priv->config->ch[channel].parity=(priv->config->ch[channel].parity?0:1);
                             priv->config->ch[channel].prevbit=1;
                         }
                         else if( diff==(priv->config->ch[channel].prevbit ?  TSIC_DIFF_BIT_CLEAR : TSIC_DIFF_BIT_EQUAL))
                         {
                             /*zero*/
                             priv->config->ch[channel].prevbit=0;
                         }
                         else
                         {
                             /* error */
                             priv->config->ch[channel].state = TSIC_STATE_ERROR;
                             break;
                         }

                         if(priv->config->ch[channel].bitcnt==7){
                             priv->config->ch[channel].state  = TSIC_STATE_PARITY1;
                         }

                         break;
                     case TSIC_STATE_PARITY1:
                         //printf("data:%08x\n",priv->config->ch[channel].data);
                         if(diff==(priv->config->ch[channel].prevbit ? TSIC_DIFF_BIT_EQUAL : TSIC_DIFF_BIT_SET ))
                         {
                             /* one */
                             priv->config->ch[channel].parity=(priv->config->ch[channel].parity?0:1);
                         }
                         else if( diff!=(priv->config->ch[channel].prevbit ?  TSIC_DIFF_BIT_CLEAR : TSIC_DIFF_BIT_EQUAL))
                         {
                             /* error */
                             priv->config->ch[channel].state = TSIC_STATE_ERROR;
                             break;
                         }
                             /* check parity */
                         if(priv->config->ch[channel].parity){
                             priv->config->ch[channel].state  = TSIC_STATE_ERROR;
                         }
                         else
                         {
                             priv->config->ch[channel].state  = TSIC_STATE_IDLE2;
                         }
                         break;

                     case TSIC_STATE_IDLE2:
                         if(/*(diff>=TSIC_DIFF_IDLE2) && */(diff < TSIC_DIFF_IDLE1)){
                             priv->config->ch[channel].state = TSIC_STATE_START2;
                         }
                         else
                         {
                             priv->config->ch[channel].state  = TSIC_STATE_ERROR;
                         }
                         break;

                     case TSIC_STATE_START2:
                         priv->config->ch[channel].data<<=1;
                         if(diff==TSIC_DIFF_BIT_0)
                         {

                             priv->config->ch[channel].parity=0;
                             priv->config->ch[channel].prevbit=0;
                         }
                         else if(diff==TSIC_DIFF_BIT_1)
                         {
                             priv->config->ch[channel].data+=1;
                             priv->config->ch[channel].parity=1;
                             priv->config->ch[channel].prevbit=1;
                         }
                         else
                         {
                             priv->config->ch[channel].state = TSIC_STATE_ERROR;
                             break;
                         }
                         priv->config->ch[channel].state  = TSIC_STATE_BYTE2;
                         priv->config->ch[channel].bitcnt = 0;

                         break;

                     case TSIC_STATE_BYTE2:
                         priv->config->ch[channel].data<<=1;
                         priv->config->ch[channel].bitcnt++;


                         if(diff==(priv->config->ch[channel].prevbit ? TSIC_DIFF_BIT_EQUAL : TSIC_DIFF_BIT_SET )){
                             /* one */
                             priv->config->ch[channel].data+=1;
                             priv->config->ch[channel].parity=(priv->config->ch[channel].parity?0:1);
                             priv->config->ch[channel].prevbit=1;
                         }
                         else if( diff==(priv->config->ch[channel].prevbit ?  TSIC_DIFF_BIT_CLEAR : TSIC_DIFF_BIT_EQUAL))
                         {
                             /*zero*/
                             priv->config->ch[channel].prevbit=0;
                         }
                         else
                         {
                             /* error */
                             priv->config->ch[channel].state = TSIC_STATE_ERROR;
                             break;
                         }

                         if(priv->config->ch[channel].bitcnt==7){
                             priv->config->ch[channel].state  = TSIC_STATE_PARITY2;
                         }

                         break;
                     case TSIC_STATE_PARITY2:
                         if(diff==(priv->config->ch[channel].prevbit ? TSIC_DIFF_BIT_EQUAL : TSIC_DIFF_BIT_SET ))
                         {
                             /* one */
                             priv->config->ch[channel].parity=(priv->config->ch[channel].parity?0:1);
                         }
                         else if( diff!=(priv->config->ch[channel].prevbit ?  TSIC_DIFF_BIT_CLEAR : TSIC_DIFF_BIT_EQUAL))
                         {
                             /* error */
                             priv->config->ch[channel].state = TSIC_STATE_ERROR;
                             break;
                         }
                             /* check parity */
                         if(priv->config->ch[channel].parity){
                             priv->config->ch[channel].state  = TSIC_STATE_ERROR;
                         }
                         else
                         {
                             /* valid sample */
                             priv->config->ch[channel].state  = TSIC_STATE_IDLE1;

                             ssize_t ret=tsic_buildbuffer(buffer, priv, channel);

                             channel++;
                             if(channel==CONFIG_TSIC_CHANNELS)channel=0;

                             return ret;
                         }
                         break;

                 }
         }

         channel++;
         if(channel==CONFIG_TSIC_CHANNELS)channel=0;

    }

  return 0;
}

/****************************************************************************
 * Name: tsic_open
 *
 * Description:
 *   This function is called whenever the TSIC device is opened.
 *
 ****************************************************************************/

static int tsic_open(FAR struct file *filep)
{
//  FAR struct inode      *inode = filep->f_inode;
//  FAR struct tsic_dev_s *priv  = inode->i_private;

  return OK;
}

/****************************************************************************
 * Name: tsic_close
 *
 * Description:
 *   This routine is called when the TSIC device is closed.
 *
 ****************************************************************************/

static int tsic_close(FAR struct file *filep)
{
//  FAR struct inode        *inode = filep->f_inode;
//  FAR struct tsic_dev_s   *priv  = inode->i_private;

  return OK;
}

/****************************************************************************
 * Name: tsic_read
 ****************************************************************************/

static ssize_t tsic_read(FAR struct file *filep, FAR char *buffer,
                           size_t buflen)
{
  FAR struct inode        *inode = filep->f_inode;
  FAR struct tsic_dev_s   *priv  = inode->i_private;
//  FAR uint32_t            *press = (FAR uint32_t *) buffer;
//  int ret;

  if (!buffer)
    {
      snerr("Buffer is null\n");
      return -1;
    }

  if (buflen < sizeof(struct tsic_msg_s))
    {
      snerr("You can't read something other than 32 bits (4 bytes)\n");
      return -1;
    }

  /* Get the temperature */

  return tsic_readtemperature((struct tsic_msg_s*)buffer, priv);

}

/****************************************************************************
 * Name: tsic_ioctl
 ****************************************************************************/

static int tsic_ioctl(FAR struct file *filep, int cmd, unsigned long arg)
{
  FAR struct inode *inode = filep->f_inode;
  FAR struct tsic_dev_s *priv  = inode->i_private;
  int ret = OK;

  switch (cmd)
    {
#if 0
      case SNIOC_TSIC_ENABLE:
        priv->enable = (int)arg;
        break;
#endif
      case SNIOC_TSIC_FLUSH:
        ret = tsic_flush(priv);
        break;

      case SNIOC_TSIC_UPDATE:
        ret = tsic_update(priv);
        break;

      default:
        snerr("Unrecognized cmd: %d\n", cmd);
        ret = - ENOTTY;
        break;
    }

  return ret;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: tsic_instantiate
 *
 * Description:
 *   Instantiate an optional register the TSIC character device as 'devpath'
 *
 * Input Parameters:
 *   devpath - The full path to the driver to register. E.g., "/dev/tsic"
 *   config  - Persistent board configuration data
 *             TSIC
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/

int tsic_instantiate(FAR const char *devpath, FAR struct tsic_config_s *config)
{
  FAR struct tsic_dev_s *priv;
  int ret;

  /* Initialize the TSIC device structure */

  priv = (FAR struct tsic_dev_s *)kmm_malloc(sizeof(struct tsic_dev_s));
  if (!priv)
    {
      snerr("Failed to allocate instance\n");
      return -ENOMEM;
    }

  priv->config = config;

  ret = tsic_initialize(priv);
  if (ret < 0)
    {
      snerr("Failed to initialize physical device tsic:%d\n", ret);
      kmm_free(priv);
      return ret;
    }

#ifdef CONFIG_TSIC_REGISTER_DEVICE
  /* Register the character driver */
  sninfo("register %s\n",devpath);
  ret = register_driver(devpath, &g_tsicfops, 0666, priv);
  if (ret < 0)
    {
      snerr("Failed to register driver: %d\n", ret);
      kmm_free(priv);
    }
#endif
  sninfo("TSIC driver loaded successfully!\n");
  return ret;
}

#endif

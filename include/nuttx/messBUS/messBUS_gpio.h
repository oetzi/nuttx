/*
 * messBUS_gpio.h
 *
 *  Created on: 27.04.2020
 */

#ifndef NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_GPIO_H_
#define NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_GPIO_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include <stdint.h>
#include <stdbool.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/************************************************************************************
 * Public Data
 ************************************************************************************/

#ifndef __ASSEMBLY__

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/************************************************************************************
 * Public Function Prototypes
 ************************************************************************************/

/************************************************************************************
 * Name: messBUS_gpiowrite
 *
 * Description:
 *   Write one or zero to the selected GPIO pin.
 *
 *   NOTE: This is a dirty shortcut to allow access to GPIO Pin from user code.
 *         This should be used for test purposes only, when writing a driver is too
 *         much overhead.
 *
 ************************************************************************************/

void messBUS_gpiowrite(uint32_t pinset, bool value);

/************************************************************************************
 * Name: messBUS_gpioread
 *
 * Description:
 *   Read one or zero from the selected GPIO pin
 *
 *   NOTE: This is a dirty shortcut to allow access to GPIO Pin from user code.
 *         This should be used for test purposes only, when writing a driver is too
 *         much overhead.
 *
 ************************************************************************************/

bool messBUS_gpioread(uint32_t pinset);

#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* __ASSEMBLY__ */



#endif /* NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_GPIO_H_ */

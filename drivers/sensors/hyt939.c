/****************************************************************************
 * drivers/sensors/hyt939.c
 * Character driver for HYT939 Humidity/Temperature modul
 *
 *   Copyright (C) 2015 Omni Hoverboards Inc. All rights reserved.
 *   Author: Paul Alexander Patience <paul-a.patience@polymtl.ca>
 *   Updated by: Karim Keddam <karim.keddam@polymtl.ca>
 *
 *   Copyright (C) 2016 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <errno.h>
#include <debug.h>
#include <stdlib.h>
#include <stdbool.h>

#include <nuttx/kmalloc.h>
#include <nuttx/fs/fs.h>
#include <nuttx/arch.h>
#include <nuttx/i2c/i2c_master.h>
#include <nuttx/sensors/hyt939.h>

#if defined(CONFIG_I2C) && defined(CONFIG_SENSORS_HYT939)

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#ifndef CONFIG_HYT939_I2C_FREQUENCY
#  define CONFIG_HYT939_I2C_FREQUENCY I2C_SPEED_STANDARD  /* 100 kHz */
#endif

#define HYT939_TRIGGER_CMD            0x80

#ifndef OK
#  define OK 0
#endif

/****************************************************************************
 * Private Types
 ****************************************************************************/

struct hyt939_dev_s
{
  FAR struct i2c_master_s *i2c;     /* I2C interface */
  uint8_t                 addr;     /* I2C address */

  bool cmode;                       /* If true, element is in command mode */
  bool stalled;                     /* If true, no new value has been created
                                     * since the last reading
                                     */

  /* Measurement */

  struct hyt939_measure_s measure;
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/
/* I2C Helpers */

static int hyt939_i2c_write(FAR struct hyt939_dev_s *priv,
                            FAR const uint8_t *buffer, int buflen);
static int hyt939_i2c_read(FAR struct hyt939_dev_s *priv,
                           FAR uint8_t *buffer, int buflen);
static int hyt939_fetchData(FAR struct hyt939_dev_s *priv,
                            FAR uint16_t* raw_temp,
                            FAR uint16_t* raw_humidity);
static int hyt939_startMeasurementRequest(FAR struct hyt939_dev_s *priv);
static int hyt939_measure(FAR struct hyt939_dev_s *priv);
static float hyt939_convertTemp(FAR struct hyt939_dev_s *priv, uint16_t raw_temp);
static float hyt939_convertHumidity(FAR struct hyt939_dev_s *priv, uint16_t raw_humidity);

/* Character Driver Methods */

static int     hyt939_open(FAR struct file *filep);
static int     hyt939_close(FAR struct file *filep);
static ssize_t hyt939_read(FAR struct file *filep, FAR char *buffer,
                           size_t buflen);
static ssize_t hyt939_write(FAR struct file *filep, FAR const char *buffer,
                            size_t buflen);
static int     hyt939_ioctl(FAR struct file *filep, int cmd,
                            unsigned long arg);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct file_operations g_fops =
{
  hyt939_open,
  hyt939_close,
  hyt939_read,
  hyt939_write,
  NULL,
  hyt939_ioctl,
  NULL
#ifndef CONFIG_DISABLE_PSEUDOFS_OPERATIONS
  , NULL
#endif
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: hyt939_i2c_write
 *
 * Description:
 *   Write to the I2C device.
 *
 ****************************************************************************/

static int hyt939_i2c_write(FAR struct hyt939_dev_s *priv,
                             FAR const uint8_t *buffer, int buflen)
{
  struct i2c_msg_s msg;
  int ret;

  /* Setup for the transfer */

  msg.frequency = CONFIG_HYT939_I2C_FREQUENCY,
  msg.addr      = priv->addr;
  msg.flags     = 0;
  msg.buffer    = (FAR uint8_t *)buffer;  /* Override const */
  msg.length    = buflen;

  /* Then perform the transfer. */

  ret = I2C_TRANSFER(priv->i2c, &msg, 1);
  return (ret >= 0) ? OK : ret;
}

/****************************************************************************
 * Name: hyt939_i2c_read
 *
 * Description:
 *   Read from the I2C device.
 *
 ****************************************************************************/

static int hyt939_i2c_read(FAR struct hyt939_dev_s *priv,
                            FAR uint8_t *buffer, int buflen)
{
  struct i2c_msg_s msg;
  int ret;

  /* Setup for the transfer */

  msg.frequency = CONFIG_HYT939_I2C_FREQUENCY,
  msg.addr      = priv->addr,
  msg.flags     = I2C_M_READ;
  msg.buffer    = buffer;
  msg.length    = buflen;

  /* Then perform the transfer. */

  ret = I2C_TRANSFER(priv->i2c, &msg, 1);
  return (ret >= 0) ? OK : ret;
}

/****************************************************************************
 * Name: hyt939_startMeasurementRequest
 *
 * Description:
 *   By a measurement request command, the sleep mode is terminated and the
 *   humidity module executes a measurement cycle. The measuring cycle begins
 *   with the temperature measurement, followed by humidity measurement,
 *   digital signal processing (linearizing, temperature compensation) and
 *   finally writes the processed measured values into the output register.
 *
 ****************************************************************************/

static int hyt939_startMeasurementRequest(FAR struct hyt939_dev_s *priv)
{
  uint8_t regaddr;
  int ret;

  regaddr = HYT939_TRIGGER_CMD;
  sninfo("addr: %02x\n", regaddr);

  /* Write the register address */

  ret = hyt939_i2c_write(priv, &regaddr, sizeof(regaddr));
  if (ret < 0)
    {
      snerr("ERROR: i2c_write failed: %d\n", ret);
      return ret;
    }

  /* Update Timestamp */

  clock_gettime(CLOCK_MONOTONIC, &priv->measure.timeStamp);

  return ret;
}

/****************************************************************************
 * Name: hyt939_convertHumidity
 *
 * Description:
 *       Converts the raw sensor data to a humidity in percent.
 *
 ****************************************************************************/

static float hyt939_convertHumidity(FAR struct hyt939_dev_s *priv,
                                    uint16_t raw_humidity)
{
    return (raw_humidity * 100.0f) / 16383;;
}

/****************************************************************************
 * Name: hyt939_convertTemp
 *
 * Description:
 *       Converts the raw sensor data to a temperature value in degree
 *       celsius.
 *
 ****************************************************************************/

static float hyt939_convertTemp(FAR struct hyt939_dev_s *priv, uint16_t raw_temp)
{
    return (raw_temp * 165.0f) / 16383 - 40;
}

/****************************************************************************
 * Name: hyt939_fetchData
 *
 * Description:
 *
 ****************************************************************************/

static int hyt939_fetchData(FAR struct hyt939_dev_s *priv, FAR uint16_t* raw_temp,
                          FAR uint16_t* raw_humidity)
{
  int ret = 0;
  uint8_t buffer[4];

  /* Restart and read 32 bits from the register */

  ret = hyt939_i2c_read(priv, buffer, sizeof(buffer));
  if (ret < 0)
    {
      snerr("ERROR: i2c_read failed: %d\n", ret);
      return ret;
    }

  /* Read measurement data */

  *raw_humidity = ((buffer[0] & 0x3F) << 8) + (buffer[1] & 0xFF);
  *raw_temp     = (((buffer[2] & 0xFF) << 8) + (buffer[3] & 0xFC)) >> 2;

  /* Read status bits */

  priv->cmode   = buffer[0] & 0x80;
  priv->stalled = buffer[0] & 0x40;

  sninfo("cdc: %06x %06x ret: %d\n", *raw_temp, *raw_humidity, ret);
  return ret;
}

/****************************************************************************
 * Name: hyt939_measure
 *
 * Description:
 *   Measure the temperature.
 *
 ****************************************************************************/

static int hyt939_measure(FAR struct hyt939_dev_s *priv)
{
  int ret;

  /* Read the value from the ADC */

  uint16_t raw_temp;        /* 16 bit raw temp value */
  uint16_t raw_humidity;    /* 16 bit raw humidity value */

  ret = hyt939_fetchData(priv, &raw_temp, &raw_humidity);
  if (ret < 0)
    {
      snerr("ERROR: hyt939_readadc failed: %d\n", ret);
      return ret;
    }

  /* Convert the raw value to a valid floating value */

  priv->measure.temperature = hyt939_convertTemp(priv, raw_temp);
  priv->measure.humidity    = hyt939_convertHumidity(priv, raw_humidity);

  return OK;
}

/****************************************************************************
 * Name: hyt939_open
 *
 * Description:
 *   This method is called when the device is opened.
 *
 ****************************************************************************/

static int hyt939_open(FAR struct file *filep)
{
  return OK;
}

/****************************************************************************
 * Name: hyt939_close
 *
 * Description:
 *   This method is called when the device is closed.
 *
 ****************************************************************************/

static int hyt939_close(FAR struct file *filep)
{
  return OK;
}

/****************************************************************************
 * Name: hyt939_read
 *
 * Description:
 *   A dummy read method.
 *
 ****************************************************************************/

static ssize_t hyt939_read(FAR struct file *filep, FAR char *buffer,
                           size_t buflen)
{
  FAR struct inode *inode        = filep->f_inode;
  FAR struct hyt939_dev_s *priv  = inode->i_private;
  FAR struct hyt939_measure_s *p = (FAR struct hyt939_measure_s *)buffer;

  if (buflen >= sizeof(*p))
    {
      if (hyt939_measure(priv) < 0)
        {
          return -1;
        }

      *p = priv->measure;
    }

  return buflen;
}

/****************************************************************************
 * Name: hyt939_write
 *
 * Description:
 *   A dummy write method.
 *
 ****************************************************************************/

static ssize_t hyt939_write(FAR struct file *filep, FAR const char *buffer,
                            size_t buflen)
{
  return -ENOSYS;
}

/****************************************************************************
 * Name: hyt939_ioctl
 *
 * Description:
 *   The standard ioctl method.
 *
 ****************************************************************************/

static int hyt939_ioctl(FAR struct file *filep, int cmd, unsigned long arg)
{
  FAR struct inode        *inode = filep->f_inode;
  FAR struct hyt939_dev_s *priv  = inode->i_private;
  int                      ret   = OK;

  /* Handle ioctl commands */

  switch (cmd)
    {
      /* Starts the conversion sequence. Arg: None. */

      case SNIOC_START_CONVERSION:
        ret = hyt939_startMeasurementRequest(priv);
        break;

      /* Measure the temperature and the pressure. Arg: None. */

      case SNIOC_MEASURE:
        ret = hyt939_measure(priv);
        break;

      /* Return the temperature last measured. Arg: float* pointer. */

      case SNIOC_TEMPERATURE:
        {
          FAR float *ptr = (FAR float *)((uintptr_t)arg);
          DEBUGASSERT(ptr != NULL);
          *ptr = priv->measure.temperature;
          sninfo("temp: %3.3f\n", *ptr);
        }
        break;

      /* Unrecognized commands */

      default:
        snerr("ERROR: Unrecognized cmd: %d arg: %ld\n", cmd, arg);
        ret = -ENOTTY;
        break;
    }

  return ret;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: hyt939_register
 *
 * Description:
 *   Register the HYT939 character device as 'devpath'.
 *
 * Input Parameters:
 *   devpath - The full path to the driver to register, e.g., "/dev/press0".
 *   i2c     - An I2C driver instance.
 *   addr    - The I2C address of the HYT939.
 *   osr     - The oversampling ratio.
 *   model   - The HYT939 model.
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/

int hyt939_register(FAR const char *devpath, FAR struct i2c_master_s *i2c,
                    uint8_t addr)
{
  FAR struct hyt939_dev_s *priv;
  int ret;

  /* Sanity check */

  DEBUGASSERT(i2c != NULL);
  DEBUGASSERT(addr == HYT939_ADDR0);

  /* Initialize the device's structure */

  priv = (FAR struct hyt939_dev_s *)kmm_malloc(sizeof(*priv));
  if (priv == NULL)
    {
      snerr("ERROR: Failed to allocate instance\n");
      return -ENOMEM;
    }

  priv->i2c     = i2c;
  priv->addr    = addr;
  priv->stalled = false;
  priv->cmode   = false;

  priv->measure.temperature       = 0;
  priv->measure.humidity          = 0;
  priv->measure.timeStamp.tv_sec  = 0;
  priv->measure.timeStamp.tv_nsec = 0;

  /* Already trigger initial measurement */

  ret = hyt939_startMeasurementRequest(priv);
  if (ret < 0)
    {
      snerr("ERROR: startMeasurementRequest failed: %d\n", ret);
      goto errout;
    }

  /* Register the character driver */

  ret = register_driver(devpath, &g_fops, 0666, priv);
  if (ret < 0)
    {
      snerr("ERROR: Failed to register driver: %d\n", ret);
      goto errout;
    }

  return ret;

errout:
  kmm_free(priv);
  return ret;
}

#endif /* CONFIG_I2C && CONFIG_SENSORS_HYT939 */

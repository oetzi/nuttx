/****************************************************************************
 * config/stm32f4_mB_AD24/src/stm32_bringup.c
 *
 *   Copyright (C) 2012, 2014-2016 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <nuttx/board.h>
#include <nuttx/leds/userled.h>

#include <sys/mount.h>
#include <stdbool.h>
#include <stdio.h>
#include <debug.h>
#include <errno.h>

#ifdef CONFIG_MESSBUS_CLIENT
#include "stm32_messBUSClient.h"
#endif

#ifdef CONFIG_MTD
#include <nuttx/mtd/mtd.h>
#endif

#include "stm32f4_mB_AD24.h"
#include "stm32.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: stm32_bringup
 *
 * Description:
 *   Perform architecture-specific initialization
 *
 *   CONFIG_BOARD_INITIALIZE=y :
 *     Called from board_initialize().
 *
 *   CONFIG_BOARD_INITIALIZE=n && CONFIG_LIB_BOARDCTL=y :
 *     Called from the NSH library
 *
 ****************************************************************************/

int stm32_bringup(void)
{
  int ret = OK;

#if defined(CONFIG_MTD) && defined(HAVE_PROGMEM_CHARDEV)
  FAR struct mtd_dev_s *mtd;
#endif

#if !defined(CONFIG_ARCH_LEDS) && defined(CONFIG_USERLED_LOWER)
  /* Register the LED driver */
    userled_lower_initialize(LED_DRIVER_PATH);
/* seen in nucleo-144
  ret = userled_lower_initialize(LED_DRIVER_PATH);
  if (ret < 0)
    {
      syslog(LOG_ERR, "ERROR: userled_lower_initialize() failed: %d\n", ret);
    }
*/
#endif

#if defined(CONFIG_MTD) && defined (HAVE_PROGMEM_CHARDEV)

  /* Create an instance of the STM32F7 FLASH program memory device driver
   * arch/arm/src/stm32f7/stm32_flash.c
   */

  mtd = progmem_initialize();
  if (!mtd)
  {
      syslog(LOG_ERR, "ERROR: MTD Driver Initializiation failed\n");
  }
  else
  {
      syslog(LOG_INFO, "Successfully initialized FLASH MTD driver\n");

      /* prepare flash sectors for use with smartfs (mount/umount/etc.) */
      if (smart_initialize(0, mtd, NULL) < 0)
      {
          syslog(LOG_ERR, "Error: Smart driver init failed\n");
      }
      else
      {
          syslog(LOG_INFO, "Successfully initialized smart driver\n");
      }
  }
#endif

#ifdef CONFIG_MESSBUS_CLIENT
    messBUSClient_initialize();
#endif

#ifdef CONFIG_ARCH_HAVE_LEDS
    board_userled_initialize();
#endif

#ifdef CONFIG_HPTC
  /* Initialize and register the HPTC device. */
  ret = stm32_hptc_setup();
  if (ret < 0)
    {
      syslog(LOG_ERR, "ERROR: stm32_hptc_setup() failed: %d\n", ret);
    }
#endif

/* AD5662 initalization */
#ifdef CONFIG_DAC_AD5662
  ret = stm32_ad5662_initialize("/dev/temp0");
#endif

/* AD7190 initalization */
#if defined(CONFIG_SPI) && defined(CONFIG_ADC_AD7190)
  stm32_ad7190_initialize("/dev/AD7190",CONFIG_AD7190_BUS, CONFIG_AD7190_NUMBEROFDEVICES);
#endif

  return ret;
}

/************************************************************************************
 * configs/mB_IAcc/src/stm32_boot.c
 *
 *   Copyright (C) 2016 Gregory Nutt. All rights reserved.
 *   Authors: Gregory Nutt <gnutt@nuttx.org>
 *            David Sidrane <david_s5@nscdg.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <debug.h>

#include <nuttx/board.h>
#include <arch/board/board.h>

#include "up_arch.h"
#include "mB_IAcc.h"

#ifdef CONFIG_MESSBUS_CLIENT
#include "stm32_messBUSClient.h"
#endif

#ifdef CONFIG_MTD
#include <nuttx/mtd/mtd.h>
#endif

#ifdef CONFIG_MESSBUS_DEBUG
#include <nuttx/messBUS/messBUS_Debug.h>
#endif

/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_boardinitialize
 *
 * Description:
 *   All STM32 architectures must provide the following entry point.  This entry point
 *   is called early in the initialization -- after all memory has been configured
 *   and mapped but before any devices have been initialized.
 *
 ************************************************************************************/

void stm32_boardinitialize(void)
{
#ifdef CONFIG_ARCH_LEDS
  /* Configure on-board LEDs if LED support has been selected. */

  board_autoled_initialize();
#endif

#ifdef CONFIG_MESSBUS_CLIENT
	stm32_configgpio(GPIO_mB_CH1_RE); //disable RE on messBUS
	stm32_configgpio(GPIO_mB_CH1_DE); //disable DE on messBUS
#endif

#if defined(CONFIG_SPI)
  /* Configure SPI chip selects */

  stm32_spidev_initialize();
#endif

#ifdef CONFIG_MESSBUS_USERLED
	board_userled_initialize();
#endif
}

/************************************************************************************
 * Name: board_late_initialize
 *
 * Description:
 *   If CONFIG_BOARD_LATE_INITIALIZE is selected, then an additional initialization call
 *   will be performed in the boot-up sequence to a function called
 *   board_late_initialize().  board_late_initialize() will be called immediately after
 *   up_initialize() is called and just before the initial application is started.
 *   This additional initialization phase may be used, for example, to initialize
 *   board-specific device drivers.
 *
 ************************************************************************************/

#ifdef CONFIG_BOARD_LATE_INITIALIZE
void board_late_initialize(void)
{
	int ret = 0;

#if defined(CONFIG_MTD) && defined(HAVE_PROGMEM_CHARDEV)
	FAR struct mtd_dev_s *mtd;
#endif

#ifdef CONFIG_HPTC

	/* Initialize and register the HPTC device. */

	ret = stm32_hptc_setup();
	if (ret < 0)
	{
		syslog(LOG_ERR, "ERROR: stm32_hptc_setup() failed: %d\n", ret);
	}

#endif

#if defined(CONFIG_MTD) && defined (HAVE_PROGMEM_CHARDEV)

	/* Create an instance of the STM32F7 FLASH program memory device driver
	 * arch/arm/src/stm32f7/stm32_flash.c
	 */

	mtd = progmem_initialize();
	if (!mtd)
	{
		syslog(LOG_ERR, "ERROR: MTD Driver Initializiation failed\n");
	}
	else
	{
		syslog(LOG_INFO, "Successfully initialized FLASH MTD driver\n");

		/* prepare flash sectors for use with smartfs (mount/umount/etc.) */
		ret = smart_initialize(0, mtd, NULL);
		if (ret < 0)
		{
			syslog(LOG_ERR, "Error: Smart driver init failed\n");
		}
		else
		{
			syslog(LOG_INFO, "Successfully initialized smart driver\n");
		}
	}
#endif

#ifdef CONFIG_MESSBUS_DEBUG

	int ret2 = stm32_configgpio(GPIO_MESSBUS_DEBUG_PIN);
	if(ret2 == OK) printf("init debug pin done\n");
	else printf("Debug pin already in use\n");

#endif

#ifdef CONFIG_MESSBUS_CLIENT
	messBUSClient_initialize();
#endif

#if defined(CONFIG_NSH_LIBRARY) && !defined(CONFIG_LIB_BOARDCTL)

	/* Perform NSH initialization here instead of from the NSH.  This
	 * alternative NSH initialization is necessary when NSH is ran in user-space
	 * but the initialization function must run in kernel space.
	 */

	(void)board_app_initialize();

#endif

	/* Perform board-specific initialization */

//	(void)stm32_bringup();
}
#endif

/****************************************************************************
 * config/mB_I2C/src/stm32_bringup.c
 *
 *   Copyright (C) 2012, 2014-2016 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <nuttx/board.h>
#include <nuttx/leds/userled.h>

#include <sys/mount.h>
#include <stdbool.h>
#include <stdio.h>
#include <debug.h>
#include <errno.h>

#ifdef CONFIG_MESSBUS_CLIENT
# include "stm32_messBUSClient.h"
#endif

#ifdef CONFIG_MTD
# include <nuttx/mtd/mtd.h>
#endif

#if defined(CONFIG_SENSORS_TSYS01)
# include <nuttx/sensors/tsys01.h>
#endif

#if defined(CONFIG_SENSORS_CDC)
# include <nuttx/sensors/cdc.h>
#endif

#if defined(CONFIG_SENSORS_HYT939)
# include <nuttx/sensors/hyt939.h>
#endif

#if defined(CONFIG_SENSORS_SFM3000)
# include <nuttx/sensors/sfm3000.h>
#endif

#if defined(CONFIG_SENSORS_SFM4100)
# include <nuttx/sensors/sfm4100.h>
#endif

#include "mB_I2C.h"
#include "stm32.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: stm32_bringup
 *
 * Description:
 *   Perform architecture-specific initialization
 *
 *   CONFIG_BOARD_INITIALIZE=y :
 *     Called from board_initialize().
 *
 *   CONFIG_BOARD_INITIALIZE=n && CONFIG_LIB_BOARDCTL=y :
 *     Called from the NSH library
 *
 ****************************************************************************/

int stm32_bringup(void)
{
  int ret = OK;

#if defined(CONFIG_I2C)
# if defined(CONFIG_SENSORS_SFM4100)
  /* This is a dirty workaround for the SFM4100 mass flow sensor.
   *
   * This sensor needs the I2C lines to be low when powering up.
   * In general these lines a pulled up by external resistors.
   * So we are going to force these lines temporally down giving the
   * sensor some time to come up. Afterwards we revert the pin
   * configuration before invoking stm32_i2cbus_initialize().
   * This function will finally configure both pins as opendrain
   * I2C pins.
   */

  stm32_configgpio(GPIO_SCL);
  stm32_configgpio(GPIO_SDA);
  stm32_gpiowrite(GPIO_SCL, false);
  stm32_gpiowrite(GPIO_SDA, false);
  usleep(500 * 1000);
  stm32_unconfiggpio(GPIO_SCL);
  stm32_unconfiggpio(GPIO_SDA);
#endif

# if defined(CONFIG_SENSORS_AMS5812) || defined(CONFIG_SENSORS_TSYS01) \
   || defined(CONFIG_SENSORS_CDC)    || defined(CONFIG_SENSORS_HYT939) \
   || defined(CONFIG_SENSORS_SFM4100)

  g_i2c = stm32_i2cbus_initialize(MB_I2C_AMS5812_BUS);
  if (g_i2c == NULL)
    {
      syslog(LOG_ERR, "ERROR: Failed to get I2C%d interface\n", MB_I2C_AMS5812_BUS);
    }
# endif

  usleep(100);

# if defined(CONFIG_SENSORS_TSYS01)

  /* Register the TSYS01 temperature sensor */

  ret = tsys01_register("/dev/tsys01", g_i2c, TSYS01_ADDR);
  if (ret < 0)
    {
      snerr("ERROR: Error registering TSYS01\n");
    }
# endif

# if defined(CONFIG_SENSORS_CDC)

  /* Register the cdc */

  ret = cdc_register("/dev/cdc0", g_i2c, CDC_ADDR_DEFAULT);
  if (ret < 0)
    {
      snerr("ERROR: Error registering CDC\n");
    }
# endif

# if defined(CONFIG_SENSORS_HYT939)

  /* Register the HYT939 temperature sensor */

  ret = hyt939_register("/dev/hyt939", g_i2c, HYT939_ADDR);
  if (ret < 0)
    {
      snerr("ERROR: Error registering HYT939\n");
    }
# endif

  usleep(500);

# if defined(CONFIG_SENSORS_SFM3000)

  /* Register the SFM3000 temperature sensor */

  ret = sfm3000_register("/dev/sfm3000", g_i2c, SFM3000_ADDR0);
  if (ret < 0)
    {
      snerr("ERROR: Error registering SFM3000\n");
    }
# endif

# if defined(CONFIG_SENSORS_SFM4100)

  /* Register the SFM4100 temperature sensor */

  ret = sfm4100_register("/dev/sfm4100", g_i2c, SFM4100_ADDR0);
  if (ret < 0)
    {
      snerr("ERROR: Error registering SFM4100\n");
    }

  sleep(3);
# endif
#endif

#if defined(CONFIG_MTD) && defined(HAVE_PROGMEM_CHARDEV)
  FAR struct mtd_dev_s *mtd;

  /* Create an instance of the STM32F4 FLASH program memory device driver
   * arch/arm/src/stm32f7/stm32_flash.c
   */

  mtd = progmem_initialize();
  if (!mtd)
  {
      syslog(LOG_ERR, "ERROR: MTD Driver Initializiation failed\n");
  }
  else
  {
      syslog(LOG_INFO, "Successfully initialized FLASH MTD driver\n");

      /* prepare flash sectors for use with smartfs (mount/umount/etc.) */
      if (smart_initialize(0, mtd, NULL) < 0)
      {
          syslog(LOG_ERR, "Error: Smart driver init failed\n");
      }
      else
      {
          syslog(LOG_INFO, "Successfully initialized smart driver\n");
      }
  }
#endif

#ifdef CONFIG_MESSBUS_CLIENT
    messBUSClient_initialize();
#endif

#ifdef CONFIG_ARCH_HAVE_LEDS
    board_userled_initialize();
#endif

#ifdef CONFIG_HPTC
  /* Initialize and register the HPTC device. */
  ret = stm32_hptc_setup();
  if (ret < 0)
    {
      syslog(LOG_ERR, "ERROR: stm32_hptc_setup() failed: %d\n", ret);
    }
#endif

  return ret;
}

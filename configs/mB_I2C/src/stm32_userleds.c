/****************************************************************************
 * configs/mB_I2C/src/stm32_userleds.c
 *
 *   Copyright (C) 2011, 2015-2016 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdint.h>
#include <stdbool.h>
#include <debug.h>

#include <nuttx/board.h>
#include <nuttx/power/pm.h>
#include <arch/board/board.h>

#include "chip.h"
#include "up_arch.h"
#include "up_internal.h"
#include "stm32.h"
#include "mB_I2C.h"

#ifdef CONFIG_MESSBUS_USERLED
#include <nuttx/messBUS/messBUS_led.h>
#endif

#ifndef CONFIG_ARCH_LEDS

/****************************************************************************
 * Private Data
 ****************************************************************************/
/* This array maps an LED number to GPIO pin configuration */

static uint32_t g_ledcfg[BOARD_NLEDS] =
{
	GPIO_LED1,
	GPIO_LED2,
};

/****************************************************************************
 * Private Function Protototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/


/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: board_userled_initialize
 ****************************************************************************/

void board_userled_initialize(void)
{
    /* Configure LED 1-2 GPIOs for output */

    for (int i = 0; i < BOARD_NLEDS; i++)
    {
        stm32_configgpio(g_ledcfg[i]);
    }
}

/****************************************************************************
 * Name: board_userled
 ****************************************************************************/

void board_userled(int led, bool ledon)
{
    if ((unsigned) led < BOARD_NLEDS)
    {
        stm32_gpiowrite(g_ledcfg[led], ledon);
    }
}

/****************************************************************************
 * Name: board_userled_all
 ****************************************************************************/

void board_userled_all(uint8_t ledset)
{
    /* Configure LED1-2 GPIOs for output */

    for (int i = 0; i < BOARD_NLEDS; i++)
    {
        stm32_gpiowrite(g_ledcfg[i], (ledset & (1 << i)) != 0);
    }
}

#ifdef  CONFIG_MESSBUS_USERLED
/****************************************************************************
 * Name: switch_LED
 ****************************************************************************/

void switch_LED(int led, bool state)
{
	if ((unsigned) led < BOARD_NLEDS)
	{
		board_userled(led, state);
	}
}

/****************************************************************************
 * Name: toggle_LED
 ****************************************************************************/

void toggle_LED(int led)
{
	if ((unsigned) led < BOARD_NLEDS)
	{
		if (stm32_gpioread(g_ledcfg[led]))
			switch_LED(led, false);
		else
			switch_LED(led, true);
	}
}

/****************************************************************************
 * Name: switch_LED_red_on
 ****************************************************************************/
void switch_LED_red_on()
{
	switch_LED(BOARD_LED_RED, true);
//	putreg32(1 << 8, STM32_GPIOA_BSRR);
}

/****************************************************************************
 * Name: switch_LED_red_off
 ****************************************************************************/
void switch_LED_red_off()
{
	switch_LED(BOARD_LED_RED, false);
//	putreg32((1 << 8) << 16, STM32_GPIOA_BSRR);
}

/****************************************************************************
 * Name: toggle_LED_red
 ****************************************************************************/
void toggle_LED_red()
{
	if (stm32_gpioread(g_ledcfg[BOARD_LED_RED]))
		switch_LED_red_off();
	else
		switch_LED_red_on();
}

/****************************************************************************
 * Name: switch_LED_green_on
 ****************************************************************************/
void switch_LED_green_on()
{
	switch_LED(BOARD_LED_GREEN, true);
//	putreg32(1 << 9, STM32_GPIOA_BSRR);
}

/****************************************************************************
 * Name: switch_LED_green_off
 ****************************************************************************/
void switch_LED_green_off()
{
	switch_LED(BOARD_LED_GREEN, false);
//	putreg32((1 << 9) << 16, STM32_GPIOA_BSRR);
}

/****************************************************************************
 * Name: toggle_LED_green
 ****************************************************************************/
void toggle_LED_green()
{
	if (stm32_gpioread(g_ledcfg[BOARD_LED_GREEN]))
		switch_LED_green_off();
	else
		switch_LED_green_on();
}

#endif /*  CONFIG_MESSBUS_USERLED */

#endif /* !CONFIG_ARCH_LEDS */

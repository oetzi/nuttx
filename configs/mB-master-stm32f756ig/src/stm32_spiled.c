/*
 * stm32_spiled.c
 *
 *  Created on: 11.02.2019
 *      Author: bbrandt
 */

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#if defined(CONFIG_STM32F7_SPI4) && defined(CONFIG_ARCH_HAVE_SPI_LEDS) && defined(CONFIG_MESSBUS_USERLED)
#include <nuttx/board.h>
#include <arch/board/board.h>
#include <nuttx/spi/spi.h>

#include <unistd.h>
#include <debug.h>
#include <errno.h>
#include <sys/types.h>

#include "stm32_gpio.h"
#include "stm32_spi.h"
#include "mB-master-stm32f756ig.h"

#include <nuttx/messBUS/messBUS_led.h>

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/
#ifndef OK
#  define OK 0
#endif

/****************************************************************************
 * Private Data
 ****************************************************************************/
/* SPI device representing led driver */
static struct spi_dev_s *spi_led = NULL;

/* 16-bit value representing leds status (1: on, 0: off)*/
static uint16_t led_values = 0;

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: switch_SPI_LED
 *
 * Description:
 *      Switches a single SPI LED on/off depending on input 'state'.
 *      LEDs can be referenced by board specific defines LED_SPI_X.
 *      Output of LED driver has to be enabled by enable_SPI_LED();
 *
 *      WARNING: Call is blocking!
 *
 ****************************************************************************/
void switch_SPI_LED(uint16_t led, bool state)
{
	if (state)
		set_SPI_LEDS(led_values | led);
	else if(led_values & led)
		set_SPI_LEDS(led_values ^ led);
}

/****************************************************************************
 * Name: set_SPI_LEDS
 *
 * Description:
 *      Sets entire set of LED values to LED driver. Each bit represents one
 *      LED (1: on, 0: off).
 *      LEDs can be referenced by board specific defines LED_SPI_X.
 *      Output of LED driver has to be enabled by enable_SPI_LED();
 *
 *      WARNING: Call is blocking!
 *
 ****************************************************************************/
void set_SPI_LEDS(uint16_t ledval)
{
	DEBUGASSERT(spi_led != NULL);

	if (spi_led != NULL)
	{
		led_values = ledval;

		SPI_LOCK(spi_led, true);
		SPI_SNDBLOCK(spi_led, &led_values, sizeof(led_values));
		SPI_LOCK(spi_led, false);

		stm32_gpiowrite(GPIO_LED_SPI_LD, true);
		stm32_gpiowrite(GPIO_LED_SPI_LD, false);
	}
}

/****************************************************************************
 * Name: enable_SPI_LED
 *
 * Description:
 *       Enables output of LED driver.
 *
 ****************************************************************************/
void enable_SPI_LED()
{
	stm32_gpiowrite(GPIO_LED_SPI_OEN, false);
}

/****************************************************************************
 * Name: disable_SPI_LED
 *
 * Description:
 *       Disables output of LED driver.
 *
 ****************************************************************************/
void disable_SPI_LED()
{
	stm32_gpiowrite(GPIO_LED_SPI_OEN, true);
}

/****************************************************************************
 * Name: stm32_spiled_initialize
 *
 * Description:
 *   Called to create the defined SPI buses and to initialize SPI LED
 *   functionality.
 *
 ****************************************************************************/
int stm32_spiled_initialize()
{
	spi_led = stm32_spibus_initialize(SPI_LED_BUS);

	if (!spi_led)
	{
		syslog(LOG_ERR, "ERROR Failed to initialize LED SPI bus\n");
		return -ENODEV;
	}
	SPI_SETFREQUENCY(spi_led, SPI_LED_BUS_FREQ);
	SPI_SETBITS(spi_led, SPI_LED_BUS_NBITS);
	SPI_SETMODE(spi_led, SPI_LED_BUS_MODE);

	stm32_configgpio(GPIO_LED_SPI_LD);
	stm32_configgpio(GPIO_LED_SPI_OEN);

	return OK;
}

#endif

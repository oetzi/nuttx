/************************************************************************************
 * configs/stm32f4discovery/src/stm32_ad7190.c
 *
 * Copyright (C) 2015, 2017 Gregory Nutt. All rights reserved.
 * Based on stm32_ducspi from Alan Carvalho de Assis <acassis@gmail.com>>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/


#include <nuttx/config.h>

#include <errno.h>
#include <debug.h>

#include <nuttx/spi/spi.h>
#include <nuttx/analog/ad7190.h>

#include "stm32.h"
#include "stm32_spi.h"
#include "stm32f4_mB_AD24.h"
#include <stdio.h>

#if defined(CONFIG_SPI) && defined(CONFIG_ADC_AD7190)

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_ad7190_initialize
 *
 * Description:
 *   Initialize SPI and start the register function from the AD7190 driver.
 *   register path /dev/AD7190_"num"
 *
 * Input parameters:
 *   num - the number of the driver
 *
 * Returned Value:
 *   Zero (OK) on success; a -1 value on failure.
 *
 ************************************************************************************/

int stm32_ad7190_initialize(FAR const char *devpath,int spibus, int num)
{
  FAR struct spi_dev_s *spi;
  int ret = 0;
  /*Initialize the SPI driver*/
  spi = stm32_spibus_initialize(spibus);

  if (!spi)
  {
	  return -1;
  }
  /*register the ad7190 character driver */

  ad7190_register(devpath,spi,num);
  return ret;
}

#endif /* CONFIG_SPI && CONFIG_AD7190 */


/************************************************************************************
 * configs/mB_Heat/src/stm32_ltc1865.c
 *
 *   Copyright (C) 2020 Stefan Nowak
 *   Author: Stefan Nowak <stefan.nowak@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <errno.h>
#include <debug.h>

#include <nuttx/spi/spi.h>
#include <nuttx/analog/ltc1865.h>

#include "stm32_spi.h"

#include "mB_Heat.h"
#include <stm32_gpio.h>

#if defined(CONFIG_MB_HEAT_LTC1865) && defined(CONFIG_ADC_LTC1865) && defined(CONFIG_STM32F7_SPI1) && defined(CONFIG_STM32F7_SPI4)


/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_ltc1865_setup
 *
 * Description:
 *   Initialize and register the ltc1865 DACs.
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ************************************************************************************/
 
int stm32_ltc1865_setup()
{

  struct spi_dev_s *spi2, *spi3;
  int ret;

  ainfo("ADC LTC1865 (dirty twindevice): \n");

  ainfo("Initializing SPI2\n");
  spi2 = stm32_spibus_initialize(2);
  if(!spi2)
    {
      spierr("Failed to initialize SPI2\n");
      return -ENODEV;
    }

  ainfo("Initializing SPI3\n");
  spi3 = stm32_spibus_initialize(3);
  if (!spi3)
    {
      spierr("Failed to initialize SPI3\n");
      return -ENODEV;
    }



  ainfo("ltc1865_register adc \n");
  ret = ltc1865_register("/dev/adc", spi2, 3, spi3, 4);
  if (ret){
	  aerr("Failed to register /dev/adc: %d\n",ret);
	  return ret;
  }



  ainfo("done.\n");
  return OK;
}

#endif

/****************************************************************************
 * configs/mB_trigger/src/stm32_userleds.c
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include "mB_trigger.h"


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
#ifndef OK
#  define OK 0
#endif


/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: board_app_initialize
 *
 * Description:
 *   Perform application specific initialization.  This function is never
 *   called directly from application code, but only indirectly via the
 *   (non-standard) boardctl() interface using the command BOARDIOC_INIT.
 *
 ****************************************************************************/

int board_app_initialize(uintptr_t arg)
{
    /* nothing to do here */

  return OK;
}

/************************************************************************************
 * configs/mB_Heat/src/stm32_ltc2642.c
 *
 *   Copyright (C) 2020 Stefan Nowak
 *   Author: Stefan Nowak <stefan.nowak@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <errno.h>
#include <debug.h>

#include <nuttx/spi/spi.h>
#include <nuttx/analog/ltc2642.h>

#include "stm32_spi.h"

#include "mB_Heat.h"
#include <stm32_gpio.h>

#if defined(CONFIG_MB_HEAT_LTC2642) && defined(CONFIG_DAC_LTC2642) && defined(CONFIG_STM32F7_SPI1) && defined(CONFIG_STM32F7_SPI4)


/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_ltc2642_setup
 *
 * Description:
 *   Initialize and register the ltc2642 DACs.
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ************************************************************************************/
 
int stm32_ltc2642_setup()
{

  struct spi_dev_s *spi1, *spi4;
  int ret;

  ainfo("DAC LTC2642 (dirty twindevice): \n");

  ainfo("Initializing SPI1\n");
  spi1 = stm32_spibus_initialize(1);
  if (!spi1)
    {
      spierr("Failed to initialize SPI1\n");
      return -ENODEV;
    }

  ainfo("Initializing SPI4\n");
  spi4 = stm32_spibus_initialize(4);
  if (!spi4)
    {
      spierr("Failed to initialize SPI4\n");
      return -ENODEV;
    }



  ainfo("ltc2642_register dac \n");
  ret = ltc2642_register("/dev/dac", spi1, 1, spi4, 2);
  if (ret){
	  aerr("Failed to register /dev/dac: %d\n",ret);
	  return ret;
  }



  ainfo("done.\n");
  return OK;
}

#endif

/************************************************************************************
 * include/nuttx/timers/hptc_io.h
 *
 *   Copyright (C) 2007-2008, 2012-2015, 2018 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *           Stefan Nowak <stefan.nowak@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

#ifndef __INCLUDE_NUTTX_TIMERS_HPTC_IO_H
#define __INCLUDE_NUTTX_TIMERS_HPTC_IO_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <stdint.h>
#include <stdbool.h>
#include <semaphore.h>
#include <errno.h>


#include <nuttx/fs/fs.h>

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/


#define HPTC_IO_HW_OC_FLAG 1
#define HPTC_IO_SW_OC_FLAG 2

#define HPTC_IO_RISING_EDGE 1
#define HPTC_IO_FALLING_EDGE 2

#define HPTC_IO_WF_DISABLED 0
#define HPTC_IO_WF_SINGLE 1
#define HPTC_IO_WF_PULSE 2
#define HPTC_IO_WF_CONTINUOUS 3

/* Maximum number of threads than can be waiting for POLL events */

#ifndef CONFIG_HPTC_IO_NPOLLWAITERS
#  define CONFIG_HPTC_IO_NPOLLWAITERS 2
#endif

/* vtable access helpers */

#define hptc_io_setup(dev)          dev->ops->setup(dev)
#define hptc_io_shutdown(dev)       dev->ops->shutdown(dev)
#define hptc_io_attach(dev)         dev->ops->attach(dev)
#define hptc_io_detach(dev)         dev->ops->detach(dev)
#define hptc_io_enabletxint(dev)    dev->ops->txint(dev, true)
#define hptc_io_disabletxint(dev)   dev->ops->txint(dev, false)
#define hptc_io_enablerxint(dev)    dev->ops->rxint(dev, true)
#define hptc_io_disablerxint(dev)   dev->ops->rxint(dev, false)
#define hptc_io_rxavailable(dev)    dev->ops->rxavailable(dev)
#define hptc_io_txready(dev)        dev->ops->txready(dev)
#define hptc_io_txempty(dev)        dev->ops->txempty(dev)
#define hptc_io_send(dev,ch)        dev->ops->send(dev,ch)
#define hptc_io_receive(dev,s)      dev->ops->receive(dev,s)

/************************************************************************************
 * Public Types
 ************************************************************************************/

/* This describes an OC message */

begin_packed_struct struct hptc_oc_msg_s
{
  int32_t      sec;               /* desired second to be executed, zero for as fast as possible */
  int32_t      nsec;              /* nanoseconds to execute first time */
  uint32_t     pulse_width_sec;   /* seconds of pulse width, 0 for single edge */
  uint32_t     pulse_width_nsec;  /*nanoseconds of pulse width, 0 for single edge */
  uint32_t     period_sec;        /* seconds of preiod, 0 for single edge or pulse */
  uint32_t     period_nsec;       /* nanoseconds of period, 0 for single edge or pulse */
  uint32_t     period_nsec_num;   /* nanoseconds numerator */
  uint32_t     period_nsec_denom; /* nanoseconds denumerator */
  uint8_t      edge;              /* HPTC_IO_RISING_EDGE or HPTC_IO_FALLING_EDGE */
} end_packed_struct;

struct hptc_oc_wf_s {
        uint8_t type;
        uint8_t state;
        int32_t time_s;
        int32_t time_ns;
        uint32_t high_s;
        uint32_t high_ns;
        uint32_t low_s;
        uint32_t low_ns;
        uint32_t numerator;
        uint32_t denominator;
        uint32_t residual;
        uint32_t recovery_s;
        uint32_t recovery_ns;
        uint32_t recovery_numerator;
};

/* This describes an IC message */

begin_packed_struct struct hptc_ic_msg_s
{
  int32_t      sec;
  int32_t      nsec;
  uint8_t	   flags;
  /*uint8_t      channel;*/
} end_packed_struct;


/* This structure defines the HPTC_IO IC buffer.  The HPTC_IO infrastructure will
 * initialize the 'sem' field but all other fields must be initialized by the
 * caller of hptc_io_register().
 */

struct hptc_ic_buffer_s
{
  sem_t            sem;    /* Used to control exclusive access to the buffer */
  volatile int16_t head;   /* Index to the head [IN] index in the buffer */
  volatile int16_t tail;   /* Index to the tail [OUT] index in the buffer */
  int16_t          size;   /* The allocated size of the buffer */
  bool             overflow;
  FAR struct hptc_ic_msg_s        *buffer; /* Pointer to the allocated buffer memory */
};

/* This structure defines the HPTC_IO OC buffer.  The HPTC_IO infrastructure will
 * initialize the 'sem' field but all other fields must be initialized by the
 * caller of hptc_io_register().
 */

struct hptc_oc_buffer_s
{
  sem_t            sem;    /* Used to control exclusive access to the buffer */
  volatile int16_t head;   /* Index to the head [IN] index in the buffer */
  volatile int16_t tail;   /* Index to the tail [OUT] index in the buffer */
  int16_t          size;   /* The allocated size of the buffer */
  FAR struct hptc_oc_wf_s        *buffer; /* Pointer to the allocated buffer memory */
};




/* This structure defines all of the operations provided by the architecture specific
 * logic.  All fields must be provided with non-NULL function pointers by the
 * caller of hptc_io_register().
 */

struct hptc_io_dev_s;
struct hptc_io_ops_s
{
  /* Initialize HPTC_IO
   */

  CODE int (*initialize)(FAR struct hptc_io_dev_s *dev);

  /* All ioctl calls will be routed through this method */

  CODE int (*ioctl)(FAR struct file *filep, int cmd, unsigned long arg);

};

/* This is the device structure used by the driver.  The caller of
 * hptc_io_register() must allocate and initialize this structure.  The
 * calling logic need only set all fields to zero except:
 *
 *   'isconsole', 'ocbuf.buffer', 'rcv.buffer', the elements
 *   of 'ops', and 'private'
 *
 * The common logic will initialize all semaphores.
 */

struct hptc_io_dev_s
{
  /* State data */

  uint8_t              open_count;   /* Number of times the device has been opened */
  volatile bool        ocbufwaiting;  /* true: User waiting for space in ocbuf.buffer */
  volatile bool        icbufwaiting;  /* true: User waiting for data in icbuf.buffer */
//  volatile bool        iobufwaiting; /* true: User waiting for buffer */
  /* Semaphores */

  sem_t                closesem;     /* Locks out new open while close is in progress */
  sem_t                ocbufsem;      /* Wakeup user waiting for space in ocbuf.buffer */
  sem_t                icbufsem;      /* Wakeup user waiting for data in icbuf.buffer */
//  sem_t                iobufsem;     /* Wakeup user waiting for buffer */
  #ifndef CONFIG_DISABLE_POLL
  sem_t                pollsem;      /* Manages exclusive access to fds[] */
#endif

  /* I/O buffers */
 // struct hptc_io_buffer_s iobuf;          /* Describes the buffer*/
 struct hptc_oc_buffer_s ocbuf;         /* Describes transmit buffer */
 struct hptc_ic_buffer_s icbuf;         /* Describes receive buffer */


  /* Driver interface */

  FAR const struct hptc_io_ops_s *ops;  /* Arch-specific operations */
  FAR void            *priv;         /* Used by the arch-specific logic */

  /* The following is a list if poll structures of threads waiting for
   * driver events. The 'struct pollfd' reference for each open is also
   * retained in the f_priv field of the 'struct file'.
   */

#ifndef CONFIG_DISABLE_POLL
  struct pollfd *fds[CONFIG_HPTC_IO_NPOLLWAITERS];
#endif
};

typedef struct hptc_io_dev_s hptc_io_dev_t;

/************************************************************************************
 * Public Data
 ************************************************************************************/

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: hptc_io_register
 *
 * Description:
 *   Register HPTC_IO console and HPTC_IO ports.
 *
 ************************************************************************************/

int hptc_io_register(FAR const char *path, FAR hptc_io_dev_t *dev, int16_t ic_bufsize, int16_t oc_bufsize);




/************************************************************************************
 * Name: hptc_ic_fifo_put
 *
 * Description:
 *   This function is called from the HPTC interrupt handler when input capture data
 *   is available  This function will add data to head of ic buffer.  Driver read()
 *   logic will take data from the tail of the buffer.
 *
 ************************************************************************************/

void hptc_ic_fifo_put(FAR hptc_io_dev_t *dev, uint8_t ch, uint8_t flags, int32_t capture_s, int32_t capture_ns);


/************************************************************************************
 * Name: hptc_oc_fifo_get
 *
 * Description:
 *   This function is called from the HPTC interrupt handler to get output compare
 *   waveform data prior given time
 *   This function will take data from the tail of the oc_buffer.
 *
 ************************************************************************************/

int hptc_oc_fifo_get(FAR hptc_io_dev_t *dev, uint8_t channel, struct hptc_oc_wf_s *oc_wf, int32_t time_s, int32_t time_ns);

/****************************************************************************
 * Name: hptc_io_condition_oc_wf
 *
 * Description:
 *   condition a waveform for usage in systick_isr
 *
 * Input Parameters: FAR struct hptc_oc_wf_s
 *
 ****************************************************************************/

void hptc_oc_wf_condition(FAR struct hptc_oc_wf_s *wf,
             uint8_t rising,
             int32_t time_s,
             int32_t time_ns,
             uint32_t pulsewidth_s,
             uint32_t pulsewidth_ns,
             uint32_t period_s,
             uint32_t period_ns,
             uint32_t numerator,
             uint32_t denominator);

/************************************************************************************
 * Name: hptc_io_xmitchars
 *
 * Description:
 *   This function is called from the HPTC_IO interrupt handler when an interrupt
 *   is received indicating that there is more space in the transmit FIFO.  This
 *   function will send characters from the tail of the xmit buffer while the driver
 *   write() logic adds data to the head of the xmit buffer.
 *
 ************************************************************************************/

void hptc_io_xmitchars(FAR hptc_io_dev_t *dev);

/************************************************************************************
 * Name: hptc_io_receivechars
 *
 * Description:
 *   This function is called from the HPTC_IO interrupt handler when an interrupt
 *   is received indicating that are bytes available to be received.  This
 *   function will add chars to head of receive buffer.  Driver read() logic will take
 *   characters from the tail of the buffer.
 *
 ************************************************************************************/

void hptc_io_recvchars(FAR hptc_io_dev_t *dev);

/************************************************************************************
 * Name: hptc_io_datareceived
 *
 * Description:
 *   This function is called from hptc_io_recvchars when new HPTC_IO data is place in
 *   the driver's circular buffer.  This function will wake-up any stalled read()
 *   operations that are waiting for incoming data.
 *
 ************************************************************************************/

void hptc_io_datareceived(FAR hptc_io_dev_t *dev);

/************************************************************************************
 * Name: hptc_io_datasent
 *
 * Description:
 *   This function is called from hptc_io_xmitchars after HPTC_IO data has been sent,
 *   freeing up some space in the driver's circular buffer. This function will
 *   wake-up any stalled write() operations that was waiting for space to buffer
 *   outgoing data.
 *
 ************************************************************************************/

void hptc_io_datasent(FAR hptc_io_dev_t *dev);

/************************************************************************************
 * Name: hptc_io_reset_sem
 *
 * Description:
 *   This function is called when need reset hptc_io semphore, this may used in kill one
 *   process, but this process was reading/writing with the semphore.
 *
 ************************************************************************************/

void hptc_io_reset_sem(FAR hptc_io_dev_t *dev);

#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* __INCLUDE_NUTTX_TIMERS_HPTC_IO_H */

/****************************************************************************
 * configs/mB_Heat/src/stm32_smt172.c
 *
 *   Copyright 2020 Leichtwerk Research GmbH
 *   Author: Stefan Nowak
 *
 ****************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <debug.h>
#include <stddef.h>
#include <sys/ioctl.h>

#include <nuttx/board.h>
#include <arch/board/board.h>
#include <nuttx/sensors/smt172.h>

#include "mB_Heat.h"
#include "up_arch.h"
#include "stm32_exti.h"
//#include "stm32_tim.h"
#include "stm32_capture.h"
#include "stm32_dma.h"
#include "stm32_rcc.h"

#include <nuttx/clock.h>
#include <fcntl.h>
#include <nuttx/arch.h>

#include <stdio.h> //XXX remove me, debug only

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* DMA control words */

#  define SMT172_DMA_CONTROL_WORD      \
              (DMA_SCR_DIR_P2M       | \
               DMA_SCR_MINC          | \
               DMA_SCR_PSIZE_16BITS   | \
               DMA_SCR_MSIZE_16BITS   | \
               DMA_SCR_PRIMED  | \
               DMA_SCR_PBURST_SINGLE | \
               DMA_SCR_MBURST_SINGLE)

/*allign to cacheline, size of multiple cachlines (32 byte) */
#define STM32_SMT172_DMA_BUFFER_SIZE 64

#define SMT172_TIM_PRESCALER (STM32_APB2_TIM1_CLKIN/SMT172_TIMER_FREQ_DESIRED)

/****************************************************************************
 * Private Types
 ****************************************************************************/
#if 1
struct smt172configPriv_s
{
     /* A reference to a structure of this type must be passed to the SMT172
      * driver.  This structure provides information about the configuration
      * of the SMT172 and provides some board-specific hooks.
      */

     struct smt172_config_s config;

     /* Additional private definitions only known to this driver */
     int channel;
     volatile uint16_t* fifowrite;//[CONFIG_SMT172_CHANNELS];
     volatile uint16_t* fiforead;
     DMA_HANDLE dma_handle[CONFIG_SMT172_CHANNELS];
};
#endif


/****************************************************************************
 * Static Function Prototypes
 ****************************************************************************/

static int stm32_smt172_update(FAR struct smt172_config_s *conf);


/****************************************************************************
 * Private Data
 ****************************************************************************/


volatile static uint16_t g_smt172_fifo1[STM32_SMT172_DMA_BUFFER_SIZE] __attribute__((aligned(ARMV7M_DCACHE_LINESIZE)));
volatile static uint16_t g_smt172_fifo2[STM32_SMT172_DMA_BUFFER_SIZE] __attribute__((aligned(ARMV7M_DCACHE_LINESIZE)));
//static DMA_HANDLE dma_handle;


static struct smt172configPriv_s g_smt172config =
{

  .config =
  {
      .fifo         =   NULL,
      .fifosize    =   0,
      .update       =   stm32_smt172_update,
      .channel      = 0,
  },
  .channel=1,
  .fifowrite =g_smt172_fifo1,
  .fiforead  =g_smt172_fifo2,

};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

void stm32_smt172_timer_initialize(struct smt172configPriv_s *conf)
{

 //printf("\n\nstm32_smt172_timer_initialize ...\n\n");


    /* enable logic */
    modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_TIM1EN);

    /* disable timer, set OPM */
    putreg16(ATIM_CR1_OPM,STM32_TIM1_CR1);

    /* set ARR to max */
    putreg32(0xFFFF,STM32_TIM1_ARR);

    /* set prescaler */
    uint32_t prescaler = SMT172_TIM_PRESCALER;

    if (prescaler > 0)
      {
        prescaler--;
      }

    if (prescaler > 0xffff)
      {
        prescaler = 0xffff;
      }

    conf->config.period_cnt_min=(STM32_APB2_TIM1_CLKIN/SMT172_OUTPUT_FREQ_MAX)/(prescaler+1);
    conf->config.period_cnt_max=(STM32_APB2_TIM1_CLKIN/SMT172_OUTPUT_FREQ_MIN)/(prescaler+1);

    putreg16(prescaler,STM32_TIM1_PSC);

    putreg16(ATIM_DCR_DBL(2)| 13 ,STM32_TIM1_DCR);

    stm32_configgpio(GPIO_TIM1_CH1IN);
    stm32_configgpio(GPIO_TIM1_CH2IN);


}



void stm32_smt172_dma_initialize(struct smt172configPriv_s *conf){

#if CONFIG_SMT172_CHANNELS >0

   /* Try to take timer's DMA stream in the OS. It will be locked with
    * a semaphore so that no other driver can use the stream. If another
    * driver already uses the stream stm32_dmachannel waits until the
    * specified stream is released. This is a safety feature to make sure
    * the driver has exclusive access to the DMA stream.
    */
    conf->dma_handle[0]=stm32_dmachannel(DMAMAP_TIM1_CH1);

    /* Configure for circular DMA reception into the DMA FIFO */

    stm32_dmasetup(conf->dma_handle[0],
             STM32_TIM1_DMAR,
            /* (uint32_t)conf->fifo[0],*/
             (uint32_t)g_smt172_fifo1,
             STM32_SMT172_DMA_BUFFER_SIZE,
             SMT172_DMA_CONTROL_WORD);
#endif


#if CONFIG_SMT172_CHANNELS >1
    /* Try to take timer's DMA stream in the OS. It will be locked with
     * a semaphore so that no other driver can use the stream. If another
     * driver already uses the stream stm32_dmachannel waits until the
     * specified stream is released. This is a safety feature to make sure
     * the driver has exclusive access to the DMA stream.
     */
     conf->dma_handle[1]=stm32_dmachannel(DMAMAP_TIM1_CH2);

     /* Configure for circular DMA reception into the DMA FIFO */

     stm32_dmasetup(conf->dma_handle[1],
              STM32_TIM1_DMAR,
             /* (uint32_t)conf->fifo[1],*/
              (uint32_t)g_smt172_fifo2,
              STM32_SMT172_DMA_BUFFER_SIZE,
              SMT172_DMA_CONTROL_WORD);


#endif


#if 0
    for(int i=0;i< CONFIG_SMT172_CHANNELS;i++)
    {
        /* set fifosize */
        conf->config.ch[i].fifosize=STM32_SMT172_DMA_BUFFER_SIZE;

        /* Enable DMA for SMT172 */
        stm32_dmaenable(conf->dma_handle[i]);
    }
#endif

return;
}


static int stm32_smt172_update(FAR struct smt172_config_s *conf){

    //printf("\nstm32_smt172_update\n");

    FAR struct smt172configPriv_s *priv = (FAR struct smt172configPriv_s *) conf;

    int channel,next_channel;//=1;

    channel=priv->channel;//=1; //XXX

    next_channel=(channel?0:1);

    priv->channel = next_channel;


    /* clear CCER */
    putreg32(0x00000000, STM32_TIM1_CCER);

    /* SMCR: disable slave mode */
    putreg32(0x00000000, STM32_TIM1_SMCR);

    priv->config.fifosize = STM32_SMT172_DMA_BUFFER_SIZE - stm32_dmaresidual(priv->dma_handle[channel]);
    //up_invalidate_dcache((uintptr_t)priv->fifo[channel], (uintptr_t)priv->fifo[channel] + (uintptr_t)(STM32_SMT172_DMA_BUFFER_SIZE * sizeof(uint16_t)));

    stm32_dmastop(priv->dma_handle[channel]);

    /* disable timer, leaving in OPM mode*/
    putreg16(ATIM_CR1_OPM,STM32_TIM1_CR1);



    //printf("ch: %d received: %d\n", channel, priv->config.fifosize);

    priv->config.channel=channel;

    if(next_channel==0)

    {
        up_invalidate_dcache((uintptr_t)g_smt172_fifo2, (uintptr_t)&g_smt172_fifo1[STM32_SMT172_DMA_BUFFER_SIZE]);
        priv->config.fifo=g_smt172_fifo2;
        /* CH1 */
        /* CCMR1: set input: channel2: indirect, channel 1 direct*/
        putreg32(  ((STM32_CAP_MAPPED_TI2 |  STM32_CAP_FILTER_NO)<<8) |  (STM32_CAP_MAPPED_TI1 |STM32_CAP_FILTER_NO), STM32_TIM1_CCMR1);

        /* IC1: falling, measure period (ATIM_CCER_CC1P | ATIM_CCER_CC1E)*/
        /* IC2: rising, measure duty cycle (ATIM_CCER_CC2E)*/
        putreg32((ATIM_CCER_CC2E)|(ATIM_CCER_CC1P | ATIM_CCER_CC1E), STM32_TIM1_CCER);

        putreg16(ATIM_DIER_CC1DE  ,STM32_TIM1_DIER);

        /* Enable DMA for SMT172 */
        stm32_dmaenable(priv->dma_handle[next_channel]);

        /* SMCR: set slave mode: combined trigger and reset on TI1FP1 for channel 1*/
        putreg32(ATIM_SMCR_TI1FP1 | ATIM_SMCR_SMS, STM32_TIM1_SMCR);

    }
    else
    {
        up_invalidate_dcache((uintptr_t)g_smt172_fifo1, (uintptr_t)&g_smt172_fifo2[STM32_SMT172_DMA_BUFFER_SIZE]);
        priv->config.fifo=g_smt172_fifo1;
        /* CH2 */

        /* CCMR1: set input: channel2: direct, channel 1 indirect*/
        putreg32(  ((STM32_CAP_MAPPED_TI1 |  STM32_CAP_FILTER_NO)<<8) |  (STM32_CAP_MAPPED_TI2 |STM32_CAP_FILTER_NO), STM32_TIM1_CCMR1);

        /* IC1: rising, measure duty cycle (ATIM_CCER_CC1E)*/
        /* IC2_ falling, measure period    (ATIM_CCER_CC2P | ATIM_CCER_CC2E)*/
        putreg32((ATIM_CCER_CC2P | ATIM_CCER_CC2E)|(ATIM_CCER_CC1E), STM32_TIM1_CCER);

        putreg16(ATIM_DIER_CC2DE  ,STM32_TIM1_DIER);

        /* Enable DMA for SMT172 */
        stm32_dmaenable(priv->dma_handle[next_channel]);

        /* SMCR: set slave mode: combined trigger and reset on TI2FP2 for channel 2*/
        putreg32(ATIM_SMCR_TI2FP2 | ATIM_SMCR_SMS, STM32_TIM1_SMCR);
    }

#if 0

    int received = priv->config.fifosize;
    channel =priv->config.channel;
    printf("ch: %d received: %d\n", channel, received);


    float dc=0;
    float temperature=0;
    bool err_flag=false;
    if(received>18){
       for(int i=2;i<18;i+=2){ /* ignore first sample */
               uint16_t duty_cnt=priv->config.fifo[(channel?i:i+1)];
               uint16_t period_cnt=priv->config.fifo[(channel?i+1:i)];

               if((duty_cnt> period_cnt) || period_cnt < priv->config.period_cnt_min || period_cnt > priv->config.period_cnt_max){
                       err_flag=true;
                       break;
               }

               dc+=(float)(period_cnt-duty_cnt)/(float)(period_cnt);


               printf("%d: ccr1: %6hu, ccr2: %6hu\n",i/2,priv->config.fifo[i],priv->config.fifo[i+1]);

       }

       if(err_flag){

           printf("SMT172 period error\n");
       } else {
           dc/=8.0;
           temperature = -1.43*dc*dc+214.56*dc-68.6;
           printf("T=%6.3fdegC\n",temperature);
       }


    }


#endif



    return 0;
}


/****************************************************************************
 * Public Functions
 ****************************************************************************/

/************************************************************************************
 * Name: stm32_smt172_initialize
 *
 * Description:
 *   Called to configure and initialize SMT172 driver.
 *
 ************************************************************************************/

void stm32_smt172_initialize(void)
{
    int ret;

    stm32_smt172_dma_initialize(&g_smt172config);

    stm32_smt172_timer_initialize(&g_smt172config);

    ret = smt172_instantiate("/dev/smt172", &g_smt172config.config);
    if(ret<0)
    {
        snerr("ERROR: Failed to instantiate the SMT172 driver\n");
        return;
    }
}


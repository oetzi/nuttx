/****************************************************************************
 * config/mB-master-stm32f756ig/src/stm32_bringup.c
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/board.h>

#include <sys/mount.h>
#include <stdbool.h>
#include <stdio.h>
#include <debug.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mount.h>
#include <syslog.h>
#include <errno.h>

#include "mB-master-stm32f756ig.h"

#ifdef CONFIG_MTD
#  include <nuttx/mtd/mtd.h>
#endif

#ifdef CONFIG_MESSBUS_MASTER
#include <nuttx/messBUS/messBUSMaster.h>
#endif

#ifdef CONFIG_MESSBUS_DEBUG
# include <nuttx/messBUS/messBUS_Debug.h>
# include "stm32_gpio.h"
#endif

#ifdef CONFIG_NET_CDCECM
#  include <nuttx/usb/cdcecm.h>
#  include <net/if.h>
#endif

#ifdef CONFIG_RNDIS
#  include <nuttx/usb/rndis.h>
#endif

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
#ifndef OK
#  define OK 0
#endif

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: stm32_bringup
 *
 * Description:
 *   Perform architecture-specific initialization
 *
 *   CONFIG_BOARD_LATE_INITIALIZE=y :
 *     Called from board_late_initialize().
 *
 *   CONFIG_BOARD_LATE_INITIALIZE=n && CONFIG_LIB_BOARDCTL=y :
 *     Called from the NSH library
 *
 ****************************************************************************/

int stm32_bringup(void)
{
#ifdef CONFIG_MESSBUS_USERLED
    board_userled_initialize();
#endif


#if defined(CONFIG_MTD) && defined(HAVE_PROGMEM_CHARDEV)
    FAR struct mtd_dev_s *mtd;
#endif
    int ret = OK;

#ifdef CONFIG_MESSBUS_DEBUG
    ret = stm32_configgpio(GPIO_MESSBUS_DEBUG_PIN);
    ret = stm32_configgpio(GPIO_MESSBUS_DEBUG_PIN2);
    ret = stm32_configgpio(GPIO_MESSBUS_DEBUG_PIN3);
    if (ret < 0)
    {
        syslog(LOG_ERR, "Error: DEBUG PIN init failed\n");
        return ret;
    }
    else
    {
        syslog(LOG_INFO, "Successfully initialized DEBUG PIN\n");
    }
#endif

#ifdef CONFIG_HPTC
  /* Initialize and register the HPTC device. */
  ret = stm32_hptc_setup();
  if (ret < 0)
    {
      syslog(LOG_ERR, "ERROR: stm32_hptc_setup() failed: %d\n", ret);
    }
#endif

#ifdef CONFIG_PWM
  /* Initialize PWM and register the PWM device. */

  ret = stm32_pwm_setup();
  if (ret < 0)
    {
      syslog(LOG_ERR, "ERROR: stm32_pwm_setup() failed: %d\n", ret);
    }
#endif

#if defined(CONFIG_MTD) && defined (HAVE_PROGMEM_CHARDEV)

    /* Create an instance of the STM32F7 FLASH program memory device driver
     * arch/arm/src/stm32f7/stm32_flash.c
     */

    mtd = progmem_initialize();
    if (!mtd)
    {
        syslog(LOG_ERR, "ERROR: MTD Driver Initializiation failed\n");
    }
    else
    {
        syslog(LOG_INFO, "Successfully initialized FLASH MTD driver\n");

        /* prepare flash sectors for use with smartfs (mount/umount/etc.) */
        ret = smart_initialize(0, mtd, NULL);
        if (ret < 0)
        {
            syslog(LOG_ERR, "Error: Smart driver init failed\n");
            return ret;
        }
        else
        {
            syslog(LOG_INFO, "Successfully initialized smart driver\n");
        }
    }
#endif



#if defined(CONFIG_STM32F7_SPI1) || defined(CONFIG_STM32F7_SPI2) || \
    defined(CONFIG_STM32F7_SPI3) || defined(CONFIG_STM32F7_SPI4) || \
    defined(CONFIG_STM32F7_SPI5)
    /* Configure SPI chip selects if 1) SPI is not disabled, and 2) the weak function
     * stm32_spidev_initialize() has been brought into the link.
     */

    if (stm32_spidev_initialize)
    {
        stm32_spidev_initialize();
    }

#if defined(CONFIG_STM32F7_SPI4) && defined(CONFIG_ARCH_HAVE_SPI_LEDS) && defined(CONFIG_MESSBUS_USERLED)


    /* Initialize SPI BUS for 16-bit LED driver */

    ret = stm32_spiled_initialize();
    if (ret < 0)
    {
        syslog(LOG_ERR, "Error: SPI LED driver init failed\n");
        return ret;
    }
    else
    {
        syslog(LOG_INFO, "Successfully initialized SPI LED driver\n");
    }
#endif

#endif /* SPI */

#ifdef CONFIG_DAC_AD5662
    ret = stm32_ad5662_initialize();
#endif


#ifdef CONFIG_MESSBUS_MASTER
  messBUSMaster_initialize();
#endif

#ifdef CONFIG_NET_CDCECM
ret = cdcecm_initialize(0, NULL);
if (ret < 0)
{
    syslog(LOG_ERR, "ERROR: cdcecm_initialize() failed: %d\n", ret);
}
#endif

#if defined(CONFIG_RNDIS)
  uint8_t mac[6];
  mac[0] = 0xa0; /* TODO */
  mac[1] = (CONFIG_NETINIT_MACADDR_2 >> (8 * 0)) & 0xff;
  mac[2] = (CONFIG_NETINIT_MACADDR_1 >> (8 * 3)) & 0xff;
  mac[3] = (CONFIG_NETINIT_MACADDR_1 >> (8 * 2)) & 0xff;
  mac[4] = (CONFIG_NETINIT_MACADDR_1 >> (8 * 1)) & 0xff;
  mac[5] = (CONFIG_NETINIT_MACADDR_1 >> (8 * 0)) & 0xff;
  usbdev_rndis_initialize(mac);
#endif

  return ret;
}
